import numpy as np
import matplotlib.pyplot as plt
import json
import matplotlib.gridspec as gridspec

# plt.style.use('seaborn-white')

plt.rcParams.update({'font.size': 14})


plot_demand = True


if plot_demand:

    demand = [0.00833, 0.00833, 0.013333, 0.01666, 0.01666, 0.05, 0.06667, 0.03333, 0.01666, 0.0125, 0.01, 0.00833 ]
    demand = [i*60 for i in demand]
    stop_numbers = [i+1 for i in range(12)]

    plt.bar(stop_numbers, demand, color='#247BA0')
    plt.xlabel('Stop number')
    plt.ylabel('Demand (pax/min)')
    plt.title('Demand route stops')
    plt.show()

    route_length_dict = {
        "AB": ("lognormal", 360, 20, 180, 720),
        "BC": ("lognormal", 360, 20, 180, 720),
        "CD": ("lognormal", 300, 20, 150, 600),
        "DE": ("lognormal", 240, 20, 120, 480),
        "EF": ("lognormal", 240, 20, 120, 480),
        "FG": ("lognormal", 120, 20, 60, 240),
        "GH": ("lognormal", 90, 20, 45, 180),
        "HI": ("lognormal", 150, 20, 75, 300),
        "IJ": ("lognormal", 240, 20, 120, 480),
        "JK": ("lognormal", 240, 20, 120, 480),
        "KL": ("lognormal", 240, 20, 120, 480),
        "LA": ("lognormal", 300, 20, 150, 600),
    }


    route_lengths = [i[1]/60 for _, i in route_length_dict.items()]

    plt.bar(stop_numbers, route_lengths, color='#247BA0')
    plt.xlabel('Route section')
    plt.ylabel('Route length (min)')
    plt.title('Length route sections')
    plt.show()
































with open('percentiles_scenario_5.json', 'r') as reading_file:
    scenario_data = json.load(reading_file)

with open('percentiles_scenario_5_holding.json', 'r') as reading_file:
    scenario_holding_data = json.load(reading_file)

with open('percentiles_scenario_5_no.json', 'r') as reading_file:
    scenario_no_data = json.load(reading_file)



# voor elk scenario wil ik voor elke metric een grouped bar chart maken met de holding bench en de marl
# holding apart?
# travel time een stacked bar chart? met waiting en travel?
# Dan laatste 4 plots in een raster





N = 5
# marl_holding = scenario_data['Average_holding_time']
# holding_holding = scenario_holding_data['Average_holding_time']

marl_holding = [np.mean(values) for values in scenario_data['overview_data_complete']['holding']] #scenario_data['Average_experienced_crowding']
holding_holding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['holding']] #scenario_holding_data['Average_experienced_crowding']
no_holding = [np.mean(values) for values in scenario_no_data['overview_data_complete']['holding']]

print('Het 3e holding percentiel voor MARL, Holding en No is: ', marl_holding[2], '\t',  holding_holding[2], '\t', no_holding[2])


ind = np.arange(N)
width = 0.35
plt.bar(ind, marl_holding, width, label='MARL', color='#247BA0')
plt.bar(ind + width, holding_holding, width,
    label='Holding benchmark', color='#F25F5C')

plt.ylabel('Holding time (s)')
plt.xlabel('Percentile interval')
plt.title('Average holding time per stop')


plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.legend(loc='best')
plt.show()




# create travel time data

# waiting_time_marl = scenario_data['Average_passenger_waiting_time']
# waiting_time_holding = scenario_holding_data['Average_passenger_waiting_time']
#
# vehicle_time_marl = scenario_data['Average_passenger_in_vehicle_time']
# vehicle_time_holding = scenario_holding_data['Average_passenger_in_vehicle_time']

waiting_time_marl = [np.mean(values) for values in scenario_data['overview_data_complete']['waiting_time']]
vehicle_time_marl = [np.mean(values) for values in scenario_data['overview_data_complete']['in_vehicle_time']]

waiting_time_holding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['waiting_time']]
vehicle_time_holding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['in_vehicle_time']]

waiting_time_no = [np.mean(values) for values in scenario_no_data['overview_data_complete']['waiting_time']]
vehicle_time_no = [np.mean(values) for values in scenario_no_data['overview_data_complete']['in_vehicle_time']]

print('De waiting time voor MARL, Holding en No is: ', waiting_time_marl[2], '\t', waiting_time_holding[2], '\t', waiting_time_no[2])
print('De in vehicle time voor MARL, Holding en No is: ', vehicle_time_marl[2], '\t', vehicle_time_holding[2], '\t', vehicle_time_no[2])


ind = np.arange(N)
width = 0.35
plt.subplot(2,2,1)
plt.bar(ind, waiting_time_marl, width, label='MARL waiting time', color='#247BA0')
plt.bar(ind, vehicle_time_marl, width, bottom=waiting_time_marl, label='MARL in vehicle time', color='#70C1B3')
plt.bar(ind + width, waiting_time_holding, width, label='Benchmark waiting time', color='#F25F5C')
plt.bar(ind + width, vehicle_time_holding, width, bottom=waiting_time_holding,
    label='Benchmark in vehicle time', color='#FFE066')

plt.ylabel('Time (s)')
# plt.xlabel('Percentile')
plt.title('Average total travel time')

plt.ylim(0, 3900)
plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.legend(loc='best', ncol=2)
# plt.show()





# Experience crowding

N = 5
# marl_crowding = scenario_data['Average_experienced_crowding']
# holding_crowding = scenario_holding_data['Average_experienced_crowding']

marl_crowding = [np.mean(values) for values in scenario_data['overview_data_complete']['experienced_crowding']] #scenario_data['Average_experienced_crowding']
holding_crowding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['experienced_crowding']] #scenario_holding_data['Average_experienced_crowding']
no_crowding = [np.mean(values) for values in scenario_no_data['overview_data_complete']['experienced_crowding']]

print('De crowding voor MARL, Holding, No: ', marl_crowding[2], holding_crowding[2], no_crowding[2])

ind = np.arange(N)
width = 0.35
plt.subplot(2,2,2)
plt.bar(ind, marl_crowding, width, label='MARL', color='#247BA0')
plt.bar(ind + width, holding_crowding, width,
    label='Holding benchmark', color='#F25F5C')

plt.ylabel('Experienced crowding (# passengers)')
# plt.xlabel('Percentile')
plt.title('Average experienced crowding')

# plt.yscale('log')
# plt.ylim(0, 100)
plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.legend(loc='best')
# plt.show()




# COEF OF VAR STOPS

N = 5
# marl_cov = scenario_data['av cov stops']
# holding_cov = scenario_holding_data['av cov stops']

marl_cov = [np.mean(values) for values in scenario_data['overview_data_complete']['cov_of_var']] #scenario_data['av cov stops']
holding_cov = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['cov_of_var']] #scenario_holding_data['av cov stops']
no_cov = [np.mean(values) for values in scenario_no_data['overview_data_complete']['cov_of_var']] #scenario_holding_data['av cov stops']

print('De coef of var voor MARL, Holding en No is: ', marl_cov[2], '\t', holding_cov[2], '\t', no_cov[2])

ind = np.arange(N)
width = 0.35
plt.subplot(2,2,3)
plt.bar(ind, marl_cov, width, label='MARL', color='#247BA0')
plt.bar(ind + width, holding_cov, width,
    label='Holding benchmark', color='#F25F5C')

plt.ylabel('Coefficient of variation')
plt.xlabel('Percentile interval')
plt.title('Average coefficient of variation of the headways')

# plt.ylim(0, )
plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.legend(loc='best')
# plt.show()





# EXCESS WAITING TIME

N = 5
# marl_excess = scenario_data['av excess stops']
# holding_excess = scenario_holding_data['av excess stops']

marl_excess = [np.mean(values) for values in scenario_data['overview_data_complete']['excess_waiting_time']] #scenario_data['av excess stops']
holding_excess = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['excess_waiting_time']] #scenario_holding_data['av excess stops']
no_excess = [np.mean(values) for values in scenario_no_data['overview_data_complete']['excess_waiting_time']] #scenario_holding_data['av excess stops']

print('De excess voor MARL, Holding en No is: ', marl_excess[2], '\t', holding_excess[2], '\t', no_excess[2])

ind = np.arange(N)
width = 0.35
plt.subplot(2,2,4)
plt.bar(ind, marl_excess, width, label='MARL', color='#247BA0')
plt.bar(ind + width, holding_excess, width,
    label='Holding benchmark', color='#F25F5C')

plt.ylabel('Excess waiting time (s)')
plt.xlabel('Percentile interval')
plt.title('Average excess waiting time')

# plt.yscale('log')
plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.legend(loc='best')
plt.show()
























# ==========================================================
fig = plt.figure(figsize=(10, 8))
outer = gridspec.GridSpec(2, 2, wspace=0.2, hspace=0.2)



######################## TRAVEL TIME

inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[0], wspace=0.1, hspace=0.1)

ax = plt.Subplot(fig, inner[0])

waiting_time_marl = [np.mean(values) for values in scenario_data['overview_data_complete']['waiting_time']]
vehicle_time_marl = [np.mean(values) for values in scenario_data['overview_data_complete']['in_vehicle_time']]

waiting_time_holding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['waiting_time']]
vehicle_time_holding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['in_vehicle_time']]

ind = np.arange(N)
# labels = ['5th', '15th', '50th', '85th', '95th']
labels = ['0-5', '5-15', '15-50', '50-85', '85-95']
width = 0.35

ax.bar(labels, waiting_time_marl, width, label='MARL waiting time', color='#247BA0')
ax.bar(labels, vehicle_time_marl, width, bottom=waiting_time_marl, label='MARL in vehicle time', color='#70C1B3')
ax.bar(ind + width, waiting_time_holding, width, label='Benchmark waiting time', color='#F25F5C')
ax.bar(ind + width, vehicle_time_holding, width, bottom=waiting_time_holding,
    label='Benchmark in vehicle time', color='#FFE066')


# ax.ylabel('Time (s)')
ax.set_ylabel('Time(s)')
# plt.xlabel('Percentile')
# ax.title('Percentiles of average total travel time')

# ax.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
ax.legend(loc='best', ncol=2)
# ax.set_xlabel('Percentiles of average total travel time')
ax.set_title('Average total travel time')


ax.set_ylim( 0, 8200 )
# ax.text(0.1, 5, 'hallo', color='black')#, fontweight='bold', fontsize=40)
# ax.spines['bottom'].set_visible(False)
# ax.xaxis.tick_top()
ax.tick_params(labeltop=False, direction='out')

# d = .015
# kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
# ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
# ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

fig.add_subplot(ax)

# inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[0], wspace=0.1, hspace=0.1)

# ax = plt.Subplot(fig, inner[1])
#
# ind = np.arange(N)
# width = 0.35
#
# plt.bar(ind, waiting_time_marl, width, label='MARL waiting time', color='#247BA0')
# plt.bar(ind, vehicle_time_marl, width, bottom=waiting_time_marl, label='MARL in vehicle time', color='#70C1B3')
# plt.bar(ind + width, waiting_time_holding, width, label='Benchmark waiting time', color='#F25F5C')
# plt.bar(ind + width, vehicle_time_holding, width, bottom=waiting_time_holding,
#     label='Benchmark in vehicle time', color='#FFE066')
#
# plt.ylabel('Time (s)')
# # plt.xlabel('Percentile')
# plt.title('Percentiles of average total travel time')
#
#
# plt.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
# # plt.legend(loc='best')
#
#
#
# ax.set_ylim( 1500, 2200 )
# ax.spines['top'].set_visible(False)
# ax.set_xlabel('Number of trips per schedule with SoC < 0, simulated')
# ax.set_ylabel('Number of occurences')
# ax.get_yaxis().set_label_coords(-0.1, 0.9)
#
# d= .015
# kwargs.update(transform=ax.transAxes)  # switch to the bottom axes
# ax.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
# ax.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal


# fig.add_subplot(ax)

# -------------

#################### EXPERIENCED CROWDING
inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[1], wspace=0.1, hspace=0.1)

ax = plt.Subplot(fig, inner[0])

N = 5
marl_crowding = [np.mean(values) for values in scenario_data['overview_data_complete']['experienced_crowding']] #scenario_data['Average_experienced_crowding']
holding_crowding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['experienced_crowding']] #scenario_holding_data['Average_experienced_crowding']

ind = np.arange(N)
width = 0.35

ax.bar(labels, marl_crowding, width, label='MARL', color='#247BA0')
ax.bar(ind + width, holding_crowding, width,
    label='Holding benchmark', color='#F25F5C')

# ax.set_ylabel('Experienced crowding (# passengers)')
# plt.xlabel('Percentile')
# ax.title('Percentiles of average experienced crowding')

# plt.yscale('log')
# ax.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
ax.legend(loc='best')
ax.set_title('Average experienced crowding')


ax.set_ylim( 500 , 680 )
ax.spines['bottom'].set_visible(False)
ax.tick_params(labelbottom=False, bottom=False)

d = .015
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

fig.add_subplot(ax)





inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[1], wspace=0.1, hspace=0.1)

ax = plt.Subplot(fig, inner[1])


N = 5
marl_crowding = [np.mean(values) for values in scenario_data['overview_data_complete']['experienced_crowding']] #scenario_data['Average_experienced_crowding']
holding_crowding = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['experienced_crowding']] #scenario_holding_data['Average_experienced_crowding']

ind = np.arange(N)
width = 0.35

ax.bar(labels, marl_crowding, width, label='MARL', color='#247BA0')
ax.bar(ind + width, holding_crowding, width,
    label='Holding benchmark', color='#F25F5C')

ax.set_ylabel('Experienced crowding (# passengers)')
ax.get_yaxis().set_label_coords(-0.1, 1)
# plt.xlabel('Percentile')
# ax.title('Percentiles of average experienced crowding')

# plt.yscale('log')
# ax.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
# ax.legend(loc='best')



ax.set_ylim( 0, 180 )
ax.spines['top'].set_visible(False)
# ax.set_xlabel('Percentiles average experienced crowding')
# ax.get_yaxis().set_label_coords(-0.1, 0.9)

d= .015
kwargs.update(transform=ax.transAxes)  # switch to the bottom axes
ax.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal


fig.add_subplot(ax)

# --------------



############## COEFF OF VARIATION

inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[2], wspace=0.1, hspace=0.1)

ax = plt.Subplot(fig, inner[0])


N = 5
marl_cov = [np.mean(values) for values in scenario_data['overview_data_complete']['cov_of_var']] #scenario_data['av cov stops']
holding_cov = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['cov_of_var']] #scenario_holding_data['av cov stops']

ind = np.arange(N)
width = 0.35

ax.bar(labels, marl_cov, width, label='MARL', color='#247BA0')
ax.bar(ind + width, holding_cov, width,
    label='Holding benchmark', color='#F25F5C')

ax.set_ylabel('Coefficient of variation')
ax.set_xlabel('Percentile interval')
# ax.title('Percentiles of average coefficient of variation of the headways')

# ax.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
ax.legend(loc='best')
ax.set_title('Average coefficient of variation of the headways')
# inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[2], wspace=0.1, hspace=0.1)

# ax = plt.Subplot(fig, inner[0])
# ax.hist( delay_trips_list, density=False, bins=50 )
# ax.set_ylim( np.sort(hx3)[-1] - int(np.sort(hx3)[-2]), np.sort(hx3)[-1] + int(np.sort(hx3)[-2]/4) )
# ax.spines['bottom'].set_visible(False)
# ax.xaxis.tick_top()
# ax.tick_params(labeltop=False, direction='in')
#
# d = .015
# kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
# ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
# ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal
#
# fig.add_subplot(ax)
#
# inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[2], wspace=0.1, hspace=0.1)
#
# ax = plt.Subplot(fig, inner[1])
# ax.hist( delay_trips_list, density=False, bins=50 )
# ax.set_ylim( 0, np.sort(hx3)[-2] + int(np.sort(hx3)[-2]/4) )
# ax.spines['top'].set_visible(False)
# ax.set_xlabel('Delay at the start of the trip (s)')
# ax.set_ylabel('Number of occurences')
# ax.get_yaxis().set_label_coords(-0.1, 0.9)
#
# d= .015
# kwargs.update(transform=ax.transAxes)  # switch to the bottom axes
# ax.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
# ax.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal


fig.add_subplot(ax)

# ----------

inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[3], wspace=0.1, hspace=0.1)

ax = plt.Subplot(fig, inner[0])



# EXCESS WAITING TIME

N = 5
marl_excess = [np.mean(values) for values in scenario_data['overview_data_complete']['excess_waiting_time']] #scenario_data['av excess stops']
holding_excess = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['excess_waiting_time']] #scenario_holding_data['av excess stops']

ind = np.arange(N)
width = 0.35
ax.bar(ind, marl_excess, width, label='MARL', color='#247BA0')
ax.bar(ind + width, holding_excess, width,
    label='Holding benchmark', color='#F25F5C')

# ax.set_ylabel('Excess waiting time (s)')
# ax.set_xlabel('Percentile')
# ax.title('Percentiles of average excess waiting time')

# plt.yscale('log')
# ax.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
ax.legend(loc='best')
ax.set_title('Average excess waiting time')

ax.set_ylim(1780, 2000)
# ax.text(0.1, 2 , 'asdl;fj', color='black')
ax.spines['bottom'].set_visible(False)
ax.xaxis.tick_top()
ax.tick_params(labeltop=False, direction='out')

d = .015
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

fig.add_subplot(ax)





inner = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[3], wspace=0.1, hspace=0.1)

ax = plt.Subplot(fig, inner[1])

N = 5
marl_excess = [np.mean(values) for values in scenario_data['overview_data_complete']['excess_waiting_time']] #scenario_data['av excess stops']
holding_excess = [np.mean(values) for values in scenario_holding_data['overview_data_complete']['excess_waiting_time']] #scenario_holding_data['av excess stops']

ind = np.arange(N)
width = 0.35
ax.bar(labels, marl_excess, width, label='MARL', color='#247BA0')
ax.bar(ind + width, holding_excess, width,
    label='Holding benchmark', color='#F25F5C')

ax.set_ylabel('Excess waiting time (s)')
ax.set_xlabel('Percentile interval')
# ax.title('Percentiles of average excess waiting time')

# plt.yscale('log')
# ax.xticks(ind + width / 2, ('5th', '15th', '50th', '85th', '95th'))
# ax.legend(loc='best')



ax.set_ylim(0, 220)
ax.spines['top'].set_visible(False)
ax.get_yaxis().set_label_coords(-0.1, 1)

d = .015
kwargs.update(transform=ax.transAxes)  # switch to the bottom axes
ax.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

fig.add_subplot(ax)

# -----------------


fig.show()

plt.show()















############################################################################################
# BEHAVIOUR
############################################################################################

plt.rcParams.update({'font.size': 13})





scenario_data_list = []

for i in range(5):
    with open('percentiles_scenario_' + str(i+1) + '.json', 'r') as reading_file:
        scenario_data_list.append(json.load(reading_file))

scenario_holding_data_list = []
for i in range(5):
    if i != 2:
        with open('percentiles_scenario_' + str(i+1) + '_holding.json', 'r') as reading_file:
            scenario_holding_data_list.append(json.load(reading_file))
    if i == 2:
        with open('percentiles_scenario_' + str(i) + '_holding.json', 'r') as reading_file:
            scenario_holding_data_list.append(json.load(reading_file))



scenario_names = ['Idealised', 'Stochastic', 'Stochastic - Traffic Light Control', 'Heterogeneous - Deterministic', 'Realistic']



### SCENARIO P5 COMPARISON / OVERVIEW

### TRAVEL TIME

# Oude manier; overal de individuele p5 waarde van pakken

travel_time_p5 = True

travel_time_data, travel_time_data_holding = [], []
exp_crowding_data, exp_crowding_data_holding = [], []
av_coef_data, av_coef_data_holding = [], []
av_excess_data, av_excess_data_holding = [], []

if not travel_time_p5:

    for i in range(5):
        travel_time_data.append( scenario_data_list[i]["Average_travel_time_passengers"][0] )
        travel_time_data_holding.append( scenario_holding_data_list[i]["Average_travel_time_passengers"][0] )

        exp_crowding_data.append( scenario_data_list[i]["Average_experienced_crowding"][0] )
        exp_crowding_data_holding.append( scenario_holding_data_list[i]["Average_experienced_crowding"][0] )


        av_coef_data.append( scenario_data_list[i]["av cov stops"][0] )
        av_coef_data_holding.append( scenario_holding_data_list[i]["av cov stops"][0] )


        av_excess_data.append( scenario_data_list[i]["av excess stops"][0] )
        av_excess_data_holding.append( scenario_holding_data_list[i]["av excess stops"][0] )

else:
    for i in range(5):
        travel_time_data.append(scenario_data_list[i]['overview_data'][0])
        travel_time_data_holding.append(scenario_holding_data_list[i]['overview_data'][0])

        exp_crowding_data.append(scenario_data_list[i]['overview_data'][1])
        exp_crowding_data_holding.append(scenario_holding_data_list[i]['overview_data'][1])

        av_coef_data.append(scenario_data_list[i]['overview_data'][2])
        av_coef_data_holding.append(scenario_holding_data_list[i]['overview_data'][2])

        av_excess_data.append(scenario_data_list[i]['overview_data'][3])
        av_excess_data_holding.append(scenario_holding_data_list[i]['overview_data'][3])









N=5
ind = np.arange(N)
width = 0.35

# travel time

plt.subplot(2,2,1)
plt.bar(ind, travel_time_data, width, label='MARL', color='#247BA0')
plt.bar(ind + width, travel_time_data_holding, width, label='Holding benchmark', color='#F25F5C')

plt.title('Average total travel time')
plt.ylim(0, 3500)
plt.ylabel('Seconds')
plt.xticks(ind + width/2 , [1, 2, 3, 4, 5])
plt.legend(loc='best')


# experienced crowding

plt.subplot(2,2,2)
plt.bar(ind, exp_crowding_data, width, label='MARL', color='#247BA0')
plt.bar(ind + width, exp_crowding_data_holding, width, label='Holding benchmark', color='#F25F5C')

plt.title('Average experienced crowding')
plt.ylabel('Number of Passengers')
plt.xticks(ind + width/2 , [1, 2, 3, 4, 5])
plt.legend(loc='best')


# average coef of variation

plt.subplot(2,2,3)
plt.bar(ind, av_coef_data, width, label='MARL', color='#247BA0')
plt.bar(ind + width, av_coef_data_holding, width, label='Holding benchmark', color='#F25F5C')

plt.title('Average coefficient of variation of the headways')
plt.ylabel('')
plt.xlabel('Scenario')
plt.xticks(ind + width/2 , [1, 2, 3, 4, 5])
plt.legend(loc='best')


# excess waiting time

plt.subplot(2,2,4)
plt.bar(ind, av_excess_data, width, label='MARL', color='#247BA0')
plt.bar(ind + width, av_excess_data_holding, width, label='Holding benchmark', color='#F25F5C')

plt.title('Average excess waiting time')
plt.ylabel('Seconds')
plt.xlabel('Scenario')
plt.xticks(ind + width/2 , [1, 2, 3, 4, 5])
plt.legend(loc='best')


plt.show()






### HOLDING

marl_holding_overview = [np.mean(scenario_data_list[i]['overview_data_complete']['holding'][0]) for i in range(5)]
holding_holding_overview = [np.mean(scenario_holding_data_list[i]['overview_data_complete']['holding'][0]) for i in range(5)]



N=5
ind = np.arange(N)
width = 0.35

# travel time

plt.bar(ind, marl_holding_overview, width, label='MARL', color='#247BA0')
plt.bar(ind + width, holding_holding_overview, width, label='Holding benchmark', color='#F25F5C')

plt.title('Average holding time per stop')
# plt.ylim(0, 3500)
plt.ylabel('Seconds')
plt.xlabel('Scenario')
plt.xticks(ind + width/2 , [1, 2, 3, 4, 5])
plt.legend(loc='best')

plt.show()










### ACTIONS BUSSES
for i in range(5):
    N = 7
    marl_actions = scenario_data_list[i]['actions_busses']
    holding_actions = scenario_holding_data_list[i]['actions_busses']

    ind = np.arange(N)
    width = 0.35
    plt.subplot(2,3, i+1)
    plt.bar(ind, marl_actions, width, label='MARL', color='#247BA0')
    plt.bar(ind + width, holding_actions, width,
            label='Holding benchmark', color='#F25F5C')

    if i ==0 or i == 3:
        plt.ylabel('Fraction')
    if i != 0 and i != 1:
        plt.xlabel('Holding time (s)')
    plt.title(scenario_names[i])

    plt.xticks(ind + width / 2, ['0', '30', '60', '90', '120', '150', '180'])
    plt.legend(loc='best')

    if i == 4:
        plt.show()




# P5

for i in range(5):
    N = 7
    marl_actions = scenario_data_list[i]['actions_busses_p5']
    holding_actions = scenario_holding_data_list[i]['actions_busses_p5']

    ind = np.arange(N)
    width = 0.35
    plt.subplot(2,3, i+1)
    plt.bar(ind, marl_actions, width, label='MARL', color='#247BA0')
    plt.bar(ind + width, holding_actions, width,
            label='Holding benchmark', color='#F25F5C')

    if i ==0 or i == 3:
        plt.ylabel('Fraction')
    if i != 0 and i != 1:
        plt.xlabel('Holding time (s)')
    plt.title(scenario_names[i])

    plt.xticks(ind + width / 2, ['0', '30', '60', '90', '120', '150', '180'])
    plt.legend(loc='best')

    if i == 4:
        plt.show()













### TRAFFIC LIGHTS
tfl_scenarios = [2, 4]

for i in tfl_scenarios:
    N = 3
    marl_actions = scenario_data_list[i]['actions_tfls']
    marl_actions_p5 = scenario_data_list[i]['actions_tfls_p5']

    ind = np.arange(N)
    width = 0.35
    plt.subplot(1,2, int(i/2) )
    plt.bar(ind, marl_actions, width, label='MARL', color='#247BA0')
    plt.bar(ind+width, marl_actions_p5, width, label='MARL 5th percentile', color='#F25F5C')

    if i == 0:
        plt.ylabel('Fraction')
    plt.xlabel('Action')
    plt.title(scenario_names[i])

    plt.xticks(ind + width / 2, ['Nothing', '-10 s delay', '+10 s delay'])
    plt.legend(loc='best')

    if i == 4:
        plt.show()




### POLICIES BUSSES

for i in range(5):
    actions_present = scenario_data_list[i]['actions_present']
    actions_present_p5 = scenario_data_list[i]['actions_present_p5']
    policy_busses15 = scenario_data_list[i]['policy_busses15']
    policy_busses15_p5 = scenario_data_list[i]['policy_busses15_p5']
    policy_busses50 = scenario_data_list[i]['policy_busses50']
    policy_busses50_p5 = scenario_data_list[i]['policy_busses50_p5']
    policy_busses85 = scenario_data_list[i]['policy_busses85']
    policy_busses85_p5 = scenario_data_list[i]['policy_busses85_p5']

    plt.subplot(2,3, i+1)
    plt.plot(actions_present, policy_busses50, color='#247BA0', label='MARL - median')
    plt.plot(actions_present_p5, policy_busses50_p5, color='#F25F5C', label='MARL - fastest')
    plt.fill_between(actions_present, policy_busses15, policy_busses85, alpha=0.2, facecolor='#247BA0')
    # plt.fill_between(actions_present_p5, policy_busses15_p5, policy_busses85_p5, alpha=0.2, facecolor='#F25F5C')

    if i == 0 or i == 3:
        plt.ylabel('Observation received')
    plt.xticks(np.arange(7),  ['0', '30', '60', '90', '120', '150', '180'])

    if i != 0 and i != 1:
        plt.xlabel('Holding time (s)')
    plt.title(scenario_names[i])

    plt.legend(loc='best')

    if i == 4:
        plt.show()
