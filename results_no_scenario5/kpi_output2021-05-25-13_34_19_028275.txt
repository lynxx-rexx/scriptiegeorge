{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        491,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 8197.11178459271,
    "Average_passenger_waiting_time": 3625.8328939198996,
    "Average_passenger_in_vehicle_time": 4571.278890673055,
    "Average_speed_busses": 0.020458333333333332,
    "Average_experienced_crowding": 845.3112638540026,
    "Average_covariation_headway": 2.374742948985879,
    "Average_covariation_headway_stops": {
        "H": 2.217144342406653,
        "G": 1.9419201820890764,
        "F": 1.9520619443491871,
        "L": 2.3809864535917393,
        "E": 2.291504138249347,
        "K": 2.360979838874452,
        "I": 2.3260912079566127,
        "A": 2.3593597568986535,
        "J": 2.343424028937547,
        "B": 2.3382568713561453,
        "C": 2.307587102254551,
        "D": 2.2690822176510768
    },
    "Average_excess_waiting_time": {
        "H": 3459.974307506968,
        "G": 2768.859706553111,
        "F": 3265.645573779979,
        "L": 3961.4927820759103,
        "E": 3892.449837228015,
        "K": 3942.7499273853737,
        "I": 3826.793137438436,
        "A": 3955.045154864968,
        "J": 3940.2552134589396,
        "B": 3946.5571699537977,
        "C": 3919.0172031750153,
        "D": 3861.763210324144
    },
    "Holding_per_stop": {
        "H": 0,
        "F": 0,
        "G": 0,
        "L": 0,
        "E": 0,
        "K": 0,
        "I": 0,
        "A": 0,
        "J": 0,
        "B": 0,
        "C": 0,
        "D": 0
    },
    "Actions_tfl_edges": {
        "FG": [
            39,
            0,
            0
        ],
        "GH": [
            40,
            0,
            0
        ],
        "JK": [
            41,
            0,
            0
        ],
        "HI": [
            40,
            0,
            0
        ],
        "CD": [
            42,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 24.571428571428573,
            "I": 22.714285714285715,
            "J": 20.571428571428573,
            "K": 19.571428571428573,
            "L": 18.428571428571427,
            "A": 15.428571428571429,
            "B": 11.857142857142858,
            "C": 8.571428571428571,
            "D": 6.428571428571429,
            "E": 4.857142857142857,
            "F": 13.5,
            "G": 29.333333333333332
        },
        "bus2": {
            "G": 11.285714285714286,
            "H": 11.571428571428571,
            "I": 10.428571428571429,
            "J": 10.285714285714286,
            "K": 8.714285714285714,
            "L": 7,
            "A": 6.428571428571429,
            "B": 5.714285714285714,
            "C": 5.857142857142857,
            "D": 5.428571428571429,
            "E": 4.857142857142857,
            "F": 7.833333333333333
        },
        "bus1": {
            "F": 4.571428571428571,
            "G": 5.857142857142857,
            "H": 6.714285714285714,
            "I": 6.571428571428571,
            "J": 6.428571428571429,
            "K": 5.714285714285714,
            "L": 5.428571428571429,
            "A": 4.428571428571429,
            "B": 4,
            "C": 3.142857142857143,
            "D": 2.142857142857143,
            "E": 1.4285714285714286
        },
        "bus5": {
            "L": 775.2857142857143,
            "A": 688.1428571428571,
            "B": 600.2857142857143,
            "C": 559.8571428571429,
            "D": 541.4285714285714,
            "E": 536.4285714285714,
            "F": 786.5714285714286,
            "G": 1069.8333333333333,
            "H": 1175.5,
            "I": 1152.8333333333333,
            "J": 1092,
            "K": 997.6666666666666
        },
        "bus0": {
            "E": 3.125,
            "F": 7,
            "G": 10.142857142857142,
            "H": 11.142857142857142,
            "I": 11.285714285714286,
            "J": 10.714285714285714,
            "K": 10.285714285714286,
            "L": 8.714285714285714,
            "A": 7.142857142857143,
            "B": 5.285714285714286,
            "C": 4.142857142857143,
            "D": 3.5714285714285716
        },
        "bus4": {
            "K": 93,
            "L": 80.85714285714286,
            "A": 65.42857142857143,
            "B": 52.857142857142854,
            "C": 41.714285714285715,
            "D": 32.42857142857143,
            "E": 30,
            "F": 73.83333333333333,
            "G": 148,
            "H": 151.66666666666666,
            "I": 138.33333333333334,
            "J": 123
        }
    },
    "Obs_and_actions_busses": {
        "0": 37.539179437474395
    },
    "Obs_and_actions_tfls": {
        "0": -0.11396943883381402
    }
}