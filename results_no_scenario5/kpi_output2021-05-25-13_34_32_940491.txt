{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        564,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 7177.125033345918,
    "Average_passenger_waiting_time": 3088.178069525764,
    "Average_passenger_in_vehicle_time": 4088.9469638200553,
    "Average_speed_busses": 0.0235,
    "Average_experienced_crowding": 734.6681638171245,
    "Average_covariation_headway": 2.2393170018506425,
    "Average_covariation_headway_stops": {
        "H": 2.1779108620134298,
        "I": 2.3166540682236954,
        "A": 2.329764495807678,
        "B": 2.335365614928537,
        "F": 2.0392382020392357,
        "D": 2.295577362520986,
        "J": 2.325678756853156,
        "G": 1.8882707340900267,
        "E": 2.288163137369442,
        "K": 2.3187786942124555,
        "C": 2.2003333980034574,
        "L": 2.3200390372324353
    },
    "Average_excess_waiting_time": {
        "H": 3055.513452537386,
        "I": 3376.1049799999982,
        "A": 3513.6823158640555,
        "B": 3506.2684308237795,
        "F": 2626.457798968764,
        "D": 3151.504342207223,
        "J": 3449.6548627644697,
        "G": 2397.694582468942,
        "E": 3178.8306209755574,
        "K": 3475.833673997552,
        "C": 3538.9495501450083,
        "L": 3518.5500202354297
    },
    "Holding_per_stop": {
        "H": 0,
        "I": 0,
        "A": 0,
        "B": 0,
        "D": 0,
        "J": 0,
        "F": 0,
        "G": 0,
        "K": 0,
        "E": 0,
        "C": 0,
        "L": 0
    },
    "Actions_tfl_edges": {
        "CD": [
            45,
            0,
            0
        ],
        "GH": [
            47,
            0,
            0
        ],
        "HI": [
            48,
            0,
            0
        ],
        "JK": [
            48,
            0,
            0
        ],
        "FG": [
            46,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 122.625,
            "I": 111.375,
            "J": 100.75,
            "K": 90,
            "L": 78.75,
            "A": 65.875,
            "B": 52.25,
            "C": 36.57142857142857,
            "D": 28.571428571428573,
            "E": 23.857142857142858,
            "F": 58.714285714285715,
            "G": 131
        },
        "bus4": {
            "I": 844.875,
            "J": 803.25,
            "K": 737,
            "L": 669,
            "A": 594.625,
            "B": 517.75,
            "C": 470.375,
            "D": 410.7142857142857,
            "E": 415,
            "F": 620.4285714285714,
            "G": 910.5714285714286,
            "H": 991.7142857142857
        },
        "bus5": {
            "A": 8.777777777777779,
            "B": 6.777777777777778,
            "C": 7.375,
            "D": 7.875,
            "E": 6.875,
            "F": 12.625,
            "G": 18.5,
            "H": 18,
            "I": 17,
            "J": 15.75,
            "K": 14.25,
            "L": 12.375
        },
        "bus0": {
            "B": 19.444444444444443,
            "C": 19,
            "D": 17.5,
            "E": 17.5,
            "F": 29.25,
            "G": 43.875,
            "H": 48,
            "I": 47.5,
            "J": 43.75,
            "K": 38.75,
            "L": 33.375,
            "A": 27.75
        },
        "bus2": {
            "F": 25,
            "G": 47.125,
            "H": 49.5,
            "I": 46,
            "J": 43.25,
            "K": 38.75,
            "L": 35.625,
            "A": 30.125,
            "B": 25.125,
            "C": 22.714285714285715,
            "D": 19.714285714285715,
            "E": 18
        },
        "bus1": {
            "D": 7.125,
            "E": 7.25,
            "F": 12,
            "G": 19.625,
            "H": 20.25,
            "I": 17.5,
            "J": 16.125,
            "K": 14,
            "L": 13.375,
            "A": 12.125,
            "B": 10.5,
            "C": 10.571428571428571
        }
    },
    "Obs_and_actions_busses": {
        "0": 32.269098128346656
    },
    "Obs_and_actions_tfls": {
        "0": -860.7125226601158
    }
}