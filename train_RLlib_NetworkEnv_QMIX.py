#from sklearn import tree
import ray
from ray import rllib
from ray import tune
from ray.rllib.agents.registry import get_agent_class
from ray.rllib.models import ModelCatalog
from ray.tune import run_experiments
from ray.tune.registry import register_env
from ray.rllib.agents.ppo import PPOTrainer




# Import environment definition
# from environment import IrrigationEnv
from Network_implementation_V2 import NetworkEnv


node_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                     ]


kwargs_dict = {
            'edges': adjacency_matrix,
            'node_names': node_list,
            'num_agents': 3,
            'routes': [node_list, node_list, node_list],
            'resetted': False,
            'qmix': True,
            'holding_fixed': False,
            'incl_pass_stops': False

        }




# Driver code for training
def setup_and_train():

    # Create a single environment and register it
    def env_creator(_):
        return NetworkEnv( adjacency_matrix, node_list, 2, qmix=True )
    single_env = NetworkEnv( adjacency_matrix, node_list, 2, qmix=True )
    env_name = "NetworkEnv"
    register_env(env_name, env_creator)

    # Get environment obs, action spaces and number of agents
    obs_space = single_env.observation_space
    act_space = single_env.action_space
    num_agents = single_env.num_agents

    # Create a policy mapping
    def gen_policy():
        return (None, obs_space, act_space, {})

    policy_graphs = {}
    for i in range(num_agents):
        policy_graphs['agent-' + str(i)] = gen_policy()

    def policy_mapping_fn(agent_id):
        return 'agent-' + str(agent_id)

    # Define configuration with hyperparam and training details
    config={
                "log_level": "WARN",
                # "num_workers": 3,
                "num_cpus_for_driver": 1,
                "num_cpus_per_worker": 1,
                # "num_gpus_per_worker": 0.3,
                # "resources_per_trial": {"cpu": 1, "gpu": 0},
                # "num_sgd_iter": 10,
                # "train_batch_size": tune.grid_search([128, 256, 512]),
                # "train_batch_size": 256,
                # "lr": 5e-3,
                #"lr": tune.uniform(0, 0.15),
                # "lr": tune.grid_search([0.01, 0.001, 0.0001]),
                # "model":{"fcnet_hiddens": [16, 16]},


                # === QMix ===
                # Mixing network. Either "qmix", "vdn", or None
                "mixer": "qmix",
                # Size of the mixing network embedding
                "mixing_embed_dim": 32,
                # Whether to use Double_Q learning
                "double_q": True,
                # Optimize over complete episodes by default.
                "batch_mode": "complete_episodes",

                # === Exploration Settings ===
                "exploration_config": {
                    # The Exploration class to use.
                    "type": "EpsilonGreedy",
                    # Config for the Exploration class' constructor:
                    "initial_epsilon": 1.0,
                    "final_epsilon": 0.02,
                    "epsilon_timesteps": 10000,  # Timesteps over which to anneal epsilon.

                    # For soft_q, use:
                    # "exploration_config" = {
                    #   "type": "SoftQ"
                    #   "temperature": [float, e.g. 1.0]
                    # }
                },

                # === Evaluation ===
                # Evaluate with epsilon=0 every `evaluation_interval` training iterations.
                # The evaluation stats will be reported under the "evaluation" metric key.
                # Note that evaluation is currently not parallelized, and that for Ape-X
                # metrics are already only reported for the lowest epsilon workers.
                "evaluation_interval": None,
                # Number of episodes to run per evaluation period.
                "evaluation_num_episodes": 10,
                # Switch to greedy actions in evaluation workers.
                "evaluation_config": {
                    "explore": False,
                },

                # Number of env steps to optimize for before returning
                "timesteps_per_iteration": 4100,
                # Update the target network every `target_network_update_freq` steps.
                "target_network_update_freq": 410,

                # === Replay buffer ===
                # Size of the replay buffer in steps.
                "buffer_size": 41000,

                # === Optimization ===
                # Learning rate for RMSProp optimizer
                "lr": 0.0005,
                # RMSProp alpha
                "optim_alpha": 0.99,
                # RMSProp epsilon
                "optim_eps": 0.00001,
                # If not None, clip gradients during optimization at this value
                "grad_norm_clipping": 10,
                # How many steps of the model to sample before learning starts.
                "learning_starts": 410,
                # Update the replay buffer with this many samples at once. Note that
                # this setting applies per-worker if num_workers > 1.
                "rollout_fragment_length": 4,
                # Size of a batched sampled from replay buffer for training. Note that
                # if async_updates is set, then each worker returns gradients for a
                # batch of this size.
                "train_batch_size": 32,

                # === Parallelism ===
                # Number of workers for collecting samples with. This only makes sense
                # to increase if your environment is particularly slow to sample, or if
                # you"re using the Async or Ape-X optimizers.
                "num_workers": 2,
                # Whether to use a distribution of epsilons across workers for exploration.
                "per_worker_exploration": False,
                # Whether to compute priorities on workers.
                "worker_side_prioritization": False,
                # Prevent iterations from going lower than this time span
                "min_iter_time_s": 50,

                # === Model ===
                "model": {
                    "lstm_cell_size": 64,
                    "max_seq_len": 999999,
                },




                "multiagent": {
                    "policies": policy_graphs,
                    "policy_mapping_fn": policy_mapping_fn,
                },
                "env": "NetworkEnv"}

    # Define experiment details
    exp_name = 'my_exp'
    exp_dict = {
            'name': exp_name,
            'run_or_experiment': 'QMIX',
            "stop": {
                # training_iteration geeft aan hoe vaak hij de training herhaalt
                "training_iteration": 50
            },
            'checkpoint_freq': 2,
            # "restore": 'C:\\Users\\georg\\ray_results\\my_exp\\PPO_NetworkEnv_09293_00000_0_2020-11-23_13-47-24\\checkpoint_250\\checkpoint-250',
            "config": config,
            # num_samples geeft aan hoe vaak je elke gridsearch punt wil bezoeken
            # "num_samples": 1,
            #"local_dir": "~/tune_results",
        }

    # Initialize ray and run
    ray.init()
    results = tune.run(**exp_dict)
    print('##### Best config #####')
    print( results.get_best_config(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )
    print('##### Best trial #####')
    print( results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )

    # print( results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )
    # print(type(results.results), results.results )
    # for key, value in results.results.items():
    #     print('###  ', key, '  ###')
    #     for key2, val2 in value.items():
    #         print(key2)
    #
    # avg_reward_list = []
    #
    # for key, val in results.results.items():
    #     avg_reward_list.append(val['episode_reward_mean'])
    #
    # print(avg_reward_list)

    #print(type(results.stats()), results.stats())

    #print(type(results.runner_data()), results.runner_data())

if __name__=='__main__':
    setup_and_train()
