import pandas as pd
import numpy as np
import simpy
import random
import statistics
from functools import wraps, partial
import matplotlib.pyplot as plt

wait_times = []






# MONITORING SECITON
### ---------------------------------------------------------------------------------------------------------- ###

def patch_resource(resource, pre=None, post=None):
    """Patch *resource* so that it calls the callable *pre* before each
    put/get/request/release operation and the callable *post* after each
    operation.  The only argument to these functions is the resource
    instance.
    """
    def get_wrapper(func):
        # Generate a wrapper for put/get/request/release
        @wraps(func)
        def wrapper(*args, **kwargs):
            # This is the actual wrapper
            # Call "pre" callback
            if pre:
                pre(resource)
            # Perform actual operation
            ret = func(*args, **kwargs)
            # Call "post" callback
            if post:
                post(resource)
            return ret
        return wrapper
    # Replace the original operations with our wrapper
    for name in ['put', 'get', 'request', 'release']:
        if hasattr(resource, name):
            setattr(resource, name, get_wrapper(getattr(resource, name)))

def monitor(data, resource):
    """This is our monitoring callback."""
    item = (
        resource._env.now,  # The current simulation time
        resource.count,  # The number of users
        len(resource.queue),  # The number of queued processes
    )
    data.append(item)

### ----------------------------------------------------------------------------------------------------------- ###






class Theater(object):
    def __init__(self, env, num_cashiers, num_ushers, num_servers):
        self.env = env
        self.cashier = simpy.Resource(env, num_cashiers)
        self.usher = simpy.Resource(env, num_ushers)
        self.server = simpy.Resource(env, num_servers)
        self.cashier_observations = []
        self.usher_observations = []
        self.server_observations = []


        # Bind *data* as first argument to monitor()
        # see https://docs.python.org/3/library/functools.html#functools.partial
        self.monitor_cashier = partial(monitor, self.cashier_observations)
        patch_resource(self.cashier, post=self.monitor_cashier)  # Patches (only) this resource instance

        self.monitor_usher = partial(monitor, self.usher_observations)
        patch_resource(self.usher, post=self.monitor_usher)  # Patches (only) this resource instance

        self.monitor_server = partial(monitor, self.server_observations)
        patch_resource(self.server, post=self.monitor_server)  # Patches (only) this resource instance

    def purchase_ticket(self, moviegoer):
        yield self.env.timeout(random.randint(1,3))

    def check_ticket(self, moviegoer):
        yield self.env.timeout( 3/60 )

    def sell_food(self, moviegoer):
        yield self.env.timeout(random.randint(1,5))

    def plot_queues(self):
        cashier_Q = [i[2] for i in self.cashier_observations]
        cashier_xs = [i[0] for i in self.cashier_observations]
        usher_Q = [i[2] for i in self.usher_observations]
        usher_xs = [i[0] for i in self.usher_observations]
        server_Q = [i[2] for i in self.server_observations]
        server_xs = [i[0] for i in self.server_observations]

        plt.plot(cashier_xs, cashier_Q, label='Cashier Q')
        plt.plot(usher_xs, usher_Q, label='Usher Q')
        plt.plot(server_xs, server_Q, label='Server Q')
        plt.legend()
        plt.show()


def go_to_movies(env, moviegoer, theater, final=False):
    arrival_time = env.now

    with theater.cashier.request() as request:
        yield request
        yield env.process( theater.purchase_ticket(moviegoer) )

    with theater.usher.request() as request:
        yield request
        yield env.process(theater.check_ticket(moviegoer))

    if random.choice([True, False]):
        with theater.server.request() as request:
            yield request
            yield env.process(theater.sell_food(moviegoer))

    wait_times.append(env.now - arrival_time)


    if False:
        cashier_Q = [i[2] for i in theater.cashier_observations]
        cashier_xs = [i[0] for i in theater.cashier_observations]
        usher_Q = [i[2] for i in theater.usher_observations]
        usher_xs = [i[0] for i in theater.usher_observations]
        server_Q = [i[2] for i in theater.server_observations]
        server_xs = [i[0] for i in theater.server_observations]

        plt.plot(cashier_xs, cashier_Q, label='Cashier Q')
        plt.plot(usher_xs, usher_Q, label='Usher Q')
        plt.plot(server_xs, server_Q, label='Server Q')
        plt.legend()
        plt.show()




def run_theater(env, num_cashiers, num_ushers, num_servers):
    theater = Theater(env, num_cashiers, num_ushers, num_servers)

    for moviegoer in range(3):
        env.process(go_to_movies(env, moviegoer, theater))

    number_of_visitors = 100
    while moviegoer < number_of_visitors:

        if moviegoer == number_of_visitors - 1:
            yield env.timeout(0.2)
            moviegoer += 1
            env.process(go_to_movies(env, moviegoer, theater, final=True))

        yield env.timeout(0.2)
        moviegoer += 1
        env.process(go_to_movies(env, moviegoer, theater))

    yield env.timeout(220)

    theater.plot_queues()





def get_average_wait_time(wait_times):
    average_wait = statistics.mean(wait_times)

    minutes, frac_minutes = divmod(average_wait, 1)
    seconds = frac_minutes * 60
    return round(minutes), round(seconds)


def get_user_input():
    num_cashiers = input("Input the # of cashiers working: ")
    num_servers = input("Input the # of servers working: ")
    num_ushers = input("Input the # of ushers working: ")

    params = [num_cashiers, num_servers, num_ushers]

    if all(str(i).isdigit() for i in params):
        params = [int(x) for x in params]

    else:
        print('Could not parse input. The simulation will use default values:', \
              '\n1 cashier, 1 server, 1 usher.')
        params = [1,1,1]

    return params






def main():
    random.seed(10)

    num_cashiers, num_servers, num_ushers = get_user_input()

    env = simpy.Environment()
    env.process(run_theater(env, num_cashiers, num_ushers, num_servers))
    env.run(until=300)

    mins, secs = get_average_wait_time(wait_times)

    print('Simulation complete average waiting time=\t', f" {mins} minutes and {secs} seconds. " )





if __name__ == "__main__":
    main()
