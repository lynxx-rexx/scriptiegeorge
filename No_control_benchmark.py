import gym
import numpy as np
import mlflow

from Network_implementation_V2 import NetworkEnv
# CHECKLIST
# is the result folder set correctly?
# is the scenario set correctly?
# is the observation option set to "benchmark"?


node_list = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
]

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
]


kwargs_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": True,
    "result_folder": "results_no_scenario5\\",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(3),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "simple_headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        False,
        False,
        True,
        False,
        False,
        True,
        True,
        True,
        False,
        True,
        False,
        False,
    ],  # False, False, False ],
    "cycle_props": [
        None,
        None,
        (75, 40),
        None,
        None,
        (75, 40),
        (75, 40),
        (75, 40),
        None,
        (75, 40),
        None,
        None,
    ],  # None, None, None],# (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833, 0.00833, 0.013333, 0.01666, 0.01666, 0.05, 0.06667, 0.03333, 0.01666, 0.0125, 0.01, 0.00833 ],
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("lognormal", 360, 20, 180, 720),
        "BC": ("lognormal", 360, 20, 180, 720),
        "CD": ("lognormal", 300, 20, 150, 600),
        "DE": ("lognormal", 240, 20, 120, 480),
        "EF": ("lognormal", 240, 20, 120, 480),
        "FG": ("lognormal", 120, 20, 60, 240),
        "GH": ("lognormal", 90, 20, 45, 180),
        "HI": ("lognormal", 150, 20, 75, 300),
        "IJ": ("lognormal", 240, 20, 120, 480),
        "JK": ("lognormal", 240, 20, 120, 480),
        "KL": ("lognormal", 240, 20, 120, 480),
        "LA": ("lognormal", 300, 20, 150, 600),
    },
    #     "LM": 240,
    #     "MN": 240,
    #     "NO": 240,
    #     "OA": 240,
    # },
    "training_run": False
}









def execute_control(example_network_object):
    obs = {}
    done = {"__all__": False}
    while not done["__all__"]:
        action = {}
        if obs:
            for bus in obs:
                action[bus] = 0

        obs, rew, done, info = example_network_object.step(action)


if __name__ =="__main__":
    for i in range(10):
        example_network_object = NetworkEnv(**kwargs_dict)

        execute_control(example_network_object)