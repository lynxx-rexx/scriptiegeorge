import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json

# https://coolors.co/50514f-f25f5c-ffe066-247ba0-70c1b3
plt.rcParams.update({'font.size': 18})
scenario = 2
holding_ylim = True
ylim = [1, 1, 0, 1]



scenario_holding_data_list = []
scenario_no_data_list = []
for i in range(5):
    if i != 2:
        with open('results_holding_scenario' + str(i+1) + '\\average_kpis.json', 'r') as reading_file:
            scenario_holding_data_list.append(json.load(reading_file))
        with open('results_no_scenario' + str(i+1) + '\\average_kpis.json', 'r') as reading_file:
            scenario_no_data_list.append(json.load(reading_file))
    if i == 2:
        with open('results_holding_scenario' + str(i) + '\\average_kpis.json', 'r') as reading_file:
            scenario_holding_data_list.append(json.load(reading_file))
        with open('results_no_scenario' + str(i) + '\\average_kpis.json', 'r') as reading_file:
            scenario_no_data_list.append(json.load(reading_file))



scenario_names = ['Idealised', 'Stochastic', 'Stochastic - Traffic Light Control', 'Deterministic - Heterogeneous', 'Realistic']





# GEMIDDELDE METRIC WAARDEN VOOR ELK SCENARIO (holding benchmark & no control)

average_holding_holding = []
average_holding_no = []

average_travel_time_holding = []
average_travel_time_no = []
average_in_time_holding = []
average_in_time_no = []
average_waiting_time_holding = []
average_waiting_time_no = []

average_crowding_holding = []
average_crowding_no = []
average_coef_holding = []
average_coef_no = []
average_excess_holding = []
average_excess_no = []



for i in range(5):
    average_holding_holding.append(scenario_holding_data_list[i]['Average_holding_time'][0])
    average_holding_no.append(scenario_no_data_list[i]['Average_holding_time'][0])

    average_travel_time_holding.append(scenario_holding_data_list[i]['Average_travel_time_passengers'][0])
    average_travel_time_no.append(scenario_no_data_list[i]['Average_travel_time_passengers'][0])
    average_in_time_holding.append(scenario_holding_data_list[i]['Average_passenger_in_vehicle_time'][0])
    average_in_time_no.append(scenario_no_data_list[i]['Average_passenger_in_vehicle_time'][0])
    average_waiting_time_holding.append(scenario_holding_data_list[i]['Average_passenger_waiting_time'][0])
    average_waiting_time_no.append(scenario_no_data_list[i]['Average_passenger_waiting_time'][0])

    average_crowding_holding.append(scenario_holding_data_list[i]['Average_experienced_crowding'][0])
    average_crowding_no.append(scenario_no_data_list[i]['Average_experienced_crowding'][0])

    temp_list_coef = []
    temp_list_coef_no = []
    temp_list_excess = []
    temp_list_excess_no = []
    for dictionary_stops in scenario_holding_data_list[i]['Average_covariation_headway_stops']:
        for key, value in dictionary_stops.items():
            temp_list_coef.append(value)
    for dictionary_stops in scenario_no_data_list[i]['Average_covariation_headway_stops']:
        for key, value in dictionary_stops.items():
            temp_list_coef_no.append(value)

    for dictionary_stops in scenario_holding_data_list[i]['Average_excess_waiting_time']:
        for key, value in dictionary_stops.items():
            temp_list_excess.append(value)
    for dictionary_stops in scenario_no_data_list[i]['Average_excess_waiting_time']:
        for key, value in dictionary_stops.items():
            temp_list_excess_no.append(value)


    average_coef_holding.append(np.mean(temp_list_coef))
    average_coef_no.append(np.mean(temp_list_coef_no))

    average_excess_holding.append(np.mean(temp_list_excess))
    average_excess_no.append(np.mean(temp_list_excess_no))


# nu heb ik voor beide benchmarks voor elke metric een lijst met de gemiddelde waarden van de metrics voor alle 5 scenarios








holding_data_policies, waiting_data_policies, in_data_policies, exp_data_policies, coef_data_policies, excess_data_policies = [], [], [], [], [], []

for i in range(5):
    # data = pd.read_csv('results_scenario' + str(scenario) + '\\' + str(i) + '\\' + 'average_kpis.json')
    with open('results_scenario' + str(scenario) + '\\' + str(i) + '\\' + 'average_kpis.json', 'r') as reading_file:
        data = json.load(reading_file)

        holding_data_policies.append(data['Average_holding_time'][0])
        waiting_data_policies.append(data['Average_passenger_waiting_time'][0])
        in_data_policies.append(data['Average_passenger_in_vehicle_time'][0])
        exp_data_policies.append(data['Average_experienced_crowding'][0])

        temp_list_coef = []
        temp_list_coef_no = []
        temp_list_excess = []
        temp_list_excess_no = []
        for dictionary_stops in data['Average_covariation_headway_stops']:
            for key, value in dictionary_stops.items():
                temp_list_coef.append(value)

        for dictionary_stops in data['Average_excess_waiting_time']:
            for key, value in dictionary_stops.items():
                temp_list_excess.append(value)


        coef_data_policies.append(np.mean(temp_list_coef))
        excess_data_policies.append(np.mean(temp_list_excess))




# HOLDING



N = 5
ind = np.arange(N)
width = 0.35
plt.bar(ind, holding_data_policies, width, color='#247BA0', label='MARL')
plt.axhline(y=average_holding_holding[scenario-1], color='#F25F5C', label='Benchmark')


plt.ylabel('Time (s)')
plt.xlabel('Policy')
plt.title('Average holding time per stop')

if holding_ylim:
    plt.ylim(0, int(average_holding_holding[scenario-1] * 1.1) )
plt.xticks(ind, ind+1)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='best')
plt.show()

















# TRAVEL TIME

N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,1)
plt.bar(ind, waiting_data_policies, width, label='Waiting time', color='#247BA0')
plt.bar(ind, in_data_policies, width, bottom=waiting_data_policies, label='In vehicle time', color='#70C1B3')
plt.axhline(y=average_waiting_time_holding[scenario-1], color='#F25F5C', label='Waiting time benchmark')
plt.axhline(y=average_in_time_holding[scenario-1] + average_waiting_time_holding[scenario-1], color='#50514F', label='Total travel time benchmark')


plt.ylabel('Time (s)')
# plt.xlabel('Percentile')
plt.title('Average total travel time')

if ylim[0]:
    plt.ylim(0, int((average_in_time_holding[scenario-1] + average_waiting_time_holding[scenario-1]) * 1.1) )
plt.xticks(ind, ind + 1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
# plt.legend(loc='best', ncol=2)
plt.legend(loc='center right')
# plt.show()




# EXPERIENCED CROWDING

N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,2)
plt.bar(ind, exp_data_policies, width, color='#247BA0', label='MARL')
plt.axhline(y=average_crowding_holding[scenario-1], color='#F25F5C', label='Benchmark')

plt.ylabel('Number of passengers')
# plt.xlabel('Percentile')
plt.title('Average experienced crowding')

if ylim[1]:
    plt.ylim(0, average_crowding_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, ind+1)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='best')
# plt.show()





# COEFF OF VAR

N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,3)
plt.bar(ind, coef_data_policies, width, color='#247BA0', label='MARL')
plt.axhline(y=average_coef_holding[scenario-1], color='#F25F5C', label='Benchmark')

plt.ylabel('Coefficient of variation')
plt.xlabel('Policy')
plt.title('Average coefficient of variation of the headways')

if ylim[2]:
    plt.ylim(0, average_coef_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, ind+1)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='best')
# plt.show()





# EXCESS WAITING TIME


N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,4)
plt.bar(ind, excess_data_policies, width, color='#247BA0', label='MARL')
plt.axhline(y=average_excess_holding[scenario-1], color='#F25F5C', label='Benchmark')

plt.ylabel('Time (s)')
plt.xlabel('Policy')
plt.title('Average excess waiting time')

if ylim[3]:
    plt.ylim(0, average_excess_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, ind+1)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='best')
plt.show()





