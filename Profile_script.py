from Network_implementation_V2 import NetworkEnv
import json
import gym
import numpy as np
import cProfile, pstats, io


node_list = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"]

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]


init_dict = {
    "test_run": False,
    "monitoring": False,
    "run_logging": False,
    "passenger_logging": False,
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(2),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "off",
    "qmix": False,
    "holding_fixed": True,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "traffic_light_on_edges": [False, False, False, False, False, False, False, True, True, True, False, False, False, False, False ],
    "cycle_props": [None, None, None, None, None, None, None, (90, 65), (90, 65), (90, 65), None, None, None, None, None],
    "arrival_function_nodes": ["poisson"] * 15,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0],
        [0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
        [0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833]*15,
    "boarding_rate": 5,
    "alighting_rate": 5,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("gamma", 150, 15, 80, 220),
        "BC": ("gamma", 150, 15, 80, 220),
        "CD": ("gamma", 150, 15, 80, 220),
        "DE": ("gamma", 150, 15, 80, 220),
        "EF": ("gamma", 150, 15, 80, 220),
        "FG": ("gamma", 150, 15, 80, 220),
        "GH": ("gamma", 150, 15, 80, 220),
        "HI": ("gamma", 150, 15, 80, 220),
        "IJ": ("gamma", 150, 15, 80, 220),
        "JK": ("gamma", 150, 15, 80, 220),
        "KL": ("gamma", 150, 15, 80, 220),
        "LM": ("gamma", 150, 15, 80, 220),
        "MN": ("gamma", 150, 15, 80, 220),
        "NO": ("gamma", 150, 15, 80, 220),
        "OA": ("gamma", 150, 15, 80, 220),
    },
}







def profile(fnc):

    """A decorator that uses cProfile to profile a function"""

    def inner(*args, **kwargs):

        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval

    return inner


# def generate_network(init_dict):
#     return NetworkEnv(**init_dict)


@profile
def run_simulation(network_object):
    obs = {}
    done = {"__all__": False}
    while not done["__all__"]:
        action = {}
        if obs:
            for bus in obs:
                action[bus] = 1
        obs, rew, done, info = network_object.step(action)




if __name__== "__main__":

    run_simulation(NetworkEnv(**init_dict))