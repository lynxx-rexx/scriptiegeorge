import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statistics
import scipy.stats as stats
import json

std_list = []

with open("processed_data_full.json", 'r') as reading_file:
    output_dict = json.load(reading_file)


plot_list = [55000600, 55040660, 55040680, 56230080, 56330600, 56330620, 56330640, 56330660, 56330680, 57430760, 57430780, 57430800, 57330600, 57330221, 57330020,
             57240600, 57240060, 58252000, 57252670, 57250530, 57252680, 58252010, 57240050, 57240610, 57330010, 57330224, 57330610, 57430750, 57430730, 56330710,
             56330690, 56330670, 56330650, 56330630, 56230610, 56230070, 55040670, 55000630,]

def check_not_outlier(x, median, std_dev):
    if (x < median + 3*std_dev) and (x > median - 3*std_dev):
        return True
    else:
        return False

# INDIVIDUAL PLOTS
# https://coolors.co/50514f-f25f5c-ffe066-247ba0-70c1b3
for number, i in enumerate(plot_list):
    x_data = output_dict[str(i)]["realised"]
    x_data = [x for x in x_data if x > 0]
    std_dev = statistics.stdev(x_data)
    median = statistics.median(x_data)
    x_data = [x for x in x_data if check_not_outlier(x, median, std_dev)]
    planned_value = output_dict[str(i)]["planned"]

    plt.hist(x_data, label="Realised", density=True,color="#70c1b3", edgecolor="w")
    plt.axvline(x=planned_value[0], color="#50514f", label='Planned')

    # gamma = stats.gamma
    lognorm = stats.lognorm
    x = np.linspace(min(x_data), max(x_data), 100)

    # fit
    # param = gamma.fit(x_data)
    # returns shape, loc, scale --> std = shape & mean = log(scale)
    param = lognorm.fit(x_data)
    std_list.append(param[0])
    # pdf_fitted = gamma.pdf(x, *param)
    pdf_fitted = lognorm.pdf(x, *param)
    plt.plot(x, pdf_fitted, color='#f25f5c', label="Lognorm Fit")

    plt.legend(fontsize=10)
    plt.title("Route section " + str(number), fontsize=16)
    plt.xlabel("Time (minutes)", fontsize=13)
    plt.ylabel("Probability density", fontsize=13)

    plt.show()


print(statistics.mean(std_list)*60) # =9


# GRID PLOTS
for number, i in enumerate(plot_list):
    x_data = output_dict[str(i)]["realised"]
    x_data = [x for x in x_data if x > 0]
    std_dev = statistics.stdev(x_data)
    median = statistics.median(x_data)
    x_data = [x for x in x_data if check_not_outlier(x, median, std_dev)]
    planned_value = output_dict[str(i)]["planned"]

    plt.subplot(2, 3, (number % 6) +1)
    plt.hist(x_data, label="Realised", density=True,color="#70c1b3", edgecolor="w")
    plt.axvline(x=planned_value[0], color="#50514f", label='Planned')

    # gamma = stats.gamma
    lognorm = stats.lognorm
    x = np.linspace(min(x_data), max(x_data), 100)

    # fit
    # param = gamma.fit(x_data)
    param = lognorm.fit(x_data)
    # pdf_fitted = gamma.pdf(x, *param)
    pdf_fitted = lognorm.pdf(x, *param)
    plt.plot(x, pdf_fitted, color='#f25f5c', label="Lognorm Fit")

    plt.legend()
    plt.title("Route section " + str(number), fontsize=16)

    if (number % 6) > 2:
        plt.xlabel("Time (minutes)", fontsize=13)

    if (number) % 3 == 0:
        plt.ylabel("Probability density", fontsize=13)

    if (number+1) % 6 == 0:
        plt.show()