{
    "Average_holding_time": 11.25,
    "Average_holding_actions": [
        987,
        0,
        131,
        0,
        34,
        0
    ],
    "Average_travel_time_passengers": 2639.2401874483494,
    "Average_passenger_waiting_time": 331.64495732081747,
    "Average_passenger_in_vehicle_time": 2307.5952301274015,
    "Average_speed_busses": 0.048,
    "Average_experienced_crowding": 110.95946482288123,
    "Average_covariation_headway": 0.3445129983842451,
    "Average_covariation_headway_stops": {
        "H": 0.3151254144846201,
        "I": 0.330346787138832,
        "J": 0.31461525981305855,
        "K": 0.30052928438010656,
        "L": 0.28858530394663384,
        "C": 0.24282419011562964,
        "A": 0.266908877282903,
        "D": 0.23075273733056725,
        "B": 0.25422251360534753,
        "E": 0.22856358293987203,
        "F": 0.2335906888516129,
        "G": 0.26450266935992667
    },
    "Average_excess_waiting_time": {
        "H": 73.0161798102485,
        "I": 73.7650723143716,
        "J": 68.22491509285987,
        "K": 63.75431702224324,
        "L": 59.819670458519056,
        "C": 55.050495851378685,
        "A": 57.62172868872034,
        "D": 55.61077421110667,
        "B": 58.000392264815275,
        "E": 56.88479086265119,
        "F": 60.49109206030715,
        "G": 65.92484979125248
    },
    "Holding_per_stop": {
        "I": 16.5625,
        "H": 8.526315789473685,
        "K": 11.1340206185567,
        "L": 8.65979381443299,
        "J": 11.25,
        "C": 6.185567010309279,
        "A": 7.422680412371134,
        "D": 6.25,
        "B": 10.3125,
        "E": 15.3125,
        "F": 7.578947368421052,
        "G": 26.170212765957448
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus1": {
            "H": 130.625,
            "I": 126.625,
            "J": 119.1875,
            "K": 110.25,
            "L": 101.1875,
            "A": 91.4375,
            "B": 83.3125,
            "C": 78.6875,
            "D": 72.1875,
            "E": 70.75,
            "F": 95.53333333333333,
            "G": 127.46666666666667
        },
        "bus2": {
            "I": 133.0625,
            "J": 125.6875,
            "K": 116.375,
            "L": 107.8125,
            "A": 97.5625,
            "B": 88,
            "C": 82.875,
            "D": 78.8125,
            "E": 76.625,
            "F": 101.0625,
            "G": 134.06666666666666,
            "H": 146.26666666666668
        },
        "bus3": {
            "J": 120.5,
            "K": 111.0625,
            "L": 102.8125,
            "A": 93.3125,
            "B": 83.1875,
            "C": 79.125,
            "D": 75.375,
            "E": 73.875,
            "F": 97.0625,
            "G": 131,
            "H": 138.5,
            "I": 134.9375
        },
        "bus4": {
            "K": 100.29411764705883,
            "L": 90.76470588235294,
            "A": 79.6875,
            "B": 70.9375,
            "C": 67.3125,
            "D": 65.3125,
            "E": 65.3125,
            "F": 88.6875,
            "G": 117.6875,
            "H": 126.1875,
            "I": 121.1875,
            "J": 115.8125
        },
        "bus5": {
            "L": 86.47058823529412,
            "A": 79.47058823529412,
            "B": 71.4375,
            "C": 68.75,
            "D": 66.0625,
            "E": 66.9375,
            "F": 87.1875,
            "G": 112,
            "H": 119.125,
            "I": 115.375,
            "J": 110.0625,
            "K": 100.6875
        },
        "bus0": {
            "C": 84.47058823529412,
            "D": 82.1875,
            "E": 80.375,
            "F": 104.125,
            "G": 138.3125,
            "H": 147.75,
            "I": 141.875,
            "J": 133.6875,
            "K": 125.3125,
            "L": 115.5625,
            "A": 105.25,
            "B": 95.1875
        }
    },
    "Obs_and_actions_busses": {
        "5": 502.27731806480995,
        "0": -180.61634825730246,
        "2": 112.05900523292487
    },
    "Obs_and_actions_tfls": {}
}