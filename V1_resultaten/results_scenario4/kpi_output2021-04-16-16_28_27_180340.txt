{
    "Average_holding_time": 12.31304347826087,
    "Average_holding_actions": [
        868,
        137,
        130,
        0,
        15,
        0
    ],
    "Average_travel_time_passengers": 2610.5326629511815,
    "Average_passenger_waiting_time": 308.8717699588752,
    "Average_passenger_in_vehicle_time": 2301.6608929921576,
    "Average_speed_busses": 0.04791666666666667,
    "Average_experienced_crowding": 104.32048911367097,
    "Average_covariation_headway": 0.22361177497756202,
    "Average_covariation_headway_stops": {
        "E": 0.09452891554075687,
        "F": 0.11415362602956972,
        "J": 0.1024374294595673,
        "D": 0.07921193036102712,
        "A": 0.08477832529237646,
        "C": 0.08011360940112244,
        "G": 0.09268854132673952,
        "K": 0.0659900836877851,
        "H": 0.07603918880493321,
        "I": 0.0931921619997292,
        "B": 0.05898351809366435,
        "L": 0.0691238840275114
    },
    "Average_excess_waiting_time": {
        "E": 41.08968302124612,
        "F": 40.646591035790834,
        "J": 42.34584319304196,
        "D": 41.99840909428195,
        "A": 42.840928167874154,
        "C": 43.53465652567945,
        "G": 40.58773587598358,
        "K": 41.92471306353548,
        "H": 41.03996467957876,
        "I": 43.08250915869576,
        "B": 43.765590437182425,
        "L": 43.608647623738136
    },
    "Holding_per_stop": {
        "F": 14.536082474226804,
        "E": 23.4375,
        "J": 16.5625,
        "D": 5.3125,
        "A": 6.5625,
        "G": 24.0625,
        "C": 1.5789473684210527,
        "H": 1.25,
        "K": 11.875,
        "I": 27.8125,
        "B": 0.631578947368421,
        "L": 13.894736842105264
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus2": {
            "E": 71.25,
            "F": 94.4375,
            "G": 125.125,
            "H": 131.9375,
            "I": 128.3125,
            "J": 121.4375,
            "K": 113.8125,
            "L": 104.75,
            "A": 95.25,
            "B": 84.8125,
            "C": 79.125,
            "D": 75.3125
        },
        "bus3": {
            "F": 92.17647058823529,
            "G": 123.94117647058823,
            "H": 129.875,
            "I": 127.75,
            "J": 119.875,
            "K": 111.375,
            "L": 102.375,
            "A": 93.875,
            "B": 84.6875,
            "C": 78.9375,
            "D": 74.6875,
            "E": 73.625
        },
        "bus4": {
            "J": 119.375,
            "K": 111.125,
            "L": 101.625,
            "A": 92.6875,
            "B": 84.4375,
            "C": 79.4375,
            "D": 76.1875,
            "E": 74.25,
            "F": 98.6875,
            "G": 132.5625,
            "H": 139.1875,
            "I": 134.75
        },
        "bus1": {
            "D": 70.4375,
            "E": 71.5625,
            "F": 95.8125,
            "G": 126.5,
            "H": 133.4375,
            "I": 130,
            "J": 121.9375,
            "K": 112.375,
            "L": 101.75,
            "A": 90.75,
            "B": 80.5625,
            "C": 75.8
        },
        "bus5": {
            "A": 86.4375,
            "B": 77.1875,
            "C": 73.625,
            "D": 72.8125,
            "E": 73.125,
            "F": 97.125,
            "G": 125.375,
            "H": 133.25,
            "I": 128.8125,
            "J": 121.3125,
            "K": 112.375,
            "L": 101.4
        },
        "bus0": {
            "C": 73.375,
            "D": 70.3125,
            "E": 69.75,
            "F": 90.125,
            "G": 123.75,
            "H": 130.75,
            "I": 127.25,
            "J": 118.8125,
            "K": 110.875,
            "L": 103.0625,
            "A": 93.5,
            "B": 85
        }
    },
    "Obs_and_actions_busses": {
        "2": 64.74387045781565,
        "0": -186.45428612189147,
        "5": 188.112807355196,
        "1": -1.8649776320845168
    },
    "Obs_and_actions_tfls": {}
}01457730477574
    },
    "Obs_and_actions_tfls": {}
}