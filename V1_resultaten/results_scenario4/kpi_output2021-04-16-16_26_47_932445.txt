{
    "Average_holding_time": 15.330396475770925,
    "Average_holding_actions": [
        826,
        137,
        139,
        0,
        33,
        0
    ],
    "Average_travel_time_passengers": 2659.388398609098,
    "Average_passenger_waiting_time": 321.7599821349966,
    "Average_passenger_in_vehicle_time": 2337.628416473977,
    "Average_speed_busses": 0.04729166666666667,
    "Average_experienced_crowding": 108.34486687818264,
    "Average_covariation_headway": 0.2556006284044025,
    "Average_covariation_headway_stops": {
        "H": 0.21255492586542074,
        "I": 0.21422536914563237,
        "J": 0.20307931517994288,
        "L": 0.16496833958265167,
        "D": 0.14779034475475022,
        "A": 0.14906519337595883,
        "K": 0.1733903517686803,
        "E": 0.14772684947890147,
        "B": 0.11854515332065224,
        "F": 0.14929301121601027,
        "C": 0.1360507307301301,
        "G": 0.16686284152378886
    },
    "Average_excess_waiting_time": {
        "H": 59.997890090918304,
        "I": 58.02098689598944,
        "J": 54.82371696805802,
        "L": 50.258717611453164,
        "D": 50.33087433874118,
        "A": 47.497194092279415,
        "K": 53.016853644005835,
        "E": 51.91996669419092,
        "B": 47.173415556888926,
        "F": 53.62583799564777,
        "C": 50.844875666110624,
        "G": 56.80188063909594
    },
    "Holding_per_stop": {
        "I": 27.789473684210527,
        "J": 22.105263157894736,
        "H": 5.425531914893617,
        "D": 2.210526315789474,
        "A": 7.8125,
        "L": 14.842105263157896,
        "K": 14.842105263157896,
        "E": 30,
        "B": 1.894736842105263,
        "F": 25.21276595744681,
        "C": 4.148936170212766,
        "G": 28.06451612903226
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus1": {
            "H": 138.1875,
            "I": 134,
            "J": 127.125,
            "K": 119.5,
            "L": 105.875,
            "A": 93.9375,
            "B": 85.625,
            "C": 81.13333333333334,
            "D": 79.66666666666667,
            "E": 79.06666666666666,
            "F": 102.33333333333333,
            "G": 138.13333333333333
        },
        "bus2": {
            "I": 123.3125,
            "J": 117.1875,
            "K": 110,
            "L": 100,
            "A": 89.625,
            "B": 81.25,
            "C": 76.875,
            "D": 73.25,
            "E": 74,
            "F": 96.26666666666667,
            "G": 126.86666666666666,
            "H": 134.66666666666666
        },
        "bus3": {
            "J": 111.5,
            "K": 101.375,
            "L": 93.3125,
            "A": 84.5,
            "B": 76.5,
            "C": 72.5,
            "D": 72,
            "E": 71.375,
            "F": 94.625,
            "G": 122.73333333333333,
            "H": 130.26666666666668,
            "I": 125.8
        },
        "bus4": {
            "L": 94.875,
            "A": 84.875,
            "B": 76.8125,
            "C": 70.5625,
            "D": 69.6875,
            "E": 70.1875,
            "F": 91.75,
            "G": 120.3125,
            "H": 129.75,
            "I": 127.5625,
            "J": 121.26666666666667,
            "K": 110.66666666666667
        },
        "bus0": {
            "D": 74.25,
            "E": 70.8125,
            "F": 95.375,
            "G": 130.8125,
            "H": 139.5,
            "I": 136.625,
            "J": 131.5,
            "K": 124.0625,
            "L": 113.875,
            "A": 103.25,
            "B": 93.8,
            "C": 85.53333333333333
        },
        "bus5": {
            "A": 96.9375,
            "B": 87.1875,
            "C": 81.25,
            "D": 80.25,
            "E": 76.0625,
            "F": 100.25,
            "G": 135.1875,
            "H": 143.3125,
            "I": 138.625,
            "J": 130.375,
            "K": 122.5625,
            "L": 113.2
        }
    },
    "Obs_and_actions_busses": {
        "5": 294.58629771923296,
        "1": -1.8852391212528326,
        "2": 63.682765625081366,
        "0": -192.23344715490163
    },
    "Obs_and_actions_tfls": {}
}