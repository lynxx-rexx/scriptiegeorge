{
    "Average_holding_time": 14.36731107205624,
    "Average_holding_actions": [
        813,
        229,
        32,
        0,
        30,
        0
    ],
    "Average_travel_time_passengers": 2669.0612538548976,
    "Average_passenger_waiting_time": 329.80006717035616,
    "Average_passenger_in_vehicle_time": 2339.2611866844336,
    "Average_speed_busses": 0.04741666666666667,
    "Average_experienced_crowding": 113.00674267117968,
    "Average_covariation_headway": 0.30594476802249193,
    "Average_covariation_headway_stops": {
        "H": 0.2925581537596202,
        "G": 0.269454845669732,
        "E": 0.27224402693821936,
        "J": 0.2549767298341052,
        "K": 0.24265858367415724,
        "L": 0.24072580545244326,
        "I": 0.2708751729745072,
        "A": 0.21966726197538453,
        "F": 0.2500145814382334,
        "B": 0.2122182134474441,
        "C": 0.22811638600813156,
        "D": 0.2631472970000811
    },
    "Average_excess_waiting_time": {
        "H": 69.5555665046968,
        "G": 67.71346769903039,
        "E": 68.62016776493329,
        "J": 62.71489113024347,
        "K": 58.9247890036786,
        "L": 56.81347392538822,
        "I": 66.95935116342633,
        "A": 55.822580292040186,
        "F": 66.72784597914574,
        "B": 57.05598338927325,
        "C": 61.60689761540533,
        "D": 68.95837332489964
    },
    "Holding_per_stop": {
        "H": 10.421052631578947,
        "L": 14.0625,
        "J": 23.68421052631579,
        "K": 15.625,
        "G": 23.29787234042553,
        "I": 19.894736842105264,
        "E": 17.872340425531913,
        "A": 10,
        "F": 18.51063829787234,
        "B": 6.631578947368421,
        "C": 6.0638297872340425,
        "D": 6.382978723404255
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus2": {
            "H": 125.625,
            "I": 121.6875,
            "J": 115.375,
            "K": 106.8125,
            "L": 96.875,
            "A": 86.9375,
            "B": 77.75,
            "C": 74.4375,
            "D": 72.9375,
            "E": 71.93333333333334,
            "F": 96.33333333333333,
            "G": 126.4
        },
        "bus1": {
            "G": 133.875,
            "H": 142.5625,
            "I": 139.875,
            "J": 133.25,
            "K": 124,
            "L": 114.5,
            "A": 104.875,
            "B": 94.25,
            "C": 88.6,
            "D": 84.6,
            "E": 82.2,
            "F": 108.26666666666667
        },
        "bus0": {
            "E": 74,
            "F": 100.875,
            "G": 136.5,
            "H": 146.4375,
            "I": 142.8125,
            "J": 137.9375,
            "K": 127.4375,
            "L": 116.5,
            "A": 107.875,
            "B": 97.46666666666667,
            "C": 89.2,
            "D": 82.33333333333333
        },
        "bus3": {
            "J": 115.5625,
            "K": 106.5625,
            "L": 96.75,
            "A": 86.625,
            "B": 76.4375,
            "C": 71.625,
            "D": 70.3125,
            "E": 70.875,
            "F": 92.9375,
            "G": 124.33333333333333,
            "H": 133.66666666666666,
            "I": 130.53333333333333
        },
        "bus4": {
            "K": 88.4375,
            "L": 81.375,
            "A": 74.9375,
            "B": 67.5625,
            "C": 62.3125,
            "D": 60.6875,
            "E": 61,
            "F": 79.5625,
            "G": 110.125,
            "H": 112.6875,
            "I": 107.375,
            "J": 101.86666666666666
        },
        "bus5": {
            "L": 114.5625,
            "A": 103.5625,
            "B": 93.0625,
            "C": 86.125,
            "D": 82.5,
            "E": 82.4375,
            "F": 111.125,
            "G": 151.8125,
            "H": 159.125,
            "I": 153.75,
            "J": 144.9375,
            "K": 133.5625
        }
    },
    "Obs_and_actions_busses": {
        "0": -216.24414783465838,
        "1": 24.4770687804571,
        "3": 169.5679141017818,
        "5": 424.5061981911446,
        "2": 103.17062446797718
    },
    "Obs_and_actions_tfls": {}
}