{
    "Average_holding_time": 42.62390670553936,
    "Average_holding_actions": [
        187,
        437,
        326,
        22,
        57,
        0
    ],
    "Average_travel_time_passengers": 2940.785348862718,
    "Average_passenger_waiting_time": 359.81138941342545,
    "Average_passenger_in_vehicle_time": 2580.973959449159,
    "Average_speed_busses": 0.042875,
    "Average_experienced_crowding": 122.31942212706812,
    "Average_covariation_headway": 0.2763264692363808,
    "Average_covariation_headway_stops": {
        "G": 0.24570635834966562,
        "I": 0.23832475800779074,
        "E": 0.2481898120210853,
        "J": 0.22856425770831934,
        "K": 0.20753085642857141,
        "L": 0.20198431896860056,
        "H": 0.23492593950628687,
        "A": 0.1753074326744665,
        "F": 0.22117189697355724,
        "B": 0.1702652323877216,
        "C": 0.18581700355523284,
        "D": 0.22219029143107547
    },
    "Average_excess_waiting_time": {
        "G": 98.06729373222669,
        "I": 95.92960078768641,
        "E": 98.95160139621646,
        "J": 92.21130832521686,
        "K": 87.00443654822055,
        "L": 84.2358417689133,
        "H": 98.10618754762072,
        "A": 83.05629214663861,
        "F": 96.74238198231575,
        "B": 85.14676935200868,
        "C": 89.82799043713192,
        "D": 97.47894956871397
    },
    "Holding_per_stop": {
        "L": 36.206896551724135,
        "K": 42.06896551724138,
        "G": 37.76470588235294,
        "J": 62.093023255813954,
        "I": 44.30232558139535,
        "E": 55.05882352941177,
        "A": 33.10344827586207,
        "H": 41.64705882352941,
        "F": 56.11764705882353,
        "B": 40.81395348837209,
        "C": 56.11764705882353,
        "D": 6.352941176470588
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus1": {
            "G": 139.4,
            "H": 150.73333333333332,
            "I": 149.33333333333334,
            "J": 140.92857142857142,
            "K": 130.57142857142858,
            "L": 119.07142857142857,
            "A": 109.07142857142857,
            "B": 98.42857142857143,
            "C": 90.14285714285714,
            "D": 86.28571428571429,
            "E": 85.71428571428571,
            "F": 110.57142857142857
        },
        "bus2": {
            "I": 134.8,
            "J": 126.33333333333333,
            "K": 119.06666666666666,
            "L": 108.42857142857143,
            "A": 98,
            "B": 87.71428571428571,
            "C": 81.57142857142857,
            "D": 78.07142857142857,
            "E": 78.14285714285714,
            "F": 102.78571428571429,
            "G": 141,
            "H": 149.35714285714286
        },
        "bus0": {
            "E": 85.66666666666667,
            "F": 111.06666666666666,
            "G": 148.71428571428572,
            "H": 160.21428571428572,
            "I": 155.5,
            "J": 149.71428571428572,
            "K": 139.64285714285714,
            "L": 127.14285714285714,
            "A": 115,
            "B": 104.14285714285714,
            "C": 96,
            "D": 91.42857142857143
        },
        "bus3": {
            "J": 130.8,
            "K": 123.33333333333333,
            "L": 113.2,
            "A": 103.93333333333334,
            "B": 94.92857142857143,
            "C": 88.92857142857143,
            "D": 84.64285714285714,
            "E": 82.57142857142857,
            "F": 107.78571428571429,
            "G": 141.07142857142858,
            "H": 149.07142857142858,
            "I": 146.57142857142858
        },
        "bus4": {
            "K": 113.86666666666666,
            "L": 102.2,
            "A": 92.8,
            "B": 84.13333333333334,
            "C": 77.14285714285714,
            "D": 74.35714285714286,
            "E": 73.71428571428571,
            "F": 98.85714285714286,
            "G": 135,
            "H": 140.64285714285714,
            "I": 137,
            "J": 130.85714285714286
        },
        "bus5": {
            "L": 119.33333333333333,
            "A": 109.86666666666666,
            "B": 98.33333333333333,
            "C": 92.73333333333333,
            "D": 88.26666666666667,
            "E": 87.71428571428571,
            "F": 116.21428571428571,
            "G": 151.21428571428572,
            "H": 163.35714285714286,
            "I": 158.07142857142858,
            "J": 149.78571428571428,
            "K": 140.28571428571428
        }
    },
    "Obs_and_actions_busses": {
        "5": 301.9838667496085,
        "2": -52.17051780558393,
        "1": -67.56907292088265,
        "0": -380.27861993272774,
        "4": 107.91832067297099
    },
    "Obs_and_actions_tfls": {}
}