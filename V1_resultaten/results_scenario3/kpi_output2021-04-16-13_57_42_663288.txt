{
    "Average_holding_time": 17.989276139410187,
    "Average_holding_actions": [
        750,
        223,
        0,
        0,
        5,
        0
    ],
    "Average_travel_time_passengers": 2754.5956560578156,
    "Average_passenger_waiting_time": 358.1936234920347,
    "Average_passenger_in_vehicle_time": 2396.402032565748,
    "Average_speed_busses": 0.046625,
    "Average_experienced_crowding": 87.19113629482858,
    "Average_covariation_headway": 0.4167106103654213,
    "Average_covariation_headway_stops": {
        "J": 0.3662141292453,
        "G": 0.38739948232926896,
        "E": 0.414168693628283,
        "F": 0.40114492125977214,
        "I": 0.36275322453808123,
        "H": 0.3809599059234322,
        "K": 0.3512316891649496,
        "L": 0.35736241821094683,
        "A": 0.35023390238737506,
        "B": 0.35484094448016257,
        "C": 0.37999707154603846,
        "D": 0.40065091562430544
    },
    "Average_excess_waiting_time": {
        "J": 80.20828817560567,
        "G": 91.3443024741099,
        "E": 101.55846002638208,
        "F": 96.13800636795094,
        "I": 80.70375069573089,
        "H": 87.54282771894214,
        "K": 78.5473417680227,
        "L": 81.40314959170911,
        "A": 82.01171489720736,
        "B": 85.11659167278879,
        "C": 92.58033491826433,
        "D": 100.81117439598097
    },
    "Holding_per_stop": {
        "J": 16.42105263157895,
        "G": 16.774193548387096,
        "F": 17.096774193548388,
        "I": 19.78723404255319,
        "H": 23.93617021276596,
        "E": 20.543478260869566,
        "K": 13.72340425531915,
        "L": 22.02127659574468,
        "A": 14.516129032258064,
        "B": 12.258064516129032,
        "C": 21.847826086956523,
        "D": 16.956521739130434
    },
    "Actions_tfl_edges": {
        "DE": [
            16,
            36,
            41
        ],
        "EF": [
            10,
            41,
            42
        ],
        "FG": [
            12,
            43,
            39
        ],
        "GH": [
            15,
            37,
            42
        ],
        "HI": [
            18,
            39,
            38
        ],
        "IJ": [
            20,
            43,
            32
        ],
        "JK": [
            22,
            41,
            32
        ],
        "KL": [
            20,
            34,
            40
        ],
        "LA": [
            16,
            38,
            40
        ],
        "AB": [
            18,
            37,
            38
        ],
        "BC": [
            15,
            38,
            40
        ],
        "CD": [
            17,
            37,
            38
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "J": 95.1875,
            "K": 94.25,
            "L": 94.4375,
            "A": 96.875,
            "B": 98.4375,
            "C": 97.0625,
            "D": 97.8125,
            "E": 97.625,
            "F": 97.9375,
            "G": 99.26666666666667,
            "H": 100.46666666666667,
            "I": 100.26666666666667
        },
        "bus2": {
            "G": 70.9375,
            "H": 73.5,
            "I": 73.625,
            "J": 73.8125,
            "K": 74.8125,
            "L": 76.4375,
            "A": 77.8,
            "B": 77.86666666666666,
            "C": 76.73333333333333,
            "D": 76.46666666666667,
            "E": 75.2,
            "F": 75.53333333333333
        },
        "bus0": {
            "E": 81.375,
            "F": 82.625,
            "G": 84.6875,
            "H": 84.5625,
            "I": 84.13333333333334,
            "J": 83.33333333333333,
            "K": 82.86666666666666,
            "L": 82.4,
            "A": 83.8,
            "B": 84.53333333333333,
            "C": 85.66666666666667,
            "D": 85.8
        },
        "bus1": {
            "F": 82.375,
            "G": 83,
            "H": 82.625,
            "I": 84,
            "J": 83.6875,
            "K": 84.73333333333333,
            "L": 83.93333333333334,
            "A": 84.66666666666667,
            "B": 84.8,
            "C": 85.4,
            "D": 86.06666666666666,
            "E": 86.66666666666667
        },
        "bus4": {
            "I": 64.5625,
            "J": 66.0625,
            "K": 66,
            "L": 66.5625,
            "A": 66.8125,
            "B": 66.3125,
            "C": 67.0625,
            "D": 67.25,
            "E": 63.8,
            "F": 64.86666666666666,
            "G": 68.66666666666667,
            "H": 69.53333333333333
        },
        "bus3": {
            "H": 67.875,
            "I": 66,
            "J": 66.3125,
            "K": 65.375,
            "L": 65.6875,
            "A": 65.1875,
            "B": 65.3125,
            "C": 67.75,
            "D": 67.26666666666667,
            "E": 66.93333333333334,
            "F": 66.46666666666667,
            "G": 69.2
        }
    },
    "Obs_and_actions_busses": {
        "0": -251.16480142265146,
        "3": 268.7019667884034,
        "1": 82.11122103510904,
        "5": 1492.6985350049551
    },
    "Obs_and_actions_tfls": {
        "1": 339.8212151970764,
        "0": -339.0978663610289,
        "2": 63.53197174039301
    }
}