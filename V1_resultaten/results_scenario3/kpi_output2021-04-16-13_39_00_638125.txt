{
    "Average_holding_time": 27.953271028037385,
    "Average_holding_actions": [
        584,
        32,
        435,
        0,
        19,
        0
    ],
    "Average_travel_time_passengers": 2843.550778982469,
    "Average_passenger_waiting_time": 339.8880948286779,
    "Average_passenger_in_vehicle_time": 2503.662684153823,
    "Average_speed_busses": 0.044583333333333336,
    "Average_experienced_crowding": 84.82629869226592,
    "Average_covariation_headway": 0.243641233284248,
    "Average_covariation_headway_stops": {
        "E": 0.1892263131531696,
        "J": 0.20434237474370562,
        "B": 0.20107718029963384,
        "A": 0.21219461495609912,
        "K": 0.198338951915508,
        "H": 0.18592128859127482,
        "L": 0.17307670000782294,
        "F": 0.1892366227518249,
        "C": 0.1844722831165787,
        "I": 0.17900845910968372,
        "D": 0.173232251975922,
        "G": 0.19685570156206667
    },
    "Average_excess_waiting_time": {
        "E": 68.33111563587698,
        "J": 72.74472401006409,
        "B": 68.06522963826524,
        "A": 71.83086244183659,
        "K": 69.83311352688042,
        "H": 69.98847390587997,
        "L": 68.54027468219397,
        "F": 70.95932566924301,
        "C": 67.3870732648179,
        "I": 71.71233522897785,
        "D": 68.46428362273105,
        "G": 73.44247226414376
    },
    "Holding_per_stop": {
        "E": 22,
        "B": 27.666666666666668,
        "A": 21,
        "K": 35.056179775280896,
        "J": 31.348314606741575,
        "H": 25.280898876404493,
        "L": 30.337078651685392,
        "C": 31,
        "F": 26.96629213483146,
        "I": 24.886363636363637,
        "D": 28.314606741573034,
        "G": 31.704545454545453
    },
    "Actions_tfl_edges": {
        "AB": [
            4,
            71,
            15
        ],
        "DE": [
            0,
            67,
            23
        ],
        "GH": [
            2,
            76,
            11
        ],
        "IJ": [
            2,
            65,
            22
        ],
        "JK": [
            5,
            58,
            27
        ],
        "LA": [
            4,
            63,
            23
        ],
        "EF": [
            0,
            72,
            18
        ],
        "BC": [
            2,
            74,
            14
        ],
        "KL": [
            2,
            69,
            18
        ],
        "HI": [
            2,
            67,
            20
        ],
        "CD": [
            1,
            72,
            17
        ],
        "FG": [
            0,
            69,
            20
        ]
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "E": 80.06666666666666,
            "F": 79.26666666666667,
            "G": 80.2,
            "H": 83.66666666666667,
            "I": 84.86666666666666,
            "J": 86,
            "K": 88.2,
            "L": 89,
            "A": 87.06666666666666,
            "B": 88.06666666666666,
            "C": 85.53333333333333,
            "D": 86.64285714285714
        },
        "bus3": {
            "J": 83.53333333333333,
            "K": 83.6,
            "L": 83.93333333333334,
            "A": 84.06666666666666,
            "B": 85.33333333333333,
            "C": 86.2,
            "D": 86.33333333333333,
            "E": 88.6,
            "F": 89.53333333333333,
            "G": 90.78571428571429,
            "H": 90,
            "I": 90.14285714285714
        },
        "bus0": {
            "B": 80.2,
            "C": 81.8,
            "D": 83.53333333333333,
            "E": 87.13333333333334,
            "F": 87.66666666666667,
            "G": 88.4,
            "H": 87.93333333333334,
            "I": 90.46666666666667,
            "J": 91.2,
            "K": 89.46666666666667,
            "L": 87,
            "A": 84.53333333333333
        },
        "bus5": {
            "A": 76.33333333333333,
            "B": 77.93333333333334,
            "C": 80.33333333333333,
            "D": 79.33333333333333,
            "E": 78.73333333333333,
            "F": 78.06666666666666,
            "G": 78.86666666666666,
            "H": 78.73333333333333,
            "I": 79.86666666666666,
            "J": 80.13333333333334,
            "K": 83.46666666666667,
            "L": 81.28571428571429
        },
        "bus4": {
            "K": 81.53333333333333,
            "L": 81.6,
            "A": 82.66666666666667,
            "B": 83.26666666666667,
            "C": 84.46666666666667,
            "D": 83.6,
            "E": 86.2,
            "F": 86.46666666666667,
            "G": 85.2,
            "H": 84.93333333333334,
            "I": 85.86666666666666,
            "J": 85
        },
        "bus2": {
            "H": 78.73333333333333,
            "I": 78.73333333333333,
            "J": 78.46666666666667,
            "K": 78.13333333333334,
            "L": 79.2,
            "A": 80.6,
            "B": 79.66666666666667,
            "C": 80.33333333333333,
            "D": 82.13333333333334,
            "E": 83.73333333333333,
            "F": 82.57142857142857,
            "G": 82.64285714285714
        }
    },
    "Obs_and_actions_busses": {
        "2": 78.2179687224977,
        "0": -206.21586858778065,
        "5": 347.5896083476443,
        "1": 236.62578059112704
    },
    "Obs_and_actions_tfls": {
        "1": 151.85920640728256,
        "0": 477.3839894429069,
        "2": 29.135213380039353
    }
}