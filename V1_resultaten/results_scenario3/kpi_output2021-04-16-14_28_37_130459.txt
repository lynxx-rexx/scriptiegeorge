{
    "Average_holding_time": 9.316608996539792,
    "Average_holding_actions": [
        1018,
        45,
        0,
        35,
        0,
        0
    ],
    "Average_travel_time_passengers": 2656.4778655500995,
    "Average_passenger_waiting_time": 326.59055026173866,
    "Average_passenger_in_vehicle_time": 2329.8873152883834,
    "Average_speed_busses": 0.04816666666666667,
    "Average_experienced_crowding": 80.24864530928262,
    "Average_covariation_headway": 0.3735986194975779,
    "Average_covariation_headway_stops": {
        "J": 0.26039511573107,
        "I": 0.253782366584876,
        "K": 0.2767789478312262,
        "E": 0.27712938731322484,
        "G": 0.2745455775698503,
        "C": 0.27951830695490437,
        "H": 0.24671766006616516,
        "L": 0.2583380838597233,
        "F": 0.2776355464025632,
        "D": 0.25854465273249055,
        "A": 0.27570477792247555,
        "B": 0.2895011128398336
    },
    "Average_excess_waiting_time": {
        "J": 53.43230247906138,
        "I": 53.96709294360113,
        "K": 54.42524062105599,
        "E": 56.654716533861006,
        "G": 56.75177984077402,
        "C": 56.94141975662404,
        "H": 53.692237911149164,
        "L": 52.72876646313091,
        "F": 58.72358858642474,
        "D": 55.53202440790187,
        "A": 56.82423654849873,
        "B": 60.80469471964642
    },
    "Holding_per_stop": {
        "J": 8.65979381443299,
        "K": 6.804123711340206,
        "E": 9.0625,
        "G": 14.375,
        "I": 9.6875,
        "C": 14.536082474226804,
        "H": 7.5,
        "L": 3.711340206185567,
        "F": 11.25,
        "D": 6.25,
        "A": 8.125,
        "B": 11.875
    },
    "Actions_tfl_edges": {
        "BC": [
            0,
            66,
            31
        ],
        "DE": [
            0,
            62,
            35
        ],
        "FG": [
            0,
            62,
            35
        ],
        "HI": [
            0,
            64,
            33
        ],
        "IJ": [
            0,
            61,
            36
        ],
        "JK": [
            0,
            62,
            36
        ],
        "KL": [
            0,
            64,
            33
        ],
        "EF": [
            0,
            66,
            30
        ],
        "GH": [
            0,
            65,
            31
        ],
        "CD": [
            0,
            68,
            28
        ],
        "LA": [
            0,
            65,
            32
        ],
        "AB": [
            0,
            61,
            35
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "J": 72.76470588235294,
            "K": 71.1875,
            "L": 71.4375,
            "A": 70.375,
            "B": 71.25,
            "C": 71.5625,
            "D": 71.625,
            "E": 73.5625,
            "F": 72.625,
            "G": 71.75,
            "H": 74.6875,
            "I": 75.9375
        },
        "bus3": {
            "I": 69.9375,
            "J": 70,
            "K": 69.9375,
            "L": 70.875,
            "A": 70,
            "B": 71.4375,
            "C": 71.1875,
            "D": 72.5,
            "E": 73.6875,
            "F": 74.1875,
            "G": 73.75,
            "H": 73.375
        },
        "bus5": {
            "K": 81.76470588235294,
            "L": 82.76470588235294,
            "A": 85.6875,
            "B": 87.25,
            "C": 88.3125,
            "D": 88.375,
            "E": 88.375,
            "F": 87.625,
            "G": 86.6875,
            "H": 85.625,
            "I": 84.125,
            "J": 85.75
        },
        "bus1": {
            "E": 74.29411764705883,
            "F": 77.875,
            "G": 78,
            "H": 76.625,
            "I": 76.625,
            "J": 76.6875,
            "K": 76.125,
            "L": 75.375,
            "A": 77.1875,
            "B": 77.125,
            "C": 77,
            "D": 78.75
        },
        "bus2": {
            "G": 64.25,
            "H": 65.3125,
            "I": 68,
            "J": 68.875,
            "K": 69.6875,
            "L": 69.25,
            "A": 70.4375,
            "B": 71.125,
            "C": 70.3125,
            "D": 70.5,
            "E": 69.3125,
            "F": 69.3125
        },
        "bus0": {
            "C": 82.88235294117646,
            "D": 84.5625,
            "E": 84.0625,
            "F": 81.0625,
            "G": 81.8125,
            "H": 82.25,
            "I": 81,
            "J": 82.125,
            "K": 83.875,
            "L": 85.8125,
            "A": 86.0625,
            "B": 86.8125
        }
    },
    "Obs_and_actions_busses": {
        "0": -176.81789544217042,
        "3": 200.8253043609794,
        "4": 365.22527845250295,
        "1": 151.04987101413266
    },
    "Obs_and_actions_tfls": {
        "1": 242.7032274654656,
        "2": -140.60209659987513
    }
}