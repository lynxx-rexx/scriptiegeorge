{
    "Average_holding_time": 7.551546391752577,
    "Average_holding_actions": [
        1048,
        43,
        0,
        31,
        0,
        0
    ],
    "Average_travel_time_passengers": 2624.436822939458,
    "Average_passenger_waiting_time": 317.5733711487113,
    "Average_passenger_in_vehicle_time": 2306.8634517907576,
    "Average_speed_busses": 0.0485,
    "Average_experienced_crowding": 79.58827409118132,
    "Average_covariation_headway": 0.3424489544053338,
    "Average_covariation_headway_stops": {
        "C": 0.24769299640952888,
        "E": 0.2070500387988721,
        "I": 0.24645461097323154,
        "F": 0.22746185984570802,
        "G": 0.23731943224881666,
        "A": 0.24602178994482723,
        "D": 0.22311444986434623,
        "J": 0.22585843043393142,
        "H": 0.23877278269183047,
        "B": 0.24850818106671968,
        "K": 0.24927330832636568,
        "L": 0.2506908422060279
    },
    "Average_excess_waiting_time": {
        "C": 50.84807451230313,
        "E": 45.66297200619226,
        "I": 47.1636143559889,
        "F": 46.330832725619246,
        "G": 46.55019593886681,
        "A": 49.87062803914222,
        "D": 48.44647474952285,
        "J": 45.26855903357631,
        "H": 47.89848069055893,
        "B": 52.954226334902444,
        "K": 50.32973575535465,
        "L": 51.96687933886733
    },
    "Holding_per_stop": {
        "C": 8.350515463917526,
        "I": 7.959183673469388,
        "F": 8.350515463917526,
        "G": 4.285714285714286,
        "E": 8.041237113402062,
        "A": 2.4742268041237114,
        "J": 7.422680412371134,
        "D": 13.125,
        "H": 5.876288659793815,
        "K": 6.804123711340206,
        "B": 6.5625,
        "L": 11.443298969072165
    },
    "Actions_tfl_edges": {
        "BC": [
            0,
            61,
            36
        ],
        "DE": [
            0,
            61,
            36
        ],
        "EF": [
            0,
            68,
            30
        ],
        "FG": [
            0,
            59,
            39
        ],
        "HI": [
            0,
            63,
            35
        ],
        "LA": [
            0,
            64,
            34
        ],
        "CD": [
            0,
            60,
            37
        ],
        "IJ": [
            0,
            63,
            35
        ],
        "GH": [
            0,
            55,
            43
        ],
        "AB": [
            0,
            59,
            38
        ],
        "JK": [
            0,
            60,
            37
        ],
        "KL": [
            0,
            63,
            34
        ]
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 68.58823529411765,
            "D": 68.6470588235294,
            "E": 69.4375,
            "F": 71.4375,
            "G": 70.5625,
            "H": 72.125,
            "I": 72.125,
            "J": 72.4375,
            "K": 72.8125,
            "L": 73.3125,
            "A": 73.5625,
            "B": 72.625
        },
        "bus1": {
            "E": 78.94117647058823,
            "F": 77.5625,
            "G": 76.625,
            "H": 77.5,
            "I": 76.875,
            "J": 76.5,
            "K": 75.875,
            "L": 78.9375,
            "A": 79.6875,
            "B": 79.9375,
            "C": 82.1875,
            "D": 82.6875
        },
        "bus4": {
            "I": 71.23529411764706,
            "J": 72.88235294117646,
            "K": 74,
            "L": 74.47058823529412,
            "A": 74.375,
            "B": 74.5,
            "C": 75.6875,
            "D": 75.375,
            "E": 75.125,
            "F": 74.125,
            "G": 74.1875,
            "H": 73.75
        },
        "bus2": {
            "F": 76.76470588235294,
            "G": 77.52941176470588,
            "H": 77.125,
            "I": 78.75,
            "J": 79.25,
            "K": 81.375,
            "L": 81.0625,
            "A": 81.4375,
            "B": 81.625,
            "C": 82.75,
            "D": 82.4375,
            "E": 81.125
        },
        "bus3": {
            "G": 75.94117647058823,
            "H": 75.05882352941177,
            "I": 77,
            "J": 76.25,
            "K": 78.0625,
            "L": 76.5,
            "A": 75.4375,
            "B": 73.625,
            "C": 74.8125,
            "D": 75.5625,
            "E": 77.6875,
            "F": 79.4375
        },
        "bus5": {
            "A": 76.47058823529412,
            "B": 78,
            "C": 77.6875,
            "D": 78.625,
            "E": 79.4375,
            "F": 81.4375,
            "G": 83.6875,
            "H": 83.875,
            "I": 83.9375,
            "J": 83.1875,
            "K": 81.5,
            "L": 81.8125
        }
    },
    "Obs_and_actions_busses": {
        "0": -170.4017345224163,
        "3": 187.68032274902575,
        "1": 156.32123705003454,
        "4": 335.0902150190497
    },
    "Obs_and_actions_tfls": {
        "2": -103.93684686098827,
        "1": 247.11492014337912
    }
}