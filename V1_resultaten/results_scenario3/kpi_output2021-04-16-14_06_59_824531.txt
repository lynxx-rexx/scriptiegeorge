{
    "Average_holding_time": 15.282511210762332,
    "Average_holding_actions": [
        989,
        0,
        0,
        62,
        64,
        0
    ],
    "Average_travel_time_passengers": 2768.1567842454065,
    "Average_passenger_waiting_time": 357.1651912829211,
    "Average_passenger_in_vehicle_time": 2410.9915929624713,
    "Average_speed_busses": 0.04645833333333333,
    "Average_experienced_crowding": 87.71267726500464,
    "Average_covariation_headway": 0.48334416681814824,
    "Average_covariation_headway_stops": {
        "K": 0.3703858797966476,
        "E": 0.34896163522292645,
        "D": 0.3664472045511313,
        "L": 0.37073212902680436,
        "G": 0.3469644625012815,
        "A": 0.3685950051223085,
        "H": 0.3408060077248935,
        "B": 0.36631824384344913,
        "F": 0.34148691407462517,
        "I": 0.3587186293701686,
        "C": 0.37427956530046547,
        "J": 0.3599674400056702
    },
    "Average_excess_waiting_time": {
        "K": 89.6225338073366,
        "E": 81.47865641187491,
        "D": 88.19816702551236,
        "L": 87.06321787942056,
        "G": 80.95512044228451,
        "A": 85.391247324044,
        "H": 81.88388560311967,
        "B": 86.82357483202247,
        "F": 81.81676950619885,
        "I": 86.63744435751232,
        "C": 90.22631183038965,
        "J": 88.6410421065682
    },
    "Holding_per_stop": {
        "E": 11.612903225806452,
        "G": 9.03225806451613,
        "L": 10,
        "A": 17.23404255319149,
        "K": 20.967741935483872,
        "D": 18.387096774193548,
        "H": 16.129032258064516,
        "B": 17.096774193548388,
        "F": 9.35483870967742,
        "I": 16.304347826086957,
        "C": 20.967741935483872,
        "J": 16.304347826086957
    },
    "Actions_tfl_edges": {
        "CD": [
            0,
            88,
            6
        ],
        "DE": [
            0,
            91,
            3
        ],
        "FG": [
            0,
            89,
            5
        ],
        "JK": [
            0,
            89,
            4
        ],
        "KL": [
            0,
            89,
            5
        ],
        "LA": [
            0,
            92,
            2
        ],
        "EF": [
            0,
            90,
            3
        ],
        "GH": [
            0,
            89,
            4
        ],
        "AB": [
            0,
            89,
            5
        ],
        "HI": [
            0,
            90,
            3
        ],
        "BC": [
            0,
            88,
            5
        ],
        "IJ": [
            0,
            88,
            4
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "K": 65.625,
            "L": 65.375,
            "A": 63.375,
            "B": 62.5,
            "C": 63.8125,
            "D": 66.73333333333333,
            "E": 67.26666666666667,
            "F": 67.33333333333333,
            "G": 69.4,
            "H": 70.2,
            "I": 70.46666666666667,
            "J": 70.13333333333334
        },
        "bus1": {
            "E": 72.8125,
            "F": 74,
            "G": 73.5625,
            "H": 71.8125,
            "I": 72.5,
            "J": 73.4375,
            "K": 75.125,
            "L": 75.06666666666666,
            "A": 76.33333333333333,
            "B": 77.53333333333333,
            "C": 77.53333333333333,
            "D": 78.53333333333333
        },
        "bus0": {
            "D": 72.1875,
            "E": 72.5,
            "F": 72.3125,
            "G": 74,
            "H": 73.75,
            "I": 73.53333333333333,
            "J": 73.73333333333333,
            "K": 74.93333333333334,
            "L": 74.66666666666667,
            "A": 76,
            "B": 74,
            "C": 75.33333333333333
        },
        "bus4": {
            "L": 93.125,
            "A": 92.75,
            "B": 92,
            "C": 93.0625,
            "D": 93.6875,
            "E": 94.8,
            "F": 97.2,
            "G": 98.8,
            "H": 98.33333333333333,
            "I": 98.33333333333333,
            "J": 98.4,
            "K": 97.93333333333334
        },
        "bus2": {
            "G": 75.875,
            "H": 74,
            "I": 74.1875,
            "J": 73.875,
            "K": 72.1875,
            "L": 73.25,
            "A": 75.125,
            "B": 76.66666666666667,
            "C": 76.06666666666666,
            "D": 77.33333333333333,
            "E": 78.53333333333333,
            "F": 78.93333333333334
        },
        "bus5": {
            "A": 85.5625,
            "B": 83.6875,
            "C": 82.75,
            "D": 84.1875,
            "E": 83.625,
            "F": 85.875,
            "G": 89.25,
            "H": 89.8,
            "I": 91.06666666666666,
            "J": 91.13333333333334,
            "K": 92.06666666666666,
            "L": 91.26666666666667
        }
    },
    "Obs_and_actions_busses": {
        "0": -177.96043724307756,
        "5": 258.5347864507107,
        "4": 441.88825506097237
    },
    "Obs_and_actions_tfls": {
        "1": 136.52291763162307,
        "2": -606.7378964533624
    }
}