{
    "Average_holding_time": 21.22222222222222,
    "Average_holding_actions": [
        910,
        0,
        0,
        86,
        84,
        0
    ],
    "Average_travel_time_passengers": 2899.612660259614,
    "Average_passenger_waiting_time": 400.1585952017975,
    "Average_passenger_in_vehicle_time": 2499.4540650578065,
    "Average_speed_busses": 0.045,
    "Average_experienced_crowding": 99.60564920708771,
    "Average_covariation_headway": 0.5876625782436219,
    "Average_covariation_headway_stops": {
        "J": 0.47558298754516043,
        "A": 0.4964195934166559,
        "D": 0.490634261898581,
        "B": 0.493792755449095,
        "F": 0.47730554534841685,
        "I": 0.468402433133916,
        "E": 0.4884845459827196,
        "K": 0.46515862308625244,
        "G": 0.4598438023655229,
        "C": 0.47518143323644335,
        "L": 0.4625676674952618,
        "H": 0.4646653531224529
    },
    "Average_excess_waiting_time": {
        "J": 126.24805400038167,
        "A": 134.63313851031944,
        "D": 131.6714842822052,
        "B": 132.14045018317125,
        "F": 130.41271674806262,
        "I": 128.13303876718447,
        "E": 133.35769409382993,
        "K": 125.34337469397593,
        "G": 124.60368732656474,
        "C": 129.23203076956264,
        "L": 126.35767383596948,
        "H": 129.01171494732057
    },
    "Holding_per_stop": {
        "J": 16,
        "D": 13.846153846153847,
        "B": 19.12087912087912,
        "F": 18.666666666666668,
        "A": 21,
        "I": 19.666666666666668,
        "E": 34.333333333333336,
        "K": 31.666666666666668,
        "G": 21.235955056179776,
        "C": 23,
        "L": 19.666666666666668,
        "H": 16.51685393258427
    },
    "Actions_tfl_edges": {
        "AB": [
            1,
            86,
            4
        ],
        "CD": [
            1,
            88,
            2
        ],
        "EF": [
            0,
            90,
            1
        ],
        "HI": [
            0,
            89,
            1
        ],
        "IJ": [
            0,
            90,
            1
        ],
        "LA": [
            0,
            90,
            1
        ],
        "JK": [
            0,
            90,
            0
        ],
        "DE": [
            1,
            88,
            1
        ],
        "BC": [
            2,
            87,
            2
        ],
        "FG": [
            0,
            87,
            3
        ],
        "KL": [
            0,
            90,
            0
        ],
        "GH": [
            0,
            86,
            3
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "J": 103.46666666666667,
            "K": 103,
            "L": 103.86666666666666,
            "A": 104.93333333333334,
            "B": 105.6,
            "C": 105.86666666666666,
            "D": 107.33333333333333,
            "E": 108.33333333333333,
            "F": 109.26666666666667,
            "G": 107.66666666666667,
            "H": 108.06666666666666,
            "I": 111.26666666666667
        },
        "bus5": {
            "A": 75.6,
            "B": 75.06666666666666,
            "C": 75.73333333333333,
            "D": 74.6,
            "E": 74.73333333333333,
            "F": 76.33333333333333,
            "G": 77.53333333333333,
            "H": 78,
            "I": 78.33333333333333,
            "J": 78.93333333333334,
            "K": 77.53333333333333,
            "L": 78.26666666666667
        },
        "bus1": {
            "D": 29.8125,
            "E": 28.466666666666665,
            "F": 29.733333333333334,
            "G": 31.8,
            "H": 32.6,
            "I": 34.46666666666667,
            "J": 34.13333333333333,
            "K": 33.13333333333333,
            "L": 32.733333333333334,
            "A": 32.666666666666664,
            "B": 31.8,
            "C": 31.2
        },
        "bus0": {
            "B": 50.6875,
            "C": 50.2,
            "D": 51.13333333333333,
            "E": 51,
            "F": 51.733333333333334,
            "G": 52.86666666666667,
            "H": 53.4,
            "I": 53.06666666666667,
            "J": 53.13333333333333,
            "K": 55.666666666666664,
            "L": 53.2,
            "A": 53.53333333333333
        },
        "bus2": {
            "F": 116,
            "G": 120.26666666666667,
            "H": 119.53333333333333,
            "I": 119.8,
            "J": 119,
            "K": 120.26666666666667,
            "L": 120.8,
            "A": 120.93333333333334,
            "B": 118.13333333333334,
            "C": 118.53333333333333,
            "D": 120,
            "E": 119.06666666666666
        },
        "bus3": {
            "I": 108.46666666666667,
            "J": 112.4,
            "K": 113.53333333333333,
            "L": 112,
            "A": 116.6,
            "B": 116.06666666666666,
            "C": 113.53333333333333,
            "D": 111.86666666666666,
            "E": 111.33333333333333,
            "F": 113,
            "G": 112.92857142857143,
            "H": 112.78571428571429
        }
    },
    "Obs_and_actions_busses": {
        "0": -187.17815217837966,
        "5": 245.78970371400948,
        "4": 411.47140164813885
    },
    "Obs_and_actions_tfls": {
        "1": 102.6142396083895,
        "2": -806.5470998744375,
        "0": -981.24821717962
    }
}