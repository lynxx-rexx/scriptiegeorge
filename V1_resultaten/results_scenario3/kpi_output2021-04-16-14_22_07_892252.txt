{
    "Average_holding_time": 9.26892950391645,
    "Average_holding_actions": [
        1020,
        43,
        0,
        54,
        0,
        0
    ],
    "Average_travel_time_passengers": 2662.6998501201347,
    "Average_passenger_waiting_time": 329.7432971080878,
    "Average_passenger_in_vehicle_time": 2332.9565530120794,
    "Average_speed_busses": 0.047875,
    "Average_experienced_crowding": 80.33710892778642,
    "Average_covariation_headway": 0.37490626661695636,
    "Average_covariation_headway_stops": {
        "L": 0.2835220757447494,
        "B": 0.2631301826654702,
        "H": 0.2874998869330145,
        "D": 0.24848645180978055,
        "G": 0.2739243400192947,
        "E": 0.2711557647297498,
        "I": 0.26690029713667357,
        "C": 0.2478814882105787,
        "F": 0.2617457784967218,
        "A": 0.2459125320254094,
        "J": 0.2643596697479659,
        "K": 0.2813277814965539
    },
    "Average_excess_waiting_time": {
        "L": 60.42422634517993,
        "B": 57.23671246972157,
        "H": 57.24851118437152,
        "D": 55.38541394890069,
        "G": 56.77425225479385,
        "E": 57.32250541199079,
        "I": 55.97966286801034,
        "C": 58.23418223679869,
        "F": 57.390291304402126,
        "A": 56.431677167090925,
        "J": 57.561903446442045,
        "K": 61.69670010880952
    },
    "Holding_per_stop": {
        "B": 8.75,
        "H": 12.061855670103093,
        "D": 12,
        "E": 11.875,
        "L": 9.0625,
        "G": 8.75,
        "I": 7.1875,
        "C": 12.947368421052632,
        "F": 5.9375,
        "A": 2.8421052631578947,
        "J": 8.125,
        "K": 11.68421052631579
    },
    "Actions_tfl_edges": {
        "AB": [
            0,
            53,
            43
        ],
        "CD": [
            0,
            64,
            32
        ],
        "DE": [
            0,
            59,
            37
        ],
        "FG": [
            0,
            61,
            35
        ],
        "GH": [
            0,
            66,
            31
        ],
        "KL": [
            0,
            60,
            36
        ],
        "BC": [
            0,
            56,
            40
        ],
        "HI": [
            0,
            63,
            34
        ],
        "EF": [
            0,
            64,
            32
        ],
        "LA": [
            0,
            60,
            36
        ],
        "IJ": [
            0,
            60,
            36
        ],
        "JK": [
            0,
            64,
            32
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 75.125,
            "A": 74.5625,
            "B": 74.4375,
            "C": 75.9375,
            "D": 76.5625,
            "E": 77.875,
            "F": 78.1875,
            "G": 81.1875,
            "H": 80.6875,
            "I": 79.9375,
            "J": 81.5,
            "K": 79.66666666666667
        },
        "bus0": {
            "B": 76.125,
            "C": 75.625,
            "D": 75.5,
            "E": 74.5,
            "F": 74.625,
            "G": 73.5625,
            "H": 75.625,
            "I": 77.9375,
            "J": 78.5,
            "K": 78,
            "L": 77.6875,
            "A": 80.93333333333334
        },
        "bus4": {
            "H": 71.52941176470588,
            "I": 72.625,
            "J": 73.5,
            "K": 74.3125,
            "L": 75.6875,
            "A": 74.6875,
            "B": 76.1875,
            "C": 76.0625,
            "D": 75.375,
            "E": 75.6875,
            "F": 74.5,
            "G": 75.0625
        },
        "bus1": {
            "D": 71.5,
            "E": 70.875,
            "F": 70.9375,
            "G": 73.1875,
            "H": 74.3125,
            "I": 73.6875,
            "J": 74.0625,
            "K": 73.5625,
            "L": 73.9375,
            "A": 75.9375,
            "B": 74.25,
            "C": 75.06666666666666
        },
        "bus3": {
            "G": 70.4375,
            "H": 69.5,
            "I": 70.5,
            "J": 73.375,
            "K": 73.5625,
            "L": 74.25,
            "A": 76.125,
            "B": 76.0625,
            "C": 76.25,
            "D": 76.9375,
            "E": 77.4375,
            "F": 75.625
        },
        "bus2": {
            "E": 80.375,
            "F": 82.5,
            "G": 84.625,
            "H": 84.4375,
            "I": 86.9375,
            "J": 85.6875,
            "K": 86.9375,
            "L": 88.375,
            "A": 87.875,
            "B": 88.125,
            "C": 88.125,
            "D": 86.66666666666667
        }
    },
    "Obs_and_actions_busses": {
        "4": 337.649097194349,
        "3": 188.63475369268642,
        "0": -174.613184886029,
        "1": 149.13210679941432
    },
    "Obs_and_actions_tfls": {
        "2": -108.24354579655457,
        "1": 246.8451522476433
    }
}