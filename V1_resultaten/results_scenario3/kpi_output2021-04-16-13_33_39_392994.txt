{
    "Average_holding_time": 27.404651162790696,
    "Average_holding_actions": [
        589,
        74,
        384,
        0,
        28,
        0
    ],
    "Average_travel_time_passengers": 2820.6459798784203,
    "Average_passenger_waiting_time": 352.27300327173725,
    "Average_passenger_in_vehicle_time": 2468.3729766066576,
    "Average_speed_busses": 0.04479166666666667,
    "Average_experienced_crowding": 84.81656196509793,
    "Average_covariation_headway": 0.30599840193958117,
    "Average_covariation_headway_stops": {
        "J": 0.23963320680453215,
        "G": 0.26932021034161097,
        "F": 0.27632698823718116,
        "K": 0.25082993048201624,
        "I": 0.23019444506361242,
        "C": 0.24301714169870514,
        "H": 0.23801194744673665,
        "L": 0.25962051192606633,
        "D": 0.23421291118897142,
        "A": 0.24784196898238192,
        "E": 0.2775517809100694,
        "B": 0.2476773761214796
    },
    "Average_excess_waiting_time": {
        "J": 72.06478227068118,
        "G": 79.28061881405517,
        "F": 82.50536563400743,
        "K": 72.31427100822202,
        "I": 74.5001157944082,
        "C": 75.49359357354865,
        "H": 75.73320211751388,
        "L": 75.67345394842937,
        "D": 75.81070162151747,
        "A": 75.48356695904658,
        "E": 85.16580961062846,
        "B": 77.38192232649817
    },
    "Holding_per_stop": {
        "G": 30.333333333333332,
        "J": 34,
        "K": 31.978021978021978,
        "I": 32.69662921348315,
        "C": 27.333333333333332,
        "F": 27.640449438202246,
        "H": 24.60674157303371,
        "L": 32,
        "D": 22.247191011235955,
        "A": 20.333333333333332,
        "E": 24.269662921348313,
        "B": 21.235955056179776
    },
    "Actions_tfl_edges": {
        "BC": [
            8,
            69,
            13
        ],
        "EF": [
            5,
            72,
            13
        ],
        "FG": [
            7,
            68,
            15
        ],
        "HI": [
            4,
            70,
            16
        ],
        "IJ": [
            4,
            68,
            18
        ],
        "JK": [
            4,
            70,
            17
        ],
        "GH": [
            6,
            70,
            14
        ],
        "KL": [
            6,
            68,
            17
        ],
        "CD": [
            6,
            70,
            14
        ],
        "LA": [
            7,
            67,
            16
        ],
        "DE": [
            6,
            67,
            16
        ],
        "AB": [
            5,
            73,
            12
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "J": 72.53333333333333,
            "K": 74,
            "L": 71.8,
            "A": 71.46666666666667,
            "B": 72.8,
            "C": 73.46666666666667,
            "D": 76.33333333333333,
            "E": 77.06666666666666,
            "F": 78.53333333333333,
            "G": 80.2,
            "H": 78.53333333333333,
            "I": 78.06666666666666
        },
        "bus2": {
            "G": 73.66666666666667,
            "H": 75.13333333333334,
            "I": 75.13333333333334,
            "J": 75.86666666666666,
            "K": 76.4,
            "L": 77.4,
            "A": 77.06666666666666,
            "B": 78.93333333333334,
            "C": 81.06666666666666,
            "D": 81.26666666666667,
            "E": 79.86666666666666,
            "F": 78.28571428571429
        },
        "bus1": {
            "F": 88,
            "G": 89.13333333333334,
            "H": 89,
            "I": 90.06666666666666,
            "J": 88.8,
            "K": 91.2,
            "L": 91.2,
            "A": 92.6,
            "B": 93,
            "C": 92.93333333333334,
            "D": 92.28571428571429,
            "E": 92
        },
        "bus5": {
            "K": 76.625,
            "L": 77.66666666666667,
            "A": 78,
            "B": 78.73333333333333,
            "C": 79.06666666666666,
            "D": 78.73333333333333,
            "E": 80.33333333333333,
            "F": 80.86666666666666,
            "G": 79.26666666666667,
            "H": 80.73333333333333,
            "I": 77.86666666666666,
            "J": 79.73333333333333
        },
        "bus3": {
            "I": 78.8,
            "J": 79.8,
            "K": 79.2,
            "L": 79.86666666666666,
            "A": 80.73333333333333,
            "B": 81.66666666666667,
            "C": 82.2,
            "D": 84.66666666666667,
            "E": 85.06666666666666,
            "F": 85.93333333333334,
            "G": 84.8,
            "H": 83.6
        },
        "bus0": {
            "C": 78.8,
            "D": 80.53333333333333,
            "E": 83.6,
            "F": 84.46666666666667,
            "G": 86.8,
            "H": 87.93333333333334,
            "I": 87.33333333333333,
            "J": 86.66666666666667,
            "K": 84.86666666666666,
            "L": 82.26666666666667,
            "A": 80.8,
            "B": 81
        }
    },
    "Obs_and_actions_busses": {
        "0": -225.0065224523953,
        "2": 77.13770129240007,
        "1": 233.40946382122377,
        "5": 369.4003700975765
    },
    "Obs_and_actions_tfls": {
        "1": 136.91426301705584,
        "0": 459.1039966993791,
        "2": 44.88094935201579
    }
}454,
        "2": 33.98114678220852
    }
}