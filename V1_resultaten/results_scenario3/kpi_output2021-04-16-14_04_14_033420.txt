{
    "Average_holding_time": 14.194107452339688,
    "Average_holding_actions": [
        808,
        246,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 2650.440373709586,
    "Average_passenger_waiting_time": 317.00916378719285,
    "Average_passenger_in_vehicle_time": 2333.431209922353,
    "Average_speed_busses": 0.04808333333333333,
    "Average_experienced_crowding": 77.15292467776032,
    "Average_covariation_headway": 0.26907076618849535,
    "Average_covariation_headway_stops": {
        "H": 0.18909863565621832,
        "E": 0.22012131815435979,
        "B": 0.1919436232723259,
        "C": 0.20283631388202672,
        "G": 0.21049231388923192,
        "L": 0.19741772573075303,
        "I": 0.17991934149744424,
        "F": 0.20457296981627246,
        "D": 0.18907409592418598,
        "A": 0.1704852548294565,
        "J": 0.17624690701924503,
        "K": 0.19412267253361198
    },
    "Average_excess_waiting_time": {
        "H": 42.522063736768985,
        "E": 47.43141543314266,
        "B": 45.62910034897226,
        "C": 45.38262746181965,
        "G": 46.46749332529157,
        "L": 46.135997022557945,
        "I": 42.92689838200255,
        "F": 47.60140576559672,
        "D": 45.44632797238461,
        "A": 45.09341529091489,
        "J": 44.65692823936541,
        "K": 47.535909468421664
    },
    "Holding_per_stop": {
        "H": 13.29896907216495,
        "C": 18.556701030927837,
        "E": 15.154639175257731,
        "G": 22.88659793814433,
        "B": 9.0625,
        "L": 15.625,
        "I": 12.8125,
        "F": 18.125,
        "D": 13.75,
        "A": 5.052631578947368,
        "J": 11.25,
        "K": 14.526315789473685
    },
    "Actions_tfl_edges": {
        "AB": [
            11,
            44,
            41
        ],
        "BC": [
            14,
            51,
            32
        ],
        "DE": [
            13,
            42,
            42
        ],
        "FG": [
            19,
            35,
            43
        ],
        "GH": [
            19,
            39,
            40
        ],
        "KL": [
            14,
            41,
            41
        ],
        "HI": [
            13,
            39,
            45
        ],
        "CD": [
            14,
            50,
            33
        ],
        "EF": [
            16,
            43,
            38
        ],
        "LA": [
            16,
            43,
            37
        ],
        "IJ": [
            16,
            38,
            42
        ],
        "JK": [
            14,
            39,
            43
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "H": 80.41176470588235,
            "I": 79.6470588235294,
            "J": 80.1875,
            "K": 78.8125,
            "L": 79.1875,
            "A": 77.75,
            "B": 78.375,
            "C": 79.75,
            "D": 79.625,
            "E": 80.75,
            "F": 83,
            "G": 84.0625
        },
        "bus2": {
            "E": 78.29411764705883,
            "F": 79.25,
            "G": 79.375,
            "H": 79.1875,
            "I": 77.4375,
            "J": 76.8125,
            "K": 76.625,
            "L": 78.3125,
            "A": 78.9375,
            "B": 80.625,
            "C": 84.5625,
            "D": 84.5625
        },
        "bus0": {
            "B": 75.5,
            "C": 75.125,
            "D": 75.25,
            "E": 78.75,
            "F": 80.5,
            "G": 81.9375,
            "H": 80.3125,
            "I": 81.375,
            "J": 81.1875,
            "K": 81.75,
            "L": 81,
            "A": 80.8
        },
        "bus1": {
            "C": 67.17647058823529,
            "D": 68.0625,
            "E": 71.1875,
            "F": 72.0625,
            "G": 71.9375,
            "H": 72.5,
            "I": 72.375,
            "J": 72.125,
            "K": 70.875,
            "L": 70.6875,
            "A": 70.3125,
            "B": 70.8125
        },
        "bus3": {
            "G": 70.17647058823529,
            "H": 70.1875,
            "I": 70.3125,
            "J": 70.375,
            "K": 71.8125,
            "L": 72.375,
            "A": 72.375,
            "B": 72.4375,
            "C": 72.6875,
            "D": 71.5625,
            "E": 73.9375,
            "F": 75.5625
        },
        "bus5": {
            "L": 70.3125,
            "A": 70.9375,
            "B": 70.5625,
            "C": 71.9375,
            "D": 71.4375,
            "E": 72.25,
            "F": 74.125,
            "G": 74.8125,
            "H": 75.3125,
            "I": 75.5625,
            "J": 74.8125,
            "K": 74.2
        }
    },
    "Obs_and_actions_busses": {
        "0": -206.57261766777873,
        "1": 80.21689750471168,
        "3": 213.43046828562302
    },
    "Obs_and_actions_tfls": {
        "1": 301.9184531046135,
        "0": -188.45817818253104,
        "2": 41.92362536271341
    }
}