{
    "Average_holding_time": 21.493333333333332,
    "Average_holding_actions": [
        847,
        39,
        0,
        50,
        0,
        0
    ],
    "Average_travel_time_passengers": 2721.4262286775916,
    "Average_passenger_waiting_time": 331.7313538049618,
    "Average_passenger_in_vehicle_time": 2389.6948748726636,
    "Average_speed_busses": 0.046875,
    "Average_experienced_crowding": 82.1042908732013,
    "Average_covariation_headway": 0.3416816325494916,
    "Average_covariation_headway_stops": {
        "A": 0.25701969685489856,
        "J": 0.2683199672640915,
        "F": 0.2642918317037469,
        "D": 0.2652599740557692,
        "C": 0.26543126295215086,
        "E": 0.26703922734361973,
        "G": 0.25829723553257844,
        "B": 0.27134724887405953,
        "K": 0.2474427129261745,
        "H": 0.24893083965168664,
        "L": 0.2363279503373782,
        "I": 0.24207750509416545
    },
    "Average_excess_waiting_time": {
        "A": 63.33695480192125,
        "J": 64.99805243484872,
        "F": 60.11794776577875,
        "D": 63.23698690433224,
        "C": 64.88373629192063,
        "E": 61.372953674068924,
        "G": 61.39337449011708,
        "B": 67.18583560560961,
        "K": 62.45278087412754,
        "H": 61.31341008815929,
        "L": 62.367181227515744,
        "I": 61.46761416109263
    },
    "Holding_per_stop": {
        "F": 24.63157894736842,
        "D": 19.78723404255319,
        "E": 19.78723404255319,
        "A": 18.829787234042552,
        "C": 22.02127659574468,
        "J": 20.106382978723403,
        "G": 20.74468085106383,
        "B": 22.903225806451612,
        "K": 21.93548387096774,
        "H": 26.80851063829787,
        "L": 24.838709677419356,
        "I": 15.483870967741936
    },
    "Actions_tfl_edges": {
        "BC": [
            94,
            0,
            0
        ],
        "CD": [
            95,
            0,
            0
        ],
        "DE": [
            95,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "IJ": [
            94,
            0,
            0
        ],
        "LA": [
            94,
            0,
            0
        ],
        "FG": [
            95,
            0,
            0
        ],
        "AB": [
            94,
            0,
            0
        ],
        "JK": [
            94,
            0,
            0
        ],
        "GH": [
            94,
            0,
            0
        ],
        "KL": [
            93,
            0,
            0
        ],
        "HI": [
            94,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "A": 76,
            "B": 75.3125,
            "C": 74.6875,
            "D": 76.1875,
            "E": 75.4375,
            "F": 75.8125,
            "G": 75.46666666666667,
            "H": 77.86666666666666,
            "I": 79.2,
            "J": 79.6,
            "K": 80.86666666666666,
            "L": 82.46666666666667
        },
        "bus4": {
            "J": 76.6875,
            "K": 77.75,
            "L": 80,
            "A": 80.8125,
            "B": 80.6875,
            "C": 82.8125,
            "D": 82.9375,
            "E": 81.6875,
            "F": 82.8,
            "G": 81.33333333333333,
            "H": 79.6,
            "I": 79.8
        },
        "bus3": {
            "F": 83.125,
            "G": 82.5,
            "H": 84.5625,
            "I": 84.75,
            "J": 85.8125,
            "K": 88.5,
            "L": 88.3125,
            "A": 88.3125,
            "B": 90.3125,
            "C": 90.6875,
            "D": 92.4,
            "E": 91.13333333333334
        },
        "bus1": {
            "D": 77.4375,
            "E": 77.8125,
            "F": 79.375,
            "G": 80.25,
            "H": 79.5,
            "I": 79,
            "J": 78.75,
            "K": 77.6,
            "L": 78.53333333333333,
            "A": 80.53333333333333,
            "B": 81,
            "C": 82.46666666666667
        },
        "bus0": {
            "C": 77.8125,
            "D": 78.125,
            "E": 77.4375,
            "F": 77.25,
            "G": 78.1875,
            "H": 77.875,
            "I": 77.0625,
            "J": 79.06666666666666,
            "K": 79,
            "L": 79.93333333333334,
            "A": 80.33333333333333,
            "B": 81.06666666666666
        },
        "bus2": {
            "E": 67.375,
            "F": 67.6875,
            "G": 69.625,
            "H": 71,
            "I": 74.625,
            "J": 74.3125,
            "K": 74.25,
            "L": 73.125,
            "A": 73.3125,
            "B": 71.66666666666667,
            "C": 72.46666666666667,
            "D": 72.4
        }
    },
    "Obs_and_actions_busses": {
        "4": 341.67069904664606,
        "0": -198.33821376216713,
        "1": 85.27161999486293,
        "3": 149.79950122401192
    },
    "Obs_and_actions_tfls": {
        "0": 101.69315946179265
    }
}
}