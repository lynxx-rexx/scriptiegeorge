{
    "Average_holding_time": 12.849740932642487,
    "Average_holding_actions": [
        841,
        141,
        175,
        0,
        1,
        0
    ],
    "Average_travel_time_passengers": 2695.024526947836,
    "Average_passenger_waiting_time": 353.3534206063717,
    "Average_passenger_in_vehicle_time": 2341.6711063414227,
    "Average_speed_busses": 0.04825,
    "Average_experienced_crowding": 90.18475146106377,
    "Average_covariation_headway": 0.5340407416190691,
    "Average_covariation_headway_stops": {
        "F": 0.4104863490838862,
        "I": 0.40626209577143735,
        "J": 0.39975700034757766,
        "B": 0.41254293533159303,
        "A": 0.4044114147630242,
        "D": 0.43094853775166136,
        "K": 0.39149981991353994,
        "C": 0.4196413008130781,
        "G": 0.40428745027580665,
        "E": 0.4133551439378512,
        "L": 0.40322091156548884,
        "H": 0.4007766333777181
    },
    "Average_excess_waiting_time": {
        "F": 81.26692573565941,
        "I": 82.8171499946314,
        "J": 81.3471470663626,
        "B": 83.85494060495853,
        "A": 84.01978501697329,
        "D": 87.20613493812152,
        "K": 82.45107634607285,
        "C": 85.7840472241457,
        "G": 81.81078280764712,
        "E": 84.79105263615588,
        "L": 85.5893964308043,
        "H": 83.24474525206045
    },
    "Holding_per_stop": {
        "F": 11.443298969072165,
        "J": 14.536082474226804,
        "B": 12.371134020618557,
        "D": 17.31958762886598,
        "A": 9.0625,
        "I": 12.68041237113402,
        "K": 12.1875,
        "C": 15.625,
        "G": 12.68041237113402,
        "E": 13.29896907216495,
        "L": 12,
        "H": 10.9375
    },
    "Actions_tfl_edges": {
        "AB": [
            97,
            0,
            0
        ],
        "CD": [
            97,
            0,
            0
        ],
        "EF": [
            98,
            0,
            0
        ],
        "HI": [
            97,
            0,
            0
        ],
        "IJ": [
            98,
            0,
            0
        ],
        "LA": [
            96,
            0,
            0
        ],
        "FG": [
            97,
            0,
            0
        ],
        "JK": [
            97,
            0,
            0
        ],
        "BC": [
            97,
            0,
            0
        ],
        "DE": [
            97,
            0,
            0
        ],
        "KL": [
            96,
            0,
            0
        ],
        "GH": [
            97,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "F": 84.3529411764706,
            "G": 88.23529411764706,
            "H": 91.5,
            "I": 92.625,
            "J": 92.6875,
            "K": 93.625,
            "L": 94.125,
            "A": 93.875,
            "B": 93.1875,
            "C": 90.625,
            "D": 90.625,
            "E": 90.1875
        },
        "bus3": {
            "I": 85.76470588235294,
            "J": 88.75,
            "K": 90.5625,
            "L": 91.5,
            "A": 90.625,
            "B": 92.875,
            "C": 92.375,
            "D": 91.5625,
            "E": 90.375,
            "F": 91.5,
            "G": 92.4375,
            "H": 92.125
        },
        "bus4": {
            "J": 65.94117647058823,
            "K": 66.6875,
            "L": 65.75,
            "A": 66.5625,
            "B": 69.125,
            "C": 68.8125,
            "D": 66.9375,
            "E": 67.5,
            "F": 68.8125,
            "G": 68.25,
            "H": 67.125,
            "I": 68.75
        },
        "bus0": {
            "B": 68.58823529411765,
            "C": 65.25,
            "D": 65.875,
            "E": 68.375,
            "F": 70.1875,
            "G": 70.5625,
            "H": 68.375,
            "I": 67.9375,
            "J": 68.25,
            "K": 69.8125,
            "L": 72.875,
            "A": 72.75
        },
        "bus5": {
            "A": 70.1875,
            "B": 70.5625,
            "C": 70.0625,
            "D": 68.9375,
            "E": 69.5,
            "F": 69.6875,
            "G": 71,
            "H": 72.625,
            "I": 75.75,
            "J": 76.6875,
            "K": 77,
            "L": 74.06666666666666
        },
        "bus1": {
            "D": 74.70588235294117,
            "E": 75.47058823529412,
            "F": 76.1875,
            "G": 77.1875,
            "H": 78.125,
            "I": 77.75,
            "J": 77.6875,
            "K": 77.75,
            "L": 78.0625,
            "A": 76.75,
            "B": 77.3125,
            "C": 78.8125
        }
    },
    "Obs_and_actions_busses": {
        "0": -248.21955654458696,
        "1": 150.36845996559774,
        "2": 338.1342639733317,
        "5": 626.8426949129467
    },
    "Obs_and_actions_tfls": {
        "0": 118.63472691495228
    }
}466140213
    }
}
        "0": 91.26814128230481
    }
}