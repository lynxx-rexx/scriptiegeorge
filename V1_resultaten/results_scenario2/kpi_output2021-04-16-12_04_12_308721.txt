{
    "Average_holding_time": 19.006211180124225,
    "Average_holding_actions": [
        856,
        59,
        0,
        19,
        0,
        0
    ],
    "Average_travel_time_passengers": 2704.968706063908,
    "Average_passenger_waiting_time": 329.62618891042115,
    "Average_passenger_in_vehicle_time": 2375.342517153517,
    "Average_speed_busses": 0.04695833333333333,
    "Average_experienced_crowding": 81.7093034666143,
    "Average_covariation_headway": 0.2972944099365864,
    "Average_covariation_headway_stops": {
        "E": 0.21073289556106517,
        "C": 0.22302086463887147,
        "I": 0.2156797875811517,
        "A": 0.22535801032966957,
        "K": 0.22118903232574194,
        "D": 0.20683905357971255,
        "F": 0.19775266608194478,
        "B": 0.23125977855840887,
        "L": 0.2224543771488771,
        "J": 0.22203802670911826,
        "G": 0.20680271345110954,
        "H": 0.21740971466222478
    },
    "Average_excess_waiting_time": {
        "E": 52.07324099958123,
        "C": 58.773586103321634,
        "I": 55.48740957531612,
        "A": 57.20055417637121,
        "K": 56.017724770169764,
        "D": 52.77837162201877,
        "F": 51.25707814089856,
        "B": 60.01321028575302,
        "L": 57.660279981435224,
        "J": 58.18537428776784,
        "G": 54.073509609639416,
        "H": 57.35956545635804
    },
    "Holding_per_stop": {
        "E": 17.68421052631579,
        "A": 18.19148936170213,
        "K": 18.51063829787234,
        "D": 17.23404255319149,
        "C": 27.741935483870968,
        "I": 12.127659574468085,
        "F": 11.808510638297872,
        "L": 20.425531914893618,
        "B": 19.677419354838708,
        "G": 21.70212765957447,
        "J": 18.19148936170213,
        "H": 24.893617021276597
    },
    "Actions_tfl_edges": {
        "BC": [
            94,
            0,
            0
        ],
        "CD": [
            94,
            0,
            0
        ],
        "DE": [
            95,
            0,
            0
        ],
        "HI": [
            95,
            0,
            0
        ],
        "JK": [
            95,
            0,
            0
        ],
        "LA": [
            95,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "AB": [
            94,
            0,
            0
        ],
        "KL": [
            94,
            0,
            0
        ],
        "IJ": [
            94,
            0,
            0
        ],
        "FG": [
            94,
            0,
            0
        ],
        "GH": [
            94,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "E": 80.3125,
            "F": 80.6875,
            "G": 80.0625,
            "H": 82,
            "I": 82.75,
            "J": 83.8125,
            "K": 85.125,
            "L": 86.6875,
            "A": 87.125,
            "B": 85.1875,
            "C": 86.33333333333333,
            "D": 85.53333333333333
        },
        "bus0": {
            "C": 74,
            "D": 77.5625,
            "E": 78.25,
            "F": 80.1875,
            "G": 80.8125,
            "H": 80.625,
            "I": 80.8125,
            "J": 78.75,
            "K": 79.66666666666667,
            "L": 79.2,
            "A": 78.53333333333333,
            "B": 77
        },
        "bus3": {
            "I": 72.1875,
            "J": 72.9375,
            "K": 76.3125,
            "L": 77.3125,
            "A": 81.8125,
            "B": 83.125,
            "C": 81.3125,
            "D": 78.06666666666666,
            "E": 78.8,
            "F": 77.4,
            "G": 76.6,
            "H": 77.86666666666666
        },
        "bus5": {
            "A": 83.125,
            "B": 83.125,
            "C": 80.875,
            "D": 83.3125,
            "E": 85.1875,
            "F": 86.375,
            "G": 87.3125,
            "H": 88.875,
            "I": 88.26666666666667,
            "J": 88.73333333333333,
            "K": 88.4,
            "L": 86.2
        },
        "bus4": {
            "K": 71.75,
            "L": 73,
            "A": 72.8125,
            "B": 72.3125,
            "C": 73.75,
            "D": 75.625,
            "E": 74.25,
            "F": 73.26666666666667,
            "G": 74.33333333333333,
            "H": 74.2,
            "I": 73.8,
            "J": 75.2
        },
        "bus1": {
            "D": 74.6875,
            "E": 74.1875,
            "F": 74.5,
            "G": 75.875,
            "H": 76.375,
            "I": 76.25,
            "J": 77.625,
            "K": 78.0625,
            "L": 79.8125,
            "A": 80.26666666666667,
            "B": 80.13333333333334,
            "C": 81.13333333333334
        }
    },
    "Obs_and_actions_busses": {
        "4": 360.07179728691455,
        "0": -180.45902331220913,
        "3": 152.66627008945022,
        "1": 83.75564758890405
    },
    "Obs_and_actions_tfls": {
        "0": 118.74699613876882
    }
}