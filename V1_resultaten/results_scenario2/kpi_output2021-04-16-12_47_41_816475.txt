{
    "Average_holding_time": 13.459879206212252,
    "Average_holding_actions": [
        992,
        0,
        105,
        0,
        62,
        0
    ],
    "Average_travel_time_passengers": 2668.348295122271,
    "Average_passenger_waiting_time": 329.5449177546424,
    "Average_passenger_in_vehicle_time": 2338.8033773676325,
    "Average_speed_busses": 0.04829166666666666,
    "Average_experienced_crowding": 82.05640715758767,
    "Average_covariation_headway": 0.4044375688320883,
    "Average_covariation_headway_stops": {
        "A": 0.3167890355204432,
        "E": 0.3000971121508516,
        "D": 0.3110164341600784,
        "L": 0.302747720080408,
        "H": 0.2750401079660533,
        "G": 0.2766497948572977,
        "F": 0.26970263667146843,
        "I": 0.2636881560486201,
        "B": 0.3280446343644305,
        "J": 0.2836487561534064,
        "C": 0.3189935733588645,
        "K": 0.2875659216231526
    },
    "Average_excess_waiting_time": {
        "A": 61.714311710714696,
        "E": 60.03710554565072,
        "D": 63.81471187336416,
        "L": 60.9034616253179,
        "H": 52.754253566965076,
        "G": 54.775135400094314,
        "F": 54.98293510825533,
        "I": 51.98270830986115,
        "B": 65.33857919426339,
        "J": 57.32316633244511,
        "C": 66.38795783615774,
        "K": 60.69497839820298
    },
    "Holding_per_stop": {
        "A": 6.804123711340206,
        "E": 12.989690721649485,
        "H": 15.154639175257731,
        "G": 8.969072164948454,
        "D": 19.6875,
        "L": 17.1875,
        "F": 13.75,
        "I": 8.969072164948454,
        "B": 16.082474226804123,
        "J": 12.68041237113402,
        "C": 15.3125,
        "K": 14.0625
    },
    "Actions_tfl_edges": {
        "CD": [
            97,
            0,
            0
        ],
        "DE": [
            97,
            0,
            0
        ],
        "FG": [
            97,
            0,
            0
        ],
        "GH": [
            98,
            0,
            0
        ],
        "KL": [
            97,
            0,
            0
        ],
        "LA": [
            97,
            0,
            0
        ],
        "AB": [
            97,
            0,
            0
        ],
        "EF": [
            97,
            0,
            0
        ],
        "HI": [
            97,
            0,
            0
        ],
        "IJ": [
            97,
            0,
            0
        ],
        "BC": [
            97,
            0,
            0
        ],
        "JK": [
            96,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "A": 78.88235294117646,
            "B": 79.17647058823529,
            "C": 79.875,
            "D": 83,
            "E": 83.375,
            "F": 85.3125,
            "G": 83.5625,
            "H": 84.125,
            "I": 84.25,
            "J": 85.625,
            "K": 84.375,
            "L": 85
        },
        "bus1": {
            "E": 85.05882352941177,
            "F": 83.375,
            "G": 85.375,
            "H": 85.125,
            "I": 86.625,
            "J": 86.4375,
            "K": 88.625,
            "L": 89.5,
            "A": 91.3125,
            "B": 92.25,
            "C": 91.375,
            "D": 91
        },
        "bus0": {
            "D": 68.9375,
            "E": 70.125,
            "F": 70.3125,
            "G": 69.8125,
            "H": 71.0625,
            "I": 70.0625,
            "J": 71.9375,
            "K": 73.4375,
            "L": 73.1875,
            "A": 74.4375,
            "B": 76.375,
            "C": 74.625
        },
        "bus4": {
            "L": 84.875,
            "A": 83.8125,
            "B": 82.8125,
            "C": 81.875,
            "D": 85.375,
            "E": 88.5,
            "F": 89.1875,
            "G": 89.3125,
            "H": 88.125,
            "I": 88.0625,
            "J": 88.625,
            "K": 88.4375
        },
        "bus3": {
            "H": 61.294117647058826,
            "I": 64.05882352941177,
            "J": 64.6470588235294,
            "K": 65.8125,
            "L": 65.5625,
            "A": 66.375,
            "B": 66.75,
            "C": 65.75,
            "D": 65.8125,
            "E": 64.75,
            "F": 64.75,
            "G": 65.75
        },
        "bus2": {
            "G": 71.94117647058823,
            "H": 73.88235294117646,
            "I": 73.5625,
            "J": 74.3125,
            "K": 75.9375,
            "L": 75.3125,
            "A": 75.6875,
            "B": 76.6875,
            "C": 76.5,
            "D": 76.5625,
            "E": 76.375,
            "F": 76.8125
        }
    },
    "Obs_and_actions_busses": {
        "0": -189.21674734100762,
        "5": 398.96744251320706,
        "2": 225.69232418608044
    },
    "Obs_and_actions_tfls": {
        "0": 118.70352888284727
    }
}: 97.85459486682632
    }
}