{
    "Average_holding_time": 19.27027027027027,
    "Average_holding_actions": [
        673,
        345,
        0,
        92,
        0,
        0
    ],
    "Average_travel_time_passengers": 2864.3011183296626,
    "Average_passenger_waiting_time": 420.55303857175284,
    "Average_passenger_in_vehicle_time": 2443.7480797579556,
    "Average_speed_busses": 0.04625,
    "Average_experienced_crowding": 106.22856450864164,
    "Average_covariation_headway": 0.7210485459781942,
    "Average_covariation_headway_stops": {
        "F": 0.5997260766771259,
        "D": 0.6012130018214783,
        "E": 0.592615977454066,
        "L": 0.5739710798209942,
        "K": 0.5785660949625717,
        "C": 0.5904516502489059,
        "G": 0.5857814149861428,
        "A": 0.5691293315686599,
        "H": 0.5754880461700181,
        "B": 0.5835191858475067,
        "I": 0.5825528451574463,
        "J": 0.5793621994480085
    },
    "Average_excess_waiting_time": {
        "F": 153.99281241651914,
        "D": 157.61884230151554,
        "E": 152.0918450952185,
        "L": 150.8338141854531,
        "K": 154.95420894912746,
        "C": 157.11367725272254,
        "G": 151.20754845349222,
        "A": 149.4369700947828,
        "H": 155.38668931625875,
        "B": 156.96283567784656,
        "I": 157.2239077932494,
        "J": 158.1866432891934
    },
    "Holding_per_stop": {
        "F": 14.680851063829786,
        "D": 19.032258064516128,
        "E": 12.76595744680851,
        "L": 27.419354838709676,
        "C": 18.06451612903226,
        "K": 20.543478260869566,
        "G": 15.806451612903226,
        "A": 14.673913043478262,
        "H": 27.391304347826086,
        "B": 17.608695652173914,
        "I": 22.747252747252748,
        "J": 20.76923076923077
    },
    "Actions_tfl_edges": {
        "BC": [
            93,
            0,
            0
        ],
        "CD": [
            94,
            0,
            0
        ],
        "DE": [
            94,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "JK": [
            92,
            0,
            0
        ],
        "KL": [
            93,
            0,
            0
        ],
        "FG": [
            94,
            0,
            0
        ],
        "LA": [
            93,
            0,
            0
        ],
        "GH": [
            93,
            0,
            0
        ],
        "AB": [
            92,
            0,
            0
        ],
        "HI": [
            92,
            0,
            0
        ],
        "IJ": [
            91,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "F": 61.8125,
            "G": 61.3125,
            "H": 60.6875,
            "I": 62.875,
            "J": 62.875,
            "K": 63.625,
            "L": 65.0625,
            "A": 64.06666666666666,
            "B": 64.93333333333334,
            "C": 66,
            "D": 65.33333333333333,
            "E": 66
        },
        "bus1": {
            "D": 102.9375,
            "E": 101.6875,
            "F": 101.6875,
            "G": 104.375,
            "H": 103.26666666666667,
            "I": 102.33333333333333,
            "J": 105.53333333333333,
            "K": 106.13333333333334,
            "L": 106.2,
            "A": 106.6,
            "B": 105.66666666666667,
            "C": 106.33333333333333
        },
        "bus2": {
            "E": 72.3125,
            "F": 71.3125,
            "G": 72.5625,
            "H": 74.5625,
            "I": 69.4,
            "J": 68.46666666666667,
            "K": 71.06666666666666,
            "L": 71.93333333333334,
            "A": 72.6,
            "B": 73.86666666666666,
            "C": 74.26666666666667,
            "D": 75.6
        },
        "bus5": {
            "L": 70.125,
            "A": 67.9375,
            "B": 68.375,
            "C": 68.25,
            "D": 69.75,
            "E": 68.8125,
            "F": 71.4,
            "G": 70.66666666666667,
            "H": 71.13333333333334,
            "I": 72.13333333333334,
            "J": 73,
            "K": 73.4
        },
        "bus4": {
            "K": 82.9375,
            "L": 83,
            "A": 83.25,
            "B": 84.9375,
            "C": 87,
            "D": 87.4,
            "E": 86.66666666666667,
            "F": 86.4,
            "G": 85.46666666666667,
            "H": 86,
            "I": 86.73333333333333,
            "J": 88.4
        },
        "bus0": {
            "C": 91.9375,
            "D": 92.125,
            "E": 90.0625,
            "F": 90.875,
            "G": 92,
            "H": 97.8,
            "I": 97.26666666666667,
            "J": 95,
            "K": 95.53333333333333,
            "L": 95.13333333333334,
            "A": 94.93333333333334,
            "B": 95.93333333333334
        }
    },
    "Obs_and_actions_busses": {
        "0": -319.6381534095271,
        "1": 209.71660587528513,
        "4": 441.0567932177626
    },
    "Obs_and_actions_tfls": {
        "0": 101.0637411112013
    }
}