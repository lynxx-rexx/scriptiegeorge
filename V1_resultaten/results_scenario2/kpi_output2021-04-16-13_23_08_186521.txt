{
    "Average_holding_time": 20.69333333333333,
    "Average_holding_actions": [
        566,
        392,
        123,
        0,
        3,
        0
    ],
    "Average_travel_time_passengers": 2714.3306802226043,
    "Average_passenger_waiting_time": 338.22321768642115,
    "Average_passenger_in_vehicle_time": 2376.1074625361703,
    "Average_speed_busses": 0.046875,
    "Average_experienced_crowding": 84.04778210318312,
    "Average_covariation_headway": 0.3776148329930152,
    "Average_covariation_headway_stops": {
        "C": 0.2964484009794276,
        "J": 0.3074477606662433,
        "K": 0.30519831402652364,
        "A": 0.28192507134064776,
        "D": 0.31473800817079817,
        "I": 0.29606725108978477,
        "L": 0.28735167424581276,
        "B": 0.2685129675595824,
        "E": 0.2850429099985371,
        "F": 0.27468098450375067,
        "G": 0.274312139162742,
        "H": 0.30695461117319206
    },
    "Average_excess_waiting_time": {
        "C": 66.78275425390416,
        "J": 72.22566929831032,
        "K": 68.63209526771118,
        "A": 65.09568102443797,
        "D": 68.65982348097992,
        "I": 71.69729218062486,
        "L": 67.48934259356389,
        "B": 64.68195162517799,
        "E": 64.72120037556658,
        "F": 65.81540347312614,
        "G": 69.96648279331669,
        "H": 75.9727261787524
    },
    "Holding_per_stop": {
        "J": 23.29787234042553,
        "A": 15.957446808510639,
        "K": 21.70212765957447,
        "C": 21.382978723404257,
        "D": 24.31578947368421,
        "I": 23.548387096774192,
        "L": 22.02127659574468,
        "B": 15.319148936170214,
        "E": 20.74468085106383,
        "F": 20.425531914893618,
        "G": 20.967741935483872,
        "H": 18.58695652173913
    },
    "Actions_tfl_edges": {
        "BC": [
            95,
            0,
            0
        ],
        "CD": [
            95,
            0,
            0
        ],
        "HI": [
            93,
            0,
            0
        ],
        "IJ": [
            94,
            0,
            0
        ],
        "JK": [
            94,
            0,
            0
        ],
        "LA": [
            95,
            0,
            0
        ],
        "AB": [
            94,
            0,
            0
        ],
        "KL": [
            94,
            0,
            0
        ],
        "DE": [
            95,
            0,
            0
        ],
        "EF": [
            94,
            0,
            0
        ],
        "FG": [
            94,
            0,
            0
        ],
        "GH": [
            93,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 67.25,
            "D": 69.1875,
            "E": 69.6875,
            "F": 69.25,
            "G": 69.9375,
            "H": 70,
            "I": 72.3125,
            "J": 72.9375,
            "K": 71.33333333333333,
            "L": 70.8,
            "A": 70.33333333333333,
            "B": 70.26666666666667
        },
        "bus3": {
            "J": 73.3125,
            "K": 74.125,
            "L": 75.0625,
            "A": 73,
            "B": 73.375,
            "C": 74.3125,
            "D": 74.9375,
            "E": 75.8125,
            "F": 77.26666666666667,
            "G": 75.6,
            "H": 77.13333333333334,
            "I": 77.26666666666667
        },
        "bus4": {
            "K": 74.375,
            "L": 74.5,
            "A": 74.1875,
            "B": 75.3125,
            "C": 76.1875,
            "D": 75.25,
            "E": 76.4375,
            "F": 74.25,
            "G": 78.26666666666667,
            "H": 79,
            "I": 79.26666666666667,
            "J": 79.53333333333333
        },
        "bus5": {
            "A": 72.25,
            "B": 74,
            "C": 75.8125,
            "D": 76.3125,
            "E": 76.4375,
            "F": 77.1875,
            "G": 77.9375,
            "H": 77.26666666666667,
            "I": 75.73333333333333,
            "J": 75.53333333333333,
            "K": 75.73333333333333,
            "L": 76.53333333333333
        },
        "bus1": {
            "D": 88.9375,
            "E": 89,
            "F": 90.4375,
            "G": 92.625,
            "H": 94.1875,
            "I": 93.125,
            "J": 96.125,
            "K": 98,
            "L": 96.5625,
            "A": 95.86666666666666,
            "B": 95.13333333333334,
            "C": 96.06666666666666
        },
        "bus2": {
            "I": 82.8125,
            "J": 83.625,
            "K": 85.75,
            "L": 86.5625,
            "A": 86.9375,
            "B": 87.4375,
            "C": 86,
            "D": 85.86666666666666,
            "E": 86.6,
            "F": 87.06666666666666,
            "G": 86.13333333333334,
            "H": 87.33333333333333
        }
    },
    "Obs_and_actions_busses": {
        "3": 358.0136429258254,
        "1": 54.458232951587895,
        "2": 202.3789053190437,
        "0": -303.0901070961168,
        "5": 980.5318625411977
    },
    "Obs_and_actions_tfls": {
        "0": 119.35968324255849
    }
}