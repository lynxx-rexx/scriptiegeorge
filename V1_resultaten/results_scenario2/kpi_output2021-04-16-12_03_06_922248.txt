{
    "Average_holding_time": 19.33862433862434,
    "Average_holding_actions": [
        858,
        61,
        0,
        25,
        0,
        0
    ],
    "Average_travel_time_passengers": 2686.4709209256807,
    "Average_passenger_waiting_time": 328.3786953098749,
    "Average_passenger_in_vehicle_time": 2358.092225615782,
    "Average_speed_busses": 0.04725,
    "Average_experienced_crowding": 80.27964575716045,
    "Average_covariation_headway": 0.3362096115628259,
    "Average_covariation_headway_stops": {
        "H": 0.2467842685490512,
        "D": 0.23988271189354454,
        "L": 0.26294397699830124,
        "C": 0.25707217105546604,
        "I": 0.2413472724287398,
        "F": 0.23419466598599895,
        "E": 0.24396639306020096,
        "J": 0.24364406868453148,
        "A": 0.2504085569933126,
        "G": 0.23773701339286935,
        "K": 0.25187014490266607,
        "B": 0.23758953562559895
    },
    "Average_excess_waiting_time": {
        "H": 57.71753917841846,
        "D": 56.54430094200501,
        "L": 60.51733791592778,
        "C": 60.49122325131157,
        "I": 54.75245912371503,
        "F": 55.46522336302962,
        "E": 58.74304835038703,
        "J": 57.11634045593303,
        "A": 59.79321109739402,
        "G": 58.23425604001636,
        "K": 60.58428268709315,
        "B": 59.27784842069883
    },
    "Holding_per_stop": {
        "D": 23.05263157894737,
        "L": 29.05263157894737,
        "I": 16.105263157894736,
        "F": 14.526315789473685,
        "H": 25.263157894736842,
        "C": 24.893617021276597,
        "J": 15.157894736842104,
        "E": 20.74468085106383,
        "A": 11.170212765957446,
        "G": 22.340425531914892,
        "K": 20.425531914893618,
        "B": 9.25531914893617
    },
    "Actions_tfl_edges": {
        "BC": [
            95,
            0,
            0
        ],
        "CD": [
            95,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "GH": [
            95,
            0,
            0
        ],
        "HI": [
            96,
            0,
            0
        ],
        "KL": [
            95,
            0,
            0
        ],
        "DE": [
            95,
            0,
            0
        ],
        "LA": [
            95,
            0,
            0
        ],
        "IJ": [
            95,
            0,
            0
        ],
        "FG": [
            95,
            0,
            0
        ],
        "JK": [
            95,
            0,
            0
        ],
        "AB": [
            94,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 74.5,
            "I": 75.125,
            "J": 76.125,
            "K": 75.5625,
            "L": 76.6875,
            "A": 77.9375,
            "B": 77.0625,
            "C": 78,
            "D": 78.5,
            "E": 80.26666666666667,
            "F": 81.4,
            "G": 80.4
        },
        "bus1": {
            "D": 72.25,
            "E": 72.8125,
            "F": 72.5,
            "G": 74.1875,
            "H": 75.6875,
            "I": 75.3125,
            "J": 76.875,
            "K": 77.5625,
            "L": 77.25,
            "A": 76.33333333333333,
            "B": 77.33333333333333,
            "C": 77.4
        },
        "bus5": {
            "L": 78.9375,
            "A": 80.4375,
            "B": 80.25,
            "C": 81.1875,
            "D": 80.25,
            "E": 79.125,
            "F": 81.6875,
            "G": 81.5,
            "H": 82,
            "I": 83.66666666666667,
            "J": 82.2,
            "K": 84
        },
        "bus0": {
            "C": 80.5,
            "D": 80.5,
            "E": 82.4375,
            "F": 81.125,
            "G": 82.875,
            "H": 83.0625,
            "I": 84.5625,
            "J": 85,
            "K": 85.8,
            "L": 85.93333333333334,
            "A": 86.66666666666667,
            "B": 86.93333333333334
        },
        "bus4": {
            "I": 70.875,
            "J": 71.5,
            "K": 74.5,
            "L": 76.8125,
            "A": 73.875,
            "B": 75.125,
            "C": 75.6875,
            "D": 78.0625,
            "E": 78,
            "F": 79,
            "G": 78,
            "H": 76.26666666666667
        },
        "bus2": {
            "F": 66.875,
            "G": 66.8125,
            "H": 67.6875,
            "I": 67.6875,
            "J": 69.75,
            "K": 72.1875,
            "L": 70.1875,
            "A": 71.375,
            "B": 71.75,
            "C": 71.46666666666667,
            "D": 72.13333333333334,
            "E": 72.2
        }
    },
    "Obs_and_actions_busses": {
        "4": 408.14155059188744,
        "3": 163.7134980428462,
        "0": -199.2707857315361,
        "1": 84.88195454257952
    },
    "Obs_and_actions_tfls": {
        "0": 103.78663035306892
    }
}