{
    "Average_holding_time": 17.679509632224168,
    "Average_holding_actions": [
        754,
        297,
        0,
        79,
        12,
        0
    ],
    "Average_travel_time_passengers": 2678.797735953047,
    "Average_passenger_waiting_time": 325.61517882331947,
    "Average_passenger_in_vehicle_time": 2353.1825571296786,
    "Average_speed_busses": 0.04758333333333333,
    "Average_experienced_crowding": 79.928403215389,
    "Average_covariation_headway": 0.32101370256510686,
    "Average_covariation_headway_stops": {
        "H": 0.2417182503988991,
        "D": 0.24082389042678135,
        "B": 0.22635536767258566,
        "G": 0.2554201432490947,
        "K": 0.2415527627634836,
        "A": 0.21382641184979864,
        "C": 0.23291534072682712,
        "L": 0.21232821185045317,
        "E": 0.23123575994817855,
        "I": 0.24483896882850173,
        "F": 0.24475177311049226,
        "J": 0.22616131564393993
    },
    "Average_excess_waiting_time": {
        "H": 53.3103791407155,
        "D": 53.60211845727258,
        "B": 51.335260887232096,
        "G": 57.45135772493569,
        "K": 56.559934936218326,
        "A": 51.467895607250114,
        "C": 53.972418604282666,
        "L": 53.75457877885725,
        "E": 53.88019220884706,
        "I": 56.283133176725926,
        "F": 57.671828873484515,
        "J": 55.42236457697936
    },
    "Holding_per_stop": {
        "H": 18.75,
        "B": 15.625,
        "D": 15,
        "K": 20.526315789473685,
        "A": 9.157894736842104,
        "G": 23.68421052631579,
        "C": 18.31578947368421,
        "E": 13.894736842105264,
        "I": 15.473684210526315,
        "L": 19.148936170212767,
        "F": 24.31578947368421,
        "J": 18.31578947368421
    },
    "Actions_tfl_edges": {
        "AB": [
            96,
            0,
            0
        ],
        "CD": [
            96,
            0,
            0
        ],
        "FG": [
            96,
            0,
            0
        ],
        "GH": [
            96,
            0,
            0
        ],
        "JK": [
            96,
            0,
            0
        ],
        "LA": [
            95,
            0,
            0
        ],
        "HI": [
            96,
            0,
            0
        ],
        "BC": [
            96,
            0,
            0
        ],
        "DE": [
            96,
            0,
            0
        ],
        "KL": [
            95,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "IJ": [
            95,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 71.75,
            "I": 74.125,
            "J": 74.6875,
            "K": 75,
            "L": 74.6875,
            "A": 74.4375,
            "B": 73.875,
            "C": 75.9375,
            "D": 77.5,
            "E": 76.8125,
            "F": 76.8125,
            "G": 76.13333333333334
        },
        "bus1": {
            "D": 75.375,
            "E": 75.8125,
            "F": 76.6875,
            "G": 77.8125,
            "H": 77.9375,
            "I": 78.5625,
            "J": 79.6875,
            "K": 79.5625,
            "L": 78.625,
            "A": 79.75,
            "B": 80.75,
            "C": 81
        },
        "bus0": {
            "B": 76.1875,
            "C": 77.8125,
            "D": 79.25,
            "E": 80.875,
            "F": 82.9375,
            "G": 81.25,
            "H": 80,
            "I": 79.875,
            "J": 80.0625,
            "K": 78.9375,
            "L": 80.5625,
            "A": 80.6
        },
        "bus2": {
            "G": 76.9375,
            "H": 77.3125,
            "I": 76.8125,
            "J": 77.0625,
            "K": 78.75,
            "L": 79.625,
            "A": 79.3125,
            "B": 81.125,
            "C": 84.8125,
            "D": 82.8125,
            "E": 80.8,
            "F": 80.86666666666666
        },
        "bus4": {
            "K": 69.5625,
            "L": 69.75,
            "A": 70.9375,
            "B": 72.125,
            "C": 73.75,
            "D": 74.375,
            "E": 75.5625,
            "F": 77.375,
            "G": 76.875,
            "H": 77.6875,
            "I": 75.73333333333333,
            "J": 74.46666666666667
        },
        "bus5": {
            "A": 76.75,
            "B": 76.8125,
            "C": 77,
            "D": 76.6875,
            "E": 76.375,
            "F": 75.8125,
            "G": 75.6875,
            "H": 76.125,
            "I": 79.8125,
            "J": 79.5625,
            "K": 82.93333333333334,
            "L": 83.53333333333333
        }
    },
    "Obs_and_actions_busses": {
        "0": -231.88099318057968,
        "1": 96.3642536018481,
        "4": 239.3065353667069,
        "5": 418.81080532265145
    },
    "Obs_and_actions_tfls": {
        "0": 119.58831674495957
    }
}