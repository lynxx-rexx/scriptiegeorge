{
    "Average_holding_time": 11.776859504132231,
    "Average_holding_actions": [
        940,
        121,
        128,
        7,
        14,
        0
    ],
    "Average_travel_time_passengers": 2521.0960839452027,
    "Average_passenger_waiting_time": 292.752679689175,
    "Average_passenger_in_vehicle_time": 2228.3434042559434,
    "Average_speed_busses": 0.050416666666666665,
    "Average_experienced_crowding": 73.88783132663194,
    "Average_covariation_headway": 0.11313672855277931,
    "Average_covariation_headway_stops": {
        "B": 0.1197235724542117,
        "E": 0.10003438374949225,
        "G": 0.09238562013277574,
        "H": 0.093425026856406,
        "I": 0.09880254809714556,
        "A": 0.12343569895175255,
        "C": 0.07601139470689629,
        "J": 0.07851008711870137,
        "F": 0.07309809810583576,
        "D": 0.07096698557736165,
        "K": 0.08590291032776055,
        "L": 0.10349488577221375
    },
    "Average_excess_waiting_time": {
        "B": 34.445694362436996,
        "E": 34.78492365974785,
        "G": 34.079237041368515,
        "H": 32.747626118691926,
        "I": 31.4880050716406,
        "A": 36.291607316873524,
        "C": 33.43162069719085,
        "J": 32.08034520205388,
        "F": 34.744173455733005,
        "D": 34.719174622321646,
        "K": 33.930490097832774,
        "L": 36.469899596517735
    },
    "Holding_per_stop": {
        "B": 8.910891089108912,
        "I": 12.352941176470589,
        "H": 8.910891089108912,
        "A": 3.267326732673267,
        "E": 9.207920792079207,
        "G": 9.801980198019802,
        "C": 12.475247524752476,
        "J": 11.287128712871286,
        "F": 16.8,
        "D": 8.4,
        "K": 27.623762376237625,
        "L": 12.3
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 69.41176470588235,
            "C": 69.88235294117646,
            "D": 69.29411764705883,
            "E": 70.94117647058823,
            "F": 71.29411764705883,
            "G": 73,
            "H": 73.11764705882354,
            "I": 72.70588235294117,
            "J": 72.94117647058823,
            "K": 72.29411764705883,
            "L": 71.5,
            "A": 73.1875
        },
        "bus1": {
            "E": 69.47058823529412,
            "F": 69.94117647058823,
            "G": 71.76470588235294,
            "H": 72.3529411764706,
            "I": 74.05882352941177,
            "J": 73.76470588235294,
            "K": 73.70588235294117,
            "L": 74.82352941176471,
            "A": 75.29411764705883,
            "B": 76.9375,
            "C": 76.875,
            "D": 74.5625
        },
        "bus2": {
            "G": 71.05882352941177,
            "H": 70.6470588235294,
            "I": 71.82352941176471,
            "J": 71,
            "K": 71.41176470588235,
            "L": 72.3529411764706,
            "A": 72.47058823529412,
            "B": 72.94117647058823,
            "C": 72.58823529411765,
            "D": 73.6875,
            "E": 73.9375,
            "F": 74.25
        },
        "bus3": {
            "H": 71.70588235294117,
            "I": 72.3529411764706,
            "J": 72.58823529411765,
            "K": 73,
            "L": 73.6470588235294,
            "A": 73.76470588235294,
            "B": 73.29411764705883,
            "C": 73.17647058823529,
            "D": 76.76470588235294,
            "E": 75.23529411764706,
            "F": 76.5625,
            "G": 75.875
        },
        "bus4": {
            "I": 75.11764705882354,
            "J": 76.23529411764706,
            "K": 76.58823529411765,
            "L": 76.70588235294117,
            "A": 77.41176470588235,
            "B": 76.88235294117646,
            "C": 75.3529411764706,
            "D": 75.82352941176471,
            "E": 77.6470588235294,
            "F": 76.29411764705883,
            "G": 77.6470588235294,
            "H": 78.125
        },
        "bus5": {
            "A": 72.6470588235294,
            "B": 71.41176470588235,
            "C": 73.05882352941177,
            "D": 73,
            "E": 72.23529411764706,
            "F": 72.70588235294117,
            "G": 72.6470588235294,
            "H": 73.23529411764706,
            "I": 73.41176470588235,
            "J": 74.5625,
            "K": 75.5,
            "L": 76.5
        }
    },
    "Obs_and_actions_busses": {
        "0": -111.50102594038141,
        "2": 58.48699685584731,
        "1": 25.43597333464932,
        "4": 41.14503102316728,
        "5": 264.90972556899845
    },
    "Obs_and_actions_tfls": {}
}