{
    "Average_holding_time": 10.024630541871922,
    "Average_holding_actions": [
        1126,
        0,
        0,
        53,
        39,
        0
    ],
    "Average_travel_time_passengers": 2505.683361999836,
    "Average_passenger_waiting_time": 301.3815557997902,
    "Average_passenger_in_vehicle_time": 2204.301806199969,
    "Average_speed_busses": 0.05075,
    "Average_experienced_crowding": 74.83956828880548,
    "Average_covariation_headway": 0.2757245339153694,
    "Average_covariation_headway_stops": {
        "B": 0.2001396807411191,
        "D": 0.1753031926010871,
        "F": 0.17031264488804207,
        "K": 0.22033049267711516,
        "L": 0.21336687815959543,
        "A": 0.19469539631288002,
        "C": 0.1773965545868317,
        "E": 0.16623196468694862,
        "G": 0.1594864178438826,
        "H": 0.1638109195939024,
        "I": 0.1847669914108876,
        "J": 0.21014061063574757
    },
    "Average_excess_waiting_time": {
        "B": 39.49201995487874,
        "D": 36.15022255775432,
        "F": 36.26796186074597,
        "K": 46.462695750333296,
        "L": 44.21110524951308,
        "A": 40.391390678322125,
        "C": 38.48109778930694,
        "E": 37.49266803186407,
        "G": 36.713511206824364,
        "H": 38.5501020269657,
        "I": 42.186253316123214,
        "J": 46.794956380743486
    },
    "Holding_per_stop": {
        "B": 10.588235294117647,
        "D": 5.588235294117647,
        "A": 2.6470588235294117,
        "F": 9.117647058823529,
        "L": 16.633663366336634,
        "K": 16.633663366336634,
        "C": 13.823529411764707,
        "E": 12.352941176470589,
        "G": 6.176470588235294,
        "H": 9.207920792079207,
        "I": 10.693069306930694,
        "J": 6.9
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 70.41176470588235,
            "C": 68.70588235294117,
            "D": 69.6470588235294,
            "E": 69.23529411764706,
            "F": 71.76470588235294,
            "G": 72.29411764705883,
            "H": 72.94117647058823,
            "I": 74.52941176470588,
            "J": 73.70588235294117,
            "K": 73.23529411764706,
            "L": 75.3529411764706,
            "A": 76.6470588235294
        },
        "bus1": {
            "D": 66.6470588235294,
            "E": 67.6470588235294,
            "F": 69.3529411764706,
            "G": 69.52941176470588,
            "H": 70.76470588235294,
            "I": 71.6470588235294,
            "J": 72,
            "K": 71.6470588235294,
            "L": 73.52941176470588,
            "A": 73.47058823529412,
            "B": 72.47058823529412,
            "C": 73.17647058823529
        },
        "bus2": {
            "F": 77.88235294117646,
            "G": 76.29411764705883,
            "H": 77,
            "I": 78.41176470588235,
            "J": 79.58823529411765,
            "K": 80.05882352941177,
            "L": 81.41176470588235,
            "A": 81.6470588235294,
            "B": 83.41176470588235,
            "C": 84.52941176470588,
            "D": 84.29411764705883,
            "E": 83.70588235294117
        },
        "bus3": {
            "K": 70.3529411764706,
            "L": 72.05882352941177,
            "A": 72.82352941176471,
            "B": 72.47058823529412,
            "C": 73.88235294117646,
            "D": 73.47058823529412,
            "E": 75.82352941176471,
            "F": 77.05882352941177,
            "G": 77.11764705882354,
            "H": 76.75,
            "I": 76.5625,
            "J": 77.25
        },
        "bus4": {
            "L": 71.94117647058823,
            "A": 70.47058823529412,
            "B": 69.41176470588235,
            "C": 69.29411764705883,
            "D": 70.23529411764706,
            "E": 69.3529411764706,
            "F": 71.47058823529412,
            "G": 71.88235294117646,
            "H": 74.05882352941177,
            "I": 74.47058823529412,
            "J": 74.875,
            "K": 76.1875
        },
        "bus5": {
            "A": 67.76470588235294,
            "B": 68.52941176470588,
            "C": 68.41176470588235,
            "D": 66.41176470588235,
            "E": 67,
            "F": 68.58823529411765,
            "G": 68.76470588235294,
            "H": 68.29411764705883,
            "I": 69.70588235294117,
            "J": 72.47058823529412,
            "K": 73.76470588235294,
            "L": 71.25
        }
    },
    "Obs_and_actions_busses": {
        "0": -141.3252520464043,
        "4": 124.99546252664284,
        "5": 264.6453480020202
    },
    "Obs_and_actions_tfls": {}
}