{
    "Average_holding_time": 10.477759472817134,
    "Average_holding_actions": [
        961,
        111,
        132,
        1,
        9,
        0
    ],
    "Average_travel_time_passengers": 2508.0826600514233,
    "Average_passenger_waiting_time": 288.5684339255274,
    "Average_passenger_in_vehicle_time": 2219.5142261258047,
    "Average_speed_busses": 0.050583333333333334,
    "Average_experienced_crowding": 73.79111901025058,
    "Average_covariation_headway": 0.10758779286447706,
    "Average_covariation_headway_stops": {
        "D": 0.0913829929301262,
        "E": 0.08569489594869674,
        "I": 0.12237402758175127,
        "J": 0.1113066418199927,
        "K": 0.10851458457076922,
        "A": 0.0755370422186901,
        "F": 0.05941156279422696,
        "L": 0.07627065869821775,
        "B": 0.04815074596396142,
        "G": 0.06359257810953847,
        "C": 0.0586317335718379,
        "H": 0.09667258269112554
    },
    "Average_excess_waiting_time": {
        "D": 32.56282828633226,
        "E": 30.772402244309433,
        "I": 35.793163996238604,
        "J": 33.59405793034125,
        "K": 32.03180160457731,
        "A": 30.42106975868137,
        "F": 31.142988589594495,
        "L": 32.024163709039556,
        "B": 30.77550642212799,
        "G": 32.754989822816185,
        "C": 32.72657328166878,
        "H": 35.76035515967607
    },
    "Holding_per_stop": {
        "J": 11.584158415841584,
        "E": 3.823529411764706,
        "K": 25.294117647058822,
        "A": 0.8823529411764706,
        "I": 11.584158415841584,
        "D": 10.693069306930694,
        "F": 14.851485148514852,
        "L": 11.881188118811881,
        "B": 3.5643564356435644,
        "G": 9.801980198019802,
        "C": 12.178217821782178,
        "H": 9.6
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "D": 69.88235294117646,
            "E": 70.47058823529412,
            "F": 72.3529411764706,
            "G": 73.47058823529412,
            "H": 72.76470588235294,
            "I": 73.47058823529412,
            "J": 73.52941176470588,
            "K": 75.29411764705883,
            "L": 75,
            "A": 73.76470588235294,
            "B": 73,
            "C": 72.25
        },
        "bus1": {
            "E": 69.41176470588235,
            "F": 70.58823529411765,
            "G": 69.47058823529412,
            "H": 70.29411764705883,
            "I": 70.94117647058823,
            "J": 71.3529411764706,
            "K": 70.47058823529412,
            "L": 72.23529411764706,
            "A": 73.76470588235294,
            "B": 74.41176470588235,
            "C": 75.47058823529412,
            "D": 72.9375
        },
        "bus2": {
            "I": 72.88235294117646,
            "J": 72.58823529411765,
            "K": 73.05882352941177,
            "L": 74.3529411764706,
            "A": 74.88235294117646,
            "B": 76.11764705882354,
            "C": 75.3529411764706,
            "D": 76.52941176470588,
            "E": 77.47058823529412,
            "F": 77.11764705882354,
            "G": 76.625,
            "H": 78.0625
        },
        "bus3": {
            "J": 68.29411764705883,
            "K": 68.05882352941177,
            "L": 69.17647058823529,
            "A": 70,
            "B": 70.11764705882354,
            "C": 71.3529411764706,
            "D": 71.41176470588235,
            "E": 71.58823529411765,
            "F": 70.17647058823529,
            "G": 71.70588235294117,
            "H": 72.52941176470588,
            "I": 73.1875
        },
        "bus4": {
            "K": 72.47058823529412,
            "L": 73,
            "A": 74.17647058823529,
            "B": 72.58823529411765,
            "C": 72.88235294117646,
            "D": 72.17647058823529,
            "E": 73.29411764705883,
            "F": 72.94117647058823,
            "G": 74.3529411764706,
            "H": 75.94117647058823,
            "I": 76.47058823529412,
            "J": 75.47058823529412
        },
        "bus5": {
            "A": 75.47058823529412,
            "B": 74.47058823529412,
            "C": 75.05882352941177,
            "D": 74.3529411764706,
            "E": 75,
            "F": 75.70588235294117,
            "G": 75.70588235294117,
            "H": 76.41176470588235,
            "I": 77.41176470588235,
            "J": 77.82352941176471,
            "K": 78.3529411764706,
            "L": 78.875
        }
    },
    "Obs_and_actions_busses": {
        "0": -113.10297914244134,
        "1": 24.67796049378546,
        "2": 53.62825300750166,
        "5": 302.3937606516382,
        "4": 102.13214220634687
    },
    "Obs_and_actions_tfls": {}
}