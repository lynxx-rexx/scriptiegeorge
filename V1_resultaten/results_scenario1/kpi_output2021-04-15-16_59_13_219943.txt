{
    "Average_holding_time": 12.004971002485501,
    "Average_holding_actions": [
        978,
        145,
        0,
        0,
        43,
        0
    ],
    "Average_travel_time_passengers": 2541.2656760251984,
    "Average_passenger_waiting_time": 298.6065909370055,
    "Average_passenger_in_vehicle_time": 2242.659085088122,
    "Average_speed_busses": 0.050291666666666665,
    "Average_experienced_crowding": 75.58487475178197,
    "Average_covariation_headway": 0.16926540607008628,
    "Average_covariation_headway_stops": {
        "B": 0.1812988791293698,
        "C": 0.17976396890809748,
        "D": 0.16435415318627106,
        "E": 0.16111419868336466,
        "H": 0.13180392950675032,
        "I": 0.11515923596446934,
        "J": 0.08082657273253685,
        "F": 0.13234736506232037,
        "K": 0.09477351332291341,
        "G": 0.12253542274687777,
        "L": 0.11831017709911985,
        "A": 0.15777533645432276
    },
    "Average_excess_waiting_time": {
        "B": 44.43740058974515,
        "C": 42.44688551502651,
        "D": 39.34930599360632,
        "E": 37.548886155282844,
        "H": 36.453919019058276,
        "I": 33.73381001426594,
        "J": 33.37372665046394,
        "F": 36.465680733002785,
        "K": 35.49861224352105,
        "G": 37.24161482007656,
        "L": 38.63854296359864,
        "A": 43.318467067873144
    },
    "Holding_per_stop": {
        "D": 9.504950495049505,
        "I": 9.117647058823529,
        "C": 17.7,
        "E": 12.178217821782178,
        "B": 11.4,
        "H": 14.257425742574258,
        "J": 12.178217821782178,
        "F": 11.584158415841584,
        "G": 11.584158415841584,
        "K": 21.6,
        "L": 11.1,
        "A": 1.8181818181818181
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 70.05882352941177,
            "C": 68.23529411764706,
            "D": 70.47058823529412,
            "E": 71.88235294117646,
            "F": 73.47058823529412,
            "G": 74.47058823529412,
            "H": 75.70588235294117,
            "I": 76.58823529411765,
            "J": 77.625,
            "K": 77.625,
            "L": 76.4375,
            "A": 75.6875
        },
        "bus1": {
            "C": 71.6470588235294,
            "D": 71.88235294117646,
            "E": 73.82352941176471,
            "F": 75.47058823529412,
            "G": 76.94117647058823,
            "H": 77.23529411764706,
            "I": 78.11764705882354,
            "J": 80.52941176470588,
            "K": 79.52941176470588,
            "L": 78.9375,
            "A": 77.9375,
            "B": 75.375
        },
        "bus2": {
            "D": 73.58823529411765,
            "E": 73.17647058823529,
            "F": 73.70588235294117,
            "G": 74.52941176470588,
            "H": 73.88235294117646,
            "I": 74.47058823529412,
            "J": 76.11764705882354,
            "K": 75.17647058823529,
            "L": 75,
            "A": 77.17647058823529,
            "B": 76,
            "C": 78.0625
        },
        "bus3": {
            "E": 70.23529411764706,
            "F": 70.47058823529412,
            "G": 70.52941176470588,
            "H": 72.41176470588235,
            "I": 75.76470588235294,
            "J": 75,
            "K": 75.58823529411765,
            "L": 76.76470588235294,
            "A": 76.70588235294117,
            "B": 75.29411764705883,
            "C": 73.0625,
            "D": 75.25
        },
        "bus4": {
            "H": 67.94117647058823,
            "I": 69.3529411764706,
            "J": 69.3529411764706,
            "K": 69.52941176470588,
            "L": 69.88235294117646,
            "A": 71.52941176470588,
            "B": 72.47058823529412,
            "C": 73.41176470588235,
            "D": 72.88235294117646,
            "E": 72.88235294117646,
            "F": 72.1875,
            "G": 71
        },
        "bus5": {
            "I": 76.3529411764706,
            "J": 74.88235294117646,
            "K": 76.3529411764706,
            "L": 76.52941176470588,
            "A": 78.17647058823529,
            "B": 80,
            "C": 80.23529411764706,
            "D": 80.05882352941177,
            "E": 82.76470588235294,
            "F": 82.11764705882354,
            "G": 81,
            "H": 80.6875
        }
    },
    "Obs_and_actions_busses": {
        "5": 224.43476975523132,
        "1": 46.73282901949334,
        "0": -137.60934079574386,
        "3": 77.61629286924004
    },
    "Obs_and_actions_tfls": {}
}