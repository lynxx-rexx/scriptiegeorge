{
    "Average_holding_time": 14.123539232053423,
    "Average_holding_actions": [
        985,
        20,
        131,
        28,
        34,
        0
    ],
    "Average_travel_time_passengers": 2546.296628092049,
    "Average_passenger_waiting_time": 298.46492767861673,
    "Average_passenger_in_vehicle_time": 2247.8317004133655,
    "Average_speed_busses": 0.049916666666666665,
    "Average_experienced_crowding": 75.45100544812907,
    "Average_covariation_headway": 0.16063678958350006,
    "Average_covariation_headway_stops": {
        "C": 0.14057273886249605,
        "F": 0.14798095780938225,
        "G": 0.13764009610825914,
        "H": 0.12937963011716205,
        "I": 0.14017031266638508,
        "K": 0.12399443601902765,
        "J": 0.11384077510054569,
        "L": 0.12419733687455477,
        "D": 0.10829338096362834,
        "A": 0.11207986065839351,
        "E": 0.12255765629734044,
        "B": 0.1348389669584087
    },
    "Average_excess_waiting_time": {
        "C": 40.764803352993965,
        "F": 42.463911342901895,
        "G": 40.31961076005177,
        "H": 37.62910632653387,
        "I": 37.730015817411186,
        "K": 36.43764220396531,
        "J": 37.31403457303287,
        "L": 38.090853348024154,
        "D": 39.69937448218167,
        "A": 38.73577168070847,
        "E": 42.42065149207713,
        "B": 41.88986116633208
    },
    "Holding_per_stop": {
        "I": 15.148514851485148,
        "K": 19.900990099009903,
        "H": 10.5,
        "G": 12,
        "C": 14.7,
        "F": 12.121212121212121,
        "J": 17.7,
        "L": 25.2,
        "D": 9.696969696969697,
        "A": 9.6,
        "B": 11.515151515151516,
        "E": 11.212121212121213
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 76.3529411764706,
            "D": 76.23529411764706,
            "E": 77,
            "F": 76.23529411764706,
            "G": 75.3529411764706,
            "H": 75.52941176470588,
            "I": 77.88235294117646,
            "J": 77.8125,
            "K": 79.1875,
            "L": 80.3125,
            "A": 79.5625,
            "B": 79.9375
        },
        "bus1": {
            "F": 70.52941176470588,
            "G": 70.94117647058823,
            "H": 72.52941176470588,
            "I": 72.6470588235294,
            "J": 72.47058823529412,
            "K": 73.52941176470588,
            "L": 74.9375,
            "A": 75.3125,
            "B": 76.625,
            "C": 76.6875,
            "D": 76.1875,
            "E": 74.625
        },
        "bus2": {
            "G": 74.52941176470588,
            "H": 74.3529411764706,
            "I": 74,
            "J": 74.23529411764706,
            "K": 73.3529411764706,
            "L": 73.41176470588235,
            "A": 73.70588235294117,
            "B": 75.125,
            "C": 75.375,
            "D": 77.125,
            "E": 78.375,
            "F": 77.3125
        },
        "bus3": {
            "H": 71.05882352941177,
            "I": 71.47058823529412,
            "J": 71.94117647058823,
            "K": 72.47058823529412,
            "L": 73.3529411764706,
            "A": 74.11764705882354,
            "B": 74.82352941176471,
            "C": 76.52941176470588,
            "D": 76.1875,
            "E": 75.8125,
            "F": 75.8125,
            "G": 74.8125
        },
        "bus4": {
            "I": 72.11764705882354,
            "J": 70.47058823529412,
            "K": 70.52941176470588,
            "L": 72.29411764705883,
            "A": 72.17647058823529,
            "B": 72.3529411764706,
            "C": 73.41176470588235,
            "D": 73.76470588235294,
            "E": 73.88235294117646,
            "F": 74.125,
            "G": 75.8125,
            "H": 76.4375
        },
        "bus5": {
            "K": 74.94117647058823,
            "L": 75.05882352941177,
            "A": 73.94117647058823,
            "B": 72.52941176470588,
            "C": 74.76470588235294,
            "D": 76.47058823529412,
            "E": 77.41176470588235,
            "F": 79.17647058823529,
            "G": 81,
            "H": 81.1875,
            "I": 80.5,
            "J": 79.75
        }
    },
    "Obs_and_actions_busses": {
        "5": 189.49332062094757,
        "2": 58.128301386091124,
        "0": -113.43247396045216,
        "4": 79.53194509379682,
        "1": -327.83881486418755
    },
    "Obs_and_actions_tfls": {}
}