{
    "Average_holding_time": 18.3657917019475,
    "Average_holding_actions": [
        898,
        42,
        159,
        47,
        35,
        0
    ],
    "Average_travel_time_passengers": 2584.89209735252,
    "Average_passenger_waiting_time": 302.2670211970754,
    "Average_passenger_in_vehicle_time": 2282.6250761553783,
    "Average_speed_busses": 0.04920833333333333,
    "Average_experienced_crowding": 76.53447047204679,
    "Average_covariation_headway": 0.17257608733244442,
    "Average_covariation_headway_stops": {
        "C": 0.12488741476283192,
        "D": 0.11567642869504403,
        "E": 0.12152996087905878,
        "H": 0.11551128878363119,
        "J": 0.10403231826800864,
        "A": 0.11786629798690729,
        "K": 0.09567888122318248,
        "F": 0.10827290162679784,
        "I": 0.08658977612283425,
        "B": 0.110993718827192,
        "L": 0.09741618607335928,
        "G": 0.09726487001668406
    },
    "Average_excess_waiting_time": {
        "C": 44.52929943535452,
        "D": 42.25820749752057,
        "E": 41.233696599992754,
        "H": 42.313308518525616,
        "J": 41.74290046622184,
        "A": 44.239136831023814,
        "K": 42.81323825057598,
        "F": 41.79279328472006,
        "I": 42.43802699050542,
        "B": 45.206614489120454,
        "L": 44.44373543473728,
        "G": 42.78173489148173
    },
    "Holding_per_stop": {
        "D": 19.696969696969695,
        "J": 17.87878787878788,
        "E": 19.393939393939394,
        "H": 16.060606060606062,
        "A": 6.428571428571429,
        "C": 28.775510204081634,
        "K": 17.448979591836736,
        "F": 19.393939393939394,
        "I": 14.081632653061224,
        "B": 15.918367346938776,
        "L": 27.244897959183675,
        "G": 18.06122448979592
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 72.70588235294117,
            "D": 71.58823529411765,
            "E": 72.41176470588235,
            "F": 74.3529411764706,
            "G": 74.8125,
            "H": 75.75,
            "I": 75.625,
            "J": 76.4375,
            "K": 76.75,
            "L": 76.625,
            "A": 78.4375,
            "B": 79
        },
        "bus1": {
            "D": 72.3529411764706,
            "E": 74.88235294117646,
            "F": 75.23529411764706,
            "G": 75.47058823529412,
            "H": 74.47058823529412,
            "I": 74.5,
            "J": 75.125,
            "K": 74.375,
            "L": 73.8125,
            "A": 75,
            "B": 75.25,
            "C": 77.1875
        },
        "bus2": {
            "E": 75,
            "F": 75.05882352941177,
            "G": 75.70588235294117,
            "H": 74.17647058823529,
            "I": 74.29411764705883,
            "J": 75.58823529411765,
            "K": 75.375,
            "L": 76.375,
            "A": 77.625,
            "B": 77.75,
            "C": 79.5,
            "D": 79.75
        },
        "bus3": {
            "H": 72.76470588235294,
            "I": 75.76470588235294,
            "J": 76.17647058823529,
            "K": 76.11764705882354,
            "L": 77.29411764705883,
            "A": 75.0625,
            "B": 75.125,
            "C": 75.875,
            "D": 75.5,
            "E": 74.8125,
            "F": 74.4375,
            "G": 74.9375
        },
        "bus4": {
            "J": 73.17647058823529,
            "K": 73.82352941176471,
            "L": 73.05882352941177,
            "A": 73.94117647058823,
            "B": 75.23529411764706,
            "C": 74.875,
            "D": 75.8125,
            "E": 75.4375,
            "F": 77.8125,
            "G": 76.8125,
            "H": 76.625,
            "I": 75.875
        },
        "bus5": {
            "A": 77.11764705882354,
            "B": 79.88235294117646,
            "C": 80,
            "D": 78.52941176470588,
            "E": 79.0625,
            "F": 78,
            "G": 77.9375,
            "H": 80,
            "I": 80.6875,
            "J": 80.4375,
            "K": 81.6875,
            "L": 81.625
        }
    },
    "Obs_and_actions_busses": {
        "0": -99.85756702627657,
        "5": 163.09298563651996,
        "4": 78.3975123491036,
        "1": -319.2808698620289,
        "2": 62.078710587087514
    },
    "Obs_and_actions_tfls": {}
}