{
    "Average_holding_time": 19.71063829787234,
    "Average_holding_actions": [
        878,
        48,
        160,
        41,
        48,
        0
    ],
    "Average_travel_time_passengers": 2605.949502149118,
    "Average_passenger_waiting_time": 304.83642663168365,
    "Average_passenger_in_vehicle_time": 2301.1130755173417,
    "Average_speed_busses": 0.04895833333333333,
    "Average_experienced_crowding": 76.69972782724352,
    "Average_covariation_headway": 0.1713019508560485,
    "Average_covariation_headway_stops": {
        "C": 0.13173970201172377,
        "D": 0.12852703339736796,
        "E": 0.13343917691572638,
        "H": 0.1151006606288908,
        "I": 0.11039478547570108,
        "A": 0.14156376476810875,
        "F": 0.12225520328639591,
        "J": 0.08396791856379363,
        "B": 0.11484350740025966,
        "G": 0.10511909261106465,
        "K": 0.09361426839821611,
        "L": 0.12050483838212964
    },
    "Average_excess_waiting_time": {
        "C": 46.9160374656106,
        "D": 44.702964281628624,
        "E": 43.43309012398487,
        "H": 43.60587488019695,
        "I": 41.67094654951546,
        "A": 47.94687646534146,
        "F": 43.9091076058499,
        "J": 41.95403532289765,
        "B": 47.23381146817195,
        "G": 44.45138226534522,
        "K": 43.92044113655123,
        "L": 47.38794361053539
    },
    "Holding_per_stop": {
        "E": 20.90909090909091,
        "I": 15.151515151515152,
        "D": 20.510204081632654,
        "H": 16.836734693877553,
        "C": 28.775510204081634,
        "A": 8.350515463917526,
        "F": 21.122448979591837,
        "J": 17.755102040816325,
        "K": 19.897959183673468,
        "B": 20.103092783505154,
        "G": 24.489795918367346,
        "L": 22.577319587628867
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 70.29411764705883,
            "D": 71.11764705882354,
            "E": 70.88235294117646,
            "F": 72.625,
            "G": 72.375,
            "H": 73.6875,
            "I": 74.25,
            "J": 75.3125,
            "K": 76.375,
            "L": 75.875,
            "A": 75.5,
            "B": 74.75
        },
        "bus1": {
            "D": 74.82352941176471,
            "E": 74.76470588235294,
            "F": 73.29411764705883,
            "G": 74.11764705882354,
            "H": 75.5625,
            "I": 76.4375,
            "J": 76.9375,
            "K": 77.9375,
            "L": 79.9375,
            "A": 79.1875,
            "B": 79.8125,
            "C": 79
        },
        "bus2": {
            "E": 70.88235294117646,
            "F": 71.76470588235294,
            "G": 72.70588235294117,
            "H": 74.23529411764706,
            "I": 72.88235294117646,
            "J": 73.875,
            "K": 76.5,
            "L": 78,
            "A": 78.4375,
            "B": 78.5,
            "C": 77.6875,
            "D": 76.4375
        },
        "bus3": {
            "H": 72.70588235294117,
            "I": 75,
            "J": 75.58823529411765,
            "K": 73.47058823529412,
            "L": 74.3125,
            "A": 75.375,
            "B": 76.5,
            "C": 76.625,
            "D": 75.6875,
            "E": 76.6875,
            "F": 75.5625,
            "G": 77.375
        },
        "bus4": {
            "I": 74.6470588235294,
            "J": 76.52941176470588,
            "K": 76.82352941176471,
            "L": 77.47058823529412,
            "A": 77.47058823529412,
            "B": 79.4375,
            "C": 80.25,
            "D": 79.1875,
            "E": 77.75,
            "F": 76.5625,
            "G": 76.3125,
            "H": 79.1875
        },
        "bus5": {
            "A": 74.11764705882354,
            "B": 77.23529411764706,
            "C": 78.94117647058823,
            "D": 79.625,
            "E": 80.125,
            "F": 81.5625,
            "G": 81.75,
            "H": 79.6875,
            "I": 77.8125,
            "J": 76.3125,
            "K": 77.4375,
            "L": 77
        }
    },
    "Obs_and_actions_busses": {
        "5": 172.4040593764743,
        "2": 61.421209316099116,
        "0": -98.73982528988586,
        "4": 74.58633631817663,
        "1": -317.87952897176916
    },
    "Obs_and_actions_tfls": {}
}