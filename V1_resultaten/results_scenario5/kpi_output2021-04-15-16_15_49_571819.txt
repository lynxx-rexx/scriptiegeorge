{
    "Average_holding_time": 25.274725274725274,
    "Average_holding_actions": [
        701,
        0,
        345,
        0,
        46,
        0
    ],
    "Average_travel_time_passengers": 2784.2639613857873,
    "Average_passenger_waiting_time": 349.7833830863323,
    "Average_passenger_in_vehicle_time": 2434.4805782994736,
    "Average_speed_busses": 0.0455,
    "Average_experienced_crowding": 116.33628511439035,
    "Average_covariation_headway": 0.3465143574743705,
    "Average_covariation_headway_stops": {
        "H": 0.2901816295114337,
        "G": 0.2595170612055188,
        "J": 0.26901912028486996,
        "F": 0.25799700183553664,
        "L": 0.24954656454193022,
        "I": 0.2822494056607648,
        "C": 0.2532523657786869,
        "K": 0.24586217105263375,
        "A": 0.24396694706495853,
        "D": 0.27129510404044993,
        "E": 0.265315714903283,
        "B": 0.26521473151433445
    },
    "Average_excess_waiting_time": {
        "H": 75.83866131615326,
        "G": 72.09124774723864,
        "J": 70.72347346513732,
        "F": 73.71829715177341,
        "L": 67.59811623168628,
        "I": 74.65388122862504,
        "C": 72.06406203999734,
        "K": 68.78868759337479,
        "A": 68.71833194769101,
        "D": 78.47038040734867,
        "E": 79.37999665240687,
        "B": 75.2753336977737
    },
    "Holding_per_stop": {
        "H": 22.82608695652174,
        "J": 29.347826086956523,
        "G": 32.967032967032964,
        "L": 18.58695652173913,
        "I": 30.65934065934066,
        "C": 14.835164835164836,
        "F": 36.666666666666664,
        "K": 28.681318681318682,
        "A": 17.47252747252747,
        "D": 21,
        "B": 18.13186813186813,
        "E": 32.333333333333336
    },
    "Actions_tfl_edges": {
        "FG": [
            0,
            15,
            76
        ],
        "GH": [
            0,
            32,
            60
        ],
        "HI": [
            0,
            11,
            81
        ],
        "JK": [
            0,
            5,
            87
        ],
        "CD": [
            0,
            5,
            86
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 126.5,
            "I": 122.75,
            "J": 115.3125,
            "K": 106.625,
            "L": 97.33333333333333,
            "A": 90.13333333333334,
            "B": 80.8,
            "C": 76.2,
            "D": 72.8,
            "E": 71.66666666666667,
            "F": 93.2,
            "G": 124.13333333333334
        },
        "bus2": {
            "G": 128.9375,
            "H": 138.6875,
            "I": 133.6,
            "J": 124.13333333333334,
            "K": 114.86666666666666,
            "L": 105.6,
            "A": 97.4,
            "B": 89.4,
            "C": 83.26666666666667,
            "D": 79.4,
            "E": 75.93333333333334,
            "F": 101.2
        },
        "bus4": {
            "J": 109.0625,
            "K": 100.8125,
            "L": 91.75,
            "A": 84.3125,
            "B": 74.66666666666667,
            "C": 71.33333333333333,
            "D": 70,
            "E": 68,
            "F": 89,
            "G": 118.26666666666667,
            "H": 126.53333333333333,
            "I": 123.4
        },
        "bus1": {
            "F": 94.06666666666666,
            "G": 124.53333333333333,
            "H": 131.4,
            "I": 129.33333333333334,
            "J": 124.46666666666667,
            "K": 117.73333333333333,
            "L": 110.66666666666667,
            "A": 99.8,
            "B": 89.6,
            "C": 84.33333333333333,
            "D": 80.2,
            "E": 78.6
        },
        "bus5": {
            "L": 112.25,
            "A": 102.9375,
            "B": 95.5,
            "C": 90.06666666666666,
            "D": 86.86666666666666,
            "E": 85.2,
            "F": 110.66666666666667,
            "G": 145.13333333333333,
            "H": 152.73333333333332,
            "I": 147.66666666666666,
            "J": 141.33333333333334,
            "K": 129.13333333333333
        },
        "bus0": {
            "C": 85.8125,
            "D": 84.73333333333333,
            "E": 82,
            "F": 110.2,
            "G": 147.4,
            "H": 156.26666666666668,
            "I": 152.93333333333334,
            "J": 143.4,
            "K": 134.26666666666668,
            "L": 123.4,
            "A": 111.26666666666667,
            "B": 99.93333333333334
        }
    },
    "Obs_and_actions_busses": {
        "0": -239.57733448859878,
        "2": 103.12593501900162,
        "5": 375.5991337844982
    },
    "Obs_and_actions_tfls": {
        "2": 135.13885231543358,
        "1": -274.50491891635335
    }
}