{
    "Average_holding_time": 26.66048237476809,
    "Average_holding_actions": [
        662,
        0,
        374,
        0,
        42,
        0
    ],
    "Average_travel_time_passengers": 2799.23105131673,
    "Average_passenger_waiting_time": 344.79719330807757,
    "Average_passenger_in_vehicle_time": 2454.4338580086905,
    "Average_speed_busses": 0.04491666666666667,
    "Average_experienced_crowding": 114.98192132235306,
    "Average_covariation_headway": 0.3419308876436542,
    "Average_covariation_headway_stops": {
        "G": 0.24447418849294547,
        "F": 0.2393610955418583,
        "K": 0.24088908694495545,
        "H": 0.25869948101383977,
        "D": 0.25886379739890714,
        "A": 0.21898329487017223,
        "B": 0.243396610006124,
        "I": 0.25512031463224344,
        "L": 0.21010548859702846,
        "E": 0.25000283586135835,
        "C": 0.23166370539420747,
        "J": 0.22881172929499924
    },
    "Average_excess_waiting_time": {
        "G": 71.75516381809058,
        "F": 73.17550364383095,
        "K": 73.59366269345509,
        "H": 74.60230192110168,
        "D": 77.42481470038558,
        "A": 70.73353982745823,
        "B": 72.12327559472385,
        "I": 75.51871258496476,
        "L": 71.158303432764,
        "E": 77.12273152538387,
        "C": 73.25214144281915,
        "J": 73.99961529674982
    },
    "Holding_per_stop": {
        "G": 29.666666666666668,
        "H": 24.666666666666668,
        "K": 35,
        "B": 19,
        "D": 19.333333333333332,
        "A": 14.666666666666666,
        "F": 32.333333333333336,
        "I": 35.666666666666664,
        "L": 19.666666666666668,
        "E": 35.056179775280896,
        "C": 17.666666666666668,
        "J": 37.41573033707865
    },
    "Actions_tfl_edges": {
        "CD": [
            0,
            4,
            87
        ],
        "FG": [
            0,
            23,
            68
        ],
        "JK": [
            0,
            8,
            82
        ],
        "GH": [
            0,
            27,
            63
        ],
        "HI": [
            0,
            12,
            78
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 124.9375,
            "H": 129.8,
            "I": 125.73333333333333,
            "J": 120.13333333333334,
            "K": 112.53333333333333,
            "L": 104.66666666666667,
            "A": 95.46666666666667,
            "B": 86.66666666666667,
            "C": 82.06666666666666,
            "D": 79.53333333333333,
            "E": 77.66666666666667,
            "F": 103.4
        },
        "bus2": {
            "F": 93.6,
            "G": 125.93333333333334,
            "H": 131.6,
            "I": 128.73333333333332,
            "J": 121.8,
            "K": 112.73333333333333,
            "L": 103.13333333333334,
            "A": 93.06666666666666,
            "B": 84.2,
            "C": 78.53333333333333,
            "D": 73.73333333333333,
            "E": 74.64285714285714
        },
        "bus4": {
            "K": 116.86666666666666,
            "L": 106.33333333333333,
            "A": 97.06666666666666,
            "B": 87.73333333333333,
            "C": 81.93333333333334,
            "D": 78,
            "E": 77.2,
            "F": 103.6,
            "G": 136.8,
            "H": 142.66666666666666,
            "I": 139.53333333333333,
            "J": 132.06666666666666
        },
        "bus1": {
            "D": 84.86666666666666,
            "E": 83.13333333333334,
            "F": 107.73333333333333,
            "G": 147.33333333333334,
            "H": 155.46666666666667,
            "I": 151.93333333333334,
            "J": 143.8,
            "K": 134.06666666666666,
            "L": 123.53333333333333,
            "A": 110.86666666666666,
            "B": 100.46666666666667,
            "C": 92.13333333333334
        },
        "bus5": {
            "A": 93.2,
            "B": 85.06666666666666,
            "C": 81.2,
            "D": 78.06666666666666,
            "E": 77.93333333333334,
            "F": 101.26666666666667,
            "G": 134.8,
            "H": 143.33333333333334,
            "I": 139.8,
            "J": 130.86666666666667,
            "K": 122.26666666666667,
            "L": 111.2
        },
        "bus0": {
            "B": 81.4,
            "C": 73.53333333333333,
            "D": 72.8,
            "E": 70.33333333333333,
            "F": 93.33333333333333,
            "G": 128,
            "H": 133.93333333333334,
            "I": 130.66666666666666,
            "J": 123.4,
            "K": 115.2,
            "L": 105.2,
            "A": 96.73333333333333
        }
    },
    "Obs_and_actions_busses": {
        "0": -244.13106387296813,
        "2": 95.65175307255593,
        "5": 331.9142492993181
    },
    "Obs_and_actions_tfls": {
        "2": 148.21348929971708,
        "1": -289.1520018046994
    }
}ls": {
        "2": 149.40097973581274,
        "1": -305.33956854369467
    }
}