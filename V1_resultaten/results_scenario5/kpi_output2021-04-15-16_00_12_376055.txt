{
    "Average_holding_time": 37.262443438914026,
    "Average_holding_actions": [
        470,
        139,
        0,
        124,
        5,
        0
    ],
    "Average_travel_time_passengers": 3701.495324159414,
    "Average_passenger_waiting_time": 661.542520186517,
    "Average_passenger_in_vehicle_time": 3039.9528039729494,
    "Average_speed_busses": 0.036833333333333336,
    "Average_experienced_crowding": 230.50756516775473,
    "Average_covariation_headway": 0.9961870180135709,
    "Average_covariation_headway_stops": {
        "H": 0.9291670284921778,
        "F": 1.0252488280226517,
        "K": 1.0186435365730844,
        "J": 1.0021253330559743,
        "A": 1.0460539152692871,
        "I": 0.9796414471792966,
        "D": 1.0576020232527357,
        "G": 0.8287807922904923,
        "L": 1.0198012770199276,
        "E": 1.077052263162362,
        "B": 1.0457388643163092,
        "C": 1.0682292050253581
    },
    "Average_excess_waiting_time": {
        "H": 427.9001134515744,
        "F": 567.0750719573438,
        "K": 490.9305616299341,
        "J": 482.89482330977285,
        "A": 513.8382792186104,
        "I": 468.92714117590367,
        "D": 540.4438767394065,
        "G": 366.8286867187884,
        "L": 497.3987290154539,
        "E": 565.4585986044289,
        "B": 521.2890426176075,
        "C": 550.3185990786369
    },
    "Holding_per_stop": {
        "H": 32.916666666666664,
        "K": 32.83783783783784,
        "J": 42.32876712328767,
        "F": 45.2112676056338,
        "A": 29.6,
        "I": 44.166666666666664,
        "D": 39.86842105263158,
        "G": 37.605633802816904,
        "L": 30.81081081081081,
        "B": 32.8,
        "E": 46.18421052631579,
        "C": 33.2
    },
    "Actions_tfl_edges": {
        "CD": [
            23,
            12,
            41
        ],
        "GH": [
            11,
            25,
            36
        ],
        "JK": [
            25,
            24,
            25
        ],
        "HI": [
            14,
            26,
            32
        ],
        "FG": [
            14,
            24,
            33
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "H": 273.0833333333333,
            "I": 266.5,
            "J": 255,
            "K": 239.16666666666666,
            "L": 223.33333333333334,
            "A": 207,
            "B": 190.58333333333334,
            "C": 181.33333333333334,
            "D": 179.25,
            "E": 182.5,
            "F": 245.41666666666666,
            "G": 273.45454545454544
        },
        "bus1": {
            "F": 72.75,
            "G": 105.16666666666667,
            "H": 110.33333333333333,
            "I": 109.16666666666667,
            "J": 104.25,
            "K": 94.66666666666667,
            "L": 84.08333333333333,
            "A": 76.58333333333333,
            "B": 68.66666666666667,
            "C": 63.833333333333336,
            "D": 60.75,
            "E": 57.5
        },
        "bus4": {
            "K": 107.53846153846153,
            "L": 99.07692307692308,
            "A": 89.92307692307692,
            "B": 84.15384615384616,
            "C": 79.3076923076923,
            "D": 72.92307692307692,
            "E": 72.92307692307692,
            "F": 103.5,
            "G": 131.66666666666666,
            "H": 137.08333333333334,
            "I": 132.83333333333334,
            "J": 125.91666666666667
        },
        "bus3": {
            "J": 197,
            "K": 183.07692307692307,
            "L": 170.46153846153845,
            "A": 156.3846153846154,
            "B": 141.76923076923077,
            "C": 134,
            "D": 126.92307692307692,
            "E": 126.92307692307692,
            "F": 173.58333333333334,
            "G": 225.25,
            "H": 240.16666666666666,
            "I": 229.66666666666666
        },
        "bus5": {
            "A": 71.53846153846153,
            "B": 64.6923076923077,
            "C": 61.46153846153846,
            "D": 59,
            "E": 58.92307692307692,
            "F": 84.66666666666667,
            "G": 109.91666666666667,
            "H": 115.16666666666667,
            "I": 110.58333333333333,
            "J": 105.66666666666667,
            "K": 95.16666666666667,
            "L": 88
        },
        "bus0": {
            "D": 47.46153846153846,
            "E": 47.15384615384615,
            "F": 69.58333333333333,
            "G": 92.08333333333333,
            "H": 94.66666666666667,
            "I": 91.5,
            "J": 87,
            "K": 80.83333333333333,
            "L": 75.66666666666667,
            "A": 67.58333333333333,
            "B": 60.25,
            "C": 54.666666666666664
        }
    },
    "Obs_and_actions_busses": {
        "1": 599.6536140136267,
        "3": -643.3580713872013,
        "0": -177.72067801430867,
        "4": 1194.2920705376966,
        "5": -6881.0457554900195
    },
    "Obs_and_actions_tfls": {
        "0": 846.3008602754946,
        "1": -14.749441759010281,
        "2": -314.2748046472017
    }
}8.88775088466775,
        "1": 13.182246118178178
    }
}