{
    "Average_holding_time": 26.263837638376383,
    "Average_holding_actions": [
        662,
        0,
        387,
        0,
        35,
        0
    ],
    "Average_travel_time_passengers": 2809.5884192358303,
    "Average_passenger_waiting_time": 340.3575202032079,
    "Average_passenger_in_vehicle_time": 2469.230899032631,
    "Average_speed_busses": 0.04516666666666667,
    "Average_experienced_crowding": 116.04726406981686,
    "Average_covariation_headway": 0.3252066004263529,
    "Average_covariation_headway_stops": {
        "I": 0.25307660185298336,
        "F": 0.22413781404723174,
        "A": 0.21141018578040371,
        "B": 0.23924287095318583,
        "L": 0.19307617796477852,
        "G": 0.21840077791057438,
        "D": 0.23738704082463333,
        "J": 0.21328713383050937,
        "H": 0.23381115508891842,
        "E": 0.22271212918577404,
        "K": 0.20543487290978596,
        "C": 0.2325662038340524
    },
    "Average_excess_waiting_time": {
        "I": 73.82635450313865,
        "F": 67.88886445486997,
        "A": 67.12245128788868,
        "B": 70.02828520948162,
        "L": 66.05391661152345,
        "G": 68.23612875902666,
        "D": 69.6195325182984,
        "J": 69.0245138183542,
        "H": 73.11618238537079,
        "E": 69.68329506486776,
        "K": 69.88050585505158,
        "C": 70.49520942057268
    },
    "Holding_per_stop": {
        "I": 28.333333333333332,
        "F": 35.604395604395606,
        "A": 20.76923076923077,
        "B": 15.824175824175825,
        "L": 23,
        "D": 18.791208791208792,
        "G": 34,
        "J": 37.666666666666664,
        "H": 21.333333333333332,
        "E": 34,
        "C": 14,
        "K": 32
    },
    "Actions_tfl_edges": {
        "CD": [
            0,
            6,
            85
        ],
        "HI": [
            0,
            17,
            74
        ],
        "FG": [
            0,
            21,
            70
        ],
        "GH": [
            0,
            35,
            55
        ],
        "JK": [
            0,
            4,
            86
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "I": 137.5,
            "J": 130.93333333333334,
            "K": 122.2,
            "L": 111.86666666666666,
            "A": 102.86666666666666,
            "B": 93.66666666666667,
            "C": 87.8,
            "D": 83.86666666666666,
            "E": 81.4,
            "F": 109.4,
            "G": 145.2,
            "H": 153.4
        },
        "bus2": {
            "F": 91.3125,
            "G": 121.125,
            "H": 130.6,
            "I": 126,
            "J": 121.06666666666666,
            "K": 114.73333333333333,
            "L": 104.93333333333334,
            "A": 96.93333333333334,
            "B": 88.33333333333333,
            "C": 80.73333333333333,
            "D": 76,
            "E": 74.73333333333333
        },
        "bus5": {
            "A": 94.375,
            "B": 86.86666666666666,
            "C": 80,
            "D": 76.93333333333334,
            "E": 74.4,
            "F": 97.66666666666667,
            "G": 128.33333333333334,
            "H": 138.13333333333333,
            "I": 134.13333333333333,
            "J": 127.13333333333334,
            "K": 118.8,
            "L": 108.6
        },
        "bus0": {
            "B": 80.6875,
            "C": 78.53333333333333,
            "D": 77.46666666666667,
            "E": 77.46666666666667,
            "F": 104.06666666666666,
            "G": 137.6,
            "H": 144.73333333333332,
            "I": 137.26666666666668,
            "J": 130,
            "K": 119.46666666666667,
            "L": 106.73333333333333,
            "A": 95.46666666666667
        },
        "bus4": {
            "L": 107.93333333333334,
            "A": 98.4,
            "B": 89.33333333333333,
            "C": 83.93333333333334,
            "D": 80.2,
            "E": 78.46666666666667,
            "F": 101.66666666666667,
            "G": 135.4,
            "H": 145,
            "I": 139.2,
            "J": 133.26666666666668,
            "K": 125.4
        },
        "bus1": {
            "D": 81.1875,
            "E": 78,
            "F": 102.93333333333334,
            "G": 141.73333333333332,
            "H": 150.66666666666666,
            "I": 149.6,
            "J": 142.8,
            "K": 133,
            "L": 122.33333333333333,
            "A": 108.4,
            "B": 97.4,
            "C": 91.4
        }
    },
    "Obs_and_actions_busses": {
        "0": -248.13420836664983,
        "2": 90.98737852435616,
        "5": 326.6595977041009
    },
    "Obs_and_actions_tfls": {
        "2": 137.99375878064248,
        "1": -260.46250136039436
    }
}