{
    "Average_holding_time": 31.71591992373689,
    "Average_holding_actions": [
        530,
        184,
        202,
        0,
        61,
        0
    ],
    "Average_travel_time_passengers": 2864.321436307347,
    "Average_passenger_waiting_time": 352.09130684478725,
    "Average_passenger_in_vehicle_time": 2512.2301294625568,
    "Average_speed_busses": 0.043708333333333335,
    "Average_experienced_crowding": 117.75731417189293,
    "Average_covariation_headway": 0.3096653297660378,
    "Average_covariation_headway_stops": {
        "H": 0.22337095269139282,
        "F": 0.2319254491471724,
        "I": 0.22883044425923457,
        "E": 0.23318627532842162,
        "B": 0.24646808837051792,
        "D": 0.26477279299814444,
        "G": 0.20464232484119949,
        "J": 0.19048136649416156,
        "K": 0.17503078916463594,
        "C": 0.24486792497223508,
        "L": 0.1742213986530215,
        "A": 0.1987925505445972
    },
    "Average_excess_waiting_time": {
        "H": 78.46322190258343,
        "F": 80.24833011913694,
        "I": 76.93733138612527,
        "E": 82.62925773937934,
        "B": 85.89242115186767,
        "D": 90.30766422486005,
        "G": 78.38299784584774,
        "J": 73.37437653753375,
        "K": 73.4262054291022,
        "C": 88.24413934326992,
        "L": 76.10784015249249,
        "A": 81.29490497257251
    },
    "Holding_per_stop": {
        "I": 38.08988764044944,
        "F": 45.34090909090909,
        "H": 24.886363636363637,
        "E": 46.89655172413793,
        "G": 35.86206896551724,
        "B": 22.413793103448278,
        "J": 40.56818181818182,
        "D": 35.86206896551724,
        "K": 38.52272727272727,
        "C": 11.511627906976743,
        "L": 17.93103448275862,
        "A": 22.06896551724138
    },
    "Actions_tfl_edges": {
        "CD": [
            72,
            8,
            7
        ],
        "GH": [
            31,
            52,
            5
        ],
        "HI": [
            52,
            24,
            13
        ],
        "FG": [
            40,
            42,
            6
        ],
        "JK": [
            76,
            11,
            1
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "H": 150.4,
            "I": 146.33333333333334,
            "J": 136.53333333333333,
            "K": 124.6,
            "L": 114.66666666666667,
            "A": 103.46666666666667,
            "B": 93.66666666666667,
            "C": 89.86666666666666,
            "D": 88.78571428571429,
            "E": 89.28571428571429,
            "F": 116,
            "G": 152.78571428571428
        },
        "bus3": {
            "F": 85.66666666666667,
            "G": 117.86666666666666,
            "H": 125.26666666666667,
            "I": 120.73333333333333,
            "J": 114.6,
            "K": 105.66666666666667,
            "L": 96.06666666666666,
            "A": 86.26666666666667,
            "B": 73.64285714285714,
            "C": 69.71428571428571,
            "D": 66.14285714285714,
            "E": 67.5
        },
        "bus5": {
            "I": 149.06666666666666,
            "J": 141.6,
            "K": 131.06666666666666,
            "L": 120.4,
            "A": 110.73333333333333,
            "B": 99.4,
            "C": 92.2,
            "D": 91.6,
            "E": 90.2,
            "F": 119.35714285714286,
            "G": 154.78571428571428,
            "H": 163
        },
        "bus2": {
            "E": 69.66666666666667,
            "F": 92.53333333333333,
            "G": 125,
            "H": 134.66666666666666,
            "I": 131.93333333333334,
            "J": 124.33333333333333,
            "K": 116,
            "L": 106.71428571428571,
            "A": 99.21428571428571,
            "B": 87.85714285714286,
            "C": 80.71428571428571,
            "D": 76
        },
        "bus0": {
            "B": 94.53333333333333,
            "C": 91.13333333333334,
            "D": 89.73333333333333,
            "E": 88.33333333333333,
            "F": 117.86666666666666,
            "G": 153.86666666666667,
            "H": 164.5,
            "I": 156.92857142857142,
            "J": 148.78571428571428,
            "K": 137.64285714285714,
            "L": 124.92857142857143,
            "A": 111.71428571428571
        },
        "bus1": {
            "D": 73.66666666666667,
            "E": 72.86666666666666,
            "F": 95.46666666666667,
            "G": 129.26666666666668,
            "H": 138.06666666666666,
            "I": 136.8,
            "J": 129.92857142857142,
            "K": 119.92857142857143,
            "L": 108.07142857142857,
            "A": 98.64285714285714,
            "B": 88.14285714285714,
            "C": 80.78571428571429
        }
    },
    "Obs_and_actions_busses": {
        "5": 305.31670095767345,
        "3": 163.80501163016214,
        "2": 70.74100600457004,
        "1": -20.560896051462052,
        "0": -279.0264693291969
    },
    "Obs_and_actions_tfls": {
        "0": 184.82835659780372,
        "1": -191.71146051518326,
        "2": 12.52536900542442
    }
}