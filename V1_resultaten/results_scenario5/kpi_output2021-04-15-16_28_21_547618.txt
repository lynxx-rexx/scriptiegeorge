{
    "Average_holding_time": 15.365853658536585,
    "Average_holding_actions": [
        950,
        0,
        0,
        0,
        48,
        0
    ],
    "Average_travel_time_passengers": 2782.256809336768,
    "Average_passenger_waiting_time": 370.0136951888575,
    "Average_passenger_in_vehicle_time": 2412.2431141478605,
    "Average_speed_busses": 0.046125,
    "Average_experienced_crowding": 121.08081002809585,
    "Average_covariation_headway": 0.5071340209874036,
    "Average_covariation_headway_stops": {
        "G": 0.3724772197019126,
        "I": 0.39904768154682496,
        "F": 0.3658319000593357,
        "H": 0.4032352707511231,
        "J": 0.41084090323059613,
        "C": 0.37838934158232806,
        "B": 0.38956805632650976,
        "K": 0.3844864104140975,
        "L": 0.3749160057740216,
        "D": 0.37745503367193806,
        "E": 0.36994069294461224,
        "A": 0.3930411687598493
    },
    "Average_excess_waiting_time": {
        "G": 88.89204116399998,
        "I": 94.44828731208634,
        "F": 89.9957001613875,
        "H": 97.8303312409542,
        "J": 95.53671234135686,
        "C": 91.77171964343046,
        "B": 96.02078521902206,
        "K": 90.62444709342446,
        "L": 90.38993904844381,
        "D": 93.53065236105436,
        "E": 93.3175907244003,
        "A": 98.4115016762438
    },
    "Holding_per_stop": {
        "G": 15.806451612903226,
        "I": 12.903225806451612,
        "H": 22.17391304347826,
        "J": 17.872340425531913,
        "C": 14.673913043478262,
        "F": 20.543478260869566,
        "B": 13.043478260869565,
        "K": 13.870967741935484,
        "L": 13.369565217391305,
        "D": 14.673913043478262,
        "E": 16.483516483516482,
        "A": 8.901098901098901
    },
    "Actions_tfl_edges": {
        "FG": [
            28,
            53,
            12
        ],
        "HI": [
            21,
            53,
            19
        ],
        "GH": [
            33,
            42,
            18
        ],
        "JK": [
            17,
            68,
            9
        ],
        "CD": [
            17,
            61,
            14
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 138.5625,
            "H": 147.1875,
            "I": 141.25,
            "J": 134.6875,
            "K": 124.8125,
            "L": 112.875,
            "A": 106.06666666666666,
            "B": 94.13333333333334,
            "C": 88.73333333333333,
            "D": 83.4,
            "E": 83.2,
            "F": 110
        },
        "bus4": {
            "I": 138.1875,
            "J": 131.625,
            "K": 124.125,
            "L": 113.5625,
            "A": 100.93333333333334,
            "B": 91.2,
            "C": 84.93333333333334,
            "D": 81.66666666666667,
            "E": 82.26666666666667,
            "F": 108.13333333333334,
            "G": 143.06666666666666,
            "H": 150.06666666666666
        },
        "bus2": {
            "F": 96.0625,
            "G": 127.5,
            "H": 135.875,
            "I": 132.75,
            "J": 129.1875,
            "K": 120.8,
            "L": 109.2,
            "A": 98.93333333333334,
            "B": 89.06666666666666,
            "C": 83.06666666666666,
            "D": 79.53333333333333,
            "E": 77.66666666666667
        },
        "bus5": {
            "J": 129.4375,
            "K": 120.4375,
            "L": 111.5,
            "A": 101.5625,
            "B": 92.375,
            "C": 86.46666666666667,
            "D": 84,
            "E": 83.33333333333333,
            "F": 108,
            "G": 142.73333333333332,
            "H": 151.73333333333332,
            "I": 146.13333333333333
        },
        "bus1": {
            "C": 59.75,
            "D": 57.875,
            "E": 55.5625,
            "F": 74.8125,
            "G": 106.1875,
            "H": 109.13333333333334,
            "I": 105.73333333333333,
            "J": 100.2,
            "K": 93.13333333333334,
            "L": 85.2,
            "A": 76.26666666666667,
            "B": 68.53333333333333
        },
        "bus0": {
            "B": 77.75,
            "C": 73,
            "D": 68.375,
            "E": 63.86666666666667,
            "F": 87.2,
            "G": 120.13333333333334,
            "H": 128.73333333333332,
            "I": 128.13333333333333,
            "J": 121.2,
            "K": 112.86666666666666,
            "L": 102.46666666666667,
            "A": 90.66666666666667
        }
    },
    "Obs_and_actions_busses": {
        "0": -183.751943105773,
        "5": 430.9278384027505,
        "3": 268.3091074279157
    },
    "Obs_and_actions_tfls": {
        "0": -353.8538500866452,
        "1": 263.38845662308375,
        "2": 10.256487027492339
    }
}394465
    }
}