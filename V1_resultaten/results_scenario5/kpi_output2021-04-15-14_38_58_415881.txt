{
    "Average_holding_time": 27.367924528301888,
    "Average_holding_actions": [
        576,
        183,
        201,
        0,
        41,
        0
    ],
    "Average_travel_time_passengers": 2825.1388928043443,
    "Average_passenger_waiting_time": 339.99551172455006,
    "Average_passenger_in_vehicle_time": 2485.1433810798358,
    "Average_speed_busses": 0.04416666666666667,
    "Average_experienced_crowding": 113.69565133558231,
    "Average_covariation_headway": 0.2737464128953919,
    "Average_covariation_headway_stops": {
        "H": 0.17697836414195206,
        "F": 0.18687350889997245,
        "E": 0.22599074050163503,
        "D": 0.23164030449867934,
        "A": 0.18533167965274916,
        "I": 0.17477554565587872,
        "G": 0.15343773086674153,
        "B": 0.19292498090936297,
        "J": 0.15763273077101528,
        "C": 0.21666611985641723,
        "K": 0.14460120120775916,
        "L": 0.1588458967400366
    },
    "Average_excess_waiting_time": {
        "H": 67.0480205834022,
        "F": 68.97616924310728,
        "E": 76.231462231803,
        "D": 78.45299591923322,
        "A": 74.33476598798961,
        "I": 68.62817766617212,
        "G": 66.94785927749075,
        "B": 72.59801880520081,
        "J": 68.52412659916843,
        "C": 78.42534406148388,
        "K": 69.41613717589263,
        "L": 72.65295650642997
    },
    "Holding_per_stop": {
        "H": 20.56179775280899,
        "F": 41.79775280898876,
        "E": 42.13483146067416,
        "I": 32.02247191011236,
        "B": 12.272727272727273,
        "G": 25.95505617977528,
        "D": 21.477272727272727,
        "A": 23.181818181818183,
        "J": 34.77272727272727,
        "C": 14.318181818181818,
        "K": 35.79545454545455,
        "L": 23.79310344827586
    },
    "Actions_tfl_edges": {
        "CD": [
            71,
            13,
            5
        ],
        "GH": [
            23,
            66,
            1
        ],
        "HI": [
            46,
            30,
            13
        ],
        "FG": [
            26,
            55,
            8
        ],
        "JK": [
            80,
            6,
            2
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "H": 143.66666666666666,
            "I": 139.8,
            "J": 132.53333333333333,
            "K": 124.26666666666667,
            "L": 113.73333333333333,
            "A": 105,
            "B": 94.8,
            "C": 87.33333333333333,
            "D": 85.73333333333333,
            "E": 84.66666666666667,
            "F": 110,
            "G": 145.85714285714286
        },
        "bus3": {
            "F": 100.8,
            "G": 131.93333333333334,
            "H": 138.46666666666667,
            "I": 133.33333333333334,
            "J": 127.53333333333333,
            "K": 119.06666666666666,
            "L": 110.53333333333333,
            "A": 99.53333333333333,
            "B": 90.6,
            "C": 86,
            "D": 84.71428571428571,
            "E": 83.14285714285714
        },
        "bus2": {
            "E": 76.4,
            "F": 103.46666666666667,
            "G": 135.86666666666667,
            "H": 143.26666666666668,
            "I": 141.26666666666668,
            "J": 133.4,
            "K": 124.6,
            "L": 113.4,
            "A": 103.66666666666667,
            "B": 91.5,
            "C": 85.78571428571429,
            "D": 82.71428571428571
        },
        "bus1": {
            "D": 72.93333333333334,
            "E": 72.26666666666667,
            "F": 93.86666666666666,
            "G": 128.26666666666668,
            "H": 136.13333333333333,
            "I": 134,
            "J": 125.8,
            "K": 116.13333333333334,
            "L": 105.33333333333333,
            "A": 95.35714285714286,
            "B": 86.21428571428571,
            "C": 81.28571428571429
        },
        "bus5": {
            "A": 99.8,
            "B": 90.86666666666666,
            "C": 82.93333333333334,
            "D": 79.2,
            "E": 78.73333333333333,
            "F": 99.86666666666666,
            "G": 134.73333333333332,
            "H": 143.21428571428572,
            "I": 140.14285714285714,
            "J": 133.64285714285714,
            "K": 125,
            "L": 116.64285714285714
        },
        "bus0": {
            "B": 85.46666666666667,
            "C": 81.26666666666667,
            "D": 78.66666666666667,
            "E": 77.06666666666666,
            "F": 100.26666666666667,
            "G": 132.6,
            "H": 139.73333333333332,
            "I": 135.86666666666667,
            "J": 128.06666666666666,
            "K": 121.07142857142857,
            "L": 108.64285714285714,
            "A": 98.92857142857143
        }
    },
    "Obs_and_actions_busses": {
        "0": -251.3260885175947,
        "5": 313.91653466873555,
        "3": 165.8466403031795,
        "2": 67.3387128494012,
        "1": -23.938259148368736
    },
    "Obs_and_actions_tfls": {
        "0": 179.5653215417535,
        "1": -120.3958474594675,
        "2": 30.130255400601108
    }
}