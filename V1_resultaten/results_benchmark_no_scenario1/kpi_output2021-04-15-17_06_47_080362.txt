{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        706,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 6560.457070184303,
    "Average_passenger_waiting_time": 2615.8836176067775,
    "Average_passenger_in_vehicle_time": 3944.5734525774506,
    "Average_speed_busses": 0.029416666666666667,
    "Average_experienced_crowding": 640.5226009463528,
    "Average_covariation_headway": 2.3141111215080503,
    "Average_covariation_headway_stops": {
        "B": 2.1464497165859893,
        "C": 2.161287603421244,
        "D": 2.1700570174976197,
        "F": 2.171903106681587,
        "K": 2.142795143982056,
        "A": 2.140764723230666,
        "E": 2.147918691992677,
        "L": 2.039277944197668,
        "G": 2.170045275214942,
        "H": 2.1529119057273283,
        "I": 2.1405506679340585,
        "J": 2.1302333795436885
    },
    "Average_excess_waiting_time": {
        "B": 2350.342020583023,
        "C": 2363.505403155552,
        "D": 2365.7159702059557,
        "F": 2387.1094941985857,
        "K": 2421.31460613989,
        "A": 2355.9249844209844,
        "E": 2354.701042085363,
        "L": 2411.0831296205643,
        "G": 2409.6092540798704,
        "H": 2403.6485771645507,
        "I": 2403.7873011629854,
        "J": 2411.5144993772815
    },
    "Holding_per_stop": {
        "B": 0,
        "C": 0,
        "D": 0,
        "A": 0,
        "K": 0,
        "F": 0,
        "E": 0,
        "L": 0,
        "G": 0,
        "H": 0,
        "I": 0,
        "J": 0
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 5.1,
            "C": 5.2,
            "D": 5.8,
            "E": 6.3,
            "F": 5.9,
            "G": 6,
            "H": 5.8,
            "I": 6.4,
            "J": 6.5,
            "K": 6,
            "L": 7.111111111111111,
            "A": 6.444444444444445
        },
        "bus1": {
            "C": 5.9,
            "D": 6.5,
            "E": 7.3,
            "F": 6.4,
            "G": 6.5,
            "H": 7,
            "I": 7.2,
            "J": 6.9,
            "K": 6.6,
            "L": 6.666666666666667,
            "A": 6.777777777777778,
            "B": 6.555555555555555
        },
        "bus2": {
            "D": 34.5,
            "E": 35.2,
            "F": 35.9,
            "G": 36.1,
            "H": 37.4,
            "I": 38.5,
            "J": 39.6,
            "K": 40.6,
            "L": 42.2,
            "A": 41,
            "B": 39.22222222222222,
            "C": 38.44444444444444
        },
        "bus3": {
            "F": 618.6,
            "G": 622.9,
            "H": 629.9,
            "I": 635.2,
            "J": 646.9,
            "K": 654.4,
            "L": 660.9,
            "A": 635.8888888888889,
            "B": 643,
            "C": 652.8888888888889,
            "D": 656.5555555555555,
            "E": 674.3333333333334
        },
        "bus4": {
            "K": 19.727272727272727,
            "L": 21.5,
            "A": 20.9,
            "B": 21.3,
            "C": 22.2,
            "D": 23.5,
            "E": 24.1,
            "F": 23.6,
            "G": 23.4,
            "H": 22.2,
            "I": 22.1,
            "J": 21.9
        },
        "bus5": {
            "A": 4.4,
            "B": 4.8,
            "C": 5.3,
            "D": 6,
            "E": 6.6,
            "F": 7,
            "G": 6.8,
            "H": 7.3,
            "I": 7.2,
            "J": 6.1,
            "K": 5.1,
            "L": 5.222222222222222
        }
    },
    "Obs_and_actions_busses": {
        "0": 160.4203675212516
    },
    "Obs_and_actions_tfls": {}
}