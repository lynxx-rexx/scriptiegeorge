{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        544,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 8436.780365111099,
    "Average_passenger_waiting_time": 3135.981808497345,
    "Average_passenger_in_vehicle_time": 5300.798556614236,
    "Average_speed_busses": 0.02266666666666667,
    "Average_experienced_crowding": 1008.1110045377884,
    "Average_covariation_headway": 2.26031700993695,
    "Average_covariation_headway_stops": {
        "I": 2.3132349754528945,
        "K": 2.377518656782801,
        "J": 2.3473459784886965,
        "E": 2.331583823909254,
        "A": 2.3779314161815672,
        "B": 2.393877193063848,
        "L": 2.355112333542823,
        "F": 2.047622842729119,
        "G": 1.8636496168772514,
        "C": 2.3639141469825673,
        "D": 2.3141455644673834,
        "H": 2.1669812397427117
    },
    "Average_excess_waiting_time": {
        "I": 3368.461796518185,
        "K": 3480.6841731899403,
        "J": 3434.253261525294,
        "E": 3453.0873871063395,
        "A": 3507.3385450104015,
        "B": 3532.3879125596295,
        "L": 3472.4007585240497,
        "F": 2837.104622563691,
        "G": 2704.970288562844,
        "C": 3511.4624563872812,
        "D": 3430.91881599696,
        "H": 3033.369685139958
    },
    "Holding_per_stop": {
        "I": 0,
        "K": 0,
        "J": 0,
        "E": 0,
        "A": 0,
        "B": 0,
        "L": 0,
        "F": 0,
        "C": 0,
        "G": 0,
        "D": 0,
        "H": 0
    },
    "Actions_tfl_edges": {
        "HI": [
            43,
            0,
            0
        ],
        "JK": [
            45,
            0,
            0
        ],
        "FG": [
            48,
            0,
            0
        ],
        "CD": [
            47,
            0,
            0
        ],
        "GH": [
            42,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "I": 4.875,
            "J": 4.75,
            "K": 4.5,
            "L": 4.125,
            "A": 4,
            "B": 3.125,
            "C": 2.625,
            "D": 2.125,
            "E": 1.75,
            "F": 2.625,
            "G": 4.714285714285714,
            "H": 4.857142857142857
        },
        "bus4": {
            "K": 43.25,
            "L": 39.75,
            "A": 36.25,
            "B": 33,
            "C": 31,
            "D": 28.25,
            "E": 26.75,
            "F": 36.25,
            "G": 54.57142857142857,
            "H": 54.57142857142857,
            "I": 52.714285714285715,
            "J": 52
        },
        "bus3": {
            "J": 12.25,
            "K": 12.125,
            "L": 10.125,
            "A": 9.25,
            "B": 8.625,
            "C": 8.375,
            "D": 7.25,
            "E": 7.375,
            "F": 10.375,
            "G": 16.714285714285715,
            "H": 15.714285714285714,
            "I": 14.714285714285714
        },
        "bus1": {
            "E": 641.75,
            "F": 853.75,
            "G": 1148.25,
            "H": 1095.2857142857142,
            "I": 1075.4285714285713,
            "J": 1043.7142857142858,
            "K": 989.1428571428571,
            "L": 912,
            "A": 837.1428571428571,
            "B": 771.4285714285714,
            "C": 738.1428571428571,
            "D": 725.8571428571429
        },
        "bus5": {
            "A": 19.375,
            "B": 16.25,
            "C": 13.875,
            "D": 12.125,
            "E": 11.625,
            "F": 20.625,
            "G": 34.42857142857143,
            "H": 33.714285714285715,
            "I": 31,
            "J": 28.857142857142858,
            "K": 26.714285714285715,
            "L": 24.142857142857142
        },
        "bus0": {
            "B": 88.375,
            "C": 78.75,
            "D": 72.875,
            "E": 73.5,
            "F": 122.25,
            "G": 191.71428571428572,
            "H": 202.28571428571428,
            "I": 190.42857142857142,
            "J": 175.85714285714286,
            "K": 157.57142857142858,
            "L": 134.42857142857142,
            "A": 115.85714285714286
        }
    },
    "Obs_and_actions_busses": {
        "0": 290.9915959802552
    },
    "Obs_and_actions_tfls": {
        "0": 227.70111736170753
    }
}