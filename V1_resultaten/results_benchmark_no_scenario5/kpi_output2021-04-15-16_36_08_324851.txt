{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        636,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 7083.912864308488,
    "Average_passenger_waiting_time": 2535.2849197666,
    "Average_passenger_in_vehicle_time": 4548.627944542051,
    "Average_speed_busses": 0.0265,
    "Average_experienced_crowding": 816.6622626993845,
    "Average_covariation_headway": 2.146980011468477,
    "Average_covariation_headway_stops": {
        "G": 1.866081404463634,
        "I": 2.240439017229547,
        "F": 1.9611569201340862,
        "H": 2.0906634777216353,
        "C": 2.1353655740018134,
        "A": 2.2457754955276545,
        "D": 2.1932639889851147,
        "J": 2.2551392432958264,
        "E": 2.182066369596339,
        "B": 2.2265986918645333,
        "K": 2.247917774333891,
        "L": 2.2401548118275936
    },
    "Average_excess_waiting_time": {
        "G": 2009.430313244037,
        "I": 2803.174253171158,
        "F": 2156.6736483870873,
        "H": 2496.5070202089346,
        "C": 2928.018245105288,
        "A": 2901.9296161662596,
        "D": 2564.254752273023,
        "J": 2870.8355773332723,
        "E": 2578.7704798426694,
        "B": 2897.8807790417504,
        "K": 2891.839196588473,
        "L": 2903.6124214502233
    },
    "Holding_per_stop": {
        "G": 0,
        "I": 0,
        "F": 0,
        "H": 0,
        "C": 0,
        "A": 0,
        "J": 0,
        "D": 0,
        "B": 0,
        "E": 0,
        "K": 0,
        "L": 0
    },
    "Actions_tfl_edges": {
        "CD": [
            51,
            0,
            0
        ],
        "FG": [
            53,
            0,
            0
        ],
        "HI": [
            54,
            0,
            0
        ],
        "GH": [
            53,
            0,
            0
        ],
        "JK": [
            54,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 112.22222222222223,
            "H": 119.88888888888889,
            "I": 110.11111111111111,
            "J": 102.55555555555556,
            "K": 92.33333333333333,
            "L": 82.66666666666667,
            "A": 72.22222222222223,
            "B": 60.44444444444444,
            "C": 47.875,
            "D": 43.125,
            "E": 37.625,
            "F": 69.25
        },
        "bus4": {
            "I": 867.8888888888889,
            "J": 836.7777777777778,
            "K": 791.6666666666666,
            "L": 734.7777777777778,
            "A": 674.7777777777778,
            "B": 622.5555555555555,
            "C": 599.1111111111111,
            "D": 519,
            "E": 518.375,
            "F": 686.25,
            "G": 916.875,
            "H": 991.5
        },
        "bus2": {
            "F": 12.88888888888889,
            "G": 24.88888888888889,
            "H": 26.88888888888889,
            "I": 25.22222222222222,
            "J": 23,
            "K": 20.555555555555557,
            "L": 18.88888888888889,
            "A": 16.11111111111111,
            "B": 14,
            "C": 10.875,
            "D": 8.125,
            "E": 7.5
        },
        "bus0": {
            "C": 10.555555555555555,
            "D": 11.555555555555555,
            "E": 11.444444444444445,
            "F": 17.11111111111111,
            "G": 22.666666666666668,
            "H": 22.555555555555557,
            "I": 20.22222222222222,
            "J": 18.333333333333332,
            "K": 16.22222222222222,
            "L": 13.555555555555555,
            "A": 11.777777777777779,
            "B": 10.333333333333334
        },
        "bus5": {
            "A": 17.5,
            "B": 14.2,
            "C": 15.222222222222221,
            "D": 16.77777777777778,
            "E": 17.666666666666668,
            "F": 24.555555555555557,
            "G": 30.88888888888889,
            "H": 33.44444444444444,
            "I": 31.11111111111111,
            "J": 28.555555555555557,
            "K": 26.22222222222222,
            "L": 22.88888888888889
        },
        "bus1": {
            "D": 82,
            "E": 84.55555555555556,
            "F": 108,
            "G": 136.11111111111111,
            "H": 141.66666666666666,
            "I": 137.11111111111111,
            "J": 129.11111111111111,
            "K": 122.22222222222223,
            "L": 110.11111111111111,
            "A": 101.55555555555556,
            "B": 92.11111111111111,
            "C": 96.375
        }
    },
    "Obs_and_actions_busses": {
        "0": 280.28869833694796
    },
    "Obs_and_actions_tfls": {
        "0": -680.4426814433799
    }
}