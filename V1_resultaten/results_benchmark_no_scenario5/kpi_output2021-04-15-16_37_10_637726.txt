{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        556,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 8265.593979289337,
    "Average_passenger_waiting_time": 3057.770527625011,
    "Average_passenger_in_vehicle_time": 5207.82345166509,
    "Average_speed_busses": 0.023166666666666665,
    "Average_experienced_crowding": 1002.3725463092018,
    "Average_covariation_headway": 2.263372939040487,
    "Average_covariation_headway_stops": {
        "G": 1.8918808681755044,
        "I": 2.3204198275273282,
        "L": 2.3570999585582917,
        "F": 2.0422790466425345,
        "A": 2.3655312157026014,
        "H": 2.1796484130681306,
        "C": 2.3418827444416674,
        "J": 2.3491163342701924,
        "B": 2.3406405201366742,
        "D": 2.300269905073462,
        "K": 2.338735270963994,
        "E": 2.2854253179996404
    },
    "Average_excess_waiting_time": {
        "G": 2644.275623729871,
        "I": 3257.0437430264574,
        "L": 3420.207639313022,
        "F": 2766.7979200241925,
        "A": 3411.8337760415507,
        "H": 2932.762290342299,
        "C": 3371.0648520201285,
        "J": 3378.2252515213477,
        "B": 3392.623550519914,
        "D": 3316.7799259538797,
        "K": 3397.242018443929,
        "E": 3336.501128789082
    },
    "Holding_per_stop": {
        "I": 0,
        "G": 0,
        "L": 0,
        "F": 0,
        "A": 0,
        "H": 0,
        "C": 0,
        "J": 0,
        "B": 0,
        "D": 0,
        "K": 0,
        "E": 0
    },
    "Actions_tfl_edges": {
        "FG": [
            50,
            0,
            0
        ],
        "HI": [
            45,
            0,
            0
        ],
        "GH": [
            44,
            0,
            0
        ],
        "CD": [
            48,
            0,
            0
        ],
        "JK": [
            45,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "G": 20.125,
            "H": 21,
            "I": 20.375,
            "J": 19.25,
            "K": 17.625,
            "L": 17,
            "A": 15.75,
            "B": 14.75,
            "C": 12.875,
            "D": 11.5,
            "E": 11.375,
            "F": 15.25
        },
        "bus3": {
            "I": 56.875,
            "J": 54.375,
            "K": 51.5,
            "L": 46.5,
            "A": 44.75,
            "B": 42.25,
            "C": 40.375,
            "D": 41.375,
            "E": 40,
            "F": 48.375,
            "G": 68.28571428571429,
            "H": 68.85714285714286
        },
        "bus4": {
            "L": 23.375,
            "A": 21.375,
            "B": 19.625,
            "C": 18,
            "D": 17.25,
            "E": 15,
            "F": 21.625,
            "G": 37.714285714285715,
            "H": 39.57142857142857,
            "I": 35.857142857142854,
            "J": 34.285714285714285,
            "K": 30.428571428571427
        },
        "bus1": {
            "F": 7.777777777777778,
            "G": 12.25,
            "H": 11.5,
            "I": 10.5,
            "J": 9.625,
            "K": 8.875,
            "L": 8.125,
            "A": 7.75,
            "B": 6.625,
            "C": 6.375,
            "D": 6,
            "E": 6.375
        },
        "bus5": {
            "A": 75,
            "B": 63.75,
            "C": 55.875,
            "D": 49.625,
            "E": 45,
            "F": 82.625,
            "G": 132.28571428571428,
            "H": 137.42857142857142,
            "I": 132.42857142857142,
            "J": 122,
            "K": 110.42857142857143,
            "L": 99.42857142857143
        },
        "bus0": {
            "C": 649.25,
            "D": 643.375,
            "E": 656.125,
            "F": 877.375,
            "G": 1166.25,
            "H": 1130.142857142857,
            "I": 1120.2857142857142,
            "J": 1070.142857142857,
            "K": 1004.5714285714286,
            "L": 933.5714285714286,
            "A": 858.5714285714286,
            "B": 781.8571428571429
        }
    },
    "Obs_and_actions_busses": {
        "0": 382.9651196000316
    },
    "Obs_and_actions_tfls": {
        "0": -435.9992481532449
    }
}