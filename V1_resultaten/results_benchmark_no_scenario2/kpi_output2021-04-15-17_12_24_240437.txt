{
    "Average_holding_time": 0,
    "Average_holding_actions": [
        710,
        0,
        0,
        0,
        0,
        0
    ],
    "Average_travel_time_passengers": 6516.640290548007,
    "Average_passenger_waiting_time": 2560.273111330297,
    "Average_passenger_in_vehicle_time": 3956.3671792173773,
    "Average_speed_busses": 0.029583333333333333,
    "Average_experienced_crowding": 630.9666257469008,
    "Average_covariation_headway": 2.243507690889787,
    "Average_covariation_headway_stops": {
        "J": 2.16601419606597,
        "L": 2.164775056131819,
        "I": 2.1561633411699033,
        "B": 2.1557134294939626,
        "F": 2.1460892612986955,
        "G": 2.160327542941812,
        "K": 2.1555616731615417,
        "A": 2.155647022078723,
        "C": 2.149912449649587,
        "H": 2.0585126595407384,
        "D": 2.1346359897887415,
        "E": 2.127720932245353
    },
    "Average_excess_waiting_time": {
        "J": 2340.225105743889,
        "L": 2359.257081136033,
        "I": 2335.1465209071534,
        "B": 2354.0914191959278,
        "F": 2405.2394228460807,
        "G": 2416.755774067553,
        "K": 2353.1303103778605,
        "A": 2368.23953357831,
        "C": 2369.1432291928522,
        "H": 2434.6619935956855,
        "D": 2368.0536292187226,
        "E": 2381.3605588983464
    },
    "Holding_per_stop": {
        "J": 0,
        "I": 0,
        "L": 0,
        "B": 0,
        "F": 0,
        "G": 0,
        "K": 0,
        "A": 0,
        "C": 0,
        "H": 0,
        "D": 0,
        "E": 0
    },
    "Actions_tfl_edges": {
        "AB": [
            60,
            0,
            0
        ],
        "EF": [
            61,
            0,
            0
        ],
        "FG": [
            62,
            0,
            0
        ],
        "HI": [
            57,
            0,
            0
        ],
        "IJ": [
            58,
            0,
            0
        ],
        "KL": [
            59,
            0,
            0
        ],
        "JK": [
            58,
            0,
            0
        ],
        "LA": [
            59,
            0,
            0
        ],
        "BC": [
            60,
            0,
            0
        ],
        "GH": [
            62,
            0,
            0
        ],
        "CD": [
            60,
            0,
            0
        ],
        "DE": [
            60,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "J": 11.5,
            "K": 12,
            "L": 13.5,
            "A": 13.3,
            "B": 13.2,
            "C": 13.6,
            "D": 13,
            "E": 11.9,
            "F": 12.1,
            "G": 11.6,
            "H": 13.222222222222221,
            "I": 12.666666666666666
        },
        "bus5": {
            "L": 36.6,
            "A": 35.7,
            "B": 36.9,
            "C": 38,
            "D": 38.9,
            "E": 40.9,
            "F": 40.4,
            "G": 41.2,
            "H": 40.77777777777778,
            "I": 39.333333333333336,
            "J": 38.44444444444444,
            "K": 39.44444444444444
        },
        "bus3": {
            "I": 6.6,
            "J": 6.5,
            "K": 6.4,
            "L": 6.7,
            "A": 7,
            "B": 6.9,
            "C": 7,
            "D": 8.2,
            "E": 7.6,
            "F": 7.5,
            "G": 6.9,
            "H": 7.555555555555555
        },
        "bus0": {
            "B": 589.9,
            "C": 595.5,
            "D": 609.5,
            "E": 619.3,
            "F": 624.2,
            "G": 626.4,
            "H": 636.2,
            "I": 600.3333333333334,
            "J": 611.6666666666666,
            "K": 620.6666666666666,
            "L": 627.7777777777778,
            "A": 642
        },
        "bus1": {
            "F": 5.545454545454546,
            "G": 5.818181818181818,
            "H": 6.7,
            "I": 6.6,
            "J": 6.6,
            "K": 6.6,
            "L": 6.3,
            "A": 5.9,
            "B": 5.9,
            "C": 6.4,
            "D": 5.5,
            "E": 5.2
        },
        "bus2": {
            "G": 37.45454545454545,
            "H": 40.2,
            "I": 39.7,
            "J": 37.6,
            "K": 38.7,
            "L": 39.3,
            "A": 40,
            "B": 41.8,
            "C": 40.9,
            "D": 39.9,
            "E": 39.1,
            "F": 39.3
        }
    },
    "Obs_and_actions_busses": {
        "0": 251.45481427120166
    },
    "Obs_and_actions_tfls": {
        "0": -602.0130513700345
    }
}