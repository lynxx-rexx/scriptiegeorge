#from sklearn import tree
import ray
from ray import rllib
from ray import tune
from ray.rllib.agents.registry import get_agent_class
from ray.rllib.models import ModelCatalog
from ray.tune import run_experiments
from ray.tune.registry import register_env
from ray.rllib.agents.ppo import PPOTrainer




# Import environment definition
# from environment import IrrigationEnv
from Network_implementation_V2 import NetworkEnv


true_multi_agent = False

node_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                     ]


kwargs_dict = {
            'edges': adjacency_matrix,
            'node_names': node_list,
            'num_agents': 3,
            'routes': [node_list, node_list, node_list],
            'resetted': False,
            'qmix': False,
            'holding_fixed': False,
            'incl_pass_stops': False

        }






# Driver code for training
def setup_and_train():

    # Create a single environment and register it
    def env_creator(_):
        return NetworkEnv( adjacency_matrix, node_list, 3 , holding_fixed=True, incl_pass_stops=False )
    single_env = NetworkEnv( adjacency_matrix, node_list, 3 , holding_fixed=True, incl_pass_stops=False )
    env_name = "NetworkEnv"
    register_env(env_name, env_creator)

    # Get environment obs, action spaces and number of agents
    obs_space = single_env.observation_space
    act_space = single_env.action_space
    num_agents = single_env.num_agents

    if true_multi_agent:
        # Create a policy mapping
        def gen_policy():
            return (None, obs_space, act_space, {})

        policy_graphs = {}
        for i in range(num_agents):
            policy_graphs['agent-' + str(i)] = gen_policy()

        def policy_mapping_fn(agent_id):
            return 'agent-' + str(agent_id)

    else:
        policy_graphs = { 'shared_policy': (None, obs_space, act_space, {}) }

        def policy_mapping_fn( agent_id ):
            return 'shared_policy'



    # Define configuration with hyperparam and training details
    config={
                "log_level": "WARN",
                "num_workers": 3,
                "num_cpus_for_driver": 1,
                "num_cpus_per_worker": 1,
                # "num_gpus_per_worker": 0.3,
                # "resources_per_trial": {"cpu": 1, "gpu": 0},
                # "num_sgd_iter": 10,
                # "train_batch_size": tune.grid_search([128, 256, 512]),
                "train_batch_size": 16,
                # "lr": 5e-3,

                # Should use a critic as a baseline (otherwise don't use value baseline;
                # required for using GAE).
                "use_critic": True,
                # If true, use the Generalized Advantage Estimator (GAE)
                # with a value function, see https://arxiv.org/pdf/1506.02438.pdf.
                "use_gae": True,
                # Size of rollout batch
                "rollout_fragment_length": 10,
                # GAE(gamma) parameter
                "lambda": 0.9,
                # Max global norm for each gradient calculated by worker
                "grad_clip": 40.0,
                # Learning rate
                "lr": 0.0005,
                # "lr": tune.grid_search([ 0.0001, 0.0005] ),
                # Learning rate schedule
                "lr_schedule": None,
                # Value Function Loss coefficient
                "vf_loss_coeff": 0.5,
                # Entropy coefficient
                "entropy_coeff": 0.01,
                # Min time per iteration
                "min_iter_time_s": 120,
                # Workers sample async. Note that this increases the effective
                # rollout_fragment_length by up to 5x due to async buffering of batches.
                "sample_async": True,


                #"lr": tune.uniform(0, 0.15),
                # "lr": tune.grid_search([0.01, 0.001, 0.0001]),
                "model":{"fcnet_hiddens": [16, 16]},
                "multiagent": {
                    "policies": policy_graphs,
                    "policy_mapping_fn": policy_mapping_fn,
                },
                "env": "NetworkEnv"}

    # Define experiment details
    exp_name = 'my_exp'
    exp_dict = {
            'name': exp_name,
            'run_or_experiment': 'A2C',
            "stop": {
                # training_iteration geeft aan hoe vaak hij de training herhaalt
                "training_iteration": 4
            },
            'checkpoint_freq': 2,
            # "restore": 'C:\\Users\\georg\\ray_results\\my_exp\\A2C_NetworkEnv_ffb99_00000_0_2020-12-07_17-33-55\\checkpoint_10\\checkpoint-10',
            "config": config,
            # num_samples geeft aan hoe vaak je elke gridsearch punt wil bezoeken
            # "num_samples": 1,
            #"local_dir": "~/tune_results",
        }

    # Initialize ray and run
    ray.init()
    results = tune.run(**exp_dict)
    print('##### Best config #####')
    print( results.get_best_config(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )
    print('##### Best trial #####')
    print( results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )

    # print( results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )
    # print(type(results.results), results.results )
    # for key, value in results.results.items():
    #     print('###  ', key, '  ###')
    #     for key2, val2 in value.items():
    #         print(key2)
    #
    # avg_reward_list = []
    #
    # for key, val in results.results.items():
    #     avg_reward_list.append(val['episode_reward_mean'])
    #
    # print(avg_reward_list)

    #print(type(results.stats()), results.stats())

    #print(type(results.runner_data()), results.runner_data())

if __name__=='__main__':
    setup_and_train()
