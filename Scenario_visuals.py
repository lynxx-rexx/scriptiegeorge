import matplotlib.pyplot as plt
import numpy as np
import ast
import json
from _collections import defaultdict
import statistics

plt.rcParams.update({'font.size': 18})



position_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',]

position_list_obs = []

for i in position_list:
    position_list_obs.append("A" + i)
    position_list_obs.append("P" + i)
    position_list_obs.append("D" + i)




output_file = 'percentiles_scenario_5.json'
file_str = "results_scenario5\\average_kpis.json"
tfl = True

output_dict = {}

with open(file_str) as reading_file:
    result_dict = json.load(reading_file)

def plot_percentiles(data_list, title):
    p5 = np.percentile(data_list, 5)
    p15 = np.percentile(data_list, 15)
    p50 = np.percentile(data_list, 50)
    p85 = np.percentile(data_list, 85)
    p95 = np.percentile(data_list, 95)
    print([p5, p50])
    plt.bar(['p5','p15','p50','p85','p95',], [p5, p15, p50, p85, p95])
    plt.title(title)
    # plt.show()

    output_dict[title] = [p5, p15, p50, p85, p95]

    plt.hist(data_list, bins=100, density=True, cumulative=True, histtype='step')
    plt.xscale('log')
    plt.show()



print('Average holding time: ', result_dict['Average_holding_time'][0])

print('Average travel time passengers: :', result_dict['Average_travel_time_passengers'][0])

print('Average experienced crowding: :', result_dict['Average_experienced_crowding'][0])


metric_list = ['Average_holding_time', 'Average_travel_time_passengers', 'Average_experienced_crowding',
               'Average_passenger_waiting_time', 'Average_passenger_in_vehicle_time']

for item in metric_list:
    plot_percentiles(result_dict[item][1], item)



av_cov_list = []

for dictionary in result_dict['Average_covariation_headway_stops']:
    temp_list = []
    for key, value in dictionary.items():
        temp_list.append(value)

    av_cov_list.append(statistics.mean(temp_list))

print('Average coefficient of variation headway at stops: ', statistics.mean(av_cov_list))
plot_percentiles(av_cov_list, 'av cov stops')




av_excess_list = []

for dictionary in result_dict['Average_excess_waiting_time']:
    temp_list = []
    for key, value in dictionary.items():
        temp_list.append(value)
    av_excess_list.append(statistics.mean(temp_list))

print('Average excess waiting time: ', statistics.mean(av_excess_list))
plot_percentiles(av_excess_list, 'av excess stops')






av_actions_busses = [0,0,0,0,0,0,0,]

for count_list in result_dict['Average_holding_actions']:
    for index, i in enumerate(count_list):
        av_actions_busses[index] += i

weighted_actions_busses = [i/sum(av_actions_busses) for i in av_actions_busses]

plt.bar(['0', '30', '60', '90', '120', '150', '180'], weighted_actions_busses)
plt.ylabel('Fraction')
plt.title('Actions chosen by the busses')
plt.show()

output_dict['actions_busses'] = weighted_actions_busses





if tfl:
    av_actions_list = [0,0,0]

    for dictionary in result_dict['Actions_tfl_edges']:
        for key, value in dictionary.items():
            av_actions_list[0] += value[0]
            av_actions_list[1] += value[1]
            av_actions_list[2] += value[2]

    weighted_actions_list = [i/sum(av_actions_list) for i in av_actions_list]
    print('Average actions chosen by traffic lights: ', weighted_actions_list)


    plt.bar(['Nothing', '-10 s delay', '+10 s delay'], weighted_actions_list)
    plt.ylabel('Fraction')
    plt.title('Actions chosen by the traffic lights')
    plt.show()

    output_dict['actions_tfls'] = weighted_actions_list






obs_and_actions_busses = defaultdict(lambda : [])

for mini_dict in result_dict['Obs_and_actions_busses']:
    for action, av_obs in mini_dict.items():
        obs_and_actions_busses[action].append(av_obs)

print(obs_and_actions_busses)
xs = []
ys = []
actions = [0, 1, 2, 3, 4, 5, 6]

for action in actions:
    xs += [action]*len(obs_and_actions_busses[str(action)])
    ys += obs_and_actions_busses[str(action)]

plt.scatter(xs, ys)
plt.xlabel('Action chosen')
plt.ylabel('Observation received')
plt.title('Policy busses')
plt.show()

p5_obs_list = []
p15_obs_list = []
p50_obs_list = []
p85_obs_list = []
p95_obs_list = []

actions_present = []


for action in actions:
    try:
        p5_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 5) )
        p15_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 15) )
        p50_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 50) )
        p85_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 85) )
        p95_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 95) )
        actions_present.append(action)
    except:
        pass


plt.plot(actions_present, p50_obs_list)
# plt.fill_between(actions, p5_obs_list, p95_obs_list, alpha=0.08, facecolor = 'b')
plt.fill_between(actions_present, p15_obs_list, p85_obs_list, alpha=0.2, facecolor = 'b')
plt.xlabel('Action chosen')
plt.ylabel('Observation received')
plt.title('Policy busses')
plt.show()


output_dict['actions_present'] = actions_present
output_dict['policy_busses15'] = p15_obs_list
output_dict['policy_busses50'] = p50_obs_list
output_dict['policy_busses85'] = p85_obs_list









####################### OVERVIEW DATA ###################################################


travel_time_list = result_dict['Average_travel_time_passengers'][1]

percentiles = ['5', '15', '50', 85, '95']

p5_value = np.percentile(travel_time_list, 5)
p15_value = np.percentile(travel_time_list, 15)
p50_value = np.percentile(travel_time_list, 50)
p85_value = np.percentile(travel_time_list, 85)
p95_value = np.percentile(travel_time_list, 95)



index_list_p5 = []
index_list_p15 = []
index_list_p50 = []
index_list_p85 = []
index_list_p95 = []

for i in travel_time_list:
    if i <= p5_value:
        index_list_p5.append(travel_time_list.index(i))
    elif i <= p15_value:
        index_list_p15.append(travel_time_list.index(i))
    elif i <= p50_value:
        index_list_p50.append(travel_time_list.index(i))
    elif i <= p85_value:
        index_list_p85.append(travel_time_list.index(i))
    elif i <= p95_value:
        index_list_p95.append(travel_time_list.index(i))

print(index_list_p5)
plt.bar( np.arange(len(index_list_p5)), index_list_p5)
plt.show()


overview_holding_data = [
    [result_dict['Average_holding_time'][1][i] for i in index_list_p5],
    [result_dict['Average_holding_time'][1][i] for i in index_list_p15],
    [result_dict['Average_holding_time'][1][i] for i in index_list_p50],
    [result_dict['Average_holding_time'][1][i] for i in index_list_p85],
    [result_dict['Average_holding_time'][1][i] for i in index_list_p95],
]

overview_waiting_time_data = [
    [result_dict['Average_passenger_waiting_time'][1][i] for i in index_list_p5],
    [result_dict['Average_passenger_waiting_time'][1][i] for i in index_list_p15],
    [result_dict['Average_passenger_waiting_time'][1][i] for i in index_list_p50],
    [result_dict['Average_passenger_waiting_time'][1][i] for i in index_list_p85],
    [result_dict['Average_passenger_waiting_time'][1][i] for i in index_list_p95],
]

overview_in_vehicle_time_data = [
    [result_dict['Average_passenger_in_vehicle_time'][1][i] for i in index_list_p5],
    [result_dict['Average_passenger_in_vehicle_time'][1][i] for i in index_list_p15],
    [result_dict['Average_passenger_in_vehicle_time'][1][i] for i in index_list_p50],
    [result_dict['Average_passenger_in_vehicle_time'][1][i] for i in index_list_p85],
    [result_dict['Average_passenger_in_vehicle_time'][1][i] for i in index_list_p95],
]

overview_exp_data = [
    [result_dict['Average_experienced_crowding'][1][i] for i in index_list_p5],
    [result_dict['Average_experienced_crowding'][1][i] for i in index_list_p15],
    [result_dict['Average_experienced_crowding'][1][i] for i in index_list_p50],
    [result_dict['Average_experienced_crowding'][1][i] for i in index_list_p85],
    [result_dict['Average_experienced_crowding'][1][i] for i in index_list_p95],
]


overview_cov_data, overview_excess_data = [ [],[],[],[],[] ], [ [],[],[],[],[] ]

for i in index_list_p5:
    mean_cov = np.mean([value for key, value in result_dict['Average_covariation_headway_stops'][i].items()])
    mean_wait = np.mean([value for key, value in result_dict['Average_excess_waiting_time'][i].items()])
    overview_cov_data[0].append( mean_cov )
    overview_excess_data[0].append(mean_wait)

for i in index_list_p15:
    mean_cov = np.mean([value for key, value in result_dict['Average_covariation_headway_stops'][i].items()])
    mean_wait = np.mean([value for key, value in result_dict['Average_excess_waiting_time'][i].items()])
    overview_cov_data[1].append( mean_cov )
    overview_excess_data[1].append(mean_wait)

for i in index_list_p50:
    mean_cov = np.mean([value for key, value in result_dict['Average_covariation_headway_stops'][i].items()])
    mean_wait = np.mean([value for key, value in result_dict['Average_excess_waiting_time'][i].items()])
    overview_cov_data[2].append( mean_cov )
    overview_excess_data[2].append(mean_wait)

for i in index_list_p85:
    mean_cov = np.mean([value for key, value in result_dict['Average_covariation_headway_stops'][i].items()])
    mean_wait = np.mean([value for key, value in result_dict['Average_excess_waiting_time'][i].items()])
    overview_cov_data[3].append( mean_cov )
    overview_excess_data[3].append(mean_wait)

for i in index_list_p95:
    mean_cov = np.mean([value for key, value in result_dict['Average_covariation_headway_stops'][i].items()])
    mean_wait = np.mean([value for key, value in result_dict['Average_excess_waiting_time'][i].items()])
    overview_cov_data[4].append( mean_cov )
    overview_excess_data[4].append(mean_wait)


# een dict met voor elk vd 4 metrics een lijst
# elk van deze lijsten bestaat uit 5 lijsten met de p5, p15, p50, p85 en p95 waarden (op basis van travel time) van die metric
output_dict['overview_data_complete'] = {'holding': overview_holding_data,
                                         'waiting_time': overview_waiting_time_data,
                                         'in_vehicle_time': overview_in_vehicle_time_data,
                                         'experienced_crowding': overview_exp_data,
                                         'cov_of_var': overview_cov_data,
                                         'excess_waiting_time': overview_excess_data,
                                         }

########



travel_time_p5_list = [result_dict['Average_travel_time_passengers'][1][i] for i in index_list_p5]
exp_p5_list = [result_dict['Average_experienced_crowding'][1][i] for i in index_list_p5]

travel_time_p5_mean = np.mean( travel_time_p5_list )
exp_crowding_p5_mean = np.mean( exp_p5_list )

cov_headway_p5_list = []
excess_waiting_p5_list = []


for i in index_list_p5:
    mean_run_cov = np.mean([value for key, value in result_dict['Average_covariation_headway_stops'][i].items()])
    mean_run_wait = np.mean([value for key, value in result_dict['Average_excess_waiting_time'][i].items()])
    cov_headway_p5_list.append( mean_run_cov )
    excess_waiting_p5_list.append(mean_run_wait)



cov_headway_p5_mean = np.mean( cov_headway_p5_list )
excess_waiting_p5_mean = np.mean( excess_waiting_p5_list )


output_dict['overview_data'] = [travel_time_p5_mean, exp_crowding_p5_mean, cov_headway_p5_mean, excess_waiting_p5_mean]




########









holding_actions_p5 = [result_dict['Average_holding_actions'][i] for i in index_list_p5]
policies_p5 = [result_dict['Obs_and_actions_busses'][i] for i in index_list_p5 ]

if tfl:
    tfl_actions_p5 = [ result_dict['Actions_tfl_edges'][i] for i in index_list_p5]






av_actions_busses = [0,0,0,0,0,0,0,]

for count_list in holding_actions_p5:
    for index, i in enumerate(count_list):
        av_actions_busses[index] += i

weighted_actions_busses = [i/sum(av_actions_busses) for i in av_actions_busses]

output_dict['actions_busses_p5'] = weighted_actions_busses





if tfl:
    av_actions_list = [0,0,0]

    for dictionary in tfl_actions_p5:
        for key, value in dictionary.items():
            av_actions_list[0] += value[0]
            av_actions_list[1] += value[1]
            av_actions_list[2] += value[2]

    weighted_actions_list = [i/sum(av_actions_list) for i in av_actions_list]

    output_dict['actions_tfls_p5'] = weighted_actions_list






obs_and_actions_busses = defaultdict(lambda : [])

for mini_dict in policies_p5:
    for action, av_obs in mini_dict.items():
        obs_and_actions_busses[action].append(av_obs)

xs = []
ys = []
actions = [0, 1, 2, 3, 4, 5, 6]

for action in actions:
    xs += [action]*len(obs_and_actions_busses[str(action)])
    ys += obs_and_actions_busses[str(action)]


p5_obs_list = []
p15_obs_list = []
p50_obs_list = []
p85_obs_list = []
p95_obs_list = []

actions_present = []


for action in actions:
    try:
        p5_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 5) )
        p15_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 15) )
        p50_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 50) )
        p85_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 85) )
        p95_obs_list.append( np.percentile(obs_and_actions_busses[str(action)], 95) )
        actions_present.append(action)
    except:
        pass


output_dict['actions_present_p5'] = actions_present
output_dict['policy_busses15_p5'] = p15_obs_list
output_dict['policy_busses50_p5'] = p50_obs_list
output_dict['policy_busses85_p5'] = p85_obs_list


















with open(output_file, 'w') as writing_file:
    json.dump(output_dict, writing_file, indent=4)
































# 1

cov_stops_list = result_dict["Average_covariation_headway_stops"]

cov_stops_mean_dict = defaultdict(lambda : [])

for number, stop in enumerate(position_list):
    for run in cov_stops_list:
        cov_stops_mean_dict[stop].append(run[stop])

for key, value in cov_stops_mean_dict.items():
    cov_stops_mean_dict[key] = statistics.mean(value)


cov_stops_mean_list = [value for key, value in cov_stops_mean_dict.items()]
plt.plot(cov_stops_mean_list, color="#70c1b3")
plt.title('Average coefficient of variation for every stop')
plt.xlabel('Stop', fontsize=18)
plt.ylabel('Coefficient of variation', fontsize=18)
plt.show()





# 2

excess_waiting_stops_list = result_dict["Average_excess_waiting_time"]

excess_waiting_stops_mean_dict = defaultdict(lambda : [])

for number, stop in enumerate(position_list):
    for run in excess_waiting_stops_list:
        excess_waiting_stops_mean_dict[stop].append(run[stop])

for key, value in excess_waiting_stops_mean_dict.items():
    excess_waiting_stops_mean_dict[key] = statistics.mean(value)


excess_waiting_stops_mean_list = [value for key, value in excess_waiting_stops_mean_dict.items()]
plt.plot(excess_waiting_stops_mean_list, color="#70c1b3")
plt.title('Average excess waiting time for every stop')
plt.xlabel('Stop', fontsize=18)
plt.ylabel('Excess waiting time (s)', fontsize=18)
plt.show()



# 3

holding_stops_list = result_dict["Holding_per_stop"]

holding_stops_mean_dict = defaultdict(lambda : [])

for number, stop in enumerate(position_list):
    for run in holding_stops_list:
        holding_stops_mean_dict[stop].append(run[stop])

for key, value in holding_stops_mean_dict.items():
    holding_stops_mean_dict[key] = statistics.mean(value)


holding_stops_mean_list = [value for key, value in holding_stops_mean_dict.items()]
plt.plot(holding_stops_mean_list, color="#70c1b3")
plt.title('Average holding time for every stop')
plt.xlabel('Stop', fontsize=18)
plt.ylabel('Holding time (s)', fontsize=18)
plt.show()


# 4

load_stops_list = result_dict["Load_per_bus_per_stop"]

load_stops_list2 = []

for run in load_stops_list:
    for bus, loads in run.items():
        load_stops_list2.append(loads)


load_stops_mean_dict = defaultdict(lambda : [])

for number, stop in enumerate(position_list):
    for run in load_stops_list2:
        load_stops_mean_dict[stop].append(run[stop])

for key, value in load_stops_mean_dict.items():
    load_stops_mean_dict[key] = statistics.mean(value)


load_stops_mean_list = [value for key, value in load_stops_mean_dict.items()]
plt.plot(load_stops_mean_list, color="#70c1b3")
plt.title('Average load at every stop')
plt.xlabel('Stop', fontsize=18)
plt.ylabel('Holding time (s)', fontsize=18)
plt.show()













