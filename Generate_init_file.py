import pickle
import gym
import random
import numpy as np

# OBS SPACES USED
# self.observation_space = gym.spaces.Box(low=np.array([0, 0]),high=np.array([len(self.position_list_obs),
#                                          len(self.position_list_obs)]),shape=(2,),
# self.observation_space = gym.spaces.Box( low=np.array([ - len( self.position_list_obs ) ]), \
#                                          high=np.array([ len( self.position_list_obs )]), shape=(1,) )
# self.observation_space = gym.spaces.Box( low=np.array([0, 0, -1]), \
#                                          high=np.array( [ len( self.position_list_obs ), len( self.position_list_obs ), 1 ] ), shape=(3,) )
# self.observation_space = gym.spaces.Box( low=np.array([ - len(self.position_list_obs) ]), \
#                                          high=np.array([ len( self.position_list_obs )]), shape=(1,) )
# self.observation_space = gym.spaces.Box(low=np.array([- len(self.position_list_obs), -1]), \
#                                         high=np.array([len(self.position_list_obs), 1]), shape=(2,))


# ACT SPACES USED
# if holding_fixed:
#     self.action_space = gym.spaces.Discrete(2)
# if not holding_fixed:
#     self.action_space = gym.spaces.Box(low=0, high=240, shape=(1,))

# SIX STEP ACTIONS
# self.action_space = gym.spaces.Discrete(6)


# QMIX
# if qmix:
#     self.observation_space = gym.spaces.Tuple(
#         [
#             gym.spaces.Box(
#                 low=np.array([0, 0]), high=np.array([15, 15]), shape=(2,)
#             )
#         ]
#     )
#
#     if holding_fixed:
#         self.action_space = gym.spaces.Tuple([gym.spaces.Discrete(2)])
#
#     if not holding_fixed:
#         self.action_space = gym.spaces.Discrete(240)


node_list = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"]

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]


# init_dict = {
#     "test_run": [True, False],
#     "monitoring": [True, False],
#     "run_logging": [True, False],
#     "passenger_logging": [True, False],
#     "kpi_logging": [True, False],
#     "result_folder": "\\results\\",
#     "observation_space": gym.spaces.Box( low=np.array([0, 0]), high=np.array([15, 15]), shape=(2,),),
#     "action_space": gym.spaces.Box( low=0, high=240, shape=(1,) ),
#     "end_trigger_range": [int, int],
#     "final_reward_option": ['headway', 'travel_time', 'off', 'travel_time_only'],
#     "resetted": [True, False],
#     "qmix": [True, False],
#     "holding_fixed": [True, False],
#     "ta": [True, False],
#     "observation_option": ['headway', 'passengers', 'headway_passengers'],
#     "reward_function_option": ['headway', 'headway_diff', 'passengers', 'headway_passengers', 'fix_headway'],
#     "holding_punish_param": range from 0 to 1/240, e.g. 0.00125 --> 0.3/240
#     "num_agents": int ,
#     "routes": [],
#     "capacity_busses" [int]*int,
#     "node_names" ['A', 'B', ...],
#     "slot_capacity_nodes" [int]*int,
#     "traffic_light_on_edges" [True/False]*int,
#     "cycle_props" [None, None, (90, 65), None],
#     "arrival_function_nodes" ['deterministic', 'poisson'],
#     "destination_distribution_nodes" ["random"]*15 OR [ [], [], [], ...], probabilities for every node for every node, so 15x15
#     "pass_arrival_rate_nodes" [0.0083]*15,
#     "dwell_time_function_nodes" [('sum', 'deterministic'), ('max_board_or_alight', 'stochastic')],
#     "edges" [],
#     "travel_time_edges" {'AB': ('gamma', mean, std, min, max), 'BC': 120 ... }, the mean, std and min are from the normal distribution,
#                                                                  they are later converted to a gamma distribution
# }

# SIMPELE TEST VERSIE
init_dict = {
    "test_run": True,
    "run_logging": True,
    "passenger_logging": True,
    "observation_space": gym.spaces.Box(
        low=np.array([0, 0, -1]), high=np.array([15, 15, 1]), shape=(3,),
    ),
    "action_space": gym.spaces.Box(low=0, high=240, shape=(1,)),
    "end_trigger_range": [500, 4000],
    "final_reward_option": "headway",
    "qmix": False,
    "holding_fixed": False,
    "ta": True,
    "observation_option": "headway",
    "reward_function_option": "headway",
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "arrival_function_nodes": [None] * 15,
    "destination_distribution_nodes": [None] * 15,
    "dwell_time_function": ["sum"],
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": 150,  # ('gamma', 150, 15, 80, 220)
        "BC": 150,
        "CD": 150,
        "DE": 150,
        "EF": 150,
        "FG": 150,
        "GH": 150,
        "HI": 150,
        "IJ": 150,
        "JK": 150,
        "KL": 150,
        "LM": 150,
        "MN": 150,
        "NO": 150,
        "OA": 150,
    },
}


# SIMPELE STOCH TEST VERSIE
init_dict = {
    "test_run": True,
    "run_logging": True,
    "passenger_logging": True,
    "observation_space": gym.spaces.Box(
        low=np.array([0, 0, -1]), high=np.array([45, 45, 1]), shape=(3,),
    ),
    "action_space": gym.spaces.Box(low=0, high=240, shape=(1,)),
    "end_trigger_range": [500, 4000],
    "final_reward_option": "headway",
    "qmix": False,
    "holding_fixed": False,
    "ta": True,
    "observation_option": "headway",
    "reward_function_option": "headway",
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "arrival_function_nodes": ["poisson"] * 15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0.1, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0.1, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
    ],
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("gamma", 150, 15, 80, 220),
        "BC": ("gamma", 150, 15, 80, 220),
        "CD": ("gamma", 150, 15, 80, 220),
        "DE": ("gamma", 150, 15, 80, 220),
        "EF": ("gamma", 150, 15, 80, 220),
        "FG": ("gamma", 150, 15, 80, 220),
        "GH": ("gamma", 150, 15, 80, 220),
        "HI": ("gamma", 150, 15, 80, 220),
        "IJ": ("gamma", 150, 15, 80, 220),
        "JK": ("gamma", 150, 15, 80, 220),
        "KL": ("gamma", 150, 15, 80, 220),
        "LM": ("gamma", 150, 15, 80, 220),
        "MN": ("gamma", 150, 15, 80, 220),
        "NO": ("gamma", 150, 15, 80, 220),
        "OA": ("gamma", 150, 15, 80, 220),
    },
}


# Eerste stoch scenario
init_dict = {
    "test_run": False,
    "run_logging": True,
    "passenger_logging": False,
    "observation_space": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "action_space": gym.spaces.Box(low=0, high=240, shape=(1,)),
    "end_trigger_range": [500, 4000],
    "final_reward_option": "off",
    "qmix": False,
    "holding_fixed": False,
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "arrival_function_nodes": ["poisson"] * 15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0],
        [0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
        [0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
    ],
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("gamma", 150, 15, 80, 220),
        "BC": ("gamma", 150, 15, 80, 220),
        "CD": ("gamma", 150, 15, 80, 220),
        "DE": ("gamma", 150, 15, 80, 220),
        "EF": ("gamma", 150, 15, 80, 220),
        "FG": ("gamma", 150, 15, 80, 220),
        "GH": ("gamma", 150, 15, 80, 220),
        "HI": ("gamma", 150, 15, 80, 220),
        "IJ": ("gamma", 150, 15, 80, 220),
        "JK": ("gamma", 150, 15, 80, 220),
        "KL": ("gamma", 150, 15, 80, 220),
        "LM": ("gamma", 150, 15, 80, 220),
        "MN": ("gamma", 150, 15, 80, 220),
        "NO": ("gamma", 150, 15, 80, 220),
        "OA": ("gamma", 150, 15, 80, 220),
    },
}


pickle.dump(init_dict, open("pickle_file.p", "wb"))

gelezen_pickle = pickle.load(open("pickle_file.p", "rb"))

print(gelezen_pickle)
print(type(gelezen_pickle["action_space"]))
print(gelezen_pickle["action_space"].high)
print(gelezen_pickle["D"][-1])


# NIEUWSTE VERSIE (met laatste nieuwe parameters)


init_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "observation_space": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "action_space": gym.spaces.Box(low=0, high=240, shape=(1,)),
    "end_trigger_range": [500, 4000],
    "final_reward_option": "off",
    "qmix": False,
    "holding_fixed": False,
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "arrival_function_nodes": ["poisson"] * 15,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0],
        [0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
        [0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833] * 15,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("gamma", 150, 15, 80, 220),
        "BC": ("gamma", 150, 15, 80, 220),
        "CD": ("gamma", 150, 15, 80, 220),
        "DE": ("gamma", 150, 15, 80, 220),
        "EF": ("gamma", 150, 15, 80, 220),
        "FG": ("gamma", 150, 15, 80, 220),
        "GH": ("gamma", 150, 15, 80, 220),
        "HI": ("gamma", 150, 15, 80, 220),
        "IJ": ("gamma", 150, 15, 80, 220),
        "JK": ("gamma", 150, 15, 80, 220),
        "KL": ("gamma", 150, 15, 80, 220),
        "LM": ("gamma", 150, 15, 80, 220),
        "MN": ("gamma", 150, 15, 80, 220),
        "NO": ("gamma", 150, 15, 80, 220),
        "OA": ("gamma", 150, 15, 80, 220),
    },
}


# MET NIEUWE HOLDING REGIME
# + nieuwe obs
# + stoplichten


init_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": True,
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(2),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "off",
    "qmix": False,
    "holding_fixed": True,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 555555555555555555555555555555555555555555555555555555555555555,
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "traffic_light_on_edges": [
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        True,
        True,
        True,
        False,
        False,
        False,
        False,
        False,
    ],
    "cycle_props": [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        (90, 65),
        (90, 65),
        (90, 65),
        None,
        None,
        None,
        None,
        None,
    ],
    "arrival_function_nodes": ["poisson"] * 15,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0],
        [0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
        [0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833] * 15,
    "boarding_rate": 5,
    "alighting_rate": 5,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("gamma", 150, 15, 80, 220),
        "BC": ("gamma", 150, 15, 80, 220),
        "CD": ("gamma", 150, 15, 80, 220),
        "DE": ("gamma", 150, 15, 80, 220),
        "EF": ("gamma", 150, 15, 80, 220),
        "FG": ("gamma", 150, 15, 80, 220),
        "GH": ("gamma", 150, 15, 80, 220),
        "HI": ("gamma", 150, 15, 80, 220),
        "IJ": ("gamma", 150, 15, 80, 220),
        "JK": ("gamma", 150, 15, 80, 220),
        "KL": ("gamma", 150, 15, 80, 220),
        "LM": ("gamma", 150, 15, 80, 220),
        "MN": ("gamma", 150, 15, 80, 220),
        "NO": ("gamma", 150, 15, 80, 220),
        "OA": ("gamma", 150, 15, 80, 220),
    },
}


node_list = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
]  # "M", "N", "O"]

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
]
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
# [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# ]


# todo
# hier beginnen de scenarios


## INIT FILE SCENARIO 1

init_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": False,
    "result_folder": "results\\",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(3),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ],  # False, False, False ],
    "cycle_props": [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],  # None, None, None],# (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.01666] * 12,
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "deterministic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": 240,
        "BC": 240,
        "CD": 240,
        "DE": 240,
        "EF": 240,
        "FG": 240,
        "GH": 240,
        "HI": 240,
        "IJ": 240,
        "JK": 240,
        "KL": 240,
        "LA": 240,
    }
    #     "LM": 240,
    #     "MN": 240,
    #     "NO": 240,
    #     "OA": 240,
    # },
}


# SCENARIO 2

init_dict = {
    "test_run": False,
    "monitoring": False,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": False,
    "result_folder": "results\\",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(1),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        True,
        True,
        True,
        True,
        True,
        True,
        True,
        True,
        True,
        True,
        True,
        True,
    ],
    "cycle_props": [
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
        (75, 40),
    ],  # (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.01666] * 12,
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("lognormal", 240, 20, 120, 480),
        "BC": ("lognormal", 240, 20, 120, 480),
        "CD": ("lognormal", 240, 20, 120, 480),
        "DE": ("lognormal", 240, 20, 120, 480),
        "EF": ("lognormal", 240, 20, 120, 480),
        "FG": ("lognormal", 240, 20, 120, 480),
        "GH": ("lognormal", 240, 20, 120, 480),
        "HI": ("lognormal", 240, 20, 120, 480),
        "IJ": ("lognormal", 240, 20, 120, 480),
        "JK": ("lognormal", 240, 20, 120, 480),
        "KL": ("lognormal", 240, 20, 120, 480),
        "LA": ("lognormal", 240, 20, 120, 480),
    }
}




## INIT FILE SCENARIO 4

init_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": False,
    "result_folder": "results\\",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45, 0]), high=np.array([45, 45]), shape=(2,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(1),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ],  # False, False, False ],
    "cycle_props": [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],  # None, None, None],# (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833, 0.00833, 0.013333, 0.01666, 0.01666, 0.05, 0.06667, 0.03333, 0.01666, 0.0125, 0.01, 0.00833 ],
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "deterministic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": 360,
        "BC": 360,
        "CD": 300,
        "DE": 240,
        "EF": 240,
        "FG": 120,
        "GH": 90,
        "HI": 150,
        "IJ": 240,
        "JK": 240,
        "KL": 240,
        "LA": 300,
    }
    #     "LM": 240,
    #     "MN": 240,
    #     "NO": 240,
    #     "OA": 240,
    # },
}


## INIT FILE SCENARIO 5

init_dict = {
    "test_run": False,
    "monitoring": False,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": False,
    "result_folder": "results\\",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45, 0]), high=np.array([45, 45]), shape=(2,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(3),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        False,
        False,
        True,
        False,
        False,
        True,
        True,
        True,
        False,
        True,
        False,
        False,
    ],  # False, False, False ],
    "cycle_props": [
        None,
        None,
        (75, 40),
        None,
        None,
        (75, 40),
        (75, 40),
        (75, 40),
        None,
        (75, 40),
        None,
        None,
    ],  # None, None, None],# (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833, 0.00833, 0.013333, 0.01666, 0.01666, 0.05, 0.06667, 0.03333, 0.01666, 0.0125, 0.01, 0.00833 ],
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("lognormal", 360, 20, 180, 720),
        "BC": ("lognormal", 360, 20, 180, 720),
        "CD": ("lognormal", 300, 20, 150, 600),
        "DE": ("lognormal", 240, 20, 120, 480),
        "EF": ("lognormal", 240, 20, 120, 480),
        "FG": ("lognormal", 120, 20, 60, 240),
        "GH": ("lognormal", 90, 20, 45, 180),
        "HI": ("lognormal", 150, 20, 75, 300),
        "IJ": ("lognormal", 240, 20, 120, 480),
        "JK": ("lognormal", 240, 20, 120, 480),
        "KL": ("lognormal", 240, 20, 120, 480),
        "LA": ("lognormal", 300, 20, 150, 600),
    }
}