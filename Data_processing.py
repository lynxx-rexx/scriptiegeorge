import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statistics
import scipy.stats as stats
import json


data = pd.read_csv("O:\\export_george\\export_george.csv")
print(len(data))
#
#
# halte_ids = data["DR_HalteCode"].unique()
#
# data["unique_stops"] = data.apply(lambda row: (row["DR_HalteNaam"], row["DR_HalteCode"]), axis=1)#(row.DR_HalteNaam, row.DR_HalteCode))

print("Checkpoint0")


########################################################################################################################

def time_str_to_tuple(time_str):
    hours, minutes, seconds = time_str.split(":")
    hours, minutes, seconds = int(hours), int(minutes), int(seconds)

    return (hours, minutes)

def minute_correction(minutes):
    if not pd.isnull(minutes):
        assert minutes > -60 and minutes < 60, "The input is outside the expected range"

        if minutes < 0:
            minutes = minutes + 60
    return minutes

def calculate_realised_minutes(planned, diff):
    return planned - (diff/60)

def filter_onbekend(input_str):
    if input_str == "Onbekend":
        return 0
    else:
        return input_str

def is_afternoon(hour):
    if (hour > 11) and (hour < 17):
        return True
    else:
        return False

def is_afternoon_peak(hour):
    if (hour > 15) and (hour < 19):
        return True
    else:
        return False

def is_night(hour):
    if hour < 6:
        return True
    else:
        return False


########################################################################################################################




halte_ids = data["DR_HalteCode"].unique()


data["unique_stops"] = data.apply(lambda row: (row["DR_HalteNaam"], row["DR_HalteCode"]), axis=1)

print("Checkpoint1")


data_small = data[["DR_HalteCode", "DR_VertrekTijd", "DR_VolgendeDagInd", "DR_VertrekPunctualiteit", "DR_AankomstPunctualiteit"]]
data_small.columns = [["HalteCode", "VertrekTijd", "VolgendeDagInd", "VertrekPunctualiteit", "AankomstPunctualiteit"]]
print("Checkpoint2")

df = data_small.head(1489237)
df["TripDuur0"] = np.nan
df = df.convert_dtypes()

print("Checkpoint3")


df["VertrekPunctualiteitFiltered"] = df.apply(lambda row: filter_onbekend(row["VertrekPunctualiteit"]) , axis=1)
df["AankomstPunctualiteitFiltered"] = df.apply(lambda row: filter_onbekend(row["AankomstPunctualiteit"]) , axis=1)
df.drop(["VertrekPunctualiteit", "AankomstPunctualiteit"], 1, inplace=True)

print("Checkpoint4")

df[["VertrekPunctualiteitNum"]] = df[["VertrekPunctualiteitFiltered"]].apply(pd.to_numeric)
df[["AankomstPunctualiteitNum"]] = df[["AankomstPunctualiteitFiltered"]].apply(pd.to_numeric)
df["VertrekTijdInt"] = df.apply(lambda row: time_str_to_tuple(row["VertrekTijd"]) , axis=1)

print("Checkpoint5")


# calculated trip duur
for i in range(len(df)):
    if i > 0:
        df.iloc[i, 3] = df.iloc[i, 8][1] - df.iloc[i - 1, 8][1]

df["TripDuur"] = df.apply(lambda row: minute_correction(row["TripDuur0"]), axis=1)
df.drop("TripDuur0", 1, inplace=True)
df.drop(["VertrekPunctualiteitFiltered", "AankomstPunctualiteitFiltered"], 1, inplace=True)
df["GerealiseerdeTripDuur"] = np.nan

print("Checkpoint6")


# calculate gerealiseerde trip duur
for i in range(len(df)):
    if i > 0:
        df.iloc[i, 7] = df.iloc[i, 6] + ( df.iloc[i, 4] - df.iloc[i-1, 3])/60

print("Checkpoint7")

output_dict = {}

for id in halte_ids:
    output_dict[str(id)] = {"planned": [], "realised": []}

for row in range(len(df)):
    HalteCode, planned, realised, hour = df.iloc[row, 0], df.iloc[row, 6], df.iloc[row, 7], df.iloc[row, 5][0]

    if is_afternoon_peak(hour):
        output_dict[str(HalteCode)]["planned"].append(planned)
        output_dict[str(HalteCode)]["realised"].append(realised)
    # output_dict[HalteCode]["planned"].append(planned)
    # output_dict[HalteCode]["realised"].append(realised)

# for key, value in output_dict.items():
#     print(key)
#     print(value)




with open("processed_data.json", 'w') as writing_file:
    json.dump(output_dict, writing_file, indent=4)

