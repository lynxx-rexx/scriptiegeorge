#from sklearn import tree
import ray
from ray import rllib
from ray import tune
from ray.rllib.agents.registry import get_agent_class
from ray.rllib.models import ModelCatalog
from ray.tune import run_experiments
from ray.tune.registry import register_env
from ray.rllib.agents.ppo import PPOTrainer
import gym
import numpy as np
import mlflow

# CHECKLIST
# Are the env settings according to the scenario?
# Is true_multi_agent set?
# Is it a test run? --> is all the logging set correctly?
# Is it a learning session or an evaluation session? --> explore: True/False
# Is the number of iterations and samples set correct?
# Should it start from scratch or build upon a given policy?
# Set the correct name etc. for the MLFlow documentation

# Import environment definition
# from environment import IrrigationEnv
from Network_implementation_V2 import NetworkEnv



true_multi_agent = False

node_list = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
]

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
]



kwargs_dict = {
    "test_run": False,
    "monitoring": False,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": False,
    "result_folder": "",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(3),
    "end_trigger_range": [6000, 6001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ],  # False, False, False ],
    "cycle_props": [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],  # None, None, None],# (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.01666] * 12,
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "deterministic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": 240,
        "BC": 240,
        "CD": 240,
        "DE": 240,
        "EF": 240,
        "FG": 240,
        "GH": 240,
        "HI": 240,
        "IJ": 240,
        "JK": 240,
        "KL": 240,
        "LA": 240,
    },
    #     "LM": 240,
    #     "MN": 240,
    #     "NO": 240,
    #     "OA": 240,
    # },,
    "training_run": False,
}






# Driver code for training
def setup_and_train():

    # Create a single environment and register it
    def env_creator(_):
        return NetworkEnv( **kwargs_dict )
    single_env = NetworkEnv( **kwargs_dict )
    env_name = "NetworkEnv"
    register_env(env_name, env_creator)




    # Get environment obs, action spaces and number of agents
    obs_space_busses = single_env.observation_space_busses
    obs_space_tfl = single_env.observation_space_tfl
    act_space_busses = single_env.action_space_busses
    act_space_tfl = single_env.action_space_tfl
    num_busses = single_env.num_agents
    num_agents = single_env.num_agents_total

    #todo
    # overwrite number of agents for training step 1
    # num_busses = 1

    if true_multi_agent:
        # Create a policy mapping
        def gen_policy_busses():
            return (None, obs_space_busses, act_space_busses, {})

        def gen_policy_tfl():
            return (None, obs_space_tfl, act_space_tfl, {})


        policy_graphs = {}
        for i in range(num_agents):
            if i < num_busses:
                policy_graphs['agent-' + str(i)] = gen_policy_busses()
            else:
                policy_graphs["agent-" + str(i)] = gen_policy_tfl()

        def policy_mapping_fn(agent_id):
            return 'agent-' + str(agent_id)

    else:
        policy_graphs = { 'shared_policy_busses': (None, obs_space_busses, act_space_busses, {}), 'shared_policy_tfl': (None, obs_space_tfl, act_space_tfl, {}) }
        # policy_graphs = { 'shared_policy_tfl': (None, obs_space_tfl, act_space_tfl, {}) }

        def policy_mapping_fn( agent_id ):
            if agent_id < num_busses:
                return "shared_policy_busses"
            else:
                return "shared_policy_tfl"

    # Define configuration with hyperparam and training details
    # train batch size = de grootte van mijn "dataset" (als je het vergelijkt met ML)
    # mini batch size = de grootte van de stukjes waarin ik mijn dataset op deel
    # num sgd iter = het aantal keer dat ik zo over mijn dataset (= 1x train batch size) heen ga voordat ik een SGD update doe
    config = {
        "log_level": "WARN",
        "num_workers": 3,
        "num_cpus_for_driver": 1,
        "num_cpus_per_worker": 1,
        # "num_gpus_per_worker": 0.3,
        # "resources_per_trial": {"cpu": 1, "gpu": 0},
        # Number of SGD iterations in each outer loop
        # (i.e., number of epochs to execute per train batch).
        "num_sgd_iter": 50,
        # === Replay buffer ===
        # Size of the replay buffer. Note that if async_updates is set, then
        # each worker will have a replay buffer of this size.
        # "buffer_size": 2000000,
        # Number of timesteps collected for each SGD round. This defines the size
        # of each SGD epoch.
        # "train_batch_size": tune.grid_search([128, 256, 512]),
        "train_batch_size": 3000,
        # "train_batch_size": 64,
        # Total SGD batch size across all devices for SGD. This defines the
        # minibatch size within each epoch.
        "sgd_minibatch_size": 32,
        # "sgd_minibatch_size": 16,
        # Whether to shuffle sequences in the batch when training (recommended).
        "shuffle_sequences": True,
        "lr": 5e-4,
        # "lr": tune.uniform(0, 0.15),
        # "lr": tune.grid_search([0.01, 0.001, 0.0001]),
        "model": {"fcnet_hiddens": [16, 16]},
        "multiagent": {
            "policies": policy_graphs,
            "policy_mapping_fn": policy_mapping_fn,
        },
        "env": "NetworkEnv",
        "min_iter_time_s": 15,
        # "evaluation_interval": 10,
        # "evaluation_num_episodes": 5,
        "explore": True,
    }

    config = {
        # === Model ===T
        # Number of atoms for representing the distribution of return. When
        # this is greater than 1, distributional Q-learning is used.
        # the discrete supports are bounded by v_min and v_max
        "num_atoms": 1,
        "v_min": -10.0,
        "v_max": 10.0,
        # Whether to use noisy network
        "noisy": False,
        # control the initial value of noisy nets
        "sigma0": 0.5,
        # Whether to use dueling dqn
        "dueling": True,
        # Dense-layer setup for each the advantage branch and the value branch
        # in a dueling architecture.
        "hiddens": [256],
        # Whether to use double dqn
        "double_q": True,
        # N-step Q learning
        "n_step": 1,

        # === Exploration Settings ===
        "exploration_config": {
            # The Exploration class to use.
            "type": "EpsilonGreedy",
            # Config for the Exploration class' constructor:
            "initial_epsilon": 1.0,
            "final_epsilon": 0.02,
            #todo was 10 000
            # toen 50 000
            "epsilon_timesteps": 100_000,  # Timesteps over which to anneal epsilon.

            # For soft_q, use:
            # "exploration_config" = {
            #   "type": "SoftQ"
            #   "temperature": [float, e.g. 1.0]
            # }
        },
        # Switch to greedy actions in evaluation workers.
        "evaluation_config": {
            "explore": False,
        },

        # Minimum env steps to optimize for per train call. This value does
        # not affect learning, only the length of iterations.
        #todo was 1000
        "timesteps_per_iteration": 8_000,
        # Update the target network every `target_network_update_freq` steps.
        "target_network_update_freq": 500,
        # === Replay buffer ===
        # Size of the replay buffer. Note that if async_updates is set, then
        # each worker will have a replay buffer of this size.
        #todo was 50 000
        # toen 100 000
        "buffer_size": 1_000_000,
        # The number of contiguous environment steps to replay at once. This may
        # be set to greater than 1 to support recurrent models.
        "replay_sequence_length": 1,
        # If True prioritized replay buffer will be used.
        "prioritized_replay": True,
        # Alpha parameter for prioritized replay buffer.
        "prioritized_replay_alpha": 0.6,
        # Beta parameter for sampling from prioritized replay buffer.
        "prioritized_replay_beta": 0.4,
        # Final value of beta (by default, we use constant beta=0.4).
        "final_prioritized_replay_beta": 0.4,
        # Time steps over which the beta parameter is annealed.
        "prioritized_replay_beta_annealing_timesteps": 20_000,
        # Epsilon to add to the TD errors when updating priorities.
        "prioritized_replay_eps": 1e-6,

        # Whether to LZ4 compress observations
        "compress_observations": False,
        # Callback to run before learning on a multi-agent batch of experiences.
        "before_learn_on_batch": None,
        # If set, this will fix the ratio of replayed from a buffer and learned on
        # timesteps to sampled from an environment and stored in the replay buffer
        # timesteps. Otherwise, the replay will proceed at the native ratio
        # determined by (train_batch_size / rollout_fragment_length).
        "training_intensity": None,

        # === Optimization ===
        # Learning rate for adam optimizer
        "lr": 5e-4,
        # Learning rate schedule
        "lr_schedule": None,
        # Adam epsilon hyper parameter
        "adam_epsilon": 1e-8,
        # If not None, clip gradients during optimization at this value
        "grad_clip": 40,
        # How many steps of the model to sample before learning starts.
        "learning_starts": 1000,
        # Update the replay buffer with this many samples at once. Note that
        # this setting applies per-worker if num_workers > 1.
        "rollout_fragment_length": 4,
        # Size of a batch sampled from replay buffer for training. Note that
        # if async_updates is set, then each worker returns gradients for a
        # batch of this size.
        "train_batch_size": 32,

        # === Parallelism ===
        # Number of workers for collecting samples with. This only makes sense
        # to increase if your environment is particularly slow to sample, or if
        # you"re using the Async or Ape-X optimizers.
        "num_workers": 0,
        # Whether to compute priorities on workers.
        "worker_side_prioritization": False,
        # Prevent iterations from going lower than this time span
        # "min_iter_time_s": 1,
        "model": {"fcnet_hiddens": [16, 16]},
        "multiagent": {
            "policies": policy_graphs,
            "policy_mapping_fn": policy_mapping_fn,
        },
        "env": "NetworkEnv",
        "min_iter_time_s": 30,
        "log_level": "WARN",
        "num_workers": 3,
        "num_cpus_for_driver": 1,
        "num_cpus_per_worker": 1,

    }



    # Define experiment details
    exp_name = 'my_exp'
    exp_dict = {
            'name': exp_name,
            'run_or_experiment': 'DQN',
            "stop": {
                # training_iteration geeft aan hoe vaak hij de training herhaalt
                "training_iteration": 200
            },
            'checkpoint_freq': 2,
            # "restore": 'C:\\Users\\GWeijs\\ray_results\\my_exp\\PPO_NetworkEnv_7a98e_00008_8_2021-03-05_19-20-38\\checkpoint_140\\checkpoint-140',
            # "restore": 'C:\\Users\\georg\\ray_results\\my_exp\\DQN_NetworkEnv_8d282_00000_0_2021-05-13_17-54-51\\checkpoint_22\\checkpoint-22',
            "config": config,
            # num_samples geeft aan hoe vaak je elke gridsearch punt wil bezoeken
            "num_samples": 1,
            #"local_dir": "~/tune_results",
        }

    # Initialize ray and run
    ray.init(object_store_memory=2*10**9)
    results = tune.run(**exp_dict)
    # print('##### Best config #####')
    # print( results.get_best_config(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )
    # print('##### Best trial #####')
    # print( results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )

    # print( results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg'), type(results.get_best_trial(metric="episode_reward_mean", mode="max", scope='avg')) )
    # print(type(results.results), results.results )
    # for key, value in results.results.items():
    #     print('###  ', key, '  ###')
    #     for key2, val2 in value.items():
    #         print(key2)

    avg_reward_list = []

    for key, val in results.results.items():
        avg_reward_list.append(val['episode_reward_mean'])

    # print(avg_reward_list)
    #
    # print(type(results.stats()), results.stats())
    #
    # print(type(results.runner_data()), results.runner_data())

    return config, avg_reward_list

if __name__=='__main__':
    mlflow.set_experiment(experiment_name="DQN_experiment")

    with mlflow.start_run(run_name="scenario_2"):

        new_kwargs_dict = kwargs_dict.copy()
        new_kwargs_dict["observation_space_busses"] = None
        new_kwargs_dict["observation_space_tfl"] = None
        new_kwargs_dict["action_space_busses"] = None
        new_kwargs_dict["action_space_tfl"] = None

        mlflow.log_param("true_multi_agent", true_multi_agent)
        mlflow.log_dict(new_kwargs_dict, "kwargs_dict.json")

        for key, value in kwargs_dict.items():
            mlflow.log_param("SIM_" + key, value)

        config, avg_reward_list = setup_and_train()

        for key, value in config.items():
            mlflow.log_param("RL_" + key, value)

        for run in range(len(avg_reward_list)):
            mlflow.log_metric("reward_run_" + str(run), avg_reward_list[run])

