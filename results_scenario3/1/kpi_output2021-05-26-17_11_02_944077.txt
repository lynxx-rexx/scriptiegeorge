{
    "Average_holding_time": 20.337591240875913,
    "Average_holding_actions": [
        615,
        370,
        0,
        40,
        0,
        0
    ],
    "Average_travel_time_passengers": 2117.975349463348,
    "Average_passenger_waiting_time": 333.78837184549855,
    "Average_passenger_in_vehicle_time": 1784.186977617856,
    "Average_speed_busses": 0.04566666666666667,
    "Average_experienced_crowding": 59.9095723738915,
    "Average_covariation_headway": 0.30644616379900774,
    "Average_covariation_headway_stops": {
        "K": 0.23343830673372637,
        "H": 0.2307575576230029,
        "D": 0.20818983862978918,
        "A": 0.22270123288176477,
        "E": 0.2159712111668985,
        "C": 0.2175431194871333,
        "I": 0.2154329967057123,
        "F": 0.2006836901745393,
        "B": 0.21597614389925437,
        "L": 0.21645644832367122,
        "J": 0.2154868897489916,
        "G": 0.20391633395857228
    },
    "Average_excess_waiting_time": {
        "K": 67.70370732253161,
        "H": 65.39563724675764,
        "D": 61.83558446741341,
        "A": 65.93644429388002,
        "E": 61.40072826853299,
        "C": 64.88867363827347,
        "I": 66.33632908169051,
        "F": 61.11748115940122,
        "B": 67.25395840503143,
        "L": 67.21998569589687,
        "J": 67.83016607989674,
        "G": 63.33745287907669
    },
    "Holding_per_stop": {
        "H": 19.565217391304348,
        "D": 20.869565217391305,
        "E": 24.130434782608695,
        "K": 23.076923076923077,
        "A": 18.791208791208792,
        "C": 20.10989010989011,
        "F": 20.543478260869566,
        "I": 21.0989010989011,
        "B": 18.791208791208792,
        "L": 18.13186813186813,
        "J": 21.75824175824176,
        "G": 17.142857142857142
    },
    "Actions_tfl_edges": {
        "BC": [
            10,
            82,
            0
        ],
        "CD": [
            10,
            82,
            0
        ],
        "DE": [
            14,
            79,
            0
        ],
        "GH": [
            11,
            81,
            0
        ],
        "JK": [
            11,
            81,
            0
        ],
        "LA": [
            14,
            78,
            0
        ],
        "HI": [
            18,
            74,
            0
        ],
        "EF": [
            13,
            79,
            0
        ],
        "KL": [
            14,
            77,
            0
        ],
        "AB": [
            11,
            80,
            0
        ],
        "FG": [
            12,
            80,
            0
        ],
        "IJ": [
            15,
            76,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "K": 57.8125,
            "L": 58.9375,
            "A": 56,
            "B": 56.333333333333336,
            "C": 56.53333333333333,
            "D": 59,
            "E": 57.86666666666667,
            "F": 59.266666666666666,
            "G": 59.4,
            "H": 60.86666666666667,
            "I": 61.2,
            "J": 61.266666666666666
        },
        "bus3": {
            "H": 56.5625,
            "I": 54.8125,
            "J": 58.3125,
            "K": 58.86666666666667,
            "L": 59.6,
            "A": 57.4,
            "B": 60,
            "C": 59,
            "D": 60.6,
            "E": 59.8,
            "F": 59.46666666666667,
            "G": 60.06666666666667
        },
        "bus1": {
            "D": 55.1875,
            "E": 55.9375,
            "F": 58.0625,
            "G": 59.2,
            "H": 58.666666666666664,
            "I": 59.2,
            "J": 57.4,
            "K": 57.8,
            "L": 57.93333333333333,
            "A": 59.666666666666664,
            "B": 59.266666666666666,
            "C": 59.333333333333336
        },
        "bus5": {
            "A": 58,
            "B": 61.6875,
            "C": 61.86666666666667,
            "D": 60.4,
            "E": 59.733333333333334,
            "F": 58.733333333333334,
            "G": 58.46666666666667,
            "H": 57.6,
            "I": 58.266666666666666,
            "J": 59,
            "K": 59,
            "L": 59.666666666666664
        },
        "bus2": {
            "E": 52.625,
            "F": 53.75,
            "G": 54.4375,
            "H": 55.1875,
            "I": 55.93333333333333,
            "J": 56.4,
            "K": 56.733333333333334,
            "L": 57.8,
            "A": 57.06666666666667,
            "B": 57.86666666666667,
            "C": 56.6,
            "D": 55.2
        },
        "bus0": {
            "C": 56.0625,
            "D": 57.0625,
            "E": 57,
            "F": 57.733333333333334,
            "G": 59.4,
            "H": 58.666666666666664,
            "I": 56.93333333333333,
            "J": 55.733333333333334,
            "K": 56.733333333333334,
            "L": 56.46666666666667,
            "A": 55.8,
            "B": 58.86666666666667
        }
    },
    "Obs_and_actions_busses": {
        "3": 316.3757479757021,
        "1": 110.7832321929022,
        "0": -141.3935087869171,
        "4": 228.34325869305044
    },
    "Obs_and_actions_tfls": {
        "1": 96.03521824406191,
        "0": 426.04779376194904
    }
}466797,
        "4": 229.29952642585286
    },
    "Obs_and_actions_tfls": {
        "1": 53.394598074621804,
        "0": 451.1402581747046
    }
}