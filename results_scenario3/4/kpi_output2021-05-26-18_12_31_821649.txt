{
    "Average_holding_time": 24.213333333333335,
    "Average_holding_actions": [
        499,
        532,
        0,
        94,
        0,
        0
    ],
    "Average_travel_time_passengers": 2098.675609161846,
    "Average_passenger_waiting_time": 352.14349925195035,
    "Average_passenger_in_vehicle_time": 1746.5321099098976,
    "Average_speed_busses": 0.046875,
    "Average_experienced_crowding": 63.3850898894469,
    "Average_covariation_headway": 0.38697055120928314,
    "Average_covariation_headway_stops": {
        "F": 0.3768251966876316,
        "E": 0.3779587245019723,
        "I": 0.34676700514360326,
        "J": 0.33689379101345746,
        "H": 0.3585854147328259,
        "G": 0.3792672075217833,
        "K": 0.299016089077262,
        "L": 0.2890843270338872,
        "A": 0.320321709006465,
        "B": 0.32749840764336346,
        "C": 0.3314650172381522,
        "D": 0.3554132946897375
    },
    "Average_excess_waiting_time": {
        "F": 89.69663311216448,
        "E": 91.03685911299556,
        "I": 76.74214034724099,
        "J": 72.61087293521575,
        "H": 81.22654698515919,
        "G": 88.20860841728029,
        "K": 67.30534415573868,
        "L": 67.0561872463532,
        "A": 74.6116342691484,
        "B": 77.2973934368245,
        "C": 79.94104130838713,
        "D": 87.75861204521073
    },
    "Holding_per_stop": {
        "F": 25.806451612903224,
        "I": 23.36842105263158,
        "J": 27.473684210526315,
        "H": 25.21276595744681,
        "E": 25.161290322580644,
        "G": 26.170212765957448,
        "K": 21.789473684210527,
        "L": 19.78723404255319,
        "A": 22.97872340425532,
        "B": 23.225806451612904,
        "C": 23.870967741935484,
        "D": 25.76086956521739
    },
    "Actions_tfl_edges": {
        "DE": [
            43,
            0,
            50
        ],
        "EF": [
            44,
            0,
            50
        ],
        "FG": [
            39,
            0,
            55
        ],
        "GH": [
            39,
            0,
            55
        ],
        "HI": [
            47,
            0,
            48
        ],
        "IJ": [
            47,
            0,
            49
        ],
        "JK": [
            47,
            0,
            48
        ],
        "KL": [
            42,
            0,
            53
        ],
        "LA": [
            50,
            0,
            44
        ],
        "AB": [
            49,
            0,
            45
        ],
        "BC": [
            44,
            0,
            49
        ],
        "CD": [
            41,
            0,
            52
        ]
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "F": 55.0625,
            "G": 55.25,
            "H": 57,
            "I": 60.875,
            "J": 60.3125,
            "K": 61.4375,
            "L": 62,
            "A": 60.93333333333333,
            "B": 62.733333333333334,
            "C": 60.6,
            "D": 58.266666666666666,
            "E": 59.06666666666667
        },
        "bus0": {
            "E": 62.375,
            "F": 62.9375,
            "G": 62.0625,
            "H": 62.8125,
            "I": 61.5625,
            "J": 62.733333333333334,
            "K": 61.53333333333333,
            "L": 63,
            "A": 64.6,
            "B": 62.86666666666667,
            "C": 63.93333333333333,
            "D": 65.13333333333334
        },
        "bus4": {
            "I": 48.6875,
            "J": 47.25,
            "K": 47.1875,
            "L": 46.3125,
            "A": 47,
            "B": 46.3125,
            "C": 46.75,
            "D": 49.375,
            "E": 50.625,
            "F": 50,
            "G": 51,
            "H": 51.53333333333333
        },
        "bus5": {
            "J": 61.8125,
            "K": 62.875,
            "L": 62.875,
            "A": 63.75,
            "B": 64.4375,
            "C": 65.8125,
            "D": 66.5,
            "E": 67.1875,
            "F": 68.5625,
            "G": 68.25,
            "H": 67.13333333333334,
            "I": 67.33333333333333
        },
        "bus3": {
            "H": 51.4375,
            "I": 50.3125,
            "J": 52,
            "K": 52.4375,
            "L": 51.5625,
            "A": 51.9375,
            "B": 53.25,
            "C": 52.25,
            "D": 53.8,
            "E": 55.333333333333336,
            "F": 54.86666666666667,
            "G": 56.2
        },
        "bus2": {
            "G": 55.875,
            "H": 55.4375,
            "I": 54.6875,
            "J": 54.625,
            "K": 53.375,
            "L": 55.25,
            "A": 57.0625,
            "B": 58.333333333333336,
            "C": 59,
            "D": 61,
            "E": 60.333333333333336,
            "F": 59.53333333333333
        }
    },
    "Obs_and_actions_busses": {
        "4": 437.67101364644316,
        "1": 98.73456277231797,
        "0": -231.17423728182695
    },
    "Obs_and_actions_tfls": {
        "0": 338.5222561698572,
        "2": -70.53329609671124
    }
}