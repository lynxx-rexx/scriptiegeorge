{
    "Average_holding_time": 35.725190839694655,
    "Average_holding_actions": [
        561,
        87,
        97,
        14,
        22,
        0
    ],
    "Average_travel_time_passengers": 2200.713719644514,
    "Average_passenger_waiting_time": 356.7742470264965,
    "Average_passenger_in_vehicle_time": 1843.9394726180344,
    "Average_speed_busses": 0.043666666666666666,
    "Average_experienced_crowding": 88.86480591028005,
    "Average_covariation_headway": 0.34979167640868175,
    "Average_covariation_headway_stops": {
        "J": 0.2589676102116376,
        "E": 0.2751469098287401,
        "D": 0.2586535049713123,
        "B": 0.249638121351879,
        "A": 0.21807448393878975,
        "C": 0.26153769647265795,
        "F": 0.2677551382796764,
        "K": 0.22997213607868192,
        "G": 0.2592435119802787,
        "H": 0.2648515444087943,
        "L": 0.21301662194416776,
        "I": 0.2675323621902774
    },
    "Average_excess_waiting_time": {
        "J": 88.55158356809534,
        "E": 85.5236464023734,
        "D": 84.69677225096325,
        "B": 86.29853985289827,
        "A": 84.30078216180351,
        "C": 86.20905490677467,
        "F": 85.83607551504707,
        "K": 85.71313004629712,
        "G": 86.72753666662072,
        "H": 89.38001956878259,
        "L": 84.518688011118,
        "I": 91.47410562201867
    },
    "Holding_per_stop": {
        "E": 35.056179775280896,
        "D": 22.84090909090909,
        "J": 44.827586206896555,
        "B": 31.724137931034484,
        "A": 24.82758620689655,
        "C": 23.863636363636363,
        "F": 37.84090909090909,
        "K": 43.10344827586207,
        "G": 38.275862068965516,
        "H": 41.724137931034484,
        "L": 40.116279069767444,
        "I": 44.827586206896555
    },
    "Actions_tfl_edges": {
        "CD": [
            60,
            0,
            29
        ],
        "JK": [
            61,
            26,
            0
        ],
        "FG": [
            60,
            0,
            28
        ],
        "GH": [
            64,
            11,
            12
        ],
        "HI": [
            67,
            18,
            2
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "J": 94.8,
            "K": 87.33333333333333,
            "L": 78.26666666666667,
            "A": 67.8,
            "B": 58,
            "C": 50.6,
            "D": 47.357142857142854,
            "E": 47,
            "F": 71.71428571428571,
            "G": 104.28571428571429,
            "H": 111.57142857142857,
            "I": 107.64285714285714
        },
        "bus3": {
            "E": 46.6,
            "F": 71.2,
            "G": 105,
            "H": 113.6,
            "I": 110.66666666666667,
            "J": 102.46666666666667,
            "K": 93.26666666666667,
            "L": 82.46666666666667,
            "A": 70.73333333333333,
            "B": 58.285714285714285,
            "C": 53.142857142857146,
            "D": 50.07142857142857
        },
        "bus2": {
            "D": 45.266666666666666,
            "E": 45.93333333333333,
            "F": 68.53333333333333,
            "G": 95.66666666666667,
            "H": 104.06666666666666,
            "I": 99.4,
            "J": 93.53333333333333,
            "K": 84.6,
            "L": 73.71428571428571,
            "A": 63.714285714285715,
            "B": 54.57142857142857,
            "C": 50.92857142857143
        },
        "bus0": {
            "B": 59.333333333333336,
            "C": 52.06666666666667,
            "D": 48.733333333333334,
            "E": 48.333333333333336,
            "F": 71.73333333333333,
            "G": 108,
            "H": 118.71428571428571,
            "I": 114.35714285714286,
            "J": 107.5,
            "K": 98.35714285714286,
            "L": 85.85714285714286,
            "A": 75.21428571428571
        },
        "bus5": {
            "A": 72.46666666666667,
            "B": 62.46666666666667,
            "C": 55.333333333333336,
            "D": 49.93333333333333,
            "E": 49,
            "F": 71.71428571428571,
            "G": 106.14285714285714,
            "H": 116.57142857142857,
            "I": 114.28571428571429,
            "J": 105.21428571428571,
            "K": 98.14285714285714,
            "L": 87.64285714285714
        },
        "bus1": {
            "C": 51.53333333333333,
            "D": 48.13333333333333,
            "E": 46.333333333333336,
            "F": 69.4,
            "G": 102.33333333333333,
            "H": 111.86666666666666,
            "I": 108.53333333333333,
            "J": 100.28571428571429,
            "K": 92.14285714285714,
            "L": 81.35714285714286,
            "A": 71.14285714285714,
            "B": 61.642857142857146
        }
    },
    "Obs_and_actions_busses": {
        "3": 198.76961764041567,
        "1": 126.11550375323232,
        "0": -173.6544483190239,
        "2": 39.597495749038295,
        "4": 322.06876804206786,
        "5": 272.8537235340541
    },
    "Obs_and_actions_tfls": {
        "0": 98.15103406301552,
        "2": -303.95298373997053,
        "1": -307.2247886621066
    }
}