{
    "Average_holding_time": 34.67048710601719,
    "Average_holding_actions": [
        401,
        294,
        235,
        21,
        37,
        0
    ],
    "Average_travel_time_passengers": 2178.0368623842373,
    "Average_passenger_waiting_time": 352.5096160434238,
    "Average_passenger_in_vehicle_time": 1825.5272463408046,
    "Average_speed_busses": 0.043625,
    "Average_experienced_crowding": 88.54857871266645,
    "Average_covariation_headway": 0.3083377593242846,
    "Average_covariation_headway_stops": {
        "I": 0.22980373919694533,
        "L": 0.22553750237323206,
        "F": 0.22801847845097334,
        "K": 0.21881147677339885,
        "J": 0.20429389688695337,
        "C": 0.21729863919846984,
        "A": 0.2344466277181116,
        "G": 0.22447700914945556,
        "D": 0.187503049765755,
        "B": 0.21667893192301824,
        "H": 0.22887025576620215,
        "E": 0.17875174050852852
    },
    "Average_excess_waiting_time": {
        "I": 84.6782101965033,
        "L": 76.49173315256706,
        "F": 84.14846449338438,
        "K": 77.8226834588985,
        "J": 77.76155660581651,
        "C": 79.39916381552877,
        "A": 80.11344439281288,
        "G": 85.21860186266525,
        "D": 77.63138758130623,
        "B": 79.96199102433377,
        "H": 87.05910997124795,
        "E": 78.20602558932404
    },
    "Holding_per_stop": {
        "L": 43.48314606741573,
        "K": 40.90909090909091,
        "J": 37.93103448275862,
        "I": 37.58620689655172,
        "C": 32.04545454545455,
        "F": 31.724137931034484,
        "A": 36.47727272727273,
        "D": 33.10344827586207,
        "G": 32.7906976744186,
        "B": 26.20689655172414,
        "E": 30,
        "H": 33.48837209302326
    },
    "Actions_tfl_edges": {
        "HI": [
            0,
            0,
            87
        ],
        "JK": [
            0,
            0,
            88
        ],
        "CD": [
            53,
            0,
            35
        ],
        "FG": [
            0,
            0,
            87
        ],
        "GH": [
            0,
            0,
            86
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "I": 105.73333333333333,
            "J": 99.6,
            "K": 92.06666666666666,
            "L": 82.73333333333333,
            "A": 71.46666666666667,
            "B": 59.07142857142857,
            "C": 51,
            "D": 47.285714285714285,
            "E": 46,
            "F": 73.57142857142857,
            "G": 111.14285714285714,
            "H": 119.14285714285714
        },
        "bus5": {
            "L": 84.6,
            "A": 72.53333333333333,
            "B": 62.86666666666667,
            "C": 55.53333333333333,
            "D": 48.86666666666667,
            "E": 48.733333333333334,
            "F": 78.33333333333333,
            "G": 115.93333333333334,
            "H": 126.07142857142857,
            "I": 120.5,
            "J": 113.35714285714286,
            "K": 100.78571428571429
        },
        "bus1": {
            "F": 69.73333333333333,
            "G": 102.46666666666667,
            "H": 112.06666666666666,
            "I": 108.53333333333333,
            "J": 102.46666666666667,
            "K": 91.73333333333333,
            "L": 81.2,
            "A": 70.64285714285714,
            "B": 60.714285714285715,
            "C": 53.785714285714285,
            "D": 52.357142857142854,
            "E": 49.92857142857143
        },
        "bus4": {
            "K": 87.2,
            "L": 80.4,
            "A": 69.2,
            "B": 60,
            "C": 54.6,
            "D": 52.333333333333336,
            "E": 49.266666666666666,
            "F": 74.5,
            "G": 106,
            "H": 113.85714285714286,
            "I": 110.71428571428571,
            "J": 102.64285714285714
        },
        "bus3": {
            "J": 93.6,
            "K": 84.2,
            "L": 76.2,
            "A": 67.06666666666666,
            "B": 57,
            "C": 52.13333333333333,
            "D": 49.785714285714285,
            "E": 47.642857142857146,
            "F": 70.21428571428571,
            "G": 103.64285714285714,
            "H": 109.78571428571429,
            "I": 108.64285714285714
        },
        "bus0": {
            "C": 54.266666666666666,
            "D": 51.733333333333334,
            "E": 49.733333333333334,
            "F": 74.13333333333334,
            "G": 104.26666666666667,
            "H": 111.4,
            "I": 106.66666666666667,
            "J": 99.85714285714286,
            "K": 88.85714285714286,
            "L": 79.92857142857143,
            "A": 71.21428571428571,
            "B": 61.785714285714285
        }
    },
    "Obs_and_actions_busses": {
        "5": 311.0522148694387,
        "1": 4.826421869906302,
        "3": 224.26010021096016,
        "2": 122.39416969609084,
        "0": -178.9537026186237,
        "4": 196.78459965015628
    },
    "Obs_and_actions_tfls": {
        "2": 4.388905469725945,
        "0": -132.68196122397774
    }
} -132.9324902461897
    }
}