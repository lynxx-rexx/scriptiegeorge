{
    "Average_holding_time": 37.17454194792671,
    "Average_holding_actions": [
        397,
        280,
        224,
        27,
        61,
        0
    ],
    "Average_travel_time_passengers": 2197.8778939660588,
    "Average_passenger_waiting_time": 361.3968553182081,
    "Average_passenger_in_vehicle_time": 1836.481038647822,
    "Average_speed_busses": 0.043208333333333335,
    "Average_experienced_crowding": 89.97389100058938,
    "Average_covariation_headway": 0.32999471833171495,
    "Average_covariation_headway_stops": {
        "G": 0.23730283380922773,
        "I": 0.25175563363423376,
        "F": 0.22895913992431943,
        "K": 0.23817619792729589,
        "J": 0.23053632847475067,
        "H": 0.24953822715106788,
        "C": 0.26094828575660256,
        "L": 0.22093349225701855,
        "D": 0.24800490834002972,
        "A": 0.21454154868107767,
        "E": 0.24438884915160736,
        "B": 0.24456956837195157
    },
    "Average_excess_waiting_time": {
        "G": 87.82033901622111,
        "I": 89.02174798610127,
        "F": 89.64137163014561,
        "K": 82.67158864536145,
        "J": 83.55404551199803,
        "H": 91.1723643984671,
        "C": 92.5439056542375,
        "L": 81.56235578764614,
        "D": 92.55106997487758,
        "A": 82.36300400346306,
        "E": 94.03107068175672,
        "B": 90.60615001261334
    },
    "Holding_per_stop": {
        "G": 38.02325581395349,
        "I": 36.55172413793103,
        "K": 49.43181818181818,
        "J": 36.206896551724135,
        "H": 34.883720930232556,
        "F": 36.627906976744185,
        "C": 39.06976744186046,
        "L": 43.44827586206897,
        "D": 40.116279069767444,
        "A": 30.689655172413794,
        "E": 33.529411764705884,
        "B": 27.209302325581394
    },
    "Actions_tfl_edges": {
        "FG": [
            0,
            0,
            87
        ],
        "HI": [
            0,
            0,
            87
        ],
        "JK": [
            0,
            0,
            88
        ],
        "GH": [
            0,
            0,
            86
        ],
        "CD": [
            39,
            0,
            47
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "G": 111.13333333333334,
            "H": 119.13333333333334,
            "I": 113.46666666666667,
            "J": 108.46666666666667,
            "K": 99.13333333333334,
            "L": 87.35714285714286,
            "A": 75,
            "B": 66,
            "C": 60.07142857142857,
            "D": 55.357142857142854,
            "E": 50.857142857142854,
            "F": 77
        },
        "bus3": {
            "I": 111.26666666666667,
            "J": 103.13333333333334,
            "K": 92.93333333333334,
            "L": 82.53333333333333,
            "A": 71.33333333333333,
            "B": 61.42857142857143,
            "C": 55.5,
            "D": 51.92857142857143,
            "E": 51.785714285714285,
            "F": 81.71428571428571,
            "G": 113.92857142857143,
            "H": 124.5
        },
        "bus1": {
            "F": 73,
            "G": 106.93333333333334,
            "H": 113.8,
            "I": 109.53333333333333,
            "J": 99.78571428571429,
            "K": 87.92857142857143,
            "L": 78.42857142857143,
            "A": 70.85714285714286,
            "B": 60.785714285714285,
            "C": 55.714285714285715,
            "D": 53.214285714285715,
            "E": 52
        },
        "bus5": {
            "K": 87.26666666666667,
            "L": 79.46666666666667,
            "A": 67.8,
            "B": 56.666666666666664,
            "C": 50.333333333333336,
            "D": 46.13333333333333,
            "E": 46.714285714285715,
            "F": 74.85714285714286,
            "G": 110.35714285714286,
            "H": 117,
            "I": 112.14285714285714,
            "J": 102.92857142857143
        },
        "bus4": {
            "J": 94.33333333333333,
            "K": 85,
            "L": 74.93333333333334,
            "A": 65.66666666666667,
            "B": 56.06666666666667,
            "C": 49.285714285714285,
            "D": 44.857142857142854,
            "E": 43.285714285714285,
            "F": 70.42857142857143,
            "G": 106.21428571428571,
            "H": 112.71428571428571,
            "I": 107.71428571428571
        },
        "bus0": {
            "C": 54.06666666666667,
            "D": 49.4,
            "E": 48.6,
            "F": 72.26666666666667,
            "G": 109.64285714285714,
            "H": 114.92857142857143,
            "I": 111.64285714285714,
            "J": 104.85714285714286,
            "K": 96,
            "L": 83.5,
            "A": 73.78571428571429,
            "B": 66.21428571428571
        }
    },
    "Obs_and_actions_busses": {
        "0": -206.17237201756885,
        "1": -2.4755435785184763,
        "2": 119.84771998248279,
        "4": 188.72002736624677,
        "5": 341.09182967775496,
        "3": 218.878334074587
    },
    "Obs_and_actions_tfls": {
        "2": 7.120885063701007,
        "0": -207.3421029677344
    }
}5806407
    }
}