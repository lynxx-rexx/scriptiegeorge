{
    "Average_holding_time": 46.19095477386934,
    "Average_holding_actions": [
        356,
        29,
        478,
        113,
        19,
        0
    ],
    "Average_travel_time_passengers": 2292.1071101004486,
    "Average_passenger_waiting_time": 368.31512673568807,
    "Average_passenger_in_vehicle_time": 1923.7919833647304,
    "Average_speed_busses": 0.04145833333333333,
    "Average_experienced_crowding": 93.20547381585828,
    "Average_covariation_headway": 0.3102655349850753,
    "Average_covariation_headway_stops": {
        "G": 0.21016890558835907,
        "F": 0.20631070781323482,
        "H": 0.21048404027149814,
        "L": 0.20585588586510334,
        "D": 0.23361628412450278,
        "A": 0.22355701671528258,
        "B": 0.1954385939960952,
        "I": 0.19901496699922308,
        "E": 0.20487407876105274,
        "C": 0.2436941636669995,
        "J": 0.1820881764207824,
        "K": 0.19784372632344047
    },
    "Average_excess_waiting_time": {
        "G": 93.8055761510281,
        "F": 95.1790162697414,
        "H": 95.82464915783135,
        "L": 99.96932473684979,
        "D": 99.73619226749008,
        "A": 99.22237673009244,
        "B": 93.10492826133031,
        "I": 96.24351917693326,
        "E": 97.18533419221899,
        "C": 103.31831605879631,
        "J": 96.10426238912919,
        "K": 100.58840695800473
    },
    "Holding_per_stop": {
        "G": 43.57142857142857,
        "H": 46.265060240963855,
        "A": 36.144578313253014,
        "D": 42.28915662650602,
        "F": 45.54216867469879,
        "B": 60.36144578313253,
        "L": 52.68292682926829,
        "I": 46.265060240963855,
        "E": 42.65060240963855,
        "C": 40.48192771084337,
        "J": 49.87951807228916,
        "K": 48.292682926829265
    },
    "Actions_tfl_edges": {
        "CD": [
            0,
            44,
            40
        ],
        "FG": [
            20,
            53,
            11
        ],
        "GH": [
            23,
            54,
            7
        ],
        "HI": [
            18,
            61,
            4
        ],
        "JK": [
            11,
            71,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 114.14285714285714,
            "H": 122.07142857142857,
            "I": 117.57142857142857,
            "J": 110.78571428571429,
            "K": 101.28571428571429,
            "L": 89.35714285714286,
            "A": 76.85714285714286,
            "B": 66.78571428571429,
            "C": 59.714285714285715,
            "D": 57.142857142857146,
            "E": 55.07142857142857,
            "F": 83.14285714285714
        },
        "bus2": {
            "F": 76.14285714285714,
            "G": 111.57142857142857,
            "H": 121.92857142857143,
            "I": 117,
            "J": 110.21428571428571,
            "K": 101.57142857142857,
            "L": 91.64285714285714,
            "A": 80.92857142857143,
            "B": 69.85714285714286,
            "C": 61,
            "D": 58.07692307692308,
            "E": 55.92307692307692
        },
        "bus4": {
            "L": 79.5,
            "A": 71.35714285714286,
            "B": 61.714285714285715,
            "C": 54.07142857142857,
            "D": 50.214285714285715,
            "E": 47.642857142857146,
            "F": 73.28571428571429,
            "G": 107.07142857142857,
            "H": 116.92857142857143,
            "I": 114.3076923076923,
            "J": 106.53846153846153,
            "K": 95.53846153846153
        },
        "bus1": {
            "D": 52.07142857142857,
            "E": 51.57142857142857,
            "F": 79.07142857142857,
            "G": 121,
            "H": 130.14285714285714,
            "I": 125.28571428571429,
            "J": 114.71428571428571,
            "K": 104.85714285714286,
            "L": 92.78571428571429,
            "A": 81.28571428571429,
            "B": 67.14285714285714,
            "C": 60
        },
        "bus5": {
            "A": 72.42857142857143,
            "B": 61.42857142857143,
            "C": 54.92857142857143,
            "D": 51.42857142857143,
            "E": 47.857142857142854,
            "F": 73.92857142857143,
            "G": 110.64285714285714,
            "H": 120.92857142857143,
            "I": 117.07142857142857,
            "J": 107.92857142857143,
            "K": 97.76923076923077,
            "L": 88.07692307692308
        },
        "bus0": {
            "B": 55.857142857142854,
            "C": 51.214285714285715,
            "D": 49.57142857142857,
            "E": 48.142857142857146,
            "F": 73.07142857142857,
            "G": 105.5,
            "H": 114.5,
            "I": 110.92857142857143,
            "J": 103.14285714285714,
            "K": 93.5,
            "L": 82.64285714285714,
            "A": 69.23076923076923
        }
    },
    "Obs_and_actions_busses": {
        "0": -219.59953016776174,
        "4": 291.7291874993463,
        "2": 57.53999394033732,
        "1": -22.694577664651188,
        "5": 84.52343317252772
    },
    "Obs_and_actions_tfls": {
        "1": 92.77232992725797,
        "2": -294.9119101988925,
        "0": -204.50497213962265
    }
}