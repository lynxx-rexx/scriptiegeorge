import pandas as pd
import numpy as np
import simpy
import random
import statistics
from functools import wraps, partial
import matplotlib.pyplot as plt
import itertools
from _collections import defaultdict
import gym
from ray.rllib.env.multi_agent_env import MultiAgentEnv
from datetime import datetime
import json
import copy


# FOR QMIX VERANDERD:
# - action space
# - observation space
# - uitlezen actie [0]
# - teruggeven observation



class NetworkEnv(MultiAgentEnv):
    def __init__(
        self,
        adjacency_matrix,
        node_names,
        num_agents,
        routes,
        test_run,
        run_logging,
        passenger_logging,
        observation_space_busses,
        observation_space_tfl,
        action_space_busses,
        action_space_tfl,
        end_trigger_range,
        final_reward_option,
        ta,
        observation_option,
        reward_function_option,
        capacity_busses,
        slot_capacity_nodes,
        traffic_light_on_edges,
        cycle_props,
        arrival_function_nodes,
        destination_distribution_nodes,
        pass_arrival_rate_nodes,
        dwell_time_function,
        travel_time_edges,
        monitoring,
        holding_times,
        boarding_rate,
        alighting_rate,
        holding_punish_param,
        kpi_logging,
        result_folder,
        training_run,
        qmix=False,
        holding_fixed=True,
    ):

        self.env = simpy.Environment()

        # COLLECTION LISTS AND DICTS
        self.node_list = []
        self.edge_list = []
        self.busses = []
        self.bus_dict = defaultdict(lambda: [])
        self.node_dict = defaultdict(lambda: [])
        self.reward_list = []
        self.reward_list_rllib = []

        # PARAMETERS
        self.position_list = node_names
        self.position_list_obs = []

        for i in self.position_list:
            self.position_list_obs.append("A" + i)
            self.position_list_obs.append("P" + i)
            self.position_list_obs.append("D" + i)

        self.step_size = 15
        self.end_time = self.step_size
        self.monitor_time_step = 10
        self.step_counter = 0
        self.end_trigger_range = end_trigger_range
        self.end_trigger = random.randrange(
            end_trigger_range[0], end_trigger_range[1], 1
        )

        # CREATING THE NETWORK
        self.slot_capacity_nodes = slot_capacity_nodes
        self.travel_time_edges = travel_time_edges
        self.adjacency_matrix = adjacency_matrix
        self.arrival_function_nodes = arrival_function_nodes
        self.destination_distribution_nodes = destination_distribution_nodes
        self.pass_arrival_rate_nodes = pass_arrival_rate_nodes
        self.boarding_rate = boarding_rate
        self.alighting_rate = alighting_rate
        self.traffic_light_on_edges = traffic_light_on_edges
        self.cycle_props = cycle_props

        self.num_agents = num_agents
        self.num_agents_total = num_agents + sum(traffic_light_on_edges)

        self.create_edge_objects(
            adjacency_matrix=adjacency_matrix,
            node_names=node_names,
            travel_time_edges=travel_time_edges,
        )
        self.create_nodes(
            node_names=node_names,
            slot_capacity_nodes=slot_capacity_nodes,
            arrival_function_nodes=arrival_function_nodes,
            destination_distribution_nodes=destination_distribution_nodes,
            pass_arrival_rate_nodes=pass_arrival_rate_nodes,
        )

        self.observation_space_busses = observation_space_busses
        self.observation_space_tfl = observation_space_tfl
        self.action_space_busses = action_space_busses
        self.action_space_tfl = action_space_tfl

        # NEW
        self.observation = None
        self.begin = True
        self.current_step = 0
        self.date_time_str = self.get_time_file_str()
        self.file_str = "monitor_output" + self.date_time_str + ".txt"
        self.file_str_kpi = result_folder + "kpi_output" + self.date_time_str + ".txt"
        self.log_file_str = "logfile.csv"
        self.log_file_trips = "logfile_trips" + self.date_time_str + ".json"
        self.qmix = qmix
        self.holding_fixed = holding_fixed
        self.positions = []
        self.routes = routes
        self.start_nodes = self.generate_start_nodes()
        self.passenger_id_generator = self.generator_passenger_id()
        self.processed_passengers = {}
        self.passenger_travel_time = 0
        self.passenger_waiting_time = 0
        self.passenger_in_vehicle_time = 0
        self.headway_reward = 0

        self.test_run = test_run
        self.run_logging = run_logging
        self.passenger_logging = passenger_logging
        self.final_reward_option = final_reward_option
        self.ta = ta
        self.observation_option = observation_option
        self.capacity_busses = capacity_busses
        self.dwell_time_function = dwell_time_function
        self.reward_function_option = reward_function_option
        self.monitoring = monitoring
        self.monitor_str = ""
        self.holding_times = holding_times
        self.holding_punish_param = holding_punish_param
        self.speed_busses = 0

        self.temp_travel_time_list = []
        self.mean_headway = None
        self.executed_holding_times = []
        self.executed_holding_times_stops = defaultdict(lambda : [])
        self.executed_tfl_actions_edges = defaultdict(lambda : [])
        self.occupancy_busses = []
        self.experienced_crowding = []
        self.cov_headways = []
        self.cov_headways_stops = defaultdict(lambda : [])
        self.excess_waiting_time = []
        self.load_busses_stops = defaultdict(lambda : defaultdict(lambda : []))
        self.kpi_logging = kpi_logging
        self.result_folder = result_folder
        self.obs_and_actions = defaultdict(lambda : {"obs": [], "acts": []})

        self.training_run = training_run
        self.action_needed_dict = {}

        for key, value in self.travel_time_edges.items():
            if type(value) is tuple:
                self.temp_travel_time_list.append(value[1])
            else:
                self.temp_travel_time_list.append(value)

        # self.headway_normalising_constant = (
        #     sum(self.temp_travel_time_list) + len(self.node_list) * self.step_size
        # ) / self.num_agents

        planned_driving_time_between_busses = sum(self.temp_travel_time_list) / self.num_agents

        # average_delay_tfl = (self.cycle_props[0][1]+1)/self.cycle_props[0][0]*self.cycle_props[0][1]/2
        self.average_delay_tfl = 41/75*20

        planned_driving_time_between_busses += (sum(self.traffic_light_on_edges)*self.average_delay_tfl) / self.num_agents

        self.headway_normalising_constant = max( 2*statistics.mean(self.pass_arrival_rate_nodes) / self.boarding_rate * planned_driving_time_between_busses , 2*self.step_size ) + planned_driving_time_between_busses

        # for number, node in enumerate(self.position_list):
        #     self.dict_headways[node] = {
        #         "busses_on_element": [],
        #         "pass_arrival_rate_node": self.pass_arrival_rate_nodes[number],
        #         "node_number": number,
        #     }
        #     self.dict_headways[node + "_edge"] = {
        #         "travel_time": self.temp_travel_time_list[number],
        #         "busses_on_element": [],
        #         "edge_number": number,
        #     }

    @property
    def next_passenger_id(self):
        return next(self.passenger_id_generator)

    def get_holding_benchmark_action(self, diff_with_planned_headway):
        action = None
        if diff_with_planned_headway > -15:
            action = 0
        elif diff_with_planned_headway > -45:
            action = 1
        elif diff_with_planned_headway > -75:
            action = 2
        elif diff_with_planned_headway > -105:
            action = 3
        elif diff_with_planned_headway > -135:
            action = 4
        elif diff_with_planned_headway > -165:
            action = 5
        else:
            action = 6

        return action

    def generate_headway_dict(self):
        headway_dict = {}
        for number, node in enumerate(self.position_list):
            headway_dict[node] = {
                "busses_on_element": [],
                "pass_arrival_rate_node": self.pass_arrival_rate_nodes[number],
                "node_number": number,
            }
            #todo dit erin of niet?
            headway_dict[node + "_edge"] = {
                "travel_time": self.temp_travel_time_list[number] + self.traffic_light_on_edges[number] * self.average_delay_tfl,
                "busses_on_element": [],
                "edge_number": number,
            }
        return headway_dict

    def generator_passenger_id(self):
        num = 0
        while True:
            yield num
            num += 1

    def get_time_file_str(self):
        return str(datetime.now()).replace(" ", "-").replace(".", "_").replace(":", "_")

    def generate_start_nodes(self):
        start_nums = []
        available_nodes_nums = [i for i in range(len(self.position_list))]

        for i in range(self.num_agents):
            chosen_node = random.choice(available_nodes_nums)
            available_nodes_nums.remove(chosen_node)
            start_nums.append(chosen_node)
        start_nums.sort()

        return [self.position_list[i] for i in start_nums]

    def get_random_node(self, excluded_possibility=None):
        options = self.position_list.copy()
        if excluded_possibility:
            options.remove(excluded_possibility)

        return random.choice(options)

    def create_edge_objects(self, adjacency_matrix, node_names, travel_time_edges):
        number_of_nodes = len(node_names)
        agent_number = self.num_agents

        for i in range(number_of_nodes):
            if self.traffic_light_on_edges[i]:
                agent_number_temp = agent_number
                agent_number += 1
            else:
                agent_number_temp = None

            for j in range(number_of_nodes):
                if adjacency_matrix[i][j] == 1:
                    name = node_names[i] + node_names[j]
                    self.edge_list.append(
                        Edge(
                            self.env,
                            network=self,
                            name=name,
                            edge_number=i,
                            travel_time=travel_time_edges[name],
                            contains_traffic_light=self.traffic_light_on_edges[i],
                            cycle_props=self.cycle_props[i],
                            agent_number=agent_number_temp,
                        )
                    )

    def create_nodes(
        self,
        node_names,
        slot_capacity_nodes,
        arrival_function_nodes,
        destination_distribution_nodes,
        pass_arrival_rate_nodes,
    ):
        for node_number, node_name in enumerate(node_names):
            self.node_list.append(
                Node(
                    self.env,
                    self,
                    name=node_name,
                    step_size=self.step_size,
                    slot_capacity=slot_capacity_nodes[node_number],
                    arrival_function=arrival_function_nodes[node_number],
                    destination_distribution=destination_distribution_nodes[
                        node_number
                    ],
                    pass_arrival_rate=pass_arrival_rate_nodes[node_number],
                    boarding_rate=self.boarding_rate,
                    alighting_rate=self.alighting_rate,
                )
            )

    def add_new_bus(self, bus_name, agent_number, capacity, route_nodes, start_node):
        self.busses.append(
            Bus(
                self.env,
                self,
                bus_name,
                agent_number,
                route_nodes,
                start_node,
                capacity=capacity,
                test_run=self.test_run,
                holding_fixed=self.holding_fixed,
            )
        )
        self.positions.append(start_node)

    def run_simulation(self):
        for i in range(self.num_agents):
            name = "bus" + str(i)
            self.add_new_bus(
                bus_name=name,
                agent_number=i,
                route_nodes=self.routes[i],
                start_node=self.start_nodes[i],
                capacity=self.capacity_busses[i],
            )

        for bus in self.busses:
            self.env.process(bus.run_bus())

    def monitor_simulation(self):
        while True:
            self.monitor_str += str(self.global_observation) + "\n"
            yield self.env.timeout(self.monitor_time_step)

    @property
    def global_observation(self):
        exp_crowding_temp_list = []
        observation = {
            "positions": {},
            "passengers_at_nodes": {},
            "passengers_in_busses": {},
            "headways": {},
        }

        for bus in self.busses:
            observation["positions"][bus.name] = bus.obs_position
            observation["passengers_in_busses"][bus.name] = bus.passenger_amount
            self.positions[bus.agent_number] = bus.position
            self.occupancy_busses.append(bus.passenger_amount)
            exp_crowding_temp_list.append(bus.passenger_amount)

        try:
            self.experienced_crowding.append( sum([i*i for i in exp_crowding_temp_list]) / sum(exp_crowding_temp_list) )
        except:
            self.experienced_crowding.append(0)

        for node in self.node_list:
            node.update_num_passengers()
            observation["passengers_at_nodes"][node.name] = node.passenger_amount

        _, headway_list = self.generate_backw_forw_headways_V2()

        headway_std_dev = statistics.stdev(headway_list)
        headway_mean = statistics.mean(headway_list)
        cov_headway = headway_std_dev / headway_mean

        self.cov_headways.append(cov_headway)

        self.excess_waiting_time.append( (sum([i*i for i in headway_list])/(2*sum(headway_list))) - (1/2) )

        observation["headways"]["std_dev"] = headway_std_dev
        observation["headways"]["individual_headway"] = headway_list

        return observation


    def get_position_bus(self, bus):
        current_node_number = self.position_list_obs.index(bus.obs_position)
        return current_node_number


    def generate_backw_forw_headways_V2(self):
        # dict_network = copy.deepcopy(self.dict_headways)
        dict_network = self.generate_headway_dict()
        # dict_network = self.dict_headways.copy()
        for bus in self.busses:
            postion_bus = bus.obs_position[1]
            node_or_edge = bus.exact_position[0]

            if node_or_edge == "node":
                dict_network[postion_bus]["busses_on_element"].append((bus, None))
            else:
                fraction = bus.exact_position[1]
                dict_network[postion_bus + "_edge"]["busses_on_element"].append(
                    (bus, fraction)
                )

        time_headway = 0
        headway_list = []
        encountered_bus = False
        number_first_bus = None

        for key, value in dict_network.items():
            if not value["busses_on_element"]:
                if "edge" in key:
                    time_headway += value["travel_time"]
                else:
                    time_headway += self.step_size + (
                        value["pass_arrival_rate_node"]
                        * (
                            time_headway
                            + self.node_list[
                                value["node_number"]
                            ].time_since_last_pickup
                        )
                        / self.boarding_rate
                    )

            else:
                if "edge" in key:
                    for k in range(len(value["busses_on_element"])):
                        if k == 0:

                            if not encountered_bus:
                                number_first_bus = value["busses_on_element"][k][
                                    0
                                ].agent_number
                                encountered_bus = True

                            time_headway += (
                                value["travel_time"] * value["busses_on_element"][k][1]
                            )  # the latter is the fraction
                            headway_list.append(time_headway)
                            time_headway = 0
                        elif k == (len(value["busses_on_element"]) - 1):
                            time_headway = value["travel_time"] * (
                                value["busses_on_element"][k][1]
                                - value["busses_on_element"][k - 1][1]
                            )
                            headway_list.append(time_headway)
                            time_headway = (
                                1 - value["busses_on_element"][k][1]
                            ) * value["travel_time"]
                        else:
                            time_headway = value["travel_time"] * (
                                value["busses_on_element"][k][1]
                                - value["busses_on_element"][k - 1][1]
                            )
                            headway_list.append(time_headway)
                            time_headway = 0
                else:
                    for k in range(len(value["busses_on_element"])):
                        if k == 0:

                            if not encountered_bus:
                                number_first_bus = value["busses_on_element"][k][
                                    0
                                ].agent_number
                                encountered_bus = True

                            time_headway += (
                                (
                                    time_headway
                                    - (
                                        (len(value["busses_on_element"]) - 1)
                                        * self.step_size
                                    )
                                )
                                * value["pass_arrival_rate_node"]
                                / self.boarding_rate
                            )
                            time_headway = max(0, time_headway)
                            headway_list.append(time_headway)
                            time_headway = 0
                        else:
                            time_headway = self.step_size
                            headway_list.append(time_headway)
                            time_headway = 0
        headway_list.append(time_headway)

        headway_list[0] = headway_list[0] + headway_list[-1]
        del headway_list[-1]

        # normalise values
        headway_list = [i / self.headway_normalising_constant for i in headway_list]

        headway_dict = {}

        for i in range(len(self.busses)):
            current_bus_number = (i + number_first_bus) % len(self.busses)

            if i == (len(self.busses) - 1):
                headway_dict[self.busses[current_bus_number].name] = (
                    headway_list[i],
                    headway_list[0],
                )
            else:
                headway_dict[self.busses[current_bus_number].name] = (
                    headway_list[i],
                    headway_list[i + 1],
                )

        self.mean_headway = statistics.mean(headway_list)

        return headway_dict, headway_list

    def generate_backw_forw_headways(self):
        headway_dict = {}

        for i in range(len(self.busses)):

            pos_bus = self.position_list_obs.index(self.busses[i].obs_position)
            update_time_bus = self.busses[i].last_position_update

            if i == (len(self.busses) - 1):
                pos_next_bus = self.position_list_obs.index(self.busses[0].obs_position)
                update_time_next_bus = self.busses[0].last_position_update
            else:
                pos_next_bus = self.position_list_obs.index(
                    self.busses[i + 1].obs_position
                )
                update_time_next_bus = self.busses[i + 1].last_position_update
            if i == 0:
                pos_prev_bus = self.position_list_obs.index(
                    self.busses[-1].obs_position
                )
                update_time_prev_bus = self.busses[-1].last_position_update
            else:
                pos_prev_bus = self.position_list_obs.index(
                    self.busses[i - 1].obs_position
                )
                update_time_prev_bus = self.busses[i - 1].last_position_update

            if pos_prev_bus == pos_bus:
                backward_diff = 0
                if pos_next_bus == pos_bus:
                    if (
                        update_time_bus < update_time_next_bus
                        and update_time_bus < update_time_prev_bus
                    ):
                        forward_diff = len(self.position_list_obs)
                    else:
                        forward_diff = 0
                elif pos_next_bus > pos_bus:
                    forward_diff = pos_next_bus - pos_bus
                elif pos_next_bus < pos_bus:
                    forward_diff = len(self.position_list_obs) - (
                        pos_bus - pos_next_bus
                    )

            if pos_prev_bus > pos_bus:
                backward_diff = len(self.position_list_obs) - (pos_prev_bus - pos_bus)
                if pos_next_bus == pos_bus:
                    forward_diff = 0
                else:
                    forward_diff = pos_next_bus - pos_bus

            if pos_prev_bus < pos_bus:
                backward_diff = pos_bus - pos_prev_bus
                if pos_next_bus == pos_bus:
                    forward_diff = 0
                elif pos_next_bus > pos_bus:
                    forward_diff = pos_next_bus - pos_bus
                elif pos_next_bus < pos_bus:
                    forward_diff = len(self.position_list_obs) - (
                        pos_bus - pos_next_bus
                    )

            headway_dict[self.busses[i].name] = (backward_diff, forward_diff)

        return headway_dict

    def generate_local_observations(self):
        observation_dict = {}

        # diff = forward_diff - backward_diff
        #
        # if backward_diff == 0 and forward_diff == 0:
        #     diff = -7
        #
        # observation_dict[self.busses[i].name] = diff
        try:
            time_observation = (self.step_counter / self.end_trigger) * 2 - 1
        except ZeroDivisionError:
            time_observation = -1
        except:
            raise Exception(
                "Something went wrong during the calculation of the time_observation variable"
            )
        if self.test_run:
            assert (
                time_observation >= -1 or time_observation <= 1
            ), "The time_observation was not between -1 and 1. It was {}".format(
                time_observation
            )

        # headway_dict = self.generate_backw_forw_headways()

        headway_dict, _ = self.generate_backw_forw_headways_V2()

        for bus in self.busses:

            if self.observation_option == "headway":
                if self.ta:
                    # KEY DIT MOET IK GEBRUIKEN VOOR TA
                    observation_dict[bus.name] = (
                        headway_dict[bus.name][0],
                        headway_dict[bus.name][1],
                        time_observation,
                    )
                else:
                    observation_dict[bus.name] = (
                        headway_dict[bus.name][0],
                        headway_dict[bus.name][1],
                    )
            elif self.observation_option == "headway_diff":
                if self.ta:
                    observation_dict[bus.name] = (
                        headway_dict[bus.name][0] - headway_dict[bus.name][1],
                        time_observation,
                    )
                else:
                    observation_dict[bus.name] = (
                        headway_dict[bus.name][0] - headway_dict[bus.name][1],
                        #todo hier moet de positional awareness aan of uit
                        # self.get_position_bus(bus=bus),
                    )
            elif self.observation_option == "benchmark":
                observation_dict[bus.name] = (headway_dict[bus.name][1],)

            elif self.observation_option == "simple_headway_forward":
                if bus.forward_headway:
                    observation_dict[bus.name] = (bus.forward_headway / self.headway_normalising_constant,)
                else:
                    observation_dict[bus.name] = (headway_dict[bus.name][1],)

            elif self.observation_option == "simple_headway_backward":
                if bus.backward_headway:
                    observation_dict[bus.name] = (bus.backward_headway / self.headway_normalising_constant,)
                else:
                    observation_dict[bus.name] = (headway_dict[bus.name][0],)

            elif self.observation_option == "simple_headway_diff":
                if bus.backward_headway and bus.forward_headway:
                    observation_dict[bus.name] = ((bus.backward_headway-bus.forward_headway) / self.headway_normalising_constant,)
                else:
                    observation_dict[bus.name] = (headway_dict[bus.name][0] - headway_dict[bus.name][1],)


            else:
                raise Exception("This observation option is not implemented yet.")

        return observation_dict

    def gaussian_func(self, x, a, mu, sigma):
        # optimal_distance = len( self.position_list_obs ) / self.num_agents
        # return (80 ** (-((x - optimal_distance) / 20) ** 2))
        # return (80 ** (-((x - optimal_distance) / 2) ** 2))
        return a ** (-(((x - mu) / sigma) ** 2))

    def generate_rewards(self, holding_time):
        """ Deze functie moet per agent de rewards berekenen, dit kan dan allemaal worden weggeschreven in een dict
        bijvoorbeeld
        """
        reward_dict = {}

        observ = self.generate_local_observations()

        for bus in self.busses:
            reward = 0
            if self.reward_function_option != "headway_diff":
                backward_headw = observ[bus.name][0]
                forward_headw = observ[bus.name][1]

            if self.reward_function_option == "headway_diff":
                diff = observ[bus.name][0]
            # diff = observ[bus.name][0]

            if self.reward_function_option == "fix_headway":
                # KEY ENV LOOPT AF ALS HEADWAY GOED IS (deze heb ik daarvoor gebruikt, sparse rewards dus)
                # if backward_headw > (
                #     len(self.position_list_obs) / self.num_agents + 15
                # ):
                if backward_headw > (self.mean_headway * 1.5):
                    reward = -1

            elif self.reward_function_option == "headway":
                # KEY ENV LOOPT AF BIJ SPECIFIEKE CONDITIE (zoals time trigger) (deze heb ik daarvoor gebruikt, sparse rewards dus)
                # if backward_headw > (
                #     len(self.position_list_obs) / self.num_agents + 10
                # ):
                #     reward = -0.1
                #
                # elif backward_headw < (
                #     len(self.position_list_obs) / self.num_agents + 2
                # ) and backward_headw > (
                #     len(self.position_list_obs) / self.num_agents - 2
                # ):
                #     reward = 1
                #     # self.headway_reward += 0.001
                #
                # self.headway_reward += 0.0001 * self.gaussian_func(
                #     x=backward_headw,
                #     a=80,
                #     mu=len(self.position_list_obs) / self.num_agents,
                #     sigma=2,
                # )

                if backward_headw > (self.mean_headway * 1.5):
                    reward = -0.1
                elif backward_headw < (self.mean_headway * 1.1) and backward_headw > (
                    self.mean_headway * 0.9
                ):
                    reward = 1 - self.holding_punish_param * holding_time

                self.headway_reward += 0.0001 * self.gaussian_func(
                    x=backward_headw, a=80, mu=self.mean_headway, sigma=0.25,
                )

            elif self.reward_function_option == "headway_diff":
                # KEY ENV LOOPT AF BIJ SPECIFIEKE CONDITIE (zoals time trigger) BUS MOET IN HET MIDDEN BLIJVEN
                # if abs(diff) < 3:
                #     reward = 1

                if abs(diff) < 0.25:
                    reward += 1 - self.holding_punish_param * holding_time

                self.headway_reward += 0.0001 * self.gaussian_func(
                    x=diff, a=80, mu=0, sigma=0.25,
                )

            elif self.reward_function_option == "headway_passagiers":
                # KEY ENV BELOOND OP BASIS VAN PASSAGIERS, STRAF VOOR EXTREME BUNCHING
                # if backward_headw > (
                #     len(self.position_list_obs) / self.num_agents + 25
                # ):
                #     reward += -0.1
                # if forward_headw < (len(self.position_list_obs) / self.num_agents - 11):
                #     reward += -0.1

                if backward_headw > (self.mean_headway * 2):
                    reward += -0.1
                if forward_headw < (self.mean_headway * 0.3):
                    reward += -0.1

            else:
                raise Exception("This reward option is not implemented yet.")

            if self.final_reward_option == "travel_time_only":
                reward = 0
                #todo --> weer aan voor headway diff als obs
                if abs(diff) > 0.9:
                    reward = -0.1
                # if forward_headw < 0.1:
                #     reward = -0.1

            reward_dict[bus.name] = reward

            # everything_zero_test = [ True if i == 0 else False for i in observ.values() ]
            #
            # if all(everything_zero_test):
            #     for bus in self.busses:
            #         reward_dict[bus.name] = len( self.position_list_obs )**2

        return reward_dict

    def set_actions(self, action_dict):
        for bus in self.busses:
            if self.test_run:
                assert (
                    bus.hold is None
                ), "Something went wrong. Bus {} received an action last step, but did not get to take it.".format(
                    bus.agent_number
                )

            bus.counter -= 1
            if bus.agent_number in action_dict:
                if self.qmix:
                    if self.holding_fixed:
                        bus.hold = action_dict[bus.agent_number][0]
                    if not self.holding_fixed:
                        bus.hold = float(action_dict[bus.agent_number][0])

                if not self.qmix:
                    if self.holding_fixed:
                        bus.hold = action_dict[bus.agent_number]
                    if not self.holding_fixed:
                        bus.hold = float(action_dict[bus.agent_number])

            elif self.training_run and bus.agent_number in self.action_needed_dict.keys():
                forward_headway = self.action_needed_dict[bus.agent_number][1]

                forward_headway_seconds = forward_headway * self.headway_normalising_constant
                diff_with_planned_headway = forward_headway_seconds - self.headway_normalising_constant

                bus.hold = self.get_holding_benchmark_action(diff_with_planned_headway=diff_with_planned_headway)

                del self.action_needed_dict[bus.agent_number]
                # remove_all_occurences_list(action_list, i)







            else:
                bus.hold = None

        for edge in self.edge_list:
            if edge.contains_traffic_light:
                if edge.agent_number in action_dict:
                    if self.qmix:
                        edge.traffic_light_action = action_dict[edge.agent_number][0]
                    else:
                        edge.traffic_light_action = action_dict[edge.agent_number]
                else:
                    edge.traffic_light_action = None



    def log_run(self):
        # This is for retracing the good and bad runs
        positions_exact = []
        observ = self.global_observation

        for bus in self.busses:
            positions_exact.append(bus.obs_position)

        with open(self.log_file_str, "a") as file:
            file.write(
                "{}, {}, {}, {}, {}\n".format(
                    sum(self.reward_list),
                    self.step_counter,
                    observ,
                    positions_exact,
                    self.file_str,
                )
            )
        if self.passenger_logging:
            # here every passenger trip can be logged
            with open(self.log_file_trips, "w") as writing_file:
                json.dump(self.processed_passengers, writing_file, indent=4)

    def reset(self):
        obs = {}

        self.start_nodes = self.generate_start_nodes()

        kwargs_dict = {
            "test_run": self.test_run,
            "monitoring": self.monitoring,
            "run_logging": self.run_logging,
            "passenger_logging": self.passenger_logging,
            "observation_space_busses": self.observation_space_busses,
            "observation_space_tfl": self.observation_space_tfl,
            "action_space_busses": self.action_space_busses,
            "action_space_tfl": self.action_space_tfl,
            "end_trigger_range": self.end_trigger_range,
            "final_reward_option": self.final_reward_option,
            "qmix": self.qmix,
            "holding_fixed": self.holding_fixed,
            "holding_times": self.holding_times,
            "ta": self.ta,
            "observation_option": self.observation_option,
            "reward_function_option": self.reward_function_option,
            "num_agents": self.num_agents,
            "routes": self.routes,
            "capacity_busses": self.capacity_busses,
            "node_names": self.position_list,
            "slot_capacity_nodes": self.slot_capacity_nodes,
            "arrival_function_nodes": self.arrival_function_nodes,
            "boarding_rate": self.boarding_rate,
            "alighting_rate": self.alighting_rate,
            "destination_distribution_nodes": self.destination_distribution_nodes,
            "pass_arrival_rate_nodes": self.pass_arrival_rate_nodes,
            "dwell_time_function": self.dwell_time_function,
            "adjacency_matrix": self.adjacency_matrix,
            "travel_time_edges": self.travel_time_edges,
            "traffic_light_on_edges": self.traffic_light_on_edges,
            "cycle_props": self.cycle_props,
            "holding_punish_param": self.holding_punish_param,
            "kpi_logging": self.kpi_logging,
            "result_folder": self.result_folder,
            "training_run": self.training_run,
        }

        self.__init__(**kwargs_dict)

        self.begin = False

        self.run_simulation()

        if self.monitoring:
            self.env.process(self.monitor_simulation())

        observ = self.generate_local_observations()

        for bus in self.busses:
            observ_for_rllib = []
            if bus.counter == 1:

                for j in range(len(observ[bus.name])):
                    observ_for_rllib.append(observ[bus.name][j])

                if self.qmix:
                    observ_for_rllib = [observ_for_rllib]

                obs[bus.agent_number] = observ_for_rllib

        for edge in self.edge_list:
            if edge.observation:
                if self.qmix:
                    obs[edge.agent_number] = [edge.observation]
                    edge.observation = None
                else:
                    obs[edge.agent_number] = edge.observation
                    edge.observation = None

        for agent_number, observation in obs.items():
            self.obs_and_actions[agent_number]["obs"].append(observation[0])


        if self.training_run:
            for i in range(self.num_agents_total):
                if i != 0 and i in obs.keys():
                    del obs[i]


        return obs

    def step(self, action_dict):

        self.step_counter += 1

        self.end_time = self.env.now + self.step_size

        obs, rew, done, info = {}, {}, {}, {}

        self.set_actions(action_dict)

        if self.begin:
            self.begin = False

            self.run_simulation()

            if self.monitoring:
                self.env.process(self.monitor_simulation())

        self.env.run(until=self.end_time)

        observ = self.generate_local_observations()

        done_boolean = False

        # done_list = [ True if i < 9 and i > -9 else False for i in observ.values() ]

        # KEY ENV LOOPT AF ALS HEADWAY GOED IS
        # done_list = [ True if i[0] < ( len(self.position_list_obs) / self.num_agents + 3 ) and
        #                       i[0] > ( len(self.position_list_obs) / self.num_agents - 3 ) else False for i in observ.values() ]

        # done_list = [ True if i < ( len(self.position_list_obs) / self.num_agents + 9 ) and
        #                       i > ( len(self.position_list_obs) / self.num_agents - 9 ) else False for i in observ.values() ]

        # KEY ENV LOOPT AF ALS HEADWAY GOED IS
        # if all(done_list):
        #     done_boolean = True

        trigger_boolean = False
        if self.step_counter >= self.end_trigger:
            trigger_boolean = True

        # KEY ENV LOOPT AF BIJ SPECIFIEKE CONDITIE
        # if sum(self.reward_list_rllib) > 12500:
        #     done_boolean = True

        # if self.env.now > 75000:
        #     done_boolean = True

        for bus in self.busses:
            observ_for_rllib = []

            for j in range(len(observ[bus.name])):
                observ_for_rllib.append(observ[bus.name][j])

            if bus.counter == 1:
                if self.qmix:
                    observ_for_rllib = [observ_for_rllib]

                obs[bus.agent_number] = observ_for_rllib
                info[bus.agent_number] = {}

            elif done_boolean or trigger_boolean:
                if self.qmix:
                    obs[bus.agent_number] = [observ_for_rllib]
                else:
                    obs[bus.agent_number] = observ_for_rllib

        for edge in self.edge_list:
            observation_edge = edge.observation
            if observation_edge:
                reward_edge = edge.reward_buffer[edge.observation_bus_number]


            if edge.observation:
                if self.qmix:
                    obs[edge.agent_number] = [edge.observation]
                    info[edge.agent_number] = {}
                else:
                    obs[edge.agent_number] = edge.observation
                    info[edge.agent_number] = {}
                edge.observation = None

                rew[edge.agent_number] = reward_edge
                edge.reward_buffer[edge.observation_bus_number] = None

            if edge.contains_traffic_light and (done_boolean or trigger_boolean):
                has_observation = False

                if observation_edge:
                    has_observation = True

                if self.qmix:
                    obs[edge.agent_number] = [observation_edge]
                else:
                    obs[edge.agent_number] = observation_edge

                if has_observation:
                    rew[edge.agent_number] = reward_edge
                else:
                    rew[edge.agent_number] = 0
                    obs[edge.agent_number] = [0]

                if trigger_boolean:
                    if self.final_reward_option == "off":
                        # KEY DIT GEBRUIKTE IK VOOR HEADW BASED REWARDS
                        rew[edge.agent_number] = -1
                    elif self.final_reward_option == "travel_time" or self.final_reward_option == "travel_time_only":
                        # KEY DIT GEBRUIKTE IK VOOR SPARSE REWARDS GEBASEERD OP PASSAGIERS, DUS HELE TIJD 0 REWARD EN OP HET EINDE DIT
                        # rew[bus.agent_number] = 100 * ( self.passenger_travel_time / self.end_trigger )
                        rew[edge.agent_number] = (
                            1000
                            * 1
                            / (
                                self.passenger_travel_time
                                / len(self.processed_passengers)
                            )
                        )
                    elif self.final_reward_option == "headway":
                        # KEY DIT GEBRUIKTE IK VOOR V2 VAN HEADW BASED REWARDS (zonder TA)
                        rew[edge.agent_number] = self.headway_reward

        # if self.run_logging:
        #     rewards = self.generate_rewards()
        #     self.reward_list.append(sum(rewards.values()))

        if done_boolean or trigger_boolean:
            if self.run_logging:
                self.log_run()
            if self.monitoring:
                with open(self.file_str, "w") as file:
                    file.write(self.monitor_str)
            if self.kpi_logging:

                load_per_bus_per_stop = {}

                for key, value in self.load_busses_stops.items():
                    load_per_bus_per_stop[key] = {stop: statistics.mean(load_list) for stop, load_list in value.items()}

                excess_waiting_time = {}

                for key, value in self.cov_headways_stops.items():
                    excess_waiting_time[key] = (sum([i*i for i in value]) / (2*sum(value))) - (self.headway_normalising_constant**2/(2*self.headway_normalising_constant))

                average_obs_per_action_busses = defaultdict(lambda : [])
                average_obs_per_action_tfls = defaultdict(lambda : [])

                for agent_number, obs_and_acts in self.obs_and_actions.items():
                    if agent_number < 6:
                        for number, action in enumerate(obs_and_acts["acts"]):
                            average_obs_per_action_busses[action].append(obs_and_acts['obs'][number])
                    else:
                        for number, action in enumerate(obs_and_acts["acts"]):
                            average_obs_per_action_tfls[action].append(obs_and_acts['obs'][number])

                with open(self.file_str_kpi, "w") as file:
                    kpi_dict = {
                        "Average_holding_time": statistics.mean(
                            self.executed_holding_times
                        ),
                        "Average_holding_actions": [self.executed_holding_times.count(0), self.executed_holding_times.count(30), \
                                                    self.executed_holding_times.count(60), self.executed_holding_times.count(90), \
                                                    self.executed_holding_times.count(120), \
                                                    self.executed_holding_times.count(150), self.executed_holding_times.count(180)],
                        "Average_travel_time_passengers": (self.passenger_travel_time)
                        / len(self.processed_passengers),
                        # "Average_occupancy_busses": statistics.mean(
                        #     self.occupancy_busses
                        # ),
                        "Average_passenger_waiting_time": self.passenger_waiting_time / len(self.processed_passengers),
                        "Average_passenger_in_vehicle_time": self.passenger_in_vehicle_time / len(self.processed_passengers),
                        "Average_speed_busses": self.speed_busses/(self.num_agents*self.end_trigger),
                        "Average_experienced_crowding": statistics.mean(self.experienced_crowding),
                        "Average_covariation_headway": statistics.mean(self.cov_headways),
                        "Average_covariation_headway_stops": {key: statistics.stdev(value)/statistics.mean(value) for key, value in self.cov_headways_stops.items()},
                        # "Average_excess_waiting_time": statistics.mean(self.excess_waiting_time),
                        "Average_excess_waiting_time": excess_waiting_time,
                        "Holding_per_stop": {key: statistics.mean(value) for key, value in self.executed_holding_times_stops.items()},
                        "Actions_tfl_edges": {key: [value.count(0), value.count(1), value.count(2)] for key, value in self.executed_tfl_actions_edges.items()},
                        "Load_per_bus_per_stop": load_per_bus_per_stop,
                        "Obs_and_actions_busses": {int(key): statistics.mean(value)*self.headway_normalising_constant for key, value in average_obs_per_action_busses.items()},
                        "Obs_and_actions_tfls": {int(key): statistics.mean(value)*self.headway_normalising_constant for key, value in average_obs_per_action_tfls.items()},
                    }
                    string_json = json.dumps(kpi_dict, indent=4)
                    file.write(string_json)

        for bus in self.busses:

            if bus.counter == 1:
                rew[bus.agent_number] = bus.reward_buffer
                self.reward_list_rllib.append(bus.reward_buffer)
                bus.reward_buffer = None

            if done_boolean:
                if bus.reward_buffer:
                    # KEY ENV LOOPT AF BIJ SPECIFIEKE CONDITIE
                    rew[bus.agent_number] = bus.reward_buffer
                    # KEY ENV LOOPT AF ALS HEADWAY GOED IS
                    # rew[bus.agent_number] = 10
                else:
                    rew[bus.agent_number] = 0
                    # KEY ENV LOOPT AF ALS HEADWAY GOED IS
                    # rew[bus.agent_number] = 10
                # done[bus.agent_number], info[bus.agent_number] = done_boolean, {}

            if trigger_boolean:
                if self.final_reward_option == "off":
                    # KEY DIT GEBRUIKTE IK VOOR HEADW BASED REWARDS
                    rew[bus.agent_number] = -1
                elif self.final_reward_option == "travel_time" or self.final_reward_option == "travel_time_only":
                    # KEY DIT GEBRUIKTE IK VOOR SPARSE REWARDS GEBASEERD OP PASSAGIERS, DUS HELE TIJD 0 REWARD EN OP HET EINDE DIT
                    # rew[bus.agent_number] = 100 * ( self.passenger_travel_time / self.end_trigger )
                    rew[bus.agent_number] = (
                        1000
                        * 1
                        / (self.passenger_travel_time / len(self.processed_passengers))
                    )
                elif self.final_reward_option == "headway":
                    # KEY DIT GEBRUIKTE IK VOOR V2 VAN HEADW BASED REWARDS (zonder TA)
                    rew[bus.agent_number] = self.headway_reward

            # if bus.agent_number in action_dict:
            #     rew[bus.agent_number] = rewards[bus.name]
            #     done[bus.agent_number], info[bus.agent_number] = done_boolean, {}

            # done[bus.agent_number], info[bus.agent_number] = done_boolean, {}

        if done_boolean or trigger_boolean:
            done["__all__"] = True
        else:
            done["__all__"] = False

        self.current_step += 1

        # print("\n\n\nOBS\n")
        # print(obs)
        # print("\nREW\n")
        # print(rew)
        # print("\nDONE\n")
        # print(done)
        # print("\nINFO\n")
        # print(info)

        for agent_number, observation in obs.items():
            self.obs_and_actions[agent_number]["obs"].append(observation[0])
        for agent_number, action in action_dict.items():
            self.obs_and_actions[agent_number]["acts"].append(action)


        if self.training_run:
            for i in range(self.num_agents_total):
                if i != 0 and i in obs.keys():
                    self.action_needed_dict[i] = obs[i]
                    del obs[i]
            for i in range(self.num_agents_total):
                if i != 0 and i in rew.keys():
                    del rew[i]
            for i in range(self.num_agents_total):
                if i != 0 and i in done.keys():
                    del done[i]
            for i in range(self.num_agents_total):
                if i != 0 and i in info.keys():
                    del info[i]


        return obs, rew, done, info


########################################################################################################################
#######################################--- Bus, edge and node classes ---###############################################
########################################################################################################################


class Edge:
    def __init__(
        self,
        env,
        name,
        edge_number,
        network,
        contains_traffic_light,
        cycle_props,
        agent_number=None,
        travel_time=150,
    ):
        self.name = name
        self.env = env
        self.network = network
        self.travel_time = travel_time
        self.contains_traffic_light = contains_traffic_light
        self.busses_on_edge = []
        self.edge_number = edge_number
        if type(self.travel_time) == tuple:
            self.average_travel_time = self.travel_time[1]
        else:
            self.average_travel_time = self.travel_time

        self.traffic_light_action = None
        self.agent_number = agent_number
        self.observation = None
        self.observation_bus_number = None
        self.reward_buffer = {i: 0 for i in range(self.network.num_agents)}
        self.cycle_props = (
            cycle_props  # tuple(cycle_length, red_to_green_moment) ; bijv (90, 65)
        )

    def get_gamma_time_fixed_minimum(self, mean, stdev, fixed_minimum, fixed_maximum):
        """
        Gamma distribution with mean and std and fixed minimum as inputted
        These parameters will be converted to scale and rate and then stochastic drawing takes place
        The minimum cannot be exceeded negatively, and will be subtracted first and added later again from the mean
        Finally, the maximum can also not be exeeded and the returned values are capped at the fixed_maximum value
        """
        # see https://www.rocscience.com/help/swedge/webhelp/swedge/Gamma_Distribution.htm
        # note that b here, is the scale parameter (theta on wiki), not the beta from wiki which is the rate
        # or inverse scale(which is 1/theta)
        mean = mean - fixed_minimum

        g_shape = (mean / stdev) ** 2
        g_scale = (stdev ** 2) / mean

        delay_distrib = np.random.gamma(g_shape, g_scale)
        delay_distrib += fixed_minimum
        delay_distrib = min(fixed_maximum, delay_distrib)
        return delay_distrib

    def get_lognormal_time_fixed_minimum(self, mean, stdev, fixed_minimum, fixed_maximum):
        """
        Log normal distribution with mean and std and fixed minimum as inputted
        The minimum cannot be exceeded negatively, and will be subtracted first and added later again from the mean
        Finally, the maximum can also not be exeeded and the returned values are redrawn if they exceed the fixed_maximum value
        """
        mean_log = (mean - fixed_minimum) / 60
        std_original = stdev / 60
        mean_original = np.log(mean_log) - 1 / 2 * std_original ** 2

        delay_distrib = np.random.lognormal(mean_original, std_original)
        delay_distrib = delay_distrib * 60
        delay_distrib += fixed_minimum

        while delay_distrib > fixed_maximum:
            delay_distrib = np.random.lognormal(mean_original, std_original)
            delay_distrib = delay_distrib * 60
            delay_distrib += fixed_minimum

        return delay_distrib

    def generate_traffic_light_obs(self, bus, headway_dict):

        observation = [None]  # [None, None]
        # observation[0], observation[1] = headway_dict[bus.name]
        observation[0] = headway_dict[bus.name][0] - headway_dict[bus.name][1]

        if self.network.observation_option == 'simple_headway_backward':
            if bus.backward_headway:
                observation[0] = bus.backward_headway / self.network.headway_normalising_constant

        self.observation = observation
        self.observation_bus_number = bus.agent_number

    def get_traffic_light_delay(self):
        cycle_length, red_to_green = self.cycle_props

        delay = None
        remainder = self.env.now % cycle_length
        diff = remainder - red_to_green

        if diff > 0:
            delay = 0
        else:
            delay = -diff

        if self.traffic_light_action == 1:
            delay += 10
        elif self.traffic_light_action == 2:
            delay = max(0, delay - 10)

        self.network.executed_tfl_actions_edges[self.name].append(self.traffic_light_action)
        self.traffic_light_action = None

        if self.network.test_run:
            assert (
                self.traffic_light_action
            ), "The traffic light did not receive an action, but it has to act."
            assert (
                delay >= 0
            ), "The delay from the traffic light was negative, this is not possible."

        return delay

    def generate_traffic_light_reward(self, bus, headway_dict):
        reward = 0
        # headways = []
        #
        # for bus, headway_obs in headway_dict.items():
        #     headways.append(headway_obs[0])
        #
        # average_headway = mean(headways)

        backw_headway_bus, forw_headway_bus = headway_dict[bus.name]
        diff = backw_headway_bus - forw_headway_bus

        if abs(diff) < 0.2:
            reward = 1

        # if self.network.final_reward_option == "travel_time_only":
        #     reward = 0
        #     if abs(diff) > 0.9:
        #         reward = -0.1

        self.reward_buffer[bus.agent_number] = reward

    def travel_through(self, bus):

        actual_travel_time = None
        if type(self.travel_time) == tuple:
            actual_travel_time = self.get_lognormal_time_fixed_minimum(
                self.travel_time[1],
                self.travel_time[2],
                self.travel_time[3],
                self.travel_time[4],
            )
        else:
            actual_travel_time = self.travel_time

        start_time = self.env.now
        bus.current_edge_props = (start_time, actual_travel_time)

        # TRAFFIC LIGHT SECTION
        # -----------------------------------------

        headway_dict, _ = self.network.generate_backw_forw_headways_V2()

        if self.contains_traffic_light:
            self.generate_traffic_light_obs(bus, headway_dict)

        yield self.env.timeout(self.network.step_size)

        # hier moet het stoplicht kunnen veranderen
        if self.contains_traffic_light:
            traffic_light_delay = self.get_traffic_light_delay()

            actual_travel_time += traffic_light_delay
        actual_travel_time -= self.network.step_size

        arrival_time = self.env.now + actual_travel_time

        # busses can not overtake each other
        if self.busses_on_edge:
            preceeding_bus_tuple = self.busses_on_edge[-1]
            if preceeding_bus_tuple[2] >= arrival_time:
                diff_arrival_times = preceeding_bus_tuple[2] - arrival_time
                if diff_arrival_times > 0:
                    actual_travel_time += diff_arrival_times + 1
                    arrival_time = self.env.now + actual_travel_time
                if self.network.test_run:
                    assert arrival_time >= (preceeding_bus_tuple[2] + 1)

        total_travel_time = actual_travel_time + self.network.step_size

        self.busses_on_edge.append((bus, start_time, arrival_time))

        bus.current_edge_props = (start_time, total_travel_time)

        yield self.env.timeout(actual_travel_time)

        self.generate_traffic_light_reward(bus, headway_dict)

        del self.busses_on_edge[0]


class Node:
    def __init__(
        self,
        env,
        network,
        name,
        step_size,
        arrival_function,
        destination_distribution,
        pass_arrival_rate,
        boarding_rate,
        alighting_rate,
        stop_capacity=999,
        slot_capacity=1,
    ):
        self.env = env
        self.name = name
        self.network = network
        self.passengers = []
        self.time_last_pickup = float(env.now)
        self.time_last_update = float(env.now)
        self.time_since_last_update = None
        self.stop_slot = simpy.Resource(env, capacity=slot_capacity)
        self.step_size = step_size
        self.arrival_function = arrival_function
        self.destination_distribution = destination_distribution
        self.pass_arrival_rate = pass_arrival_rate
        self.boarding_rate = boarding_rate
        self.alighting_rate = alighting_rate
        self.holding_times = []
        self.waiting_times = []
        self.visited_by_bus = False

    @property
    def passenger_amount(self):
        return len(self.passengers)

    @property
    def time_since_last_pickup(self):
        return self.env.now - self.time_last_pickup

    def calc_dwell_time(self, num_alighters, num_boarders):

        boarding_time = num_boarders / self.boarding_rate
        alighting_time = num_alighters / self.alighting_rate

        if self.network.dwell_time_function[0] == "sum":
            dwell_time = int(boarding_time + alighting_time)
        elif self.network.dwell_time_function[0] == "max_board_or_alight":
            dwell_time = int(max(boarding_time, alighting_time))
        if self.network.dwell_time_function[1] == "stochastic":
            dwell_time = min(2*dwell_time, max(0, int(np.random.normal(dwell_time, 1, 1))))

        dwell_time += self.step_size + 0.00000001

        return dwell_time

    def calc_counter(self, dwell_time):
        time_remaing_this_step = (
            self.step_size
            - ((float(self.env.now)) % self.step_size)
            - 0.0000000000000000000000000001
        )
        counter_value = int(
            ((dwell_time - time_remaing_this_step) // self.step_size) + 1
        )

        if self.network.test_run:
            assert (
                counter_value >= 1
            ), "The dwell time is too short, the bus arrives and has to take a decision within the same step."

        return counter_value

    def alight_bus(self, bus):
        num_of_alighters = 0
        for passenger in bus.passengers:
            if passenger.destination == self.name:
                # bus.passengers.remove(passenger)
                passenger.arrival_time = float(self.env.now)
                num_of_alighters += 1
                passenger.log_passenger()
        bus.passengers = [passenger for passenger in bus.passengers if passenger.destination != self.name]


        return num_of_alighters

    def board_bus(self, bus):
        number_of_boarders = min(
            self.passenger_amount, bus.capacity - bus.passenger_amount
        )

        if self.passengers:
            for i in range(number_of_boarders):
                boarding_passenger = self.passengers.pop(0)
                boarding_passenger.start_travel_time = float(self.env.now)
                # boarding_passenger.waiting_time = self.env.now - boarding_passenger.start_time
                bus.passengers.append(boarding_passenger)

            for waiting_passenger in self.passengers:
                waiting_passenger.denied_boarding += 1

        return number_of_boarders

    def pick_up_passengers(self, bus, hold=False):
        with self.stop_slot.request() as req:
            yield req
            bus.update_bus_status(status_tuple=(self.name, float(self.env.now), "P"))

            if self.visited_by_bus:
                bus.forward_headway = self.time_since_last_pickup
                self.network.busses[ (bus.agent_number+1) % len(self.network.busses) ].backward_headway = self.time_since_last_pickup
            self.visited_by_bus = True

            self.update_num_passengers()

            num_of_alighters = self.alight_bus(bus)

            num_of_boarders = self.board_bus(bus)

            self.network.cov_headways_stops[self.name].append(self.time_since_last_pickup)

            self.time_last_pickup = float(self.env.now)

            dwell_time = self.calc_dwell_time(num_of_alighters, num_of_boarders)
            bus.counter = self.calc_counter(dwell_time)

            if self.network.kpi_logging:
                self.network.load_busses_stops[bus.name][self.name].append(bus.passenger_amount)

            yield self.env.timeout(dwell_time)

            yield self.env.process(bus.execute_hold(self.name))

    def update_num_passengers(self):
        self.time_since_last_update = float(self.env.now) - self.time_last_update
        # DOOR DE int() KAN IK AFROND FOUTEN IN MIJN SYSTEEM KRIJGEN!!!
        # arriving_passenger_amount = int(
        #     min(999, max(int(self.time_since_last_update) / 10, 1))
        # )
        # arriving_passenger_amount = min(999, max( self.time_since_last_update / 10, 1) )
        arriving_passenger_amount = (
            self.time_since_last_update * self.pass_arrival_rate
        )  # 0.00833 #/ 120

        if self.arrival_function == "poisson":
            arriving_passenger_amount = np.random.poisson(arriving_passenger_amount)

        int_arriving_passenger_amount = int(arriving_passenger_amount)

        if self.destination_distribution == "random":
            destinations = [
                self.network.get_random_node(excluded_possibility=self.name)
                for i in range(int_arriving_passenger_amount)
            ]
        else:
            destinations = random.choices(
                self.network.position_list,
                weights=self.destination_distribution,
                k=int_arriving_passenger_amount,
            )

        # pass_batch_id = self.network.get_time_file_str() + str(random.random())

        # ids = [pass_batch_id + str(i) for i in range(int_arriving_passenger_amount)]

        ids = [
            self.network.next_passenger_id for i in range(int_arriving_passenger_amount)
        ]

        for i in range(int_arriving_passenger_amount):
            id = ids[i]  # self.network.next_passenger_id
            destination = destinations[i]
            self.passengers.append(
                Passenger(
                    self.env,
                    self.network,
                    passenger_id=id,
                    destination=destination,
                    start_node=self.name,
                )
            )
        self.time_last_update = float(self.env.now)


class Bus:
    def __init__(
        self,
        env,
        network,
        name,
        agent_number,
        route_nodes,
        start_node,
        test_run,
        capacity=9999999999,
        holding_fixed=True,
    ):
        self.env = env
        self.name = name
        self.agent_number = agent_number
        self.position = start_node
        self.last_position_update = None
        self.route_nodes = route_nodes
        self.obs_position = "D" + start_node
        self.capacity = capacity
        self.passengers = []
        self.network = network
        self.start_node = start_node
        self.node_gen = self.node_gen_f()
        self.edge_gen, self.edges_route = self.edge_gen_f()
        self.hold = None
        self.holding_fixed = holding_fixed
        self.counter = 999999
        self.reward_buffer = 0
        self.test_run = test_run
        self.current_edge_props = None  # start time, total travel time
        self.forward_headway = None
        self.backward_headway = None

    @property
    def passenger_amount(self):
        return len(self.passengers)

    @property
    def exact_position(self):
        node_or_edge, fraction_edge = None, None

        if self.obs_position[0] == "D":
            if self.env.now == 0:
                for edge in self.network.edge_list:
                    if edge.name[0] == self.position:
                        traveltime_edge = edge.average_travel_time
                starttime_edge = 0
            else:
                starttime_edge, traveltime_edge = (
                    self.current_edge_props[0],
                    self.current_edge_props[1],
                )

            node_or_edge = "edge"
            fraction_edge = (self.env.now - starttime_edge) / traveltime_edge
        else:
            node_or_edge = "node"

        return (node_or_edge, fraction_edge)

    def get_previous_node(self, current_node):
        index_current_node = self.route_nodes.index(current_node)
        if index_current_node > 0:
            previous_node = self.route_nodes[index_current_node - 1]
        else:
            previous_node = self.route_nodes[-1]

        return previous_node

    def get_coming_edge(self, current_node):
        index_current_node = self.route_nodes.index(current_node)
        coming_edge = self.edges_route[index_current_node]

        return coming_edge

    def node_gen_f(self):
        route_node_objects = []

        for node in self.route_nodes:
            for node_obj in self.network.node_list:
                if node == node_obj.name:
                    route_node_objects.append(node_obj)

        node_generator = itertools.cycle(route_node_objects)

        current_node = next(node_generator)

        while current_node.name != self.start_node:
            current_node = next(node_generator)

        return node_generator

    def edge_gen_f(self):
        edge_objects = []

        for i in range(len(self.route_nodes)):
            if i == len(self.route_nodes) - 1:
                for edge_obj in self.network.edge_list:
                    if self.route_nodes[i] + self.route_nodes[0] == edge_obj.name:
                        edge_objects.append(edge_obj)
            else:
                for edge_obj in self.network.edge_list:
                    if self.route_nodes[i] + self.route_nodes[i + 1] == edge_obj.name:
                        edge_objects.append(edge_obj)

        edge_generator = itertools.cycle(edge_objects)

        current_edge = next(edge_generator)

        while current_edge.name[1] != self.start_node:
            current_edge = next(edge_generator)

        return edge_generator, edge_objects

    def update_bus_status(self, status_tuple=None):
        # status_tuple is of format (position, time, type), where type is 'A', 'P' or 'D' for 'arrival', 'pick up' and 'departure'
        if status_tuple:
            self.position = status_tuple[0]
            self.obs_position = status_tuple[2] + status_tuple[0]
            self.last_position_update = status_tuple[1]

    def run_bus(self):
        for trip in self.edge_gen:
            yield self.env.process(trip.travel_through(self))
            arriving_node = next(self.node_gen)
            if trip.name[1] == arriving_node.name:
                self.update_bus_status(
                    status_tuple=(arriving_node.name, float(self.env.now), "A")
                )
                #todo
                # for passenger in self.passengers:
                #     passenger.log.append((arriving_node.name, 'A', self.env.now - passenger.start_travel_time, passenger.destination))


                self.current_edge_props = None
                yield self.env.process(arriving_node.pick_up_passengers(bus=self))
                self.network.speed_busses += 1
                self.update_bus_status(
                    status_tuple=(arriving_node.name, float(self.env.now), "D")
                )

                #todo
                # for passenger in self.passengers:
                #     passenger.log.append((arriving_node.name, 'D', self.env.now - passenger.start_travel_time, passenger.destination))


            else:
                print(trip.name)
                print(trip.name[1])
                print(arriving_node.name)
                print(self.name)
                raise Exception(
                    "The edges seem out of order with the nodes, please check route setup."
                )

    def execute_hold(self, node_name):
        if self.test_run:
            assert (
                self.hold is not None
            ), "Something went wrong, bus {} did not receive an action, but has to choose what to do.".format(
                self.agent_number
            )

        if self.hold is not None:
            holding_time = None
            if self.holding_fixed:
                if self.test_run:
                    assert str(self.hold) in self.network.holding_times, (
                        "Something went wrong, bus {} received an action that was \ "
                        "not in the holding_times dictionary, but it has bus.holding_fixed on True. The received"
                        "action was: {} {}  ".format(
                            self.name, type(self.hold), self.hold
                        )
                    )
                holding_tempvar = self.hold
                self.hold = None
                holding_time = self.network.holding_times[str(holding_tempvar)]

                yield self.env.timeout(holding_time)

            if not self.holding_fixed:
                holding_time = self.hold
                self.hold = None
                yield self.env.timeout(holding_time)

            # HIER MOET IK NU DE REWARD GAAN BEREKENEN
            rewards = self.network.generate_rewards(holding_time=holding_time)
            self.reward_buffer = rewards[self.name]
            self.network.executed_holding_times.append(holding_time)
            self.network.executed_holding_times_stops[node_name].append(holding_time)


class Passenger:
    def __init__(self, env, network, passenger_id, destination, start_node):
        self.env = env
        self.network = network
        self.id = passenger_id

        self.start_node = start_node
        self.destination = destination

        self.start_time = float(self.env.now)
        self.start_travel_time = None
        self.arrival_time = None

        self.denied_boarding = 0

        #todo
        self.log = []

    @property
    def waiting_time(self):
        return self.start_travel_time - self.start_time

    @property
    def travel_time(self):
        return self.arrival_time - self.start_travel_time

    @property
    def total_system_time(self):
        return self.arrival_time - self.start_time

    def log_passenger(self):
        self.network.processed_passengers[self.id] = {
            "start_node": self.start_node,
            "destination": self.destination,
            "start_time": self.start_time,
            "arrival_time": self.arrival_time,
            "waiting_time": self.waiting_time,
            "travel_time": self.travel_time,
            "total_system_time": self.total_system_time,
            "denied_boarding": self.denied_boarding,
            "log": self.log,
        }
        # self.network.passenger_travel_time += 1 / float( self.total_system_time )
        self.network.passenger_travel_time += float(self.total_system_time)
        self.network.passenger_waiting_time += float(self.waiting_time)
        self.network.passenger_in_vehicle_time += float(self.travel_time)


def main():
    kwargs_dict = {
        "edges": adjacency_matrix,
        "node_names": node_list,
        "num_agents": 3,
        "qmix": False,
        "holding_fixed": False,
        "incl_pass_stops": False,
    }

    mijnEnv = NetworkEnv(**kwargs_dict)

    action_dict = {0: 0, 1: 1, 2: 1}

    for i in range(800):
        print(
            "-----------------------------------------------------------------------------------------------------"
        )
        print(
            "-----------------------------------------------------------------------------------------------------"
        )
        print(
            "-----------------------------------------------------------------------------------------------------"
        )
        mijnEnv.step(action_dict)


if __name__ == "__main__":
    main()
