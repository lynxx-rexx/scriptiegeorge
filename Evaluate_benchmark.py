#from sklearn import tree
import ray
from ray import rllib
from ray import tune
from ray.rllib.agents.registry import get_agent_class
from ray.rllib.models import ModelCatalog
from ray.tune import run_experiments
from ray.tune.registry import register_env
from ray.rllib.agents.ppo import PPOTrainer
import gym
import numpy as np
import mlflow
import os
import glob
import json
import statistics

# CHECKLIST
# Are the env settings according to the scenario?
# Is true_multi_agent set?
# Is it a test run? --> is all the logging set correctly?
# Is it a learning session or an evaluation session? --> explore: True/False
# Is the number of iterations and samples set correct?
# Should it start from scratch or build upon a given policy?
# Set the correct name etc. for the MLFlow documentation

# Import environment definition
# from environment import IrrigationEnv
from Network_implementation_V2 import NetworkEnv




result_folder = "results_no_scenario5\\"
average_dict = {
    "Average_holding_time": [],
    "Average_holding_actions": [],
    "Average_travel_time_passengers": [],
    "Average_passenger_waiting_time": [],
    "Average_passenger_in_vehicle_time": [],
    "Average_speed_busses": [],
    "Average_experienced_crowding": [],
    "Average_covariation_headway": [],
    "Average_covariation_headway_stops": [],
    "Average_excess_waiting_time": [],
    "Actions_tfl_edges": [],
    "Holding_per_stop": [],
    "Load_per_bus_per_stop": [],
    "Obs_and_actions_busses": [],
    "Obs_and_actions_tfls": [],
}




if __name__=='__main__':
        # json_files = [pos_json for pos_json in os.listdir(result_folder) if pos_json.endswith('.txt')]
        #
        # for number, json_file in enumerate(json_files):
        #     with open(result_folder + json_file, 'r') as reading_file:
        #         temp_string = reading_file.read()
        #         if "Average" in temp_string:
        #             temp_string = temp_string.split("}")[0]+"}"
        #             json_files[number] = json.loads(temp_string)
        #         else:
        #             del json_files[number]
        #
        #
        # for json_file in json_files:
        #    for key, value in json_file.items():
        #        average_dict[key].append(value)
        #
        #
        #
        #
        # for key, value in average_dict.items():
        #     average_dict[key] = (statistics.mean(value), value)
        #
        # with open(result_folder+"average_kpis.json", 'w') as writing_file:
        #     json.dump(average_dict, writing_file, indent=4)
        #
        json_files = [pos_json for pos_json in os.listdir(result_folder) if pos_json.endswith('.txt')]
        json_files2 = [None] * len(json_files)

        for number, json_file in enumerate(json_files):
            with open(result_folder + json_file, 'r') as reading_file:
                temp_string = reading_file.read()
                if "Average" in temp_string:

                    while not json_files2[number]:
                        try:
                            json_files2[number] = json.loads(temp_string)
                        except:
                            temp_string = temp_string[:-1]

                    # temp_string = temp_string.split("}")[0]+"}"
                    # json_files[number] = json.loads(temp_string)
                else:
                    del json_files[number]

        for json_file in json_files2:
            for key, value in json_file.items():
                average_dict[key].append(value)

        for key, value in average_dict.items():
            try:
                average_dict[key] = (statistics.mean(value), value)
            except:
                print(" kon niet voor ", key)

        with open(result_folder + "average_kpis.json", 'w') as writing_file:
            json.dump(average_dict, writing_file, indent=4)

