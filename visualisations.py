import matplotlib.pyplot as plt
import numpy as np
import ast

plt.rcParams.update({'font.size': 18})
num_busses = 6
num_stops = 12



def monitor_simulation(self):

            for node in self.node_list:
                node_array = np.array(self.node_dict[node.name]).T

                plt.plot(node_array[0], node_array[1], label=node.name)

            plt.title('Number of passengers at stops')
            plt.legend()
            plt.show()


position_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']

position_list_obs = []

for i in position_list:
    position_list_obs.append("A" + i)
    position_list_obs.append("P" + i)
    position_list_obs.append("D" + i)

file_str = 'monitor_output2021-05-25-13_25_34_012745.txt'
# file_str = 'monitor_output2021-05-22-16_18_44_904788.txt'
# file_str = 'monitor_output2021-05-24-17_04_45_678290.txt'
# file_str = 'monitor_output2021-04-14-15_44_28_129463.txt'
# file_str = 'monitor_output2021-03-03-15_43_50_789449.txt'
# file_str = 'monitor_output2021-03-03-15_37_07_392327.txt'
# file_str = 'monitor_output2021-03-03-15_37_09_407980.txt'

# file_str = 'monitor_output2021-01-25-13_22_42_248710.txt'
# file_str = 'monitor_output2021-01-25-13_22_45_766894.txt'
# file_str = 'monitor_output2021-01-25-13_22_48_282883.txt'
# file_str = 'monitor_output2021-01-25-13_22_48_367501.txt'
# file_str = 'monitor_output2021-01-25-13_11_13_510214.txt'
# file_str = 'monitor_output2021-01-08-14_02_08_952625.txt'
# file_str = 'monitor_output2021-01-08-14_02_13_716316.txt'
# file_str = 'monitor_output2021-01-25-13_22_49_886643.txt'



with open(file_str) as file:
    observations0 = file.readlines()
    # observations = [ ast.literal_eval(i) for i in observations]
    observations = []
    for i in observations0:

        try:
            observations.append(ast.literal_eval(i))
        except:
            continue

        # if i == '}\n' or i == '}}\n' or i == '\n' or i == '39}}\n':
        #     continue
        #
        # try:
        #     observations.append( ast.literal_eval(i) )
        # except SyntaxError:
        #     try:
        #         observations.append( ast.literal_eval( '{' + i ) )
        #     except SyntaxError:
        #         print(i)



# PLOT 1
for bus in range(num_busses):
    # positions = [ position_list.index( i['positions']['bus{}'.format(bus)] ) for i in observations ]
    positions = [ position_list_obs.index( i['positions']['bus{}'.format(bus)] ) for i in observations ]
    xs = range( len( observations ) )
    # xs = [i for i in range( len( observations) ) ]
    plt.plot(xs, positions, label= 'Bus {}'.format(bus) )

plt.title('Position of the busses in the network')
plt.xlabel('Timesteps', fontsize=18)
plt.ylabel('Position of the busses in the network (nodes)', fontsize=18)
plt.legend()
plt.show()





# PLOT 2
for bus in range(num_busses):

    for i in observations:
        try:
            waarde = i['passengers_in_busses']['bus{}'.format(bus)]
        except:
            print(i)
            print('test _________________________________-----------------')


    num_of_passengers = [ i['passengers_in_busses']['bus{}'.format(bus)]  for i in observations ]
    xs = range( len( observations ) )
    # xs = [i for i in range( len( observations) ) ]
    plt.plot(xs, num_of_passengers, label= 'Bus {}'.format(bus) )

plt.title('Number of passengers in each bus')
plt.xlabel('Timesteps', fontsize=18)
plt.ylabel('Number of passengers in each bus', fontsize=18)
plt.legend()
plt.show()




# PLOT 3
for stop in range(num_stops):
    num_of_passengers = [ i['passengers_at_nodes'][position_list[stop]]  for i in observations ]
    xs = range( len( observations ) )
    # xs = [i for i in range( len( observations) ) ]
    plt.plot(xs, num_of_passengers, label= 'Stop {}'.format(position_list[stop]) )


plt.title('Number of passengers at stops')
plt.xlabel('Timesteps', fontsize=18)
plt.ylabel('Number of passengers at each stop', fontsize=18)
plt.legend()
plt.show()
