# import pandas as pd
import numpy as np
# import simpy
# import random

# import statistics
# from functools import wraps, partial
import matplotlib.pyplot as plt
# import itertools
# import gym

# import ray
# from ray import rllib
# from ray import tune
import json
# import gym
# import pickle

# import mlflow

from _collections import defaultdict

# import copy
# import seaborn as sns

import scipy.stats as stats
import math
import statistics

import pandas as pd









# PLOT VAN LEARNING CURVE
#todo
# printen met groter scherm of kleinere letters

plt.rcParams.update({'font.size': 18})
scenario_names = ['Idealised', 'Stochastic', 'Stochastic - Traffic Light Control', 'Deterministic - Heterogeneous', 'Realistic']






for j in range(5):

    df_mean = pd.DataFrame()
    df_min = pd.DataFrame()
    df_max = pd.DataFrame()

    for i in range(5):
        data = pd.read_csv('V2_progess' + '\\scenario' + str(j+1) + '\\progress' + str(i) + '.csv')

        mean_data = data['episode_reward_mean']
        min_data = data['episode_reward_min']
        max_data = data['episode_reward_max']

        plt.subplot(2, 3, (j+1))
        plt.plot(mean_data, color='#247BA0', alpha=0.1)
        plt.plot(min_data, color='#F25F5C', alpha=0.1)
        plt.plot(max_data, color='#70C1B3', alpha=0.1)

        df_mean = df_mean.append(mean_data, ignore_index=True)
        df_min = df_min.append(min_data, ignore_index=True)
        df_max = df_max.append(max_data, ignore_index=True)

    mean_mean_data = df_mean.mean(axis=0)
    mean_min_data = df_min.mean(axis=0)
    mean_max_data = df_max.mean(axis=0)
    plt.subplot(2,3, (j+1))
    plt.plot(mean_mean_data, color = '#247BA0', label='Mean reward')
    plt.plot(mean_min_data, color = '#F25F5C', label='Minimal reward')
    plt.plot(mean_max_data, color = '#70C1B3', label='Maximal reward')
    # plt.xlabel('Training iteration')
    # plt.title('Learning curve')
    # plt.ylabel('Episode reward')
    # plt.show()
    #
    plt.title(scenario_names[j])
    if j > 1:
        plt.xlabel('Training iteration')
    if j == 0 or j == 3:
        plt.ylabel('Episode reward')
    if j == 0 or j == 1:
        plt.xticks([0, 50, 100, 150, 200],  [])
    plt.legend(loc='lower right', framealpha=0.4)
plt.show()









# learning curve example
for j in range(1):

    df_mean = pd.DataFrame()
    df_min = pd.DataFrame()
    df_max = pd.DataFrame()

    for i in range(1):
        data = pd.read_csv('V2_progess' + '\\scenario' + str(j+2) + '\\progress' + str(i) + '.csv')

        mean_data = data['episode_reward_mean']
        min_data = data['episode_reward_min']
        max_data = data['episode_reward_max']

        # # plt.subplot(2, 3, (j+1))
        # plt.plot(mean_data, color='#247BA0', alpha=0.1)
        # plt.plot(min_data, color='#F25F5C', alpha=0.1)
        # plt.plot(max_data, color='#70C1B3', alpha=0.1)

        df_mean = df_mean.append(mean_data, ignore_index=True)
        df_min = df_min.append(min_data, ignore_index=True)
        df_max = df_max.append(max_data, ignore_index=True)

    mean_mean_data = df_mean.mean(axis=0)
    mean_min_data = df_min.mean(axis=0)
    mean_max_data = df_max.mean(axis=0)
    # plt.subplot(2,3, (j+1))
    plt.plot(mean_mean_data, color = '#247BA0', label='Mean reward')
    plt.plot(mean_min_data, color = '#F25F5C', label='Minimal reward')
    plt.plot(mean_max_data, color = '#70C1B3', label='Maximal reward')
    # plt.xlabel('Training iteration')
    # plt.title('Learning curve')
    # plt.ylabel('Episode reward')
    # plt.show()
    #
    plt.title("Learning curve")
    if j > 1:
        plt.xlabel('Training iteration')
    if j == 0 or j == 3:
        plt.ylabel('Episode reward')
    if j == 0 or j == 1:
        plt.xticks([0, 50, 100, 150, 200],  [])
    plt.legend(loc='lower right', framealpha=0.4)
plt.show()