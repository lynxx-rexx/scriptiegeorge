translate_dict = {}

string = 'abcdefghijklmnopqrstuvwxyz'

for i in range(len(string)):
    next = (i+5) % 26
    translate_dict[string[i]] = string[next]



decode_string = "wznoz fviydyvozi, ydo wzmdxco en hdnnxcdzi hjzdgdef gzznwvvm, hvvm czzao rvvmyzqjggz diajmhvodz jqzm yz gtiss: yz gtiss rjmyo gdzqzm idzo oz qzzg vvi yz oviy bzqjzgy. zi qzmbzzo qjjmvg idzo: idzon dn rvo czo gdefo"

decoded = ""

for sign in decode_string:
    if sign in string:
        decoded += translate_dict[sign]
    else:
        decoded += sign


print(decoded)