import pandas as pd
import numpy as np
import simpy
import random
import statistics
from functools import wraps, partial
import matplotlib.pyplot as plt
import itertools
from _collections import defaultdict



class Network:
    def __init__(self, env, edges, nodeNames):
        self.env = env
        self.node_list = []
        self.edge_list = []
        self.create_edge_objects(env, edges, nodeNames)
        self.create_nodes(env, nodeNames)
        self.busses = []



    def create_edge_objects(self, env, edges, nodeNames):
        number_of_nodes = len(nodeNames)

        for i in range(number_of_nodes):
            for j in range(number_of_nodes):
                if edges[i][j] == 1:
                    self.edge_list.append(Edge(env, name= nodeNames[i] + nodeNames[j] ))


    def create_nodes(self, env, nodeNames):
        for nodeName in nodeNames:
            self.node_list.append(Node(env, nodeName))

    def add_new_bus(self, bus_name, route_nodes):
        self.busses.append( Bus(self.env, self, bus_name, route_nodes) )




class Edge:
    def __init__(self,env, name, travel_time=30):
        self.name = name
        self.env = env
        self.travel_time = travel_time

    def travel_through(self):
        print('The bus starts to travel from {} to {} at {}'.format(self.name[0], self.name[1], self.env.now ) )
        yield self.env.timeout(self.travel_time)
        # return 30



class Node:
    def __init__(self,env, name, init_passengers=2, stop_capacity=999):
        self.env = env
        self.name = name
        self.passengers = simpy.Container(env, init=init_passengers, capacity=stop_capacity)
        self.time_last_pickup = env.now
        self.time_last_update = env.now
        self.time_since_last_pickup = None
        self.time_since_last_update = None
        self.stop_slot = simpy.Resource(env, capacity=1)


    def pick_up_passengers(self, bus, hold = False):
        with self.stop_slot.request() as req:
            yield req
            # print('bus {} heeft nu toegang tot {} at {}'.format(bus.name, self.name, self.env.now))
            self.update_num_passengers()
            # print('The amount of passengers at stop {} are {} at {}'.format(self.name, self.passengers.level, self.env.now ) )
            print('Starting boarding')
            dwell_time = int(self.passengers.level / 5) + 3
            bus.update_bus_status(delta_passengers=self.passengers.level)
            self.passengers.get(self.passengers.level)
            self.time_last_pickup = self.env.now
            yield self.env.timeout(dwell_time)
            if bus.hold:
                yield self.env.timeout(20)
            # print('bus {} is nu klaar en geeft de halte {} vrij na een dwell time van {} at {}'.format(bus.name, self.name, dwell_time, self.env.now))


    def update_num_passengers(self):
        self.time_since_last_update = self.env.now - self.time_last_update
        # DOOR DE int() KAN IK AFROND FOUTEN IN MIJN SYSTEEM KRIJGEN!!!
        self.passengers.put( int ( self.time_since_last_update) + 1 )
        # print('de passagiers aantallen op {} zijn verhoogd met {}'.format(self.name, int ( self.time_since_last_update) + 1))
        self.time_last_update = self.env.now

class Bus:
    def __init__(self, env, network, name, route_nodes, position=None, capacity=60, num_passengers=0):
        self.env = env
        self.name = name
        self.position = position
        self.capacity = capacity
        self.num_passengers = num_passengers
        self.network = network
        self.route_nodes = route_nodes
        self.node_gen = self.node_gen()
        self.edge_gen = self.edge_gen()
        self.hold = None

    def node_gen(self):
        route_node_objects = []

        for node in self.route_nodes:
            for node_obj in self.network.node_list:
                if node == node_obj.name:
                    route_node_objects.append(node_obj)

        return itertools.cycle(route_node_objects)


    def edge_gen(self):
        edge_objects = []

        for i in range(len(self.route_nodes)):
            if i == len(self.route_nodes) - 1:
                for edge_obj in self.network.edge_list:
                    if self.route_nodes[i] + self.route_nodes[0] == edge_obj.name:
                        edge_objects.append(edge_obj)
            else:
                for edge_obj in self.network.edge_list:
                    if self.route_nodes[i] + self.route_nodes[i + 1] == edge_obj.name:
                        edge_objects.append(edge_obj)

        return itertools.cycle(edge_objects)



    def update_bus_status(self, status_tuple=None, delta_passengers=None):
        # status_tuple is of format (position, time, type), where type is 'A' or 'D' for 'arrival' and 'departure'
        if status_tuple:
            self.position = status_tuple[0]

        if delta_passengers:
            self.num_passengers += delta_passengers


    def run_bus(self):
        next(self.node_gen)

        for trip in self.edge_gen:
            yield self.env.process( trip.travel_through() )
            arriving_node = next(self.node_gen)
            if trip.name[1] == arriving_node.name:
                print('bus {} arrives at stop {}'.format(self.name, arriving_node.name))
                self.update_bus_status(status_tuple=(arriving_node.name, self.env.now, 'A'))
                yield self.env.process( arriving_node.pick_up_passengers(bus=self) )
                self.update_bus_status(status_tuple= (arriving_node.name, self.env.now, 'D') )

            else:
                print(trip.name)
                print(trip.name[1])
                print(arriving_node.name)
                print(self.name)
                raise Exception('The edges seem out of order with the nodes, please check route setup')









def run_simulation(env, network, routes, number_of_busses=2):
    for i in range(number_of_busses):
        name = 'bus' + str(i)
        network.add_new_bus(name, routes[i])

    for bus in network.busses:
        env.process( bus.run_bus() )
        yield env.timeout(30)







def monitor_simulation(env, network, node_dict, bus_dict, position_list, duration, monitor_time_step):

    while True:
        for bus in network.busses:
            print('Bus {} is now at location {}'.format(bus.name, bus.position))
            # print('Bus {} has now {} passengers on board'.format(bus.name, bus.num_passengers))
            bus_dict[bus.name].append((env.now, bus.position, bus.num_passengers))

        for node in network.node_list:
            node.update_num_passengers()
            node_dict[node.name].append( (env.now, node.passengers.level) )

        yield env.timeout(5)

        if env.now > (duration - 2*monitor_time_step):

            for bus in network.busses:

                bus_list = bus_dict[bus.name]
                bus_converted_list = [ [i[0], position_list.index(i[1])] for i in bus_list ]
                bus_array = np.array(bus_converted_list).T
                plt.plot(bus_array[0], bus_array[1], label=bus.name)

            plt.title('Position of the busses in the network')
            plt.legend()
            plt.show()

            for bus in network.busses:
                bus_list = bus_dict[bus.name]
                bus_array = np.array(bus_list).T
                plt.plot(bus_array[0], bus_array[2], label=bus.name)

            plt.title('Number of passengers in each bus')
            plt.legend()
            plt.show()

            for node in network.node_list:
                node_array = np.array( node_dict[node.name] ).T

                plt.plot( node_array[0], node_array[1], label=node.name )

            plt.title('Number of passengers at stops')
            plt.legend()
            plt.show()




def generate_global_observation(env, network, bus_dict, node_dict):
    # Wat nou als je d.m.v. sin en cos de posities van de bussen op de cirkel van de lijn plaatst, dan wordt het verband
    # tussen de locaties duidelijker
    observation = {'positions': [], 'passengers_at_nodes': {'A': None, 'B': None, 'C': None, 'D': None}}
    for bus, status in bus_dict.items():
        observation['positions'].append(status[-1][1])

    for node in network.node_list:
        node.update_num_passengers()
        observation['passengers_at_nodes'][node.name] = node.passengers.level


    return observation

def generate_rewards(env, network, observation):
    """ Deze functie moet per agent de rewards berekenen, dit kan dan allemaal worden weggeschreven in een dict
    bijvoorbeeld
    """
    reward_dict = {}

    for bus in network.busses:
        reward = 0
        reward -= ( observation['positions'].count(bus.position) - 1 )
        total_waiting_passengers = sum( [ value for key, value in observation['passengers_at_nodes'].items() ] )
        reward -= int(total_waiting_passengers/100)

        reward_dict[bus.name] = reward
        
    return reward_dict





def generate_local_observation():
    print('Deze geeft de observatie van 1 bus te terug')
    print('Die bevat de passagiers aantallen van de haltes en de voor en achter headway')



def pre_step_func(env, network, bus_dict, node_dict, reward_list):
    print('Hier komt straks een generator die elke 5 sec een obs aanmaakt en de reward berekend')

    while True:
        obs = generate_global_observation(env, network, bus_dict, node_dict)
        rewards = generate_rewards(env, network, obs)

        print(obs)
        # print(rewards)

        reward_list.append( sum( rewards.values() ) )

        step_func(env, network, bus_dict, node_dict, reward_list)

        yield env.timeout(5)


def step_func(env, network, bus_dict, node_dict, reward_list):
    """
    Deze functie krijgt straks een action dict binnen

    en geeft het volgende terug:
    obs, rew, done, info

    :param env:
    :param network:
    :param bus_dict:
    :param node_dict:
    :param reward_list:
    :return:
    """

    for bus in network.busses:

        if bus.name == 'bus1':
            bus.hold = random.choice( [True, False] )



def main():
    # GLOBAL PARAMETERS
    bus_dict = defaultdict(lambda: [])
    node_dict = defaultdict(lambda: [])
    reward_list = []

    position_list = [None, 'A', 'B', 'C']

    duration = 900
    monitor_time_step = 5

    env = simpy.Environment()

    mijnNetwerk = Network(env, [[0,1,0,1], [1,0,1,0], [1,1,0,1], [1,0,1,0]], ['A', 'B', 'C', 'D'])

    env.process( run_simulation(env, mijnNetwerk, [['A', 'B', 'C'], ['A', 'B', 'C'] ] ) )

    env.process( monitor_simulation(env, mijnNetwerk, node_dict, bus_dict, position_list, duration, monitor_time_step) )

    env.process( pre_step_func(env, mijnNetwerk, bus_dict, node_dict, reward_list) )

    env.run(until= duration)


    print('Total reward:\t', sum(reward_list) )

    plt.plot(reward_list, label = 'Total reward over time')
    plt.legend()
    plt.show()

    # obs = generate_global_observation(env, mijnNetwerk, bus_dict, node_dict)
    #
    # generate_rewards(env, mijnNetwerk, obs)


if __name__ == '__main__':
    main()