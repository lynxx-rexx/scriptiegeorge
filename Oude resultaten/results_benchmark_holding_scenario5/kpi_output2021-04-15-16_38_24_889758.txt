{
    "Average_holding_time": 25.722273998136068,
    "Average_holding_actions": [
        691,
        156,
        83,
        34,
        12,
        37
    ],
    "Average_travel_time_passengers": 2783.9986364644105,
    "Average_passenger_waiting_time": 351.4263496603423,
    "Average_passenger_in_vehicle_time": 2432.572286804102,
    "Average_speed_busses": 0.044708333333333336,
    "Average_experienced_crowding": 115.16763280542573,
    "Average_covariation_headway": 0.32498476580795427,
    "Average_covariation_headway_stops": {
        "H": 0.27332476048006205,
        "I": 0.2763159906694508,
        "G": 0.23823061053252895,
        "L": 0.2624897368115183,
        "J": 0.26512726957142596,
        "B": 0.24958622187462812,
        "A": 0.24875118890008882,
        "K": 0.2603242760053953,
        "C": 0.19340670668621812,
        "D": 0.19512808422191966,
        "E": 0.19412366068116937,
        "F": 0.21220526829011166
    },
    "Average_excess_waiting_time": {
        "H": 81.35302449688686,
        "I": 79.94863186841053,
        "G": 77.70448423470589,
        "L": 75.30689017159204,
        "J": 75.66839345934233,
        "B": 74.9062848550953,
        "A": 75.47549982483741,
        "K": 76.85030768987951,
        "C": 67.77282650715335,
        "D": 70.38216882344449,
        "E": 71.74430090585622,
        "F": 75.43430162262086
    },
    "Holding_per_stop": {
        "L": 30.666666666666668,
        "J": 32.666666666666664,
        "H": 23.59550561797753,
        "B": 22.666666666666668,
        "I": 18,
        "G": 18.53932584269663,
        "A": 28.333333333333332,
        "K": 22.666666666666668,
        "C": 30.674157303370787,
        "D": 20.56179775280899,
        "E": 17.191011235955056,
        "F": 43.29545454545455
    },
    "Actions_tfl_edges": {
        "FG": [
            89,
            0,
            0
        ],
        "GH": [
            90,
            0,
            0
        ],
        "HI": [
            90,
            0,
            0
        ],
        "JK": [
            90,
            0,
            0
        ],
        "CD": [
            89,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "H": 130.86666666666667,
            "I": 127.2,
            "J": 118.46666666666667,
            "K": 110,
            "L": 102.33333333333333,
            "A": 90.66666666666667,
            "B": 83.2,
            "C": 78.6,
            "D": 75.26666666666667,
            "E": 73.6,
            "F": 98.64285714285714,
            "G": 132.07142857142858
        },
        "bus3": {
            "I": 125.06666666666666,
            "J": 120.06666666666666,
            "K": 113.06666666666666,
            "L": 100.73333333333333,
            "A": 91.93333333333334,
            "B": 83.06666666666666,
            "C": 77,
            "D": 73.73333333333333,
            "E": 71.8,
            "F": 95.66666666666667,
            "G": 129.46666666666667,
            "H": 137.73333333333332
        },
        "bus1": {
            "G": 122.93333333333334,
            "H": 129.13333333333333,
            "I": 128.46666666666667,
            "J": 124.06666666666666,
            "K": 117.53333333333333,
            "L": 108.4,
            "A": 96.2,
            "B": 86.4,
            "C": 80.28571428571429,
            "D": 76.71428571428571,
            "E": 74.78571428571429,
            "F": 96.57142857142857
        },
        "bus5": {
            "L": 100.0625,
            "A": 89.6,
            "B": 80.06666666666666,
            "C": 75.66666666666667,
            "D": 76.06666666666666,
            "E": 74.8,
            "F": 98.86666666666666,
            "G": 129.53333333333333,
            "H": 136.66666666666666,
            "I": 132.73333333333332,
            "J": 127.26666666666667,
            "K": 118.33333333333333
        },
        "bus4": {
            "J": 120.375,
            "K": 113.2,
            "L": 103.73333333333333,
            "A": 93,
            "B": 83.33333333333333,
            "C": 79.6,
            "D": 74.93333333333334,
            "E": 71.8,
            "F": 98.66666666666667,
            "G": 133,
            "H": 140.53333333333333,
            "I": 136.13333333333333
        },
        "bus0": {
            "B": 101.2,
            "C": 92.6,
            "D": 86.86666666666666,
            "E": 87.4,
            "F": 118.06666666666666,
            "G": 155.8,
            "H": 163.46666666666667,
            "I": 160.8,
            "J": 153.8,
            "K": 142.33333333333334,
            "L": 130.73333333333332,
            "A": 118.66666666666667
        }
    },
    "Obs_and_actions_busses": {
        "6": 264.3461408423934,
        "3": 465.04856610709174,
        "1": 525.647461992294,
        "4": 435.98038325929707,
        "0": 658.7043008298207,
        "5": 403.7879397987999,
        "2": 496.4181803312687
    },
    "Obs_and_actions_tfls": {
        "0": 60.93650181609775
    }
}