{
    "Average_holding_time": 11.681184668989546,
    "Average_travel_time_passengers": 2687.7452824734564,
    "Average_speed_busses": 0.04783333333333333,
    "Average_experienced_crowding": 83.01110299834492,
    "Average_covariation_headway": 0.3966349460851621,
    "Average_covariation_headway_stops": {
        "L": 0.2991146988798977,
        "J": 0.29704730235845367,
        "D": 0.2787314838990107,
        "E": 0.3010646728497914,
        "C": 0.28776677284309055,
        "I": 0.30827054456475017,
        "A": 0.2917157989051298,
        "K": 0.28439859473197043,
        "F": 0.2736384016855719,
        "B": 0.298379770132632,
        "G": 0.2833394254129415,
        "H": 0.3040083945026857
    },
    "Average_excess_waiting_time": {
        "L": 61.39189652947272,
        "J": 61.30781557950121,
        "D": 59.079323211461826,
        "E": 60.80173717093601,
        "C": 61.48324672777085,
        "I": 65.39703736566821,
        "A": 62.38119980579569,
        "K": 60.70047359705359,
        "F": 57.94587263212344,
        "B": 65.32974641782062,
        "G": 61.31268626970831,
        "H": 67.46917946429113
    },
    "Holding_per_stop": {
        "L": 13.125,
        "J": 12.1875,
        "D": 9.375,
        "E": 10.9375,
        "C": 8.75,
        "I": 11.052631578947368,
        "A": 10.3125,
        "F": 10.9375,
        "K": 16.25,
        "B": 10.421052631578947,
        "G": 12.631578947368421,
        "H": 14.210526315789474
    },
    "Actions_tfl_edges": {
        "BC": [
            30,
            50,
            16
        ],
        "CD": [
            28,
            52,
            16
        ],
        "DE": [
            32,
            51,
            14
        ],
        "HI": [
            29,
            53,
            14
        ],
        "IJ": [
            31,
            53,
            12
        ],
        "KL": [
            29,
            56,
            12
        ],
        "LA": [
            33,
            49,
            14
        ],
        "JK": [
            37,
            49,
            10
        ],
        "EF": [
            20,
            56,
            20
        ],
        "AB": [
            30,
            50,
            16
        ],
        "FG": [
            25,
            56,
            15
        ],
        "GH": [
            25,
            57,
            13
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 73.25,
            "A": 72.9375,
            "B": 72.625,
            "C": 71.1875,
            "D": 73.625,
            "E": 74.0625,
            "F": 75.625,
            "G": 75.25,
            "H": 74.25,
            "I": 76,
            "J": 76,
            "K": 77
        },
        "bus4": {
            "J": 81.6875,
            "K": 83.3125,
            "L": 82.75,
            "A": 82,
            "B": 82.8125,
            "C": 83.25,
            "D": 83.5,
            "E": 83.875,
            "F": 84.625,
            "G": 84.3125,
            "H": 85.8125,
            "I": 86.33333333333333
        },
        "bus1": {
            "D": 67.4375,
            "E": 67.9375,
            "F": 69.1875,
            "G": 70.875,
            "H": 72.3125,
            "I": 73.4375,
            "J": 72.9375,
            "K": 73.1875,
            "L": 71.625,
            "A": 73.375,
            "B": 73.3125,
            "C": 71.3125
        },
        "bus2": {
            "E": 77.6875,
            "F": 78.6875,
            "G": 80.1875,
            "H": 81.6875,
            "I": 82.1875,
            "J": 83.1875,
            "K": 84.4375,
            "L": 84.5625,
            "A": 86.0625,
            "B": 86.625,
            "C": 86.0625,
            "D": 87.125
        },
        "bus0": {
            "C": 81.9375,
            "D": 82.0625,
            "E": 81.875,
            "F": 81.4375,
            "G": 82.0625,
            "H": 81.875,
            "I": 83.25,
            "J": 82.6875,
            "K": 83.4375,
            "L": 83.9375,
            "A": 85.3125,
            "B": 87.26666666666667
        },
        "bus3": {
            "I": 75.4375,
            "J": 75.875,
            "K": 73.6875,
            "L": 74.8125,
            "A": 73.6875,
            "B": 77.375,
            "C": 78.6875,
            "D": 78.6875,
            "E": 78.875,
            "F": 79.5,
            "G": 78.2,
            "H": 79.13333333333334
        }
    }
}