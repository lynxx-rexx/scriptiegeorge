{
    "Average_holding_time": 10.852173913043478,
    "Average_travel_time_passengers": 2656.1484689326344,
    "Average_speed_busses": 0.04791666666666667,
    "Average_experienced_crowding": 79.87531082227278,
    "Average_covariation_headway": 0.3306440215008136,
    "Average_covariation_headway_stops": {
        "L": 0.23538098878460118,
        "I": 0.2509236554625458,
        "E": 0.2591542858775143,
        "K": 0.22862463787190365,
        "D": 0.2222304646546597,
        "C": 0.21490751899525276,
        "A": 0.2376291424661966,
        "F": 0.25563261214518307,
        "J": 0.2244524456365511,
        "G": 0.2343469271365397,
        "B": 0.21397977764313603,
        "H": 0.24333014556619145
    },
    "Average_excess_waiting_time": {
        "L": 50.89325458098233,
        "I": 54.32028477881801,
        "E": 52.67824431163916,
        "K": 51.08884095584398,
        "D": 48.71269435514171,
        "C": 49.601895925229826,
        "A": 52.928757289145096,
        "F": 53.61251822952897,
        "J": 51.74222587568397,
        "G": 51.917351487770816,
        "B": 51.20419052091154,
        "H": 54.635554612294754
    },
    "Holding_per_stop": {
        "L": 12.8125,
        "E": 10.824742268041238,
        "D": 9.375,
        "I": 10.3125,
        "K": 12.5,
        "C": 12.8125,
        "A": 6.5625,
        "F": 13.75,
        "J": 10.736842105263158,
        "B": 6.947368421052632,
        "G": 12.8125,
        "H": 10.736842105263158
    },
    "Actions_tfl_edges": {
        "BC": [
            24,
            60,
            12
        ],
        "CD": [
            27,
            55,
            15
        ],
        "DE": [
            27,
            57,
            13
        ],
        "HI": [
            26,
            53,
            17
        ],
        "JK": [
            35,
            51,
            10
        ],
        "KL": [
            30,
            54,
            13
        ],
        "LA": [
            32,
            48,
            16
        ],
        "EF": [
            33,
            45,
            19
        ],
        "IJ": [
            28,
            52,
            16
        ],
        "AB": [
            29,
            51,
            16
        ],
        "FG": [
            27,
            52,
            17
        ],
        "GH": [
            25,
            55,
            16
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 74,
            "A": 72.75,
            "B": 74.5,
            "C": 75.375,
            "D": 75.8125,
            "E": 77.375,
            "F": 77.5625,
            "G": 79,
            "H": 76.9375,
            "I": 76.4375,
            "J": 78.125,
            "K": 77.375
        },
        "bus3": {
            "I": 76.25,
            "J": 77.625,
            "K": 77.75,
            "L": 79.125,
            "A": 79.0625,
            "B": 80.5625,
            "C": 80.625,
            "D": 81.3125,
            "E": 81.5,
            "F": 82.5625,
            "G": 83.25,
            "H": 81.66666666666667
        },
        "bus2": {
            "E": 75.17647058823529,
            "F": 76.1875,
            "G": 76.9375,
            "H": 77.6875,
            "I": 78.625,
            "J": 79.0625,
            "K": 82.75,
            "L": 83.5,
            "A": 82.875,
            "B": 83.9375,
            "C": 81.0625,
            "D": 80.4375
        },
        "bus4": {
            "K": 76.9375,
            "L": 79.5625,
            "A": 82.25,
            "B": 81.1875,
            "C": 81.6875,
            "D": 81.6875,
            "E": 82.5625,
            "F": 82.4375,
            "G": 81.8125,
            "H": 81.3125,
            "I": 81.5,
            "J": 83.3125
        },
        "bus1": {
            "D": 64.125,
            "E": 64.125,
            "F": 63.9375,
            "G": 64.375,
            "H": 64.1875,
            "I": 66.1875,
            "J": 66.9375,
            "K": 67.3125,
            "L": 70.1875,
            "A": 69.5625,
            "B": 67.375,
            "C": 68.4375
        },
        "bus0": {
            "C": 77.5,
            "D": 77.25,
            "E": 79.125,
            "F": 80.25,
            "G": 78.375,
            "H": 79.9375,
            "I": 80.25,
            "J": 82.4375,
            "K": 84.875,
            "L": 84.375,
            "A": 83.4375,
            "B": 82.33333333333333
        }
    }
}  "D": 66.4,
            "E": 67.33333333333333,
            "F": 68.26666666666667,
            "G": 70
        }
    }
}