{
    "Average_holding_time": 10.987012987012987,
    "Average_travel_time_passengers": 2606.398677196081,
    "Average_speed_busses": 0.048125,
    "Average_experienced_crowding": 104.816835949725,
    "Average_covariation_headway": 0.218930219735668,
    "Average_covariation_headway_stops": {
        "G": 0.11012334281711947,
        "I": 0.10301168639650173,
        "E": 0.07865754349359412,
        "K": 0.08785386512612922,
        "B": 0.06745471721049548,
        "C": 0.05899649930129399,
        "J": 0.07343740374420935,
        "H": 0.06746452270732259,
        "L": 0.04894550220988107,
        "F": 0.05963041098799793,
        "D": 0.04564498412052912,
        "A": 0.051085753424714206
    },
    "Average_excess_waiting_time": {
        "G": 41.68042605817175,
        "I": 40.456006953291194,
        "E": 39.660007384112134,
        "K": 39.42720837606038,
        "B": 40.22192279783951,
        "C": 38.80113054703713,
        "J": 40.45442766123443,
        "H": 40.68795828587895,
        "L": 39.12218850023106,
        "F": 40.37670740460857,
        "D": 40.06510718796119,
        "A": 41.01542395494306
    },
    "Holding_per_stop": {
        "I": 22.577319587628867,
        "K": 14.536082474226804,
        "G": 22.8125,
        "E": 24.0625,
        "C": 0,
        "J": 9.375,
        "H": 0.625,
        "B": 1.5625,
        "L": 12.8125,
        "F": 16.5625,
        "D": 1.5625,
        "A": 5.3125
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 118.82352941176471,
            "H": 124.1875,
            "I": 117.875,
            "J": 112.8125,
            "K": 106.1875,
            "L": 98.875,
            "A": 90.6875,
            "B": 82.0625,
            "C": 76.0625,
            "D": 72.75,
            "E": 70.9375,
            "F": 91.3125
        },
        "bus4": {
            "I": 123.23529411764706,
            "J": 115.1875,
            "K": 107.875,
            "L": 98.875,
            "A": 91.5625,
            "B": 81.875,
            "C": 76.0625,
            "D": 72.5,
            "E": 68.4375,
            "F": 92.8125,
            "G": 122.4375,
            "H": 132.75
        },
        "bus2": {
            "E": 71.82352941176471,
            "F": 91.25,
            "G": 122.625,
            "H": 130.75,
            "I": 128.4375,
            "J": 124.75,
            "K": 117.375,
            "L": 106.6875,
            "A": 96.6875,
            "B": 87,
            "C": 82.125,
            "D": 77.4375
        },
        "bus5": {
            "K": 111.76470588235294,
            "L": 102.5,
            "A": 94.25,
            "B": 86.1875,
            "C": 81.125,
            "D": 78.875,
            "E": 76.625,
            "F": 98.1875,
            "G": 131.5625,
            "H": 139.75,
            "I": 136.0625,
            "J": 127.4375
        },
        "bus0": {
            "B": 80.125,
            "C": 76.0625,
            "D": 72.5625,
            "E": 72.5,
            "F": 94.5,
            "G": 126.5625,
            "H": 132.5,
            "I": 128.6875,
            "J": 122.4375,
            "K": 113.75,
            "L": 103,
            "A": 94.6875
        },
        "bus1": {
            "C": 74.88235294117646,
            "D": 70.375,
            "E": 71.625,
            "F": 96.9375,
            "G": 126.5625,
            "H": 135.9375,
            "I": 133.1875,
            "J": 126.9375,
            "K": 117.25,
            "L": 108.3125,
            "A": 97.8125,
            "B": 85.9375
        }
    }
}