{
    "Average_holding_time": 13.488168273444346,
    "Average_travel_time_passengers": 2623.603284228268,
    "Average_speed_busses": 0.04754166666666667,
    "Average_experienced_crowding": 107.03947438711201,
    "Average_covariation_headway": 0.24757656379246304,
    "Average_covariation_headway_stops": {
        "I": 0.20470199957908097,
        "J": 0.18889400377439206,
        "L": 0.14243463433982403,
        "D": 0.1196235268230298,
        "A": 0.12751743034141463,
        "B": 0.11354809885420118,
        "K": 0.15161970250660742,
        "E": 0.09796862627184949,
        "C": 0.10172052546420603,
        "F": 0.1076844804640493,
        "G": 0.13211966358084826,
        "H": 0.16312421918535694
    },
    "Average_excess_waiting_time": {
        "I": 56.42113961922695,
        "J": 52.761984368658204,
        "L": 48.032013617217274,
        "D": 44.24711278272105,
        "A": 45.2976282746493,
        "B": 43.267633946103615,
        "K": 50.45657457098832,
        "E": 44.173579988152085,
        "C": 44.516906511899606,
        "F": 46.644016858766406,
        "G": 49.79969636139083,
        "H": 53.83308053990146
    },
    "Holding_per_stop": {
        "J": 18,
        "I": 24.31578947368421,
        "D": 2.1875,
        "A": 8.4375,
        "B": 3.125,
        "L": 15.473684210526315,
        "K": 15.157894736842104,
        "E": 26.210526315789473,
        "C": 1.5789473684210527,
        "F": 19.263157894736842,
        "G": 26.80851063829787,
        "H": 1.5957446808510638
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus2": {
            "I": 123.25,
            "J": 117,
            "K": 107.6875,
            "L": 97.75,
            "A": 88.6875,
            "B": 79.8125,
            "C": 75.125,
            "D": 74.1875,
            "E": 74.33333333333333,
            "F": 95.06666666666666,
            "G": 129.26666666666668,
            "H": 135.46666666666667
        },
        "bus3": {
            "J": 114.125,
            "K": 106.3125,
            "L": 97,
            "A": 87.125,
            "B": 79.0625,
            "C": 74.8125,
            "D": 71.0625,
            "E": 69.875,
            "F": 95.8125,
            "G": 127.8125,
            "H": 134.53333333333333,
            "I": 127.53333333333333
        },
        "bus4": {
            "L": 101.75,
            "A": 90.3125,
            "B": 81.75,
            "C": 78.1875,
            "D": 74.0625,
            "E": 73,
            "F": 94.5,
            "G": 126.1875,
            "H": 135.8125,
            "I": 132.8125,
            "J": 126.2,
            "K": 117.53333333333333
        },
        "bus1": {
            "D": 77.125,
            "E": 74.8125,
            "F": 99.4375,
            "G": 133.25,
            "H": 144.4375,
            "I": 141.6875,
            "J": 134.6875,
            "K": 127.125,
            "L": 114.625,
            "A": 105.3125,
            "B": 96,
            "C": 89.26666666666667
        },
        "bus5": {
            "A": 90.125,
            "B": 81.4375,
            "C": 76.625,
            "D": 74.25,
            "E": 72,
            "F": 92.25,
            "G": 122.875,
            "H": 130.6875,
            "I": 126.5,
            "J": 121.75,
            "K": 114,
            "L": 105.4
        },
        "bus0": {
            "B": 82.625,
            "C": 76.6875,
            "D": 73.125,
            "E": 72.9375,
            "F": 95,
            "G": 128.25,
            "H": 135.4375,
            "I": 130.8125,
            "J": 123.875,
            "K": 115.4375,
            "L": 105.8125,
            "A": 96.8125
        }
    }
}