{
    "Average_holding_time": 14.577464788732394,
    "Average_travel_time_passengers": 2662.958315059806,
    "Average_speed_busses": 0.04733333333333333,
    "Average_experienced_crowding": 109.99112346785469,
    "Average_covariation_headway": 0.26361299096621654,
    "Average_covariation_headway_stops": {
        "G": 0.20226952592407071,
        "F": 0.19305287414056943,
        "J": 0.18539415074928278,
        "L": 0.15827521708184297,
        "H": 0.1951570768501562,
        "A": 0.14656420920585317,
        "B": 0.15689953286556074,
        "I": 0.1863494296343759,
        "K": 0.15453908888140813,
        "C": 0.1392778148689312,
        "D": 0.15469976897127485,
        "E": 0.17973181770878233
    },
    "Average_excess_waiting_time": {
        "G": 55.32980840974773,
        "F": 55.85160767897503,
        "J": 54.124971154473656,
        "L": 50.93680157147605,
        "H": 55.68063366225431,
        "A": 48.514560481045635,
        "B": 48.31715753104834,
        "I": 55.810309233600094,
        "K": 52.47195160563348,
        "C": 48.72746279306813,
        "D": 51.9571352817963,
        "E": 56.216029787552316
    },
    "Holding_per_stop": {
        "G": 25.894736842105264,
        "J": 18,
        "H": 7.894736842105263,
        "A": 6.631578947368421,
        "B": 4.0625,
        "L": 16.736842105263158,
        "F": 21.70212765957447,
        "I": 23.93617021276596,
        "K": 14.680851063829786,
        "C": 3.789473684210526,
        "D": 4.468085106382978,
        "E": 27.4468085106383
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus2": {
            "G": 124.25,
            "H": 132,
            "I": 127.625,
            "J": 120.3125,
            "K": 112.9375,
            "L": 102.1875,
            "A": 92.3125,
            "B": 84.25,
            "C": 78.75,
            "D": 76.75,
            "E": 76.66666666666667,
            "F": 100.06666666666666
        },
        "bus1": {
            "F": 93.9375,
            "G": 124.5,
            "H": 133.8125,
            "I": 131.125,
            "J": 123.125,
            "K": 114.3125,
            "L": 105.3125,
            "A": 96.6875,
            "B": 87.6875,
            "C": 86,
            "D": 81.46666666666667,
            "E": 78
        },
        "bus3": {
            "J": 117.1875,
            "K": 109.8125,
            "L": 99.375,
            "A": 90.875,
            "B": 81.5,
            "C": 76.625,
            "D": 74.0625,
            "E": 73,
            "F": 95.625,
            "G": 127.26666666666667,
            "H": 134.86666666666667,
            "I": 130.8
        },
        "bus4": {
            "L": 99.3125,
            "A": 89.5625,
            "B": 80.8125,
            "C": 76.875,
            "D": 72.875,
            "E": 71.25,
            "F": 94.8125,
            "G": 129.375,
            "H": 135.875,
            "I": 133.8,
            "J": 126.66666666666667,
            "K": 115.2
        },
        "bus5": {
            "A": 87.5625,
            "B": 79.75,
            "C": 75,
            "D": 72.875,
            "E": 72,
            "F": 95.6875,
            "G": 125.1875,
            "H": 133.3125,
            "I": 128.75,
            "J": 122.5,
            "K": 114.46666666666667,
            "L": 104.26666666666667
        },
        "bus0": {
            "B": 92.8125,
            "C": 89.75,
            "D": 84.9375,
            "E": 81.75,
            "F": 105.6875,
            "G": 142.375,
            "H": 152.375,
            "I": 147.6875,
            "J": 140.9375,
            "K": 132.4375,
            "L": 121.5,
            "A": 109.8
        }
    }
}