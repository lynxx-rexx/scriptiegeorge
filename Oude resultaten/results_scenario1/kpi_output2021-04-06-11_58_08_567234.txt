{
    "Average_holding_time": 7.5732899022801305,
    "Average_travel_time_passengers": 2496.1562294141568,
    "Average_speed_busses": 0.051166666666666666,
    "Average_experienced_crowding": 74.51599495400504,
    "Average_covariation_headway": 0.13805781825285862,
    "Average_covariation_headway_stops": {
        "B": 0.10942147481714157,
        "G": 0.1770142195090502,
        "H": 0.16517155663664085,
        "I": 0.15655727407072179,
        "K": 0.13099280067579386,
        "A": 0.10137423018610243,
        "C": 0.08522757900506686,
        "J": 0.12897239982667466,
        "L": 0.1026586111658516,
        "D": 0.0853188207747265,
        "E": 0.11291542107482677,
        "F": 0.1541896768963541
    },
    "Average_excess_waiting_time": {
        "B": 28.166609064161946,
        "G": 38.33420569199552,
        "H": 35.546490088218206,
        "I": 33.36761004037152,
        "K": 31.031901103465486,
        "A": 29.044787094910305,
        "C": 28.14006897742871,
        "J": 32.33665157208378,
        "L": 30.552290376061933,
        "D": 29.794065127750287,
        "E": 32.8633778642689,
        "F": 37.72692900468422
    },
    "Holding_per_stop": {
        "B": 5.242718446601942,
        "H": 6.764705882352941,
        "I": 9.902912621359222,
        "K": 13.980582524271844,
        "G": 7.647058823529412,
        "A": 4.077669902912621,
        "C": 9.902912621359222,
        "J": 10.588235294117647,
        "L": 8.529411764705882,
        "D": 4.117647058823529,
        "E": 5.588235294117647,
        "F": 4.455445544554456
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 77.66666666666667,
            "C": 77.27777777777777,
            "D": 79.23529411764706,
            "E": 78.94117647058823,
            "F": 79.11764705882354,
            "G": 81.47058823529412,
            "H": 81.11764705882354,
            "I": 82.05882352941177,
            "J": 83.41176470588235,
            "K": 83.76470588235294,
            "L": 83.76470588235294,
            "A": 82.58823529411765
        },
        "bus1": {
            "G": 67.29411764705883,
            "H": 68.17647058823529,
            "I": 68.82352941176471,
            "J": 69.47058823529412,
            "K": 70.6470588235294,
            "L": 71.58823529411765,
            "A": 72.41176470588235,
            "B": 74.6470588235294,
            "C": 73.76470588235294,
            "D": 73.41176470588235,
            "E": 73.41176470588235,
            "F": 72.0625
        },
        "bus2": {
            "H": 68.82352941176471,
            "I": 68,
            "J": 70.05882352941177,
            "K": 69.6470588235294,
            "L": 70.3529411764706,
            "A": 70.58823529411765,
            "B": 70.41176470588235,
            "C": 71.3529411764706,
            "D": 73.47058823529412,
            "E": 72.23529411764706,
            "F": 72.52941176470588,
            "G": 73.41176470588235
        },
        "bus3": {
            "I": 70.27777777777777,
            "J": 70.76470588235294,
            "K": 72.41176470588235,
            "L": 73.11764705882354,
            "A": 72.11764705882354,
            "B": 73,
            "C": 73.23529411764706,
            "D": 72.70588235294117,
            "E": 72.3529411764706,
            "F": 73.47058823529412,
            "G": 73.52941176470588,
            "H": 74.05882352941177
        },
        "bus4": {
            "K": 70.11111111111111,
            "L": 70.6470588235294,
            "A": 71.94117647058823,
            "B": 72.58823529411765,
            "C": 72.23529411764706,
            "D": 71.94117647058823,
            "E": 72.29411764705883,
            "F": 72.41176470588235,
            "G": 72.52941176470588,
            "H": 73.94117647058823,
            "I": 73.88235294117646,
            "J": 73.82352941176471
        },
        "bus5": {
            "A": 72.77777777777777,
            "B": 70.23529411764706,
            "C": 71.05882352941177,
            "D": 72.58823529411765,
            "E": 72.94117647058823,
            "F": 73.58823529411765,
            "G": 74.17647058823529,
            "H": 75.58823529411765,
            "I": 76,
            "J": 76.6470588235294,
            "K": 77.52941176470588,
            "L": 78.3529411764706
        }
    }
}