{
    "Average_holding_time": 14.623745819397993,
    "Average_travel_time_passengers": 2551.4432058044263,
    "Average_speed_busses": 0.049833333333333334,
    "Average_experienced_crowding": 74.93492685639735,
    "Average_covariation_headway": 0.13388414929665174,
    "Average_covariation_headway_stops": {
        "C": 0.14402349946591542,
        "F": 0.15364078108431445,
        "G": 0.14012473218021654,
        "I": 0.11346063443295071,
        "J": 0.1132913457725551,
        "K": 0.12838735747217536,
        "H": 0.10314920057110656,
        "L": 0.10197763877594115,
        "D": 0.11894224595877627,
        "A": 0.09601657574189679,
        "E": 0.13126648233997962,
        "B": 0.12062704533222968
    },
    "Average_excess_waiting_time": {
        "C": 40.54905000179707,
        "F": 42.848027442607304,
        "G": 40.338976987460285,
        "I": 38.3455443971082,
        "J": 36.88096456687367,
        "K": 36.44402358734874,
        "H": 39.059698260942355,
        "L": 36.15951656504194,
        "D": 40.030150287447384,
        "A": 37.250357485439224,
        "E": 42.57171969747873,
        "B": 40.27169397502337
    },
    "Holding_per_stop": {
        "G": 12,
        "J": 16.2,
        "K": 29.702970297029704,
        "I": 18.9,
        "C": 13.8,
        "F": 15.454545454545455,
        "H": 10,
        "L": 20.1,
        "D": 13.93939393939394,
        "A": 4.5,
        "B": 7.575757575757576,
        "E": 13.030303030303031
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 72.23529411764706,
            "D": 72.94117647058823,
            "E": 76.17647058823529,
            "F": 76.11764705882354,
            "G": 78,
            "H": 78.11764705882354,
            "I": 77.23529411764706,
            "J": 77.1875,
            "K": 77,
            "L": 75.3125,
            "A": 76.0625,
            "B": 75.9375
        },
        "bus1": {
            "F": 71.41176470588235,
            "G": 71.76470588235294,
            "H": 73.88235294117646,
            "I": 74.82352941176471,
            "J": 75,
            "K": 74.11764705882354,
            "L": 73.5625,
            "A": 74.4375,
            "B": 75.0625,
            "C": 75.6875,
            "D": 73.875,
            "E": 75.4375
        },
        "bus2": {
            "G": 72,
            "H": 71.82352941176471,
            "I": 71.58823529411765,
            "J": 73.23529411764706,
            "K": 72.6470588235294,
            "L": 73.94117647058823,
            "A": 75.58823529411765,
            "B": 76.17647058823529,
            "C": 75.125,
            "D": 76.375,
            "E": 75.8125,
            "F": 75.625
        },
        "bus3": {
            "I": 68.58823529411765,
            "J": 68.05882352941177,
            "K": 68.52941176470588,
            "L": 69.17647058823529,
            "A": 69.88235294117646,
            "B": 72.11764705882354,
            "C": 71.23529411764706,
            "D": 71.17647058823529,
            "E": 73.5,
            "F": 73.3125,
            "G": 73.5625,
            "H": 75
        },
        "bus4": {
            "J": 67.6470588235294,
            "K": 68.17647058823529,
            "L": 68,
            "A": 69.29411764705883,
            "B": 70.70588235294117,
            "C": 71.76470588235294,
            "D": 72.11764705882354,
            "E": 71.23529411764706,
            "F": 71.6470588235294,
            "G": 71.9375,
            "H": 71.625,
            "I": 72.125
        },
        "bus5": {
            "K": 76.47058823529412,
            "L": 77.17647058823529,
            "A": 78.47058823529412,
            "B": 80.17647058823529,
            "C": 81.11764705882354,
            "D": 81.88235294117646,
            "E": 80.94117647058823,
            "F": 81.58823529411765,
            "G": 81.76470588235294,
            "H": 80.70588235294117,
            "I": 79.3125,
            "J": 78.9375
        }
    }
}