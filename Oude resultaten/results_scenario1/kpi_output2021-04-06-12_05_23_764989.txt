{
    "Average_holding_time": 16.88289806234204,
    "Average_travel_time_passengers": 2552.800453746682,
    "Average_speed_busses": 0.04945833333333333,
    "Average_experienced_crowding": 75.57379723670353,
    "Average_covariation_headway": 0.1708006179618948,
    "Average_covariation_headway_stops": {
        "B": 0.1426113842641188,
        "D": 0.1292622057966878,
        "F": 0.12112304729466425,
        "G": 0.12297640890512797,
        "K": 0.144017967572681,
        "A": 0.13998088722058402,
        "E": 0.11288837379048494,
        "H": 0.10406158790197027,
        "C": 0.12449572969834267,
        "L": 0.12794482034641438,
        "I": 0.10494876751091117,
        "J": 0.12372751415141808
    },
    "Average_excess_waiting_time": {
        "B": 42.648771698011046,
        "D": 41.43016824237918,
        "F": 40.60368285216492,
        "G": 39.44190374382157,
        "K": 44.32531279108957,
        "A": 43.94636532034747,
        "E": 41.739077274074646,
        "H": 39.619996005399344,
        "C": 42.64982759411424,
        "L": 44.60535040035887,
        "I": 41.43656090768678,
        "J": 44.30701716669654
    },
    "Holding_per_stop": {
        "D": 20.303030303030305,
        "G": 20.1,
        "B": 6.96969696969697,
        "A": 5.454545454545454,
        "F": 21.21212121212121,
        "K": 26.363636363636363,
        "H": 11.818181818181818,
        "C": 28.484848484848484,
        "E": 10.909090909090908,
        "I": 19.393939393939394,
        "L": 15.306122448979592,
        "J": 16.224489795918366
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 76.29411764705883,
            "C": 77.94117647058823,
            "D": 75.82352941176471,
            "E": 76.11764705882354,
            "F": 74.94117647058823,
            "G": 75.76470588235294,
            "H": 77.6875,
            "I": 80.75,
            "J": 80,
            "K": 81.3125,
            "L": 82,
            "A": 81.1875
        },
        "bus1": {
            "D": 68.52941176470588,
            "E": 68.58823529411765,
            "F": 70.58823529411765,
            "G": 71.6470588235294,
            "H": 74.94117647058823,
            "I": 75.05882352941177,
            "J": 74.4375,
            "K": 74.1875,
            "L": 75.625,
            "A": 73.375,
            "B": 73.5625,
            "C": 74.0625
        },
        "bus2": {
            "F": 66.29411764705883,
            "G": 67.88235294117646,
            "H": 69.05882352941177,
            "I": 70.94117647058823,
            "J": 73.05882352941177,
            "K": 73.52941176470588,
            "L": 73.8125,
            "A": 72.3125,
            "B": 73.4375,
            "C": 71.875,
            "D": 71.25,
            "E": 70.625
        },
        "bus3": {
            "G": 72.76470588235294,
            "H": 75.23529411764706,
            "I": 73.70588235294117,
            "J": 72.23529411764706,
            "K": 73.94117647058823,
            "L": 75.94117647058823,
            "A": 77,
            "B": 77.9375,
            "C": 78.6875,
            "D": 79.625,
            "E": 77.8125,
            "F": 76.875
        },
        "bus4": {
            "K": 71.52941176470588,
            "L": 71.88235294117646,
            "A": 71,
            "B": 73.41176470588235,
            "C": 74.52941176470588,
            "D": 75.4375,
            "E": 75.75,
            "F": 75.125,
            "G": 74.375,
            "H": 74.3125,
            "I": 75.4375,
            "J": 74.9375
        },
        "bus5": {
            "A": 74.52941176470588,
            "B": 76.47058823529412,
            "C": 77,
            "D": 77.70588235294117,
            "E": 76.82352941176471,
            "F": 75.9375,
            "G": 77.25,
            "H": 79.4375,
            "I": 78.9375,
            "J": 79,
            "K": 78.0625,
            "L": 77.5
        }
    }
}