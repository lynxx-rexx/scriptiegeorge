{
    "Average_holding_time": 14.163197335553706,
    "Average_travel_time_passengers": 2532.6522052485348,
    "Average_speed_busses": 0.050041666666666665,
    "Average_experienced_crowding": 74.51571005281576,
    "Average_covariation_headway": 0.1534739029179457,
    "Average_covariation_headway_stops": {
        "B": 0.13325994091946095,
        "C": 0.1422661191561027,
        "F": 0.1209154200688028,
        "G": 0.10607791153817651,
        "J": 0.10465372209725168,
        "A": 0.13762520253624147,
        "D": 0.10596494531443852,
        "H": 0.06929566004646556,
        "K": 0.08636825612621656,
        "E": 0.10026306837523326,
        "I": 0.07536255361000815,
        "L": 0.11128412240699004
    },
    "Average_excess_waiting_time": {
        "B": 39.30601970721352,
        "C": 38.46762262326456,
        "F": 38.638785857122116,
        "G": 36.0241908674256,
        "J": 37.302684745162594,
        "A": 41.004815401345695,
        "D": 37.55101728044224,
        "H": 35.52783297488003,
        "K": 37.70743127689906,
        "E": 38.658147242622704,
        "I": 37.23495551520341,
        "L": 40.69090094247673
    },
    "Holding_per_stop": {
        "C": 24.95049504950495,
        "B": 6,
        "G": 12.772277227722773,
        "J": 12.9,
        "F": 18,
        "A": 3,
        "D": 13.8,
        "H": 12.3,
        "K": 17.7,
        "E": 16.5,
        "I": 13.8,
        "L": 18.181818181818183
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 67.3529411764706,
            "C": 68.58823529411765,
            "D": 70,
            "E": 71.6470588235294,
            "F": 72.76470588235294,
            "G": 70.58823529411765,
            "H": 73.88235294117646,
            "I": 74.47058823529412,
            "J": 74.125,
            "K": 72.75,
            "L": 71.375,
            "A": 71.25
        },
        "bus1": {
            "C": 71.6470588235294,
            "D": 70.82352941176471,
            "E": 71.76470588235294,
            "F": 71.94117647058823,
            "G": 74.23529411764706,
            "H": 76.41176470588235,
            "I": 77.29411764705883,
            "J": 77.52941176470588,
            "K": 77.58823529411765,
            "L": 76.9375,
            "A": 77.375,
            "B": 77.375
        },
        "bus2": {
            "F": 69.94117647058823,
            "G": 70.82352941176471,
            "H": 69.82352941176471,
            "I": 71.41176470588235,
            "J": 71.70588235294117,
            "K": 72,
            "L": 73.76470588235294,
            "A": 74.70588235294117,
            "B": 75.3125,
            "C": 76.0625,
            "D": 75.5,
            "E": 73.4375
        },
        "bus3": {
            "G": 71.29411764705883,
            "H": 71.52941176470588,
            "I": 72.11764705882354,
            "J": 75.47058823529412,
            "K": 75.3529411764706,
            "L": 76.41176470588235,
            "A": 77.05882352941177,
            "B": 77.52941176470588,
            "C": 76.94117647058823,
            "D": 76.6875,
            "E": 76.9375,
            "F": 75.5625
        },
        "bus4": {
            "J": 72.11764705882354,
            "K": 71.52941176470588,
            "L": 70,
            "A": 71.82352941176471,
            "B": 73.52941176470588,
            "C": 73,
            "D": 74.3529411764706,
            "E": 76.17647058823529,
            "F": 77.8125,
            "G": 77.5,
            "H": 77.4375,
            "I": 77.9375
        },
        "bus5": {
            "A": 74.41176470588235,
            "B": 74.47058823529412,
            "C": 74.29411764705883,
            "D": 74,
            "E": 74.3529411764706,
            "F": 73.88235294117646,
            "G": 74.23529411764706,
            "H": 74.9375,
            "I": 75.3125,
            "J": 76.6875,
            "K": 77.8125,
            "L": 78.875
        }
    }
}