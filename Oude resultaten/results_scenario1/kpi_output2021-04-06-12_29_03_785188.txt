{
    "Average_holding_time": 33.07486631016043,
    "Average_travel_time_passengers": 2712.8265297865705,
    "Average_speed_busses": 0.04675,
    "Average_experienced_crowding": 79.6296443282834,
    "Average_covariation_headway": 0.10884810046959825,
    "Average_covariation_headway_stops": {
        "C": 0.08943598475307596,
        "F": 0.08928155296446362,
        "I": 0.10329333658251111,
        "J": 0.10921778732721334,
        "K": 0.11294056808998809,
        "A": 0.10491138315537028,
        "D": 0.05055276606749607,
        "G": 0.0642599587652861,
        "L": 0.0831463577894747,
        "B": 0.070318881077943,
        "E": 0.05215168901203839,
        "H": 0.07041897695637027
    },
    "Average_excess_waiting_time": {
        "C": 55.5317577680828,
        "F": 57.23329094593686,
        "I": 59.59130736464067,
        "J": 58.35204190474127,
        "K": 56.63597782822666,
        "A": 56.35146340685736,
        "D": 55.52018114633904,
        "G": 57.80560572480613,
        "L": 56.91040703607098,
        "B": 56.0864939880891,
        "E": 57.33173557044188,
        "H": 59.851504354532096
    },
    "Holding_per_stop": {
        "C": 38.61702127659574,
        "F": 31.595744680851062,
        "K": 35.1063829787234,
        "J": 33.829787234042556,
        "A": 30.638297872340427,
        "I": 31.612903225806452,
        "D": 30.95744680851064,
        "G": 31.612903225806452,
        "B": 31.914893617021278,
        "L": 34.83870967741935,
        "E": 31.612903225806452,
        "H": 34.56521739130435
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 80.8125,
            "D": 79.9375,
            "E": 80.25,
            "F": 81.5625,
            "G": 80.75,
            "H": 81.75,
            "I": 84.625,
            "J": 84.1875,
            "K": 87.8,
            "L": 88.8,
            "A": 86.73333333333333,
            "B": 85.53333333333333
        },
        "bus1": {
            "F": 73.4375,
            "G": 74.25,
            "H": 75.0625,
            "I": 76,
            "J": 76.3125,
            "K": 76.8125,
            "L": 78.6875,
            "A": 81.33333333333333,
            "B": 80.66666666666667,
            "C": 81.6,
            "D": 79.33333333333333,
            "E": 79.4
        },
        "bus2": {
            "I": 77.3125,
            "J": 78.3125,
            "K": 78.875,
            "L": 80.0625,
            "A": 79.25,
            "B": 78.375,
            "C": 79.46666666666667,
            "D": 79.33333333333333,
            "E": 79.8,
            "F": 77.46666666666667,
            "G": 78.8,
            "H": 79.26666666666667
        },
        "bus3": {
            "J": 73.75,
            "K": 73.625,
            "L": 71.8125,
            "A": 74,
            "B": 74.6875,
            "C": 76.5625,
            "D": 75.6875,
            "E": 75.33333333333333,
            "F": 76,
            "G": 78.06666666666666,
            "H": 79,
            "I": 78.86666666666666
        },
        "bus4": {
            "K": 77.4375,
            "L": 76.375,
            "A": 78.125,
            "B": 78.1875,
            "C": 79,
            "D": 79.3125,
            "E": 77.4375,
            "F": 77.6875,
            "G": 78.93333333333334,
            "H": 80.93333333333334,
            "I": 81.06666666666666,
            "J": 80.06666666666666
        },
        "bus5": {
            "A": 76.9375,
            "B": 77.4375,
            "C": 77.9375,
            "D": 77.875,
            "E": 78.8125,
            "F": 80.25,
            "G": 80.4375,
            "H": 79.5625,
            "I": 81.86666666666666,
            "J": 82.8,
            "K": 82.4,
            "L": 81.53333333333333
        }
    }
}