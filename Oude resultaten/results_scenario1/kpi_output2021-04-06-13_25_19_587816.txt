{
    "Average_holding_time": 10.139458572600493,
    "Average_travel_time_passengers": 2495.4451908000433,
    "Average_speed_busses": 0.050791666666666666,
    "Average_experienced_crowding": 74.60689215466662,
    "Average_covariation_headway": 0.31100613985078424,
    "Average_covariation_headway_stops": {
        "C": 0.2298585179353124,
        "E": 0.1965611546656022,
        "G": 0.19520897419030608,
        "H": 0.19151979752022122,
        "J": 0.20170398280592244,
        "K": 0.20229930805685342,
        "F": 0.19090429233288503,
        "I": 0.18611116565213326,
        "L": 0.19854421372441503,
        "D": 0.19284261037942282,
        "A": 0.19485929863832113,
        "B": 0.21315967811634223
    },
    "Average_excess_waiting_time": {
        "C": 44.89640889460833,
        "E": 40.775235931203326,
        "G": 40.95189975193546,
        "H": 38.86541183409116,
        "J": 40.23343602201908,
        "K": 38.804395957201905,
        "F": 42.035452360498255,
        "I": 39.749718588299515,
        "L": 39.70929098816879,
        "D": 41.8885689170516,
        "A": 41.25401904638994,
        "B": 45.02761006835772
    },
    "Holding_per_stop": {
        "E": 8.823529411764707,
        "H": 5,
        "K": 8.446601941747574,
        "G": 8.316831683168317,
        "J": 14.117647058823529,
        "C": 20.495049504950494,
        "L": 13.529411764705882,
        "I": 5.882352941176471,
        "F": 10.990099009900991,
        "A": 5.643564356435643,
        "D": 10.099009900990099,
        "B": 10.396039603960396
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 88.41176470588235,
            "D": 89.3529411764706,
            "E": 88.52941176470588,
            "F": 90.23529411764706,
            "G": 89.05882352941177,
            "H": 89.47058823529412,
            "I": 89.76470588235294,
            "J": 90.94117647058823,
            "K": 91.82352941176471,
            "L": 91.88235294117646,
            "A": 94.3125,
            "B": 94.875
        },
        "bus1": {
            "E": 73.17647058823529,
            "F": 74.41176470588235,
            "G": 75.3529411764706,
            "H": 75.3529411764706,
            "I": 75.70588235294117,
            "J": 76.52941176470588,
            "K": 77.88235294117646,
            "L": 78.76470588235294,
            "A": 78.88235294117646,
            "B": 77.52941176470588,
            "C": 75.4375,
            "D": 77.4375
        },
        "bus2": {
            "G": 64.82352941176471,
            "H": 65.82352941176471,
            "I": 67,
            "J": 67.52941176470588,
            "K": 68.94117647058823,
            "L": 73.11764705882354,
            "A": 73.17647058823529,
            "B": 72.82352941176471,
            "C": 73.58823529411765,
            "D": 71.82352941176471,
            "E": 70.29411764705883,
            "F": 69.1875
        },
        "bus3": {
            "H": 58.88235294117647,
            "I": 60.94117647058823,
            "J": 61.11764705882353,
            "K": 60.05882352941177,
            "L": 61,
            "A": 62.470588235294116,
            "B": 61.76470588235294,
            "C": 62.76470588235294,
            "D": 64.23529411764706,
            "E": 64.58823529411765,
            "F": 63.88235294117647,
            "G": 65.94117647058823
        },
        "bus4": {
            "J": 58.705882352941174,
            "K": 58.294117647058826,
            "L": 59.11764705882353,
            "A": 58.64705882352941,
            "B": 59.411764705882355,
            "C": 59.88235294117647,
            "D": 61.88235294117647,
            "E": 61.05882352941177,
            "F": 62.94117647058823,
            "G": 63,
            "H": 63.294117647058826,
            "I": 62.76470588235294
        },
        "bus5": {
            "K": 68.88888888888889,
            "L": 68.76470588235294,
            "A": 68.6470588235294,
            "B": 69.23529411764706,
            "C": 72.76470588235294,
            "D": 73.41176470588235,
            "E": 75.23529411764706,
            "F": 75.52941176470588,
            "G": 74.05882352941177,
            "H": 75.05882352941177,
            "I": 74.88235294117646,
            "J": 75
        }
    }
}