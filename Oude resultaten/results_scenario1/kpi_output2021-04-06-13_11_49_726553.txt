{
    "Average_holding_time": 14.98744769874477,
    "Average_travel_time_passengers": 2546.7463987162423,
    "Average_speed_busses": 0.049791666666666665,
    "Average_experienced_crowding": 74.89550964837127,
    "Average_covariation_headway": 0.11939513768251028,
    "Average_covariation_headway_stops": {
        "B": 0.08261322329296113,
        "C": 0.08507092560146744,
        "F": 0.09429569597544678,
        "H": 0.09107066324379931,
        "J": 0.0887818503872361,
        "L": 0.08474202716896453,
        "K": 0.06801779532321917,
        "D": 0.06388178081041433,
        "I": 0.0695866667685438,
        "A": 0.06286997730049766,
        "G": 0.06968141616771968,
        "E": 0.07247710559781312
    },
    "Average_excess_waiting_time": {
        "B": 36.97563476434601,
        "C": 35.61708139376708,
        "F": 37.56717114501009,
        "H": 37.36503696297956,
        "J": 37.13235700936775,
        "L": 37.121367007594074,
        "K": 37.90744484138207,
        "D": 36.163446250325705,
        "I": 37.83465504634637,
        "A": 37.727267945256415,
        "G": 37.919775554698845,
        "E": 38.03291650849644
    },
    "Holding_per_stop": {
        "J": 19.8,
        "C": 14.4,
        "H": 10.5,
        "L": 16.2,
        "F": 14.1,
        "B": 7.5,
        "D": 10.8,
        "K": 39.696969696969695,
        "I": 18.78787878787879,
        "G": 12.121212121212121,
        "A": 5.151515151515151,
        "E": 10.909090909090908
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 69.76470588235294,
            "C": 70.70588235294117,
            "D": 71.41176470588235,
            "E": 71.58823529411765,
            "F": 73.3529411764706,
            "G": 73.11764705882354,
            "H": 73.3529411764706,
            "I": 71.875,
            "J": 72.3125,
            "K": 72.8125,
            "L": 71.625,
            "A": 72.9375
        },
        "bus1": {
            "C": 71,
            "D": 71.94117647058823,
            "E": 72.70588235294117,
            "F": 76,
            "G": 74.94117647058823,
            "H": 75.41176470588235,
            "I": 77.29411764705883,
            "J": 76.47058823529412,
            "K": 76.1875,
            "L": 75.125,
            "A": 74.25,
            "B": 74.125
        },
        "bus2": {
            "F": 72.3529411764706,
            "G": 72.47058823529412,
            "H": 73.29411764705883,
            "I": 72.29411764705883,
            "J": 73.70588235294117,
            "K": 74.17647058823529,
            "L": 74.41176470588235,
            "A": 75.6875,
            "B": 77.375,
            "C": 78.4375,
            "D": 77.5,
            "E": 76.375
        },
        "bus3": {
            "H": 71.88235294117646,
            "I": 72.76470588235294,
            "J": 73.94117647058823,
            "K": 73.88235294117646,
            "L": 73.70588235294117,
            "A": 74.05882352941177,
            "B": 75.76470588235294,
            "C": 78.125,
            "D": 78.125,
            "E": 79.625,
            "F": 79.875,
            "G": 78.5
        },
        "bus4": {
            "J": 74.94117647058823,
            "K": 74.47058823529412,
            "L": 74.05882352941177,
            "A": 73.58823529411765,
            "B": 73,
            "C": 73.47058823529412,
            "D": 73.70588235294117,
            "E": 75,
            "F": 75.875,
            "G": 79.1875,
            "H": 79.25,
            "I": 79.125
        },
        "bus5": {
            "L": 73.23529411764706,
            "A": 72.52941176470588,
            "B": 73.3529411764706,
            "C": 73.82352941176471,
            "D": 73.76470588235294,
            "E": 73.58823529411765,
            "F": 73.88235294117646,
            "G": 73.375,
            "H": 74.375,
            "I": 74.9375,
            "J": 75.5,
            "K": 76.625
        }
    }
}