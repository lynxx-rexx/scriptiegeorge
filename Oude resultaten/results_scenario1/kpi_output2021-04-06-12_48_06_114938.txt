{
    "Average_holding_time": 24.50302506482282,
    "Average_travel_time_passengers": 2624.228547401004,
    "Average_speed_busses": 0.04820833333333333,
    "Average_experienced_crowding": 77.19023859576919,
    "Average_covariation_headway": 0.1052058702828576,
    "Average_covariation_headway_stops": {
        "B": 0.09227872852529721,
        "D": 0.08270009045834119,
        "H": 0.10650820654080842,
        "J": 0.09224552288336728,
        "K": 0.09126521770749503,
        "A": 0.08630672006277997,
        "C": 0.060904599124199064,
        "E": 0.046975787573444415,
        "L": 0.0573152012805465,
        "I": 0.0690229232898858,
        "F": 0.04869273691598629,
        "G": 0.07989343557292504
    },
    "Average_excess_waiting_time": {
        "B": 46.03028264153221,
        "D": 45.411304673104326,
        "H": 50.02387426171475,
        "J": 49.33127785179323,
        "K": 47.62116920553569,
        "A": 47.268744052182115,
        "C": 46.05788178807927,
        "E": 45.59844307208459,
        "L": 47.66217156657143,
        "I": 49.63443941119567,
        "F": 47.12013709046522,
        "G": 49.98777220880174
    },
    "Holding_per_stop": {
        "B": 11.75257731958763,
        "D": 24.432989690721648,
        "K": 30,
        "H": 27.8125,
        "J": 26.875,
        "A": 5.257731958762887,
        "E": 26.288659793814432,
        "C": 28.762886597938145,
        "L": 32.5,
        "F": 25.9375,
        "I": 28.4375,
        "G": 26.25
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 75.6470588235294,
            "C": 77.94117647058823,
            "D": 77.4375,
            "E": 77.4375,
            "F": 80.4375,
            "G": 82.75,
            "H": 81.6875,
            "I": 80.9375,
            "J": 82.375,
            "K": 80.0625,
            "L": 79.375,
            "A": 80.125
        },
        "bus1": {
            "D": 78,
            "E": 77.82352941176471,
            "F": 76.4375,
            "G": 77.8125,
            "H": 76.4375,
            "I": 79.6875,
            "J": 79.875,
            "K": 80.5,
            "L": 80.5625,
            "A": 81.3125,
            "B": 81.625,
            "C": 83.5625
        },
        "bus2": {
            "H": 75.75,
            "I": 76.9375,
            "J": 78.25,
            "K": 78.0625,
            "L": 77.125,
            "A": 75.9375,
            "B": 76.3125,
            "C": 77.75,
            "D": 78.9375,
            "E": 78.5625,
            "F": 77.8125,
            "G": 76.8125
        },
        "bus3": {
            "J": 71.4375,
            "K": 73,
            "L": 73.875,
            "A": 74.0625,
            "B": 75.375,
            "C": 75.1875,
            "D": 76.625,
            "E": 74.9375,
            "F": 76.75,
            "G": 76.375,
            "H": 74.25,
            "I": 74.1875
        },
        "bus4": {
            "K": 71.17647058823529,
            "L": 70.125,
            "A": 72.4375,
            "B": 72.0625,
            "C": 73.8125,
            "D": 75,
            "E": 77.3125,
            "F": 77.1875,
            "G": 77.6875,
            "H": 76.5625,
            "I": 77.0625,
            "J": 76.8125
        },
        "bus5": {
            "A": 72.76470588235294,
            "B": 75.0625,
            "C": 73.375,
            "D": 72.75,
            "E": 72.875,
            "F": 73.5,
            "G": 76.25,
            "H": 77.0625,
            "I": 78.125,
            "J": 78.125,
            "K": 77,
            "L": 75.125
        }
    }
}
    }
}0.5625
        }
    }
}