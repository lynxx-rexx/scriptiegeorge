{
    "Average_holding_time": 15.490360435875942,
    "Average_travel_time_passengers": 2550.4718492407856,
    "Average_speed_busses": 0.049708333333333334,
    "Average_experienced_crowding": 75.45236980674362,
    "Average_covariation_headway": 0.1340055748897741,
    "Average_covariation_headway_stops": {
        "E": 0.17488715874654912,
        "F": 0.1694587928794278,
        "G": 0.16412051180934525,
        "H": 0.15911904256138207,
        "K": 0.12607166464559338,
        "L": 0.11908811350599677,
        "I": 0.12816193790287667,
        "A": 0.08572588182560595,
        "J": 0.10727513586937712,
        "B": 0.08549310583212827,
        "C": 0.10716906907556181,
        "D": 0.1514709543492204
    },
    "Average_excess_waiting_time": {
        "E": 47.165534762049845,
        "F": 45.01727626916977,
        "G": 42.835575558169296,
        "H": 41.03488160692666,
        "K": 39.50755534336656,
        "L": 37.58140968256714,
        "I": 39.8476926191654,
        "A": 37.171014176374456,
        "J": 39.77598628171569,
        "B": 38.66486290641427,
        "C": 41.36289595764089,
        "D": 46.32568916674762
    },
    "Holding_per_stop": {
        "F": 13.93939393939394,
        "H": 14.4,
        "L": 18.3,
        "G": 20.4,
        "K": 31.8,
        "E": 11.212121212121213,
        "I": 17.4,
        "A": 4.5,
        "J": 19.393939393939394,
        "B": 8.484848484848484,
        "C": 16.060606060606062,
        "D": 9.795918367346939
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "E": 72.94117647058823,
            "F": 72.47058823529412,
            "G": 72.6470588235294,
            "H": 72.82352941176471,
            "I": 73.94117647058823,
            "J": 74.0625,
            "K": 74.1875,
            "L": 75.25,
            "A": 77.125,
            "B": 78.0625,
            "C": 78.4375,
            "D": 77.5625
        },
        "bus1": {
            "F": 74.11764705882354,
            "G": 74.17647058823529,
            "H": 72.6470588235294,
            "I": 70.88235294117646,
            "J": 71.11764705882354,
            "K": 71.82352941176471,
            "L": 71.8125,
            "A": 74.9375,
            "B": 76.5,
            "C": 74.6875,
            "D": 76.9375,
            "E": 79.1875
        },
        "bus2": {
            "G": 67.82352941176471,
            "H": 70.17647058823529,
            "I": 73.3529411764706,
            "J": 74,
            "K": 74.41176470588235,
            "L": 73.88235294117646,
            "A": 72.94117647058823,
            "B": 75,
            "C": 71.9375,
            "D": 72.5,
            "E": 71.25,
            "F": 71.75
        },
        "bus3": {
            "H": 68,
            "I": 68.05882352941177,
            "J": 68.23529411764706,
            "K": 70.52941176470588,
            "L": 72.94117647058823,
            "A": 74.05882352941177,
            "B": 75.6470588235294,
            "C": 74.58823529411765,
            "D": 74.9375,
            "E": 73.3125,
            "F": 73.25,
            "G": 72.5
        },
        "bus4": {
            "K": 71,
            "L": 71.29411764705883,
            "A": 73.70588235294117,
            "B": 75.76470588235294,
            "C": 74.23529411764706,
            "D": 75.88235294117646,
            "E": 75.6470588235294,
            "F": 76.75,
            "G": 76,
            "H": 75.4375,
            "I": 76.0625,
            "J": 75.125
        },
        "bus5": {
            "L": 76.94117647058823,
            "A": 75.88235294117646,
            "B": 77.05882352941177,
            "C": 77.52941176470588,
            "D": 78.3529411764706,
            "E": 79.76470588235294,
            "F": 80.76470588235294,
            "G": 81.88235294117646,
            "H": 80.875,
            "I": 81,
            "J": 81.875,
            "K": 81.25
        }
    }
}K": 77.625
        }
    }
}    "K": 71.5
        }
    }
}