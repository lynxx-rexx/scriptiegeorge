{
    "Average_holding_time": 32.89544235924933,
    "Average_travel_time_passengers": 2745.995516653889,
    "Average_speed_busses": 0.046625,
    "Average_experienced_crowding": 82.27797401122616,
    "Average_covariation_headway": 0.11208816067338413,
    "Average_covariation_headway_stops": {
        "B": 0.09766977536694166,
        "C": 0.10056699982205221,
        "D": 0.10625628162059672,
        "F": 0.09388728466479633,
        "J": 0.11873854254595385,
        "K": 0.11749609907973937,
        "L": 0.09006392459608752,
        "E": 0.0734011077716635,
        "G": 0.061468045483296305,
        "A": 0.07671261349010888,
        "H": 0.05760281131736992,
        "I": 0.08808149630229775
    },
    "Average_excess_waiting_time": {
        "B": 59.423250243082066,
        "C": 57.87131537940866,
        "D": 56.49429511056195,
        "F": 55.64284690868715,
        "J": 60.906614385720445,
        "K": 59.040696083695934,
        "L": 59.21995509410601,
        "E": 56.39187262588615,
        "G": 55.777620254895965,
        "A": 60.20080534377411,
        "H": 57.32183691737953,
        "I": 60.43524162012642
    },
    "Holding_per_stop": {
        "K": 31.595744680851062,
        "D": 33.829787234042556,
        "F": 33.51063829787234,
        "C": 35.483870967741936,
        "J": 34.516129032258064,
        "B": 31.612903225806452,
        "E": 31.612903225806452,
        "L": 33.87096774193548,
        "G": 32.5531914893617,
        "A": 28.043478260869566,
        "H": 34.193548387096776,
        "I": 33.87096774193548
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 80.4375,
            "C": 82.5625,
            "D": 83.5,
            "E": 85.6875,
            "F": 86.125,
            "G": 83.75,
            "H": 84.66666666666667,
            "I": 85.66666666666667,
            "J": 85.73333333333333,
            "K": 87.8,
            "L": 85.86666666666666,
            "A": 85.46666666666667
        },
        "bus1": {
            "C": 75.875,
            "D": 77.0625,
            "E": 77.3125,
            "F": 77.5,
            "G": 76,
            "H": 77.75,
            "I": 80.1875,
            "J": 79.06666666666666,
            "K": 78.66666666666667,
            "L": 79.4,
            "A": 81.33333333333333,
            "B": 80.26666666666667
        },
        "bus2": {
            "D": 76.4375,
            "E": 76.3125,
            "F": 76.3125,
            "G": 77.375,
            "H": 77.5,
            "I": 78.9375,
            "J": 79.375,
            "K": 79.875,
            "L": 78.33333333333333,
            "A": 78.93333333333334,
            "B": 80.73333333333333,
            "C": 81.13333333333334
        },
        "bus3": {
            "F": 78.125,
            "G": 80.8125,
            "H": 80.6875,
            "I": 81.1875,
            "J": 82.6875,
            "K": 84.1875,
            "L": 83.75,
            "A": 81.8125,
            "B": 83.26666666666667,
            "C": 82,
            "D": 82.8,
            "E": 81.86666666666666
        },
        "bus4": {
            "J": 80.625,
            "K": 80.375,
            "L": 81.375,
            "A": 82.5625,
            "B": 83.3125,
            "C": 83.4375,
            "D": 84.53333333333333,
            "E": 83.93333333333334,
            "F": 84.13333333333334,
            "G": 85.6,
            "H": 85.4,
            "I": 84.93333333333334
        },
        "bus5": {
            "K": 78.875,
            "L": 82,
            "A": 83.8125,
            "B": 83.4375,
            "C": 83.375,
            "D": 84.3125,
            "E": 85.9375,
            "F": 83.66666666666667,
            "G": 85.2,
            "H": 82.73333333333333,
            "I": 83.4,
            "J": 83.73333333333333
        }
    }
}