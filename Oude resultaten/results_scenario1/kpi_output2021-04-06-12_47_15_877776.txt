{
    "Average_holding_time": 25.24804177545692,
    "Average_travel_time_passengers": 2654.0326533432567,
    "Average_speed_busses": 0.047875,
    "Average_experienced_crowding": 78.46056282067579,
    "Average_covariation_headway": 0.11557123436718904,
    "Average_covariation_headway_stops": {
        "E": 0.15776185857026626,
        "G": 0.12710224171026235,
        "H": 0.11448769513874023,
        "I": 0.11378377151939328,
        "J": 0.1295985540980937,
        "L": 0.10633143527522733,
        "K": 0.10001751992950847,
        "A": 0.0788541140117448,
        "F": 0.12807674917113598,
        "B": 0.07909659705225616,
        "C": 0.09271623227346196,
        "D": 0.1340229553675657
    },
    "Average_excess_waiting_time": {
        "E": 55.94750767203715,
        "G": 53.137475142770086,
        "H": 50.57108377614816,
        "I": 48.98665308073578,
        "J": 48.581324212889626,
        "L": 46.952514861399834,
        "K": 48.20097175000086,
        "A": 47.06865356714496,
        "F": 54.852125341208534,
        "B": 48.648989437109094,
        "C": 50.99503004932308,
        "D": 55.416344248763096
    },
    "Holding_per_stop": {
        "J": 28.144329896907216,
        "L": 28.144329896907216,
        "H": 29.6875,
        "I": 28.125,
        "E": 28.105263157894736,
        "G": 27.473684210526315,
        "K": 32.5,
        "A": 2.5,
        "B": 10.9375,
        "F": 30.63157894736842,
        "C": 31.57894736842105,
        "D": 25.263157894736842
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "E": 77.375,
            "F": 79.5,
            "G": 80.1875,
            "H": 83.5,
            "I": 83.6875,
            "J": 85.9375,
            "K": 85.8125,
            "L": 84.0625,
            "A": 83.5625,
            "B": 81.9375,
            "C": 81.53333333333333,
            "D": 82.86666666666666
        },
        "bus1": {
            "G": 73.375,
            "H": 74.125,
            "I": 75,
            "J": 76.625,
            "K": 78.375,
            "L": 78.75,
            "A": 80.1875,
            "B": 81,
            "C": 79.3125,
            "D": 81.375,
            "E": 81.9375,
            "F": 79.8
        },
        "bus2": {
            "H": 75.625,
            "I": 77,
            "J": 77.0625,
            "K": 77.3125,
            "L": 76.8125,
            "A": 78.9375,
            "B": 80.0625,
            "C": 79.5625,
            "D": 80.25,
            "E": 80.8125,
            "F": 81.0625,
            "G": 81
        },
        "bus3": {
            "I": 75.76470588235294,
            "J": 74.4375,
            "K": 73.375,
            "L": 73.5,
            "A": 75.25,
            "B": 75.6875,
            "C": 75.875,
            "D": 76.5625,
            "E": 78.5,
            "F": 78.5,
            "G": 79.1875,
            "H": 80.625
        },
        "bus4": {
            "J": 69.94117647058823,
            "K": 70.82352941176471,
            "L": 71.4375,
            "A": 72.9375,
            "B": 71.875,
            "C": 72.3125,
            "D": 73.9375,
            "E": 73.875,
            "F": 73.8125,
            "G": 72.375,
            "H": 73.375,
            "I": 73.6875
        },
        "bus5": {
            "L": 75.23529411764706,
            "A": 77.41176470588235,
            "B": 78.5625,
            "C": 78.875,
            "D": 81,
            "E": 79.9375,
            "F": 80.4375,
            "G": 79.1875,
            "H": 80.3125,
            "I": 80.125,
            "J": 76.75,
            "K": 78.125
        }
    }
}