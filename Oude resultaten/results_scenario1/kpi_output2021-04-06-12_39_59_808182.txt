{
    "Average_holding_time": 10.041118421052632,
    "Average_travel_time_passengers": 2502.843496455906,
    "Average_speed_busses": 0.050666666666666665,
    "Average_experienced_crowding": 74.83965739883276,
    "Average_covariation_headway": 0.10615338527733822,
    "Average_covariation_headway_stops": {
        "C": 0.08587708352848923,
        "D": 0.08666345440395366,
        "G": 0.08509246534834507,
        "H": 0.08521764983912451,
        "L": 0.12235778575658515,
        "A": 0.11196682810090293,
        "E": 0.05955631658566239,
        "B": 0.07927233254279055,
        "I": 0.054926735940664675,
        "F": 0.061225177699699436,
        "J": 0.05845162546701248,
        "K": 0.09908735117371066
    },
    "Average_excess_waiting_time": {
        "C": 31.9695954050124,
        "D": 30.46334846651365,
        "G": 32.01943259616672,
        "H": 30.613595649995148,
        "L": 35.825648930273246,
        "A": 33.50616941548918,
        "E": 30.762369521743608,
        "B": 33.09999393820294,
        "I": 30.770456419571588,
        "F": 32.29908125226768,
        "J": 32.23683054444433,
        "K": 35.53284388076804
    },
    "Holding_per_stop": {
        "D": 6.176470588235294,
        "A": 2.9411764705882355,
        "H": 8.235294117647058,
        "C": 13.366336633663366,
        "L": 12.772277227722773,
        "G": 10,
        "B": 5.643564356435643,
        "E": 6.470588235294118,
        "I": 13.069306930693068,
        "F": 8.316831683168317,
        "J": 11.881188118811881,
        "K": 21.9
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 68,
            "D": 69.11764705882354,
            "E": 70.70588235294117,
            "F": 71.94117647058823,
            "G": 72.05882352941177,
            "H": 70.23529411764706,
            "I": 69.3529411764706,
            "J": 71.76470588235294,
            "K": 73.88235294117646,
            "L": 73.3529411764706,
            "A": 72.94117647058823,
            "B": 74.25
        },
        "bus1": {
            "D": 74.70588235294117,
            "E": 74.29411764705883,
            "F": 73.05882352941177,
            "G": 73.82352941176471,
            "H": 74.76470588235294,
            "I": 75.17647058823529,
            "J": 77.17647058823529,
            "K": 77.05882352941177,
            "L": 77.76470588235294,
            "A": 77.70588235294117,
            "B": 79.76470588235294,
            "C": 79.94117647058823
        },
        "bus2": {
            "G": 71.70588235294117,
            "H": 72.58823529411765,
            "I": 73.17647058823529,
            "J": 74.17647058823529,
            "K": 75.11764705882354,
            "L": 76.3529411764706,
            "A": 76.6470588235294,
            "B": 76.52941176470588,
            "C": 76.94117647058823,
            "D": 78.11764705882354,
            "E": 77.29411764705883,
            "F": 76.1875
        },
        "bus3": {
            "H": 72.11764705882354,
            "I": 72.41176470588235,
            "J": 73.47058823529412,
            "K": 74.76470588235294,
            "L": 75.6470588235294,
            "A": 76.11764705882354,
            "B": 77.05882352941177,
            "C": 75.41176470588235,
            "D": 74.47058823529412,
            "E": 72.88235294117646,
            "F": 73.17647058823529,
            "G": 73.76470588235294
        },
        "bus4": {
            "L": 74.6470588235294,
            "A": 74.6470588235294,
            "B": 73.3529411764706,
            "C": 75.29411764705883,
            "D": 75.41176470588235,
            "E": 76.41176470588235,
            "F": 78.11764705882354,
            "G": 76.82352941176471,
            "H": 78.88235294117646,
            "I": 78.6470588235294,
            "J": 79.3125,
            "K": 80.0625
        },
        "bus5": {
            "A": 70.05882352941177,
            "B": 69.94117647058823,
            "C": 72.3529411764706,
            "D": 71.3529411764706,
            "E": 72.94117647058823,
            "F": 73.94117647058823,
            "G": 74,
            "H": 75.05882352941177,
            "I": 73.94117647058823,
            "J": 73.29411764705883,
            "K": 75.41176470588235,
            "L": 75.0625
        }
    }
}