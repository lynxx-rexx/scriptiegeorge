{
    "Average_holding_time": 16.897133220910625,
    "Average_travel_time_passengers": 2562.6940537420032,
    "Average_speed_busses": 0.049416666666666664,
    "Average_experienced_crowding": 75.60773279384254,
    "Average_covariation_headway": 0.14936908376628144,
    "Average_covariation_headway_stops": {
        "E": 0.1826509832628936,
        "F": 0.1694557876827126,
        "G": 0.16361964209952476,
        "I": 0.13705069080121607,
        "K": 0.11595822634076094,
        "L": 0.11887945742766878,
        "J": 0.10150037845746128,
        "A": 0.09285561546430725,
        "H": 0.1327922813864586,
        "B": 0.09834469255134165,
        "C": 0.11329373311381255,
        "D": 0.15529634607326512
    },
    "Average_excess_waiting_time": {
        "E": 48.91371877597163,
        "F": 45.95709938667511,
        "G": 43.85456732431635,
        "I": 41.36038546216054,
        "K": 40.07596914282885,
        "L": 38.925537396207744,
        "J": 40.590973934436704,
        "A": 38.7192902091071,
        "H": 42.62000374833417,
        "B": 40.42313060704538,
        "C": 42.97455302807458,
        "D": 47.80127807860424
    },
    "Holding_per_stop": {
        "F": 17.272727272727273,
        "I": 22.424242424242426,
        "L": 18,
        "G": 16.060606060606062,
        "K": 42.72727272727273,
        "E": 10.408163265306122,
        "H": 16.363636363636363,
        "A": 2.4242424242424243,
        "J": 18.78787878787879,
        "B": 10.909090909090908,
        "C": 17.448979591836736,
        "D": 9.795918367346939
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "E": 72.52941176470588,
            "F": 72.94117647058823,
            "G": 72.05882352941177,
            "H": 73.05882352941177,
            "I": 74.29411764705883,
            "J": 74.875,
            "K": 76.375,
            "L": 77.125,
            "A": 75.375,
            "B": 76.125,
            "C": 76.8125,
            "D": 77.0625
        },
        "bus1": {
            "F": 71.52941176470588,
            "G": 70.6470588235294,
            "H": 71,
            "I": 73.23529411764706,
            "J": 73.3529411764706,
            "K": 72.375,
            "L": 72.5625,
            "A": 72.375,
            "B": 73.125,
            "C": 73.75,
            "D": 75.75,
            "E": 75.5625
        },
        "bus2": {
            "G": 74.17647058823529,
            "H": 73.29411764705883,
            "I": 74.41176470588235,
            "J": 75.23529411764706,
            "K": 75.58823529411765,
            "L": 76.52941176470588,
            "A": 75.0625,
            "B": 77.1875,
            "C": 78.6875,
            "D": 78.875,
            "E": 79.3125,
            "F": 78.8125
        },
        "bus3": {
            "I": 67.3529411764706,
            "J": 68.3529411764706,
            "K": 67.6470588235294,
            "L": 68.11764705882354,
            "A": 68.29411764705883,
            "B": 67.05882352941177,
            "C": 67.1875,
            "D": 69.1875,
            "E": 69.5625,
            "F": 71.25,
            "G": 71.9375,
            "H": 71.875
        },
        "bus4": {
            "K": 72,
            "L": 73,
            "A": 74.17647058823529,
            "B": 73.58823529411765,
            "C": 75.88235294117646,
            "D": 75.11764705882354,
            "E": 76.05882352941177,
            "F": 77.25,
            "G": 77.625,
            "H": 76.625,
            "I": 75.8125,
            "J": 75.9375
        },
        "bus5": {
            "L": 75.82352941176471,
            "A": 75.88235294117646,
            "B": 75.41176470588235,
            "C": 77.70588235294117,
            "D": 78.41176470588235,
            "E": 79.94117647058823,
            "F": 82.58823529411765,
            "G": 83.70588235294117,
            "H": 82.6875,
            "I": 83.3125,
            "J": 83.5625,
            "K": 82.3125
        }
    }
}