{
    "Average_holding_time": 10.451930977814298,
    "Average_travel_time_passengers": 2512.9207905152375,
    "Average_speed_busses": 0.050708333333333334,
    "Average_experienced_crowding": 73.84061419486251,
    "Average_covariation_headway": 0.10578739394948775,
    "Average_covariation_headway_stops": {
        "B": 0.08802577880078759,
        "C": 0.0993943545210011,
        "F": 0.09472375312336195,
        "H": 0.08693057789617908,
        "K": 0.09977024355213848,
        "A": 0.09057782895171489,
        "D": 0.06995692756632568,
        "I": 0.06932498353037815,
        "L": 0.061608033442501195,
        "G": 0.060536086794366444,
        "E": 0.07171354328392689,
        "J": 0.07475329762094866
    },
    "Average_excess_waiting_time": {
        "B": 31.43123195424505,
        "C": 30.525246794792167,
        "F": 31.658017268565175,
        "H": 31.42498915943952,
        "K": 33.50302745611725,
        "A": 33.08246112061693,
        "D": 30.48919830020509,
        "I": 32.11075776606543,
        "L": 33.291739596041566,
        "G": 31.524319351318297,
        "E": 32.01500823607938,
        "J": 33.89207892842302
    },
    "Holding_per_stop": {
        "C": 12.647058823529411,
        "H": 5.882352941176471,
        "B": 5.294117647058823,
        "K": 24.653465346534652,
        "F": 16.764705882352942,
        "A": 4.752475247524752,
        "D": 7.352941176470588,
        "I": 11.287128712871286,
        "G": 7.7227722772277225,
        "L": 12.178217821782178,
        "E": 7.7227722772277225,
        "J": 9.207920792079207
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 66.52941176470588,
            "C": 66.88235294117646,
            "D": 68,
            "E": 68.05882352941177,
            "F": 68.88235294117646,
            "G": 69.6470588235294,
            "H": 69.23529411764706,
            "I": 69.76470588235294,
            "J": 69.05882352941177,
            "K": 70.58823529411765,
            "L": 69.94117647058823,
            "A": 69.4375
        },
        "bus1": {
            "C": 73.29411764705883,
            "D": 74.17647058823529,
            "E": 74.29411764705883,
            "F": 73.17647058823529,
            "G": 73.3529411764706,
            "H": 74.11764705882354,
            "I": 74.6470588235294,
            "J": 76.58823529411765,
            "K": 77.94117647058823,
            "L": 78.11764705882354,
            "A": 78.05882352941177,
            "B": 78
        },
        "bus2": {
            "F": 74.17647058823529,
            "G": 74.52941176470588,
            "H": 75.17647058823529,
            "I": 75.70588235294117,
            "J": 75.3529411764706,
            "K": 74.82352941176471,
            "L": 75.6470588235294,
            "A": 74.47058823529412,
            "B": 75.41176470588235,
            "C": 75.47058823529412,
            "D": 76.23529411764706,
            "E": 77.1875
        },
        "bus3": {
            "H": 71.76470588235294,
            "I": 71.94117647058823,
            "J": 73.17647058823529,
            "K": 72.52941176470588,
            "L": 71.82352941176471,
            "A": 72.76470588235294,
            "B": 73.3529411764706,
            "C": 74.88235294117646,
            "D": 77.05882352941177,
            "E": 77.52941176470588,
            "F": 77.82352941176471,
            "G": 77.625
        },
        "bus4": {
            "K": 66.94117647058823,
            "L": 69,
            "A": 69.17647058823529,
            "B": 70.70588235294117,
            "C": 71.3529411764706,
            "D": 71.82352941176471,
            "E": 72,
            "F": 72.05882352941177,
            "G": 71.17647058823529,
            "H": 70.94117647058823,
            "I": 69.75,
            "J": 69.3125
        },
        "bus5": {
            "A": 74.6470588235294,
            "B": 74.47058823529412,
            "C": 73.23529411764706,
            "D": 73.76470588235294,
            "E": 75.23529411764706,
            "F": 75.41176470588235,
            "G": 74.82352941176471,
            "H": 76.41176470588235,
            "I": 77.23529411764706,
            "J": 76.47058823529412,
            "K": 76.75,
            "L": 77.375
        }
    }
}