{
    "Average_holding_time": 14.949832775919733,
    "Average_travel_time_passengers": 2544.339895567984,
    "Average_speed_busses": 0.049833333333333334,
    "Average_experienced_crowding": 74.79581917317867,
    "Average_covariation_headway": 0.15039120842014747,
    "Average_covariation_headway_stops": {
        "C": 0.10978027319776705,
        "F": 0.10965011564858089,
        "I": 0.12796088839678424,
        "J": 0.12103170456921065,
        "K": 0.12250182936245962,
        "A": 0.10762772494558431,
        "D": 0.07937485591967852,
        "L": 0.10893634684199441,
        "B": 0.08786621979299625,
        "G": 0.10075721790944918,
        "E": 0.08544441807915158,
        "H": 0.10195029523805083
    },
    "Average_excess_waiting_time": {
        "C": 37.0351150071661,
        "F": 38.514200506717316,
        "I": 41.484577598791134,
        "J": 39.321739441648674,
        "K": 37.64157065798037,
        "A": 36.70510394320979,
        "D": 36.775089651590065,
        "L": 38.4477866323889,
        "B": 36.98878256912678,
        "G": 39.38302149289848,
        "E": 38.69290922667943,
        "H": 41.14895275989369
    },
    "Holding_per_stop": {
        "C": 18.9,
        "J": 14.7,
        "K": 22.8,
        "A": 4.2,
        "F": 15.9,
        "I": 17.272727272727273,
        "L": 14.7,
        "B": 8.4,
        "D": 13.5,
        "G": 18.78787878787879,
        "E": 12.727272727272727,
        "H": 17.575757575757574
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 70,
            "D": 70.41176470588235,
            "E": 69.76470588235294,
            "F": 71.52941176470588,
            "G": 72.23529411764706,
            "H": 73.29411764705883,
            "I": 72.88235294117646,
            "J": 72.29411764705883,
            "K": 72.5625,
            "L": 73.125,
            "A": 73.1875,
            "B": 73.125
        },
        "bus1": {
            "F": 72.3529411764706,
            "G": 72.17647058823529,
            "H": 71.3529411764706,
            "I": 71.76470588235294,
            "J": 73,
            "K": 73.11764705882354,
            "L": 74.11764705882354,
            "A": 75.375,
            "B": 76.75,
            "C": 75.75,
            "D": 75.75,
            "E": 76.6875
        },
        "bus2": {
            "I": 74.41176470588235,
            "J": 72.76470588235294,
            "K": 73.70588235294117,
            "L": 73.88235294117646,
            "A": 73.17647058823529,
            "B": 73.88235294117646,
            "C": 74.6875,
            "D": 75.125,
            "E": 76.625,
            "F": 77.9375,
            "G": 78.375,
            "H": 78.8125
        },
        "bus3": {
            "J": 72.82352941176471,
            "K": 72.58823529411765,
            "L": 73.70588235294117,
            "A": 74,
            "B": 73.6470588235294,
            "C": 75.3529411764706,
            "D": 76.17647058823529,
            "E": 75.125,
            "F": 76.5625,
            "G": 77.0625,
            "H": 75.8125,
            "I": 75.25
        },
        "bus4": {
            "K": 70.52941176470588,
            "L": 70.23529411764706,
            "A": 70.52941176470588,
            "B": 70.47058823529412,
            "C": 72.05882352941177,
            "D": 74.94117647058823,
            "E": 77,
            "F": 78.88235294117646,
            "G": 77.5,
            "H": 79.375,
            "I": 79.875,
            "J": 77.375
        },
        "bus5": {
            "A": 71.6470588235294,
            "B": 71.47058823529412,
            "C": 71.88235294117646,
            "D": 72.47058823529412,
            "E": 72.88235294117646,
            "F": 72.94117647058823,
            "G": 73.70588235294117,
            "H": 74.52941176470588,
            "I": 75.8125,
            "J": 76,
            "K": 76.25,
            "L": 75.75
        }
    }
}  "K": 72.875,
            "L": 72.1875
        }
    }
}