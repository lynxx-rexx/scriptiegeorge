{
    "Average_holding_time": 28.390501319261215,
    "Average_travel_time_passengers": 2672.742687489353,
    "Average_speed_busses": 0.047375,
    "Average_experienced_crowding": 79.73462459692215,
    "Average_covariation_headway": 0.1312115480084134,
    "Average_covariation_headway_stops": {
        "B": 0.10717922063757485,
        "G": 0.16987792318517628,
        "H": 0.17051194551723348,
        "I": 0.16193228556442515,
        "L": 0.10772018266134396,
        "A": 0.1033034526895812,
        "J": 0.12217764051924379,
        "C": 0.08907090283994112,
        "K": 0.1116339511598896,
        "D": 0.08065554262222888,
        "E": 0.1085698526143587,
        "F": 0.1461486456865643
    },
    "Average_excess_waiting_time": {
        "B": 50.51568055276522,
        "G": 61.0242737176182,
        "H": 59.42491764570218,
        "I": 56.73411676536551,
        "L": 54.0805447603164,
        "A": 52.040656864296125,
        "J": 54.986623634909904,
        "C": 50.9811819781803,
        "K": 55.989422171065485,
        "D": 52.22393168678701,
        "E": 55.616970490742574,
        "F": 60.30368976587056
    },
    "Holding_per_stop": {
        "I": 33.473684210526315,
        "B": 26.5625,
        "H": 30,
        "A": 23.68421052631579,
        "G": 31.595744680851062,
        "L": 32.21052631578947,
        "J": 29.05263157894737,
        "C": 20.526315789473685,
        "K": 31.595744680851062,
        "D": 26.210526315789473,
        "E": 28.085106382978722,
        "F": 27.76595744680851
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 79.125,
            "C": 78.125,
            "D": 77.4375,
            "E": 78.875,
            "F": 80.875,
            "G": 81.5625,
            "H": 80.875,
            "I": 81.25,
            "J": 81.5,
            "K": 82.9375,
            "L": 84.4375,
            "A": 84.8
        },
        "bus1": {
            "G": 77.3125,
            "H": 77.5625,
            "I": 76.75,
            "J": 75.875,
            "K": 75.375,
            "L": 76.5,
            "A": 77.75,
            "B": 79.125,
            "C": 81.4,
            "D": 81.13333333333334,
            "E": 81.66666666666667,
            "F": 81.33333333333333
        },
        "bus2": {
            "H": 76.125,
            "I": 76.3125,
            "J": 78.125,
            "K": 77.5,
            "L": 77.3125,
            "A": 78.25,
            "B": 78.6875,
            "C": 77.25,
            "D": 78.1875,
            "E": 78.26666666666667,
            "F": 79.8,
            "G": 79.6
        },
        "bus3": {
            "I": 79.6875,
            "J": 79.875,
            "K": 78,
            "L": 77.6875,
            "A": 78.6875,
            "B": 77.9375,
            "C": 78.75,
            "D": 81.3125,
            "E": 82.75,
            "F": 82.6875,
            "G": 83.66666666666667,
            "H": 85.6
        },
        "bus4": {
            "L": 78.4375,
            "A": 76.625,
            "B": 77.0625,
            "C": 75.875,
            "D": 76.0625,
            "E": 76.875,
            "F": 78.625,
            "G": 79.125,
            "H": 80.5,
            "I": 81.53333333333333,
            "J": 85.2,
            "K": 85.46666666666667
        },
        "bus5": {
            "A": 74.5625,
            "B": 74.375,
            "C": 75.8125,
            "D": 77.125,
            "E": 77.375,
            "F": 76.25,
            "G": 76.0625,
            "H": 76,
            "I": 76.125,
            "J": 75.8125,
            "K": 78,
            "L": 79.26666666666667
        }
    }
}