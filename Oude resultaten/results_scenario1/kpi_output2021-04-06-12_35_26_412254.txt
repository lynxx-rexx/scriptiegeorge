{
    "Average_holding_time": 13.15,
    "Average_travel_time_passengers": 2554.105223621532,
    "Average_speed_busses": 0.05,
    "Average_experienced_crowding": 77.20985039695432,
    "Average_covariation_headway": 0.13710330135254378,
    "Average_covariation_headway_stops": {
        "C": 0.15297426281331702,
        "G": 0.1789660551047825,
        "H": 0.16920548579695027,
        "I": 0.16898349062907037,
        "J": 0.1690181715432786,
        "K": 0.1569120620645332,
        "L": 0.1305887773907245,
        "D": 0.14597579997366275,
        "A": 0.12249334913508729,
        "E": 0.13683401600765988,
        "B": 0.13405896817601853,
        "F": 0.16563068701684702
    },
    "Average_excess_waiting_time": {
        "C": 40.04527264887088,
        "G": 45.590974535314785,
        "H": 43.22397967828766,
        "I": 41.808467763642284,
        "J": 40.26610890003565,
        "K": 37.44166264156263,
        "L": 36.94314949221621,
        "D": 40.96138705218789,
        "A": 37.7119991034603,
        "E": 41.679658897002525,
        "B": 40.035705266909304,
        "F": 45.730309920807656
    },
    "Holding_per_stop": {
        "H": 12.6,
        "I": 12,
        "K": 28.81188118811881,
        "C": 13.2,
        "J": 15.148514851485148,
        "G": 13.333333333333334,
        "L": 16.336633663366335,
        "D": 13.8,
        "A": 2.7,
        "E": 12.727272727272727,
        "B": 3.3,
        "F": 13.636363636363637
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 75.82352941176471,
            "D": 76.6470588235294,
            "E": 75.23529411764706,
            "F": 75.70588235294117,
            "G": 77.88235294117646,
            "H": 79.6470588235294,
            "I": 79.94117647058823,
            "J": 79.94117647058823,
            "K": 78.4375,
            "L": 78.4375,
            "A": 78.75,
            "B": 80.75
        },
        "bus1": {
            "G": 76.52941176470588,
            "H": 76.23529411764706,
            "I": 78.58823529411765,
            "J": 80.52941176470588,
            "K": 81.41176470588235,
            "L": 81.05882352941177,
            "A": 81.8125,
            "B": 82.125,
            "C": 81.4375,
            "D": 82.375,
            "E": 82.3125,
            "F": 82.375
        },
        "bus2": {
            "H": 72.88235294117646,
            "I": 74.29411764705883,
            "J": 75.47058823529412,
            "K": 75.52941176470588,
            "L": 76.11764705882354,
            "A": 75.70588235294117,
            "B": 76.52941176470588,
            "C": 77.25,
            "D": 76.75,
            "E": 75.375,
            "F": 75.875,
            "G": 75.1875
        },
        "bus3": {
            "I": 70.47058823529412,
            "J": 72.05882352941177,
            "K": 72.52941176470588,
            "L": 73.52941176470588,
            "A": 72.6470588235294,
            "B": 73.82352941176471,
            "C": 74.82352941176471,
            "D": 74.47058823529412,
            "E": 74.375,
            "F": 76.4375,
            "G": 76.5,
            "H": 74.8125
        },
        "bus4": {
            "J": 68.3529411764706,
            "K": 67.82352941176471,
            "L": 67.70588235294117,
            "A": 70.41176470588235,
            "B": 73.6470588235294,
            "C": 73.76470588235294,
            "D": 73.94117647058823,
            "E": 74.05882352941177,
            "F": 74.17647058823529,
            "G": 73.5,
            "H": 72.8125,
            "I": 72.8125
        },
        "bus5": {
            "K": 74.88235294117646,
            "L": 74.82352941176471,
            "A": 76.3529411764706,
            "B": 79.17647058823529,
            "C": 79.47058823529412,
            "D": 80.17647058823529,
            "E": 80.47058823529412,
            "F": 79.05882352941177,
            "G": 79,
            "H": 78.29411764705883,
            "I": 78.5,
            "J": 79
        }
    }
}