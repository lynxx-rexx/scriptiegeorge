{
    "Average_holding_time": 26.469049694856146,
    "Average_travel_time_passengers": 2631.2830115103548,
    "Average_speed_busses": 0.04779166666666667,
    "Average_experienced_crowding": 77.06383590891103,
    "Average_covariation_headway": 0.12833778646632957,
    "Average_covariation_headway_stops": {
        "F": 0.17097552032189728,
        "G": 0.16112188484360243,
        "J": 0.12188266164113565,
        "K": 0.11875508733903714,
        "L": 0.1209355132921404,
        "A": 0.1267966961133764,
        "H": 0.13494693566572274,
        "B": 0.10678002626151044,
        "I": 0.11979107161787052,
        "C": 0.09044837249591003,
        "D": 0.11244408655359575,
        "E": 0.14599898708040795
    },
    "Average_excess_waiting_time": {
        "F": 57.493469717618495,
        "G": 55.001837249480445,
        "J": 53.24992313182037,
        "K": 51.3628273446443,
        "L": 49.90687386599092,
        "A": 48.91779550902538,
        "H": 54.22367802786732,
        "B": 48.78812146132242,
        "I": 54.646864030571464,
        "C": 49.267072849711155,
        "D": 52.28483199849791,
        "E": 56.75873932849112
    },
    "Holding_per_stop": {
        "G": 27.1875,
        "A": 8.350515463917526,
        "K": 31.5625,
        "L": 29.375,
        "F": 28.736842105263158,
        "J": 32.21052631578947,
        "H": 30.94736842105263,
        "B": 17.5,
        "C": 26.5625,
        "I": 31.263157894736842,
        "D": 25.263157894736842,
        "E": 29.05263157894737
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "F": 76.25,
            "G": 75.8125,
            "H": 77.375,
            "I": 77.1875,
            "J": 77.875,
            "K": 78.6875,
            "L": 79.4375,
            "A": 79.8125,
            "B": 78.0625,
            "C": 77.75,
            "D": 79.375,
            "E": 79.93333333333334
        },
        "bus1": {
            "G": 73.5625,
            "H": 72.75,
            "I": 72.0625,
            "J": 73.25,
            "K": 73.4375,
            "L": 74.25,
            "A": 74.6875,
            "B": 75.875,
            "C": 77.4375,
            "D": 77.625,
            "E": 76.625,
            "F": 75.6875
        },
        "bus2": {
            "J": 72,
            "K": 70.8125,
            "L": 71.5,
            "A": 73.8125,
            "B": 75.25,
            "C": 75.25,
            "D": 74.625,
            "E": 76.4375,
            "F": 76.1875,
            "G": 75.375,
            "H": 75.26666666666667,
            "I": 74.73333333333333
        },
        "bus3": {
            "K": 70.9375,
            "L": 70.3125,
            "A": 71.5625,
            "B": 73.5625,
            "C": 72.1875,
            "D": 72.75,
            "E": 73.125,
            "F": 72.9375,
            "G": 73.3125,
            "H": 73.6875,
            "I": 74.875,
            "J": 75.73333333333333
        },
        "bus4": {
            "L": 71.375,
            "A": 72.4375,
            "B": 72.9375,
            "C": 73.3125,
            "D": 71.8125,
            "E": 73.375,
            "F": 75.5625,
            "G": 75.9375,
            "H": 76.75,
            "I": 76.8125,
            "J": 76,
            "K": 76.4375
        },
        "bus5": {
            "A": 79.6470588235294,
            "B": 79.6875,
            "C": 80.5625,
            "D": 81.8125,
            "E": 84.8125,
            "F": 84.625,
            "G": 84.4375,
            "H": 84.6875,
            "I": 84.875,
            "J": 86.125,
            "K": 84.75,
            "L": 83.125
        }
    }
}