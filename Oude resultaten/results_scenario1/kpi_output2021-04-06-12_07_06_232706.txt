{
    "Average_holding_time": 16.66386554621849,
    "Average_travel_time_passengers": 2578.0110917369657,
    "Average_speed_busses": 0.04958333333333333,
    "Average_experienced_crowding": 76.14907681403594,
    "Average_covariation_headway": 0.18094444210446745,
    "Average_covariation_headway_stops": {
        "F": 0.19124705814463835,
        "G": 0.18288209188163854,
        "I": 0.15113427372953891,
        "K": 0.14543791448312046,
        "L": 0.1544253691459592,
        "A": 0.1683968709189083,
        "H": 0.155686956103227,
        "J": 0.12629864266545202,
        "B": 0.14937303433667412,
        "C": 0.14393487745965905,
        "D": 0.1435008211914595,
        "E": 0.17609606853292017
    },
    "Average_excess_waiting_time": {
        "F": 49.312296357414596,
        "G": 46.841528325295656,
        "I": 43.564203320708714,
        "K": 43.34480148583509,
        "L": 42.525689779493746,
        "A": 42.41330149155402,
        "H": 45.60401280226063,
        "J": 43.26212787785886,
        "B": 42.10129511139837,
        "C": 42.7255459410128,
        "D": 44.48182695057079,
        "E": 49.02715357085674
    },
    "Holding_per_stop": {
        "G": 16.060606060606062,
        "I": 14.242424242424242,
        "L": 18.9,
        "A": 4.5,
        "K": 26.363636363636363,
        "F": 21.818181818181817,
        "H": 14.242424242424242,
        "J": 21.21212121212121,
        "B": 12.6,
        "C": 25.454545454545453,
        "D": 12.424242424242424,
        "E": 12.244897959183673
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "F": 72.76470588235294,
            "G": 74.11764705882354,
            "H": 74.29411764705883,
            "I": 74.76470588235294,
            "J": 75.82352941176471,
            "K": 77.5625,
            "L": 79,
            "A": 77.8125,
            "B": 78.0625,
            "C": 77.5,
            "D": 76.3125,
            "E": 78.1875
        },
        "bus1": {
            "G": 73.47058823529412,
            "H": 74.29411764705883,
            "I": 74.52941176470588,
            "J": 73.58823529411765,
            "K": 74.29411764705883,
            "L": 74.29411764705883,
            "A": 75.0625,
            "B": 76.5,
            "C": 76.25,
            "D": 76,
            "E": 76.0625,
            "F": 77.4375
        },
        "bus2": {
            "I": 73.94117647058823,
            "J": 74.52941176470588,
            "K": 74.3529411764706,
            "L": 75.29411764705883,
            "A": 76.47058823529412,
            "B": 77.82352941176471,
            "C": 81.5625,
            "D": 82.875,
            "E": 83.375,
            "F": 82.6875,
            "G": 80.875,
            "H": 79.1875
        },
        "bus3": {
            "K": 69.82352941176471,
            "L": 69.94117647058823,
            "A": 68.11764705882354,
            "B": 69.17647058823529,
            "C": 67.82352941176471,
            "D": 70.23529411764706,
            "E": 69.5,
            "F": 71.1875,
            "G": 72.5625,
            "H": 72.6875,
            "I": 72.5625,
            "J": 72.8125
        },
        "bus4": {
            "L": 71.23529411764706,
            "A": 73.11764705882354,
            "B": 72.29411764705883,
            "C": 72.70588235294117,
            "D": 72.6470588235294,
            "E": 73.29411764705883,
            "F": 72.82352941176471,
            "G": 72.75,
            "H": 73.5,
            "I": 74.75,
            "J": 76.6875,
            "K": 75.75
        },
        "bus5": {
            "A": 74.6470588235294,
            "B": 77.58823529411765,
            "C": 78.11764705882354,
            "D": 79.11764705882354,
            "E": 78.47058823529412,
            "F": 79.58823529411765,
            "G": 78.23529411764706,
            "H": 77.6470588235294,
            "I": 78.125,
            "J": 77.4375,
            "K": 77.0625,
            "L": 77.4375
        }
    }
}