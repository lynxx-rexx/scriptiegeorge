{
    "Average_holding_time": 14.475,
    "Average_travel_time_passengers": 2494.3271162229676,
    "Average_speed_busses": 0.05,
    "Average_experienced_crowding": 72.47474971359881,
    "Average_covariation_headway": 0.15041996761451742,
    "Average_covariation_headway_stops": {
        "C": 0.12314126230826303,
        "E": 0.13344339172490793,
        "G": 0.10839058094334295,
        "H": 0.11347474450414294,
        "I": 0.12005757411092342,
        "L": 0.10552635097173778,
        "J": 0.10612405117938419,
        "F": 0.09591377936260599,
        "A": 0.09353500015251025,
        "D": 0.109232071328382,
        "K": 0.09219454402530905,
        "B": 0.09676940883666868
    },
    "Average_excess_waiting_time": {
        "C": 38.789080106693916,
        "E": 39.50871938593855,
        "G": 37.78547162783826,
        "H": 36.53139348927067,
        "I": 35.51691626890613,
        "L": 36.5833758550379,
        "J": 36.07459091911528,
        "F": 38.374726622092965,
        "A": 37.393757739085345,
        "D": 39.261154014316844,
        "K": 37.410864107321004,
        "B": 38.67984994725316
    },
    "Holding_per_stop": {
        "I": 16.93069306930693,
        "H": 13.2,
        "E": 18.6,
        "L": 10.5,
        "G": 16.8,
        "C": 24.6,
        "J": 13.8,
        "A": 3.3,
        "F": 15.6,
        "D": 16.2,
        "K": 15,
        "B": 9.090909090909092
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 66.76470588235294,
            "D": 68,
            "E": 69.23529411764706,
            "F": 70.29411764705883,
            "G": 71.58823529411765,
            "H": 71.3529411764706,
            "I": 72.11764705882354,
            "J": 71,
            "K": 70.625,
            "L": 70.8125,
            "A": 71.0625,
            "B": 71.3125
        },
        "bus1": {
            "E": 71.11764705882354,
            "F": 71.11764705882354,
            "G": 72.3529411764706,
            "H": 73.47058823529412,
            "I": 73.94117647058823,
            "J": 74.17647058823529,
            "K": 76.3529411764706,
            "L": 77.5,
            "A": 78.8125,
            "B": 78.6875,
            "C": 77.5625,
            "D": 74.9375
        },
        "bus2": {
            "G": 69.41176470588235,
            "H": 70,
            "I": 69.3529411764706,
            "J": 70.88235294117646,
            "K": 71.58823529411765,
            "L": 72.76470588235294,
            "A": 73.94117647058823,
            "B": 74.75,
            "C": 73.875,
            "D": 73,
            "E": 73.25,
            "F": 72.625
        },
        "bus3": {
            "H": 67.52941176470588,
            "I": 67.52941176470588,
            "J": 67.6470588235294,
            "K": 67.70588235294117,
            "L": 68.3529411764706,
            "A": 69.70588235294117,
            "B": 71.11764705882354,
            "C": 70.58823529411765,
            "D": 70.82352941176471,
            "E": 71.0625,
            "F": 71.625,
            "G": 71.75
        },
        "bus4": {
            "I": 70.47058823529412,
            "J": 72.41176470588235,
            "K": 72.88235294117646,
            "L": 72.94117647058823,
            "A": 72.52941176470588,
            "B": 74.41176470588235,
            "C": 76.76470588235294,
            "D": 76.6470588235294,
            "E": 76.29411764705883,
            "F": 73.58823529411765,
            "G": 74.0625,
            "H": 73.9375
        },
        "bus5": {
            "L": 69.52941176470588,
            "A": 71.70588235294117,
            "B": 72.94117647058823,
            "C": 72.41176470588235,
            "D": 72.82352941176471,
            "E": 71.52941176470588,
            "F": 71.52941176470588,
            "G": 70.41176470588235,
            "H": 72.41176470588235,
            "I": 72.75,
            "J": 71.875,
            "K": 72.5625
        }
    }
}