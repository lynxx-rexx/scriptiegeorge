{
    "Average_holding_time": 14.648829431438127,
    "Average_travel_time_passengers": 2545.8177218670407,
    "Average_speed_busses": 0.049833333333333334,
    "Average_experienced_crowding": 76.61133657443189,
    "Average_covariation_headway": 0.1490621494743473,
    "Average_covariation_headway_stops": {
        "B": 0.15844319617090974,
        "C": 0.1472867070254903,
        "D": 0.1465988826720056,
        "E": 0.15153732736308428,
        "G": 0.13387570838173043,
        "L": 0.1875050743724302,
        "F": 0.13605218415423156,
        "H": 0.10680172657538,
        "A": 0.16124925385437797,
        "I": 0.10851308775617849,
        "J": 0.13038970404704364,
        "K": 0.16642009539075173
    },
    "Average_excess_waiting_time": {
        "B": 44.80725360331746,
        "C": 42.08632308601483,
        "D": 40.474014044852936,
        "E": 39.318003682412154,
        "G": 38.019123554053465,
        "L": 47.445615799709,
        "F": 39.7163200095099,
        "H": 37.48223135412144,
        "A": 46.421771431169134,
        "I": 39.04777473336662,
        "J": 42.01025416405855,
        "K": 47.00201924365359
    },
    "Holding_per_stop": {
        "C": 18.3,
        "E": 10.2,
        "G": 8.910891089108912,
        "D": 10.8,
        "L": 17.87878787878788,
        "B": 12.121212121212121,
        "H": 7.5,
        "F": 17.4,
        "I": 16.2,
        "A": 5.757575757575758,
        "J": 16.363636363636363,
        "K": 34.54545454545455
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 73.23529411764706,
            "C": 72.70588235294117,
            "D": 72.76470588235294,
            "E": 73.23529411764706,
            "F": 74.76470588235294,
            "G": 74.6470588235294,
            "H": 76.75,
            "I": 78.875,
            "J": 79.1875,
            "K": 78.4375,
            "L": 76.875,
            "A": 77.9375
        },
        "bus1": {
            "C": 71.29411764705883,
            "D": 72.47058823529412,
            "E": 71.47058823529412,
            "F": 72.29411764705883,
            "G": 73.70588235294117,
            "H": 73.3529411764706,
            "I": 75,
            "J": 75.3125,
            "K": 77.25,
            "L": 77.3125,
            "A": 75.25,
            "B": 74.9375
        },
        "bus2": {
            "D": 75.6470588235294,
            "E": 74.82352941176471,
            "F": 73.17647058823529,
            "G": 72.47058823529412,
            "H": 72.70588235294117,
            "I": 72.29411764705883,
            "J": 74.41176470588235,
            "K": 76.41176470588235,
            "L": 77.5,
            "A": 77.75,
            "B": 79.125,
            "C": 80.6875
        },
        "bus3": {
            "E": 69.11764705882354,
            "F": 70.17647058823529,
            "G": 70.05882352941177,
            "H": 69.70588235294117,
            "I": 71.82352941176471,
            "J": 73.17647058823529,
            "K": 72.11764705882354,
            "L": 72.41176470588235,
            "A": 73,
            "B": 71.875,
            "C": 71.625,
            "D": 72.1875
        },
        "bus4": {
            "G": 76.70588235294117,
            "H": 78.23529411764706,
            "I": 78.52941176470588,
            "J": 78.11764705882354,
            "K": 78.58823529411765,
            "L": 80.23529411764706,
            "A": 83.47058823529412,
            "B": 81.52941176470588,
            "C": 81.52941176470588,
            "D": 81.75,
            "E": 80.5,
            "F": 81.3125
        },
        "bus5": {
            "L": 73.23529411764706,
            "A": 75.23529411764706,
            "B": 75.23529411764706,
            "C": 75.6470588235294,
            "D": 75.94117647058823,
            "E": 74.11764705882354,
            "F": 75.125,
            "G": 76.875,
            "H": 78.375,
            "I": 78.0625,
            "J": 77.8125,
            "K": 77.6875
        }
    }
}