{
    "Average_holding_time": 32.16577540106952,
    "Average_travel_time_passengers": 2719.8945445156646,
    "Average_speed_busses": 0.04675,
    "Average_experienced_crowding": 80.0593536618212,
    "Average_covariation_headway": 0.1299017656278322,
    "Average_covariation_headway_stops": {
        "B": 0.1611605296420167,
        "C": 0.15338418954868904,
        "D": 0.14166207878361844,
        "E": 0.13739548877437377,
        "G": 0.11345581366921456,
        "I": 0.10462314637891491,
        "J": 0.07773818034372168,
        "F": 0.1122513936059111,
        "H": 0.086463253680022,
        "K": 0.07261161194184243,
        "L": 0.08805889016354237,
        "A": 0.13459146269617409
    },
    "Average_excess_waiting_time": {
        "B": 64.72033305018942,
        "C": 62.17127508993627,
        "D": 59.40970501885113,
        "E": 57.34121975100953,
        "G": 55.53067316854009,
        "I": 54.971859320549015,
        "J": 55.335738910686814,
        "F": 57.185142954448565,
        "H": 55.63030832077044,
        "K": 56.66564395321535,
        "L": 59.22043408036592,
        "A": 64.10063021244525
    },
    "Holding_per_stop": {
        "I": 39.8936170212766,
        "E": 30.319148936170212,
        "D": 27.4468085106383,
        "G": 33.829787234042556,
        "C": 46.12903225806452,
        "B": 21.29032258064516,
        "J": 40.212765957446805,
        "F": 36.702127659574465,
        "H": 31.595744680851062,
        "K": 48.064516129032256,
        "L": 26.774193548387096,
        "A": 3.260869565217391
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 74.4375,
            "C": 76.5625,
            "D": 76.6875,
            "E": 76.5625,
            "F": 76.5,
            "G": 77.125,
            "H": 77.8,
            "I": 78.26666666666667,
            "J": 79.53333333333333,
            "K": 78.86666666666666,
            "L": 78,
            "A": 78.46666666666667
        },
        "bus1": {
            "C": 74.75,
            "D": 74.9375,
            "E": 75.375,
            "F": 76.375,
            "G": 77.75,
            "H": 77.3125,
            "I": 77.9375,
            "J": 79.93333333333334,
            "K": 78.73333333333333,
            "L": 80.33333333333333,
            "A": 80.73333333333333,
            "B": 80.93333333333334
        },
        "bus2": {
            "D": 78.4375,
            "E": 80,
            "F": 80.25,
            "G": 80.5,
            "H": 81.1875,
            "I": 80.4375,
            "J": 80,
            "K": 78.8125,
            "L": 77.8,
            "A": 77.73333333333333,
            "B": 78.66666666666667,
            "C": 79.86666666666666
        },
        "bus3": {
            "E": 76.5,
            "F": 76.375,
            "G": 75.8125,
            "H": 77.5625,
            "I": 78.125,
            "J": 77.125,
            "K": 79.75,
            "L": 82.4375,
            "A": 82.25,
            "B": 81.8,
            "C": 81.86666666666666,
            "D": 81.2
        },
        "bus4": {
            "G": 77.25,
            "H": 76.8125,
            "I": 79.0625,
            "J": 80.3125,
            "K": 79.5625,
            "L": 78.3125,
            "A": 79.9375,
            "B": 79.125,
            "C": 78.875,
            "D": 80.4,
            "E": 82.06666666666666,
            "F": 82.2
        },
        "bus5": {
            "I": 79.1875,
            "J": 80.9375,
            "K": 79.5625,
            "L": 81.0625,
            "A": 82.4375,
            "B": 82.25,
            "C": 83.8125,
            "D": 84,
            "E": 84.4375,
            "F": 85.86666666666666,
            "G": 83.26666666666667,
            "H": 85.4
        }
    }
}