{
    "Average_holding_time": 33.270777479892764,
    "Average_travel_time_passengers": 2690.362955437995,
    "Average_speed_busses": 0.046625,
    "Average_experienced_crowding": 79.07924969932168,
    "Average_covariation_headway": 0.11988368637122729,
    "Average_covariation_headway_stops": {
        "C": 0.15917191548257256,
        "E": 0.1444646653415673,
        "F": 0.13953097425007846,
        "G": 0.13616322468431044,
        "H": 0.13861872204164688,
        "J": 0.12500871669049887,
        "K": 0.09303996356931751,
        "I": 0.11411001705839514,
        "D": 0.13660196065650745,
        "L": 0.08487139753256984,
        "A": 0.09993324304950904,
        "B": 0.13771469428488012
    },
    "Average_excess_waiting_time": {
        "C": 64.96476273287294,
        "E": 63.47564943547076,
        "F": 60.92114617323517,
        "G": 59.09287813559246,
        "H": 57.57532576450575,
        "J": 56.39612920624853,
        "K": 56.011635818539844,
        "I": 57.18789026871343,
        "D": 64.53215129178773,
        "L": 57.21609056526131,
        "A": 59.99394741623388,
        "B": 64.83440870933907
    },
    "Holding_per_stop": {
        "J": 32.5531914893617,
        "H": 33.829787234042556,
        "F": 33.54838709677419,
        "G": 32.5531914893617,
        "E": 34.516129032258064,
        "C": 36.774193548387096,
        "K": 32.5531914893617,
        "I": 35.1063829787234,
        "D": 32.93478260869565,
        "L": 35.483870967741936,
        "A": 29.032258064516128,
        "B": 30.32608695652174
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 75,
            "D": 75.875,
            "E": 77.75,
            "F": 79.6875,
            "G": 78.625,
            "H": 79.3125,
            "I": 79.26666666666667,
            "J": 78.73333333333333,
            "K": 79.46666666666667,
            "L": 79.93333333333334,
            "A": 80.86666666666666,
            "B": 80.86666666666666
        },
        "bus1": {
            "E": 70.3125,
            "F": 73.625,
            "G": 74.1875,
            "H": 74.625,
            "I": 75.375,
            "J": 76.25,
            "K": 78.53333333333333,
            "L": 78.73333333333333,
            "A": 78.06666666666666,
            "B": 75.6,
            "C": 75.53333333333333,
            "D": 74.8
        },
        "bus2": {
            "F": 73.625,
            "G": 73.125,
            "H": 75.4375,
            "I": 74,
            "J": 74.0625,
            "K": 75.3125,
            "L": 76,
            "A": 77.2,
            "B": 76.8,
            "C": 77.4,
            "D": 76.53333333333333,
            "E": 79.73333333333333
        },
        "bus3": {
            "G": 75.375,
            "H": 73.4375,
            "I": 72.5625,
            "J": 72.5,
            "K": 74.75,
            "L": 77.4375,
            "A": 78.8125,
            "B": 79.73333333333333,
            "C": 79.26666666666667,
            "D": 78.2,
            "E": 78.06666666666666,
            "F": 80.53333333333333
        },
        "bus4": {
            "H": 71.5,
            "I": 72.25,
            "J": 72.75,
            "K": 74.6875,
            "L": 77.875,
            "A": 79.3125,
            "B": 79.125,
            "C": 80.3125,
            "D": 78.8,
            "E": 78.86666666666666,
            "F": 77.73333333333333,
            "G": 78.46666666666667
        },
        "bus5": {
            "J": 82.75,
            "K": 82.0625,
            "L": 79.625,
            "A": 79.6875,
            "B": 82.9375,
            "C": 82.6875,
            "D": 84.625,
            "E": 86.5,
            "F": 88.4,
            "G": 88.06666666666666,
            "H": 87.8,
            "I": 89
        }
    }
}