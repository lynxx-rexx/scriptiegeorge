{
    "Average_holding_time": 33,
    "Average_travel_time_passengers": 2708.3549538777033,
    "Average_speed_busses": 0.04666666666666667,
    "Average_experienced_crowding": 80.87338581785541,
    "Average_covariation_headway": 0.10877341310101281,
    "Average_covariation_headway_stops": {
        "C": 0.1238239324476478,
        "E": 0.10760207190622154,
        "F": 0.10568920934650412,
        "I": 0.08969636290573114,
        "J": 0.09455008185789436,
        "K": 0.10142226133807714,
        "G": 0.06957019701775662,
        "L": 0.07089588207508934,
        "D": 0.09539180780049247,
        "H": 0.0629270716991937,
        "A": 0.07535172433525514,
        "B": 0.1001245759390638
    },
    "Average_excess_waiting_time": {
        "C": 60.462591869698485,
        "E": 59.37887633050951,
        "F": 57.553702798101995,
        "I": 58.050139447966046,
        "J": 56.73914489487959,
        "K": 55.70662018256445,
        "G": 57.05315634630995,
        "L": 55.673833873140666,
        "D": 60.395923042546826,
        "H": 58.50749171222827,
        "A": 57.72349918825171,
        "B": 60.80968629076824
    },
    "Holding_per_stop": {
        "F": 33.191489361702125,
        "K": 34.148936170212764,
        "J": 32.234042553191486,
        "C": 37.41935483870968,
        "I": 32.58064516129032,
        "E": 33.87096774193548,
        "G": 31.93548387096774,
        "L": 32.234042553191486,
        "D": 36.12903225806452,
        "H": 33.225806451612904,
        "A": 26.774193548387096,
        "B": 32.25806451612903
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 80.5625,
            "D": 80.0625,
            "E": 79.125,
            "F": 79.4375,
            "G": 81,
            "H": 82.1875,
            "I": 81.4,
            "J": 83.4,
            "K": 84.86666666666666,
            "L": 85.26666666666667,
            "A": 87.26666666666667,
            "B": 87.06666666666666
        },
        "bus1": {
            "E": 71.9375,
            "F": 71.25,
            "G": 73.3125,
            "H": 75.25,
            "I": 75.375,
            "J": 78.0625,
            "K": 77.73333333333333,
            "L": 78.26666666666667,
            "A": 78.73333333333333,
            "B": 80.8,
            "C": 78.73333333333333,
            "D": 76.86666666666666
        },
        "bus2": {
            "F": 76.0625,
            "G": 78.1875,
            "H": 81.625,
            "I": 81.625,
            "J": 83,
            "K": 82.75,
            "L": 83.625,
            "A": 82.86666666666666,
            "B": 82.53333333333333,
            "C": 83.06666666666666,
            "D": 81.93333333333334,
            "E": 81.6
        },
        "bus3": {
            "I": 78.8125,
            "J": 78.6875,
            "K": 79,
            "L": 80.75,
            "A": 83.5,
            "B": 84.75,
            "C": 86.26666666666667,
            "D": 86.4,
            "E": 87.73333333333333,
            "F": 86.26666666666667,
            "G": 84.93333333333334,
            "H": 84.26666666666667
        },
        "bus4": {
            "J": 73.4375,
            "K": 73.75,
            "L": 74.9375,
            "A": 74.5625,
            "B": 74.5,
            "C": 73.4375,
            "D": 74.875,
            "E": 75.93333333333334,
            "F": 77.46666666666667,
            "G": 77.53333333333333,
            "H": 77.86666666666666,
            "I": 78
        },
        "bus5": {
            "K": 78.8125,
            "L": 79.9375,
            "A": 80,
            "B": 81.375,
            "C": 81.75,
            "D": 82.625,
            "E": 83.0625,
            "F": 82.5625,
            "G": 82.73333333333333,
            "H": 83.26666666666667,
            "I": 84.13333333333334,
            "J": 84.46666666666667
        }
    }
}