{
    "Average_holding_time": 13.74460742018982,
    "Average_travel_time_passengers": 2656.7966976095286,
    "Average_speed_busses": 0.04829166666666666,
    "Average_experienced_crowding": 82.80593000087951,
    "Average_covariation_headway": 0.4513053431704417,
    "Average_covariation_headway_stops": {
        "G": 0.36708971581003697,
        "J": 0.35481635747234763,
        "I": 0.3549140238003009,
        "E": 0.34483797650163067,
        "D": 0.33879112822719015,
        "B": 0.30843071640603215,
        "K": 0.3344642791009445,
        "H": 0.33734232480823334,
        "F": 0.34557726606612693,
        "L": 0.3329511176103893,
        "C": 0.3221803544734179,
        "A": 0.3180269870867552
    },
    "Average_excess_waiting_time": {
        "G": 73.83479066893682,
        "J": 67.50959444424973,
        "I": 70.37671350434658,
        "E": 69.02261522226979,
        "D": 68.09957305982067,
        "B": 61.915460881213164,
        "K": 64.91891663143298,
        "H": 68.12850329746863,
        "F": 71.0013639169415,
        "L": 66.31696400453484,
        "C": 66.86958004798413,
        "A": 65.81033095869873
    },
    "Holding_per_stop": {
        "J": 21.34020618556701,
        "G": 22.577319587628867,
        "I": 13.29896907216495,
        "E": 11.5625,
        "D": 11.75257731958763,
        "K": 15.77319587628866,
        "H": 11.5625,
        "B": 7.11340206185567,
        "F": 16.875,
        "L": 18.8659793814433,
        "C": 11.25,
        "A": 2.8125
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 79.41176470588235,
            "H": 77.4375,
            "I": 77.4375,
            "J": 79.5,
            "K": 79.9375,
            "L": 79.9375,
            "A": 79.875,
            "B": 82.25,
            "C": 82.9375,
            "D": 82.1875,
            "E": 83.75,
            "F": 83.5
        },
        "bus5": {
            "J": 58.1764705882353,
            "K": 57.23529411764706,
            "L": 58.8235294117647,
            "A": 59.9375,
            "B": 60,
            "C": 61.0625,
            "D": 60.5625,
            "E": 60.1875,
            "F": 59.9375,
            "G": 60.6875,
            "H": 60.5,
            "I": 61.25
        },
        "bus4": {
            "I": 72.11764705882354,
            "J": 70.3125,
            "K": 71.4375,
            "L": 71.5625,
            "A": 75.1875,
            "B": 74.375,
            "C": 76.6875,
            "D": 76.875,
            "E": 77.25,
            "F": 78.5,
            "G": 79.4375,
            "H": 78.125
        },
        "bus2": {
            "E": 85.125,
            "F": 86.1875,
            "G": 86.1875,
            "H": 86.5625,
            "I": 88.1875,
            "J": 90.125,
            "K": 92.1875,
            "L": 91.0625,
            "A": 91,
            "B": 90.375,
            "C": 90.5625,
            "D": 91.125
        },
        "bus1": {
            "D": 89,
            "E": 93,
            "F": 92.4375,
            "G": 92.625,
            "H": 93.4375,
            "I": 93.4375,
            "J": 91.8125,
            "K": 95,
            "L": 93.8125,
            "A": 93.125,
            "B": 91.25,
            "C": 92.875
        },
        "bus0": {
            "B": 58.64705882352941,
            "C": 57.875,
            "D": 58.1875,
            "E": 58.75,
            "F": 58.1875,
            "G": 58.1875,
            "H": 61,
            "I": 62.25,
            "J": 60.5625,
            "K": 62.0625,
            "L": 62.3125,
            "A": 61.875
        }
    }
}           "H": 67.9375,
            "I": 67.66666666666667,
            "J": 68.4
        }
    }
}       "G": 63.06666666666667,
            "H": 65.06666666666666,
            "I": 65.13333333333334,
            "J": 63.6,
            "K": 63.13333333333333,
            "L": 63
        }
    }
}