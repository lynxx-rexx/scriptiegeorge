{
    "Average_holding_time": 18.183406113537117,
    "Average_travel_time_passengers": 2666.6817927154966,
    "Average_speed_busses": 0.04770833333333333,
    "Average_experienced_crowding": 79.35523849103001,
    "Average_covariation_headway": 0.26497585959311365,
    "Average_covariation_headway_stops": {
        "H": 0.20054653748272297,
        "E": 0.20551228944633637,
        "I": 0.2075070395577726,
        "K": 0.19587250200020184,
        "A": 0.16974783124825435,
        "F": 0.21988502783582634,
        "J": 0.18184899512672426,
        "B": 0.1670154231189657,
        "L": 0.17042035537149608,
        "G": 0.20021908258707438,
        "C": 0.17651172845776,
        "D": 0.18821497704799564
    },
    "Average_excess_waiting_time": {
        "H": 48.04270872706206,
        "E": 50.44693392667563,
        "I": 47.98136964299073,
        "K": 46.70731776326954,
        "A": 43.205921951674156,
        "F": 50.56846253771198,
        "J": 46.617569929798094,
        "B": 44.89475810939075,
        "L": 45.16026402954418,
        "G": 49.82869946603182,
        "C": 47.77085551029484,
        "D": 50.43106341531069
    },
    "Holding_per_stop": {
        "I": 19.0625,
        "K": 21.25,
        "A": 12.5,
        "F": 22.1875,
        "H": 19.375,
        "E": 18.31578947368421,
        "J": 16.42105263157895,
        "B": 14.210526315789474,
        "L": 21.473684210526315,
        "G": 19.57894736842105,
        "C": 19.263157894736842,
        "D": 14.526315789473685
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "H": 78.5,
            "I": 78.5625,
            "J": 78.125,
            "K": 77.6875,
            "L": 78.4375,
            "A": 79.1875,
            "B": 79.1875,
            "C": 79.125,
            "D": 80,
            "E": 81.4375,
            "F": 83.9375,
            "G": 83.13333333333334
        },
        "bus0": {
            "E": 74.875,
            "F": 73.8125,
            "G": 74.5,
            "H": 73.3125,
            "I": 74.625,
            "J": 76.125,
            "K": 78.9375,
            "L": 79.5,
            "A": 80.1875,
            "B": 82,
            "C": 83,
            "D": 81.4
        },
        "bus3": {
            "I": 69.75,
            "J": 70.75,
            "K": 72,
            "L": 72.3125,
            "A": 72.625,
            "B": 73.75,
            "C": 75.8125,
            "D": 74.9375,
            "E": 74.4375,
            "F": 71.75,
            "G": 73.6875,
            "H": 73.4375
        },
        "bus4": {
            "K": 76.125,
            "L": 76.625,
            "A": 77.8125,
            "B": 78.375,
            "C": 78.9375,
            "D": 79,
            "E": 77.875,
            "F": 78.25,
            "G": 79.0625,
            "H": 79.5625,
            "I": 81.25,
            "J": 80.375
        },
        "bus5": {
            "A": 76.375,
            "B": 77.5625,
            "C": 79.9375,
            "D": 82.8125,
            "E": 85.625,
            "F": 84.0625,
            "G": 81.875,
            "H": 83.0625,
            "I": 84.1875,
            "J": 83.5,
            "K": 82.3125,
            "L": 82.125
        },
        "bus1": {
            "F": 76.625,
            "G": 75.8125,
            "H": 76.125,
            "I": 76.8125,
            "J": 76.125,
            "K": 76.5625,
            "L": 75.125,
            "A": 76.9375,
            "B": 77.25,
            "C": 78,
            "D": 80.875,
            "E": 83.4
        }
    }
}1.4,
            "K": 91.66666666666667,
            "L": 92.13333333333334,
            "A": 92.2,
            "B": 95.13333333333334,
            "C": 94.06666666666666,
            "D": 93.93333333333334
        }
    }
}