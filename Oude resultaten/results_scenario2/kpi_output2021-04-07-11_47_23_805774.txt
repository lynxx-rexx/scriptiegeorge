{
    "Average_holding_time": 18.074866310160427,
    "Average_travel_time_passengers": 2797.2486979740947,
    "Average_speed_busses": 0.04675,
    "Average_experienced_crowding": 100.36399090472725,
    "Average_covariation_headway": 0.663866839181919,
    "Average_covariation_headway_stops": {
        "D": 0.5368106293951702,
        "E": 0.5391593857071568,
        "I": 0.5480828299897804,
        "C": 0.5321200484204088,
        "F": 0.5295880702533893,
        "G": 0.5428069683934399,
        "J": 0.5319579741970596,
        "H": 0.5434881456611191,
        "K": 0.5297403948299936,
        "L": 0.5131828829120276,
        "A": 0.5302496968949311,
        "B": 0.528159154329809
    },
    "Average_excess_waiting_time": {
        "D": 135.37485984744114,
        "E": 133.70705243576924,
        "I": 131.16451324167707,
        "C": 135.39499805495188,
        "F": 127.26953007318872,
        "G": 129.48297898098951,
        "J": 127.61237261066788,
        "H": 132.06179190801458,
        "K": 130.08126828818337,
        "L": 130.47830038883194,
        "A": 136.73257206531616,
        "B": 137.99374268093015
    },
    "Holding_per_stop": {
        "D": 22.258064516129032,
        "I": 18,
        "E": 15.319148936170214,
        "F": 17.5531914893617,
        "G": 17.36842105263158,
        "C": 18.58695652173913,
        "J": 17.68421052631579,
        "H": 15,
        "K": 21.382978723404257,
        "L": 21.195652173913043,
        "A": 15.326086956521738,
        "B": 17.282608695652176
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "D": 78.1875,
            "E": 78.6875,
            "F": 77.25,
            "G": 78.1875,
            "H": 80.6875,
            "I": 81.1875,
            "J": 82.625,
            "K": 84.4,
            "L": 86.13333333333334,
            "A": 86,
            "B": 83.66666666666667,
            "C": 83.6
        },
        "bus2": {
            "E": 67.5,
            "F": 67.9375,
            "G": 67,
            "H": 67.8125,
            "I": 68.1875,
            "J": 68.125,
            "K": 67.375,
            "L": 72.2,
            "A": 73.8,
            "B": 72.26666666666667,
            "C": 71.66666666666667,
            "D": 71.46666666666667
        },
        "bus5": {
            "I": 111.875,
            "J": 113.1875,
            "K": 112.8125,
            "L": 113.1875,
            "A": 112.625,
            "B": 115.375,
            "C": 115.6875,
            "D": 117.1875,
            "E": 117.5625,
            "F": 118.73333333333333,
            "G": 117.93333333333334,
            "H": 119.86666666666666
        },
        "bus0": {
            "C": 97.5625,
            "D": 98.6875,
            "E": 99.3125,
            "F": 100.8125,
            "G": 99.625,
            "H": 100.33333333333333,
            "I": 100.26666666666667,
            "J": 102.06666666666666,
            "K": 101.73333333333333,
            "L": 101.2,
            "A": 102.4,
            "B": 104.33333333333333
        },
        "bus3": {
            "F": 61.625,
            "G": 62.125,
            "H": 64.4375,
            "I": 64.25,
            "J": 64.1875,
            "K": 63.75,
            "L": 64.125,
            "A": 61.8,
            "B": 61.86666666666667,
            "C": 62.8,
            "D": 65,
            "E": 65.73333333333333
        },
        "bus4": {
            "G": 44.9375,
            "H": 46,
            "I": 46.0625,
            "J": 48.0625,
            "K": 50.3125,
            "L": 52.125,
            "A": 52.375,
            "B": 53.8125,
            "C": 50,
            "D": 50.93333333333333,
            "E": 50,
            "F": 49.2
        }
    }
}