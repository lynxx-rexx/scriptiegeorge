{
    "Average_holding_time": 16.865148861646233,
    "Average_travel_time_passengers": 2701.927493219041,
    "Average_speed_busses": 0.04758333333333333,
    "Average_experienced_crowding": 82.73414430368695,
    "Average_covariation_headway": 0.33347210901388824,
    "Average_covariation_headway_stops": {
        "J": 0.26673714659026404,
        "F": 0.2539808556983334,
        "C": 0.283065374323711,
        "B": 0.2955956912446125,
        "E": 0.2482177070135996,
        "G": 0.2502436972550827,
        "D": 0.2563685970636607,
        "K": 0.26018713359560425,
        "H": 0.24166605882784647,
        "L": 0.2756429611093332,
        "I": 0.22431974036739746,
        "A": 0.2901120993174318
    },
    "Average_excess_waiting_time": {
        "J": 56.88509747859632,
        "F": 55.90327271712374,
        "C": 62.871781552702885,
        "B": 66.08452809950461,
        "E": 56.71116197521201,
        "G": 52.61499691601546,
        "D": 60.08340072765759,
        "K": 57.35744396828653,
        "H": 53.25479027711498,
        "L": 61.9969329202695,
        "I": 52.08073186679883,
        "A": 66.65348228973886
    },
    "Holding_per_stop": {
        "J": 22.5,
        "C": 16.736842105263158,
        "F": 15,
        "E": 15.157894736842104,
        "G": 14.0625,
        "B": 14.842105263157896,
        "D": 19.46808510638298,
        "K": 17.05263157894737,
        "H": 19.375,
        "L": 19.894736842105264,
        "I": 11.368421052631579,
        "A": 16.914893617021278
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "J": 91.125,
            "K": 91.875,
            "L": 93.4375,
            "A": 93.125,
            "B": 92.5625,
            "C": 92.375,
            "D": 92.125,
            "E": 93,
            "F": 92.1875,
            "G": 92.5625,
            "H": 93.625,
            "I": 96.66666666666667
        },
        "bus3": {
            "F": 72.625,
            "G": 71.6875,
            "H": 71.875,
            "I": 73.375,
            "J": 74.875,
            "K": 76,
            "L": 76.5,
            "A": 78.5625,
            "B": 79.625,
            "C": 79.8125,
            "D": 77.75,
            "E": 77.86666666666666
        },
        "bus1": {
            "C": 76.9375,
            "D": 75.6875,
            "E": 78.9375,
            "F": 78.625,
            "G": 78.625,
            "H": 79.25,
            "I": 79.4375,
            "J": 79.6875,
            "K": 78.375,
            "L": 78.625,
            "A": 78.53333333333333,
            "B": 81.06666666666666
        },
        "bus0": {
            "B": 73.125,
            "C": 73.875,
            "D": 76.4375,
            "E": 76.0625,
            "F": 75.75,
            "G": 75.9375,
            "H": 75.5625,
            "I": 76.125,
            "J": 75.125,
            "K": 75.0625,
            "L": 77,
            "A": 76.86666666666666
        },
        "bus2": {
            "E": 69.8125,
            "F": 70.6875,
            "G": 71.5,
            "H": 71,
            "I": 71.625,
            "J": 69.9375,
            "K": 69.5,
            "L": 71.8125,
            "A": 72.5,
            "B": 72.25,
            "C": 73.93333333333334,
            "D": 74.46666666666667
        },
        "bus4": {
            "G": 74.625,
            "H": 75.9375,
            "I": 75.1875,
            "J": 73.25,
            "K": 75.1875,
            "L": 77.3125,
            "A": 78.4375,
            "B": 78.1875,
            "C": 78.1875,
            "D": 78.875,
            "E": 80.375,
            "F": 80.1875
        }
    }
}75.875,
            "C": 76.86666666666666
        }
    }
}