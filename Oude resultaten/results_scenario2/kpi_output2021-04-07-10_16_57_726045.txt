{
    "Average_holding_time": 17.824156305506218,
    "Average_travel_time_passengers": 2728.642718271923,
    "Average_speed_busses": 0.04691666666666667,
    "Average_experienced_crowding": 88.24912290190098,
    "Average_covariation_headway": 0.5246771140325996,
    "Average_covariation_headway_stops": {
        "G": 0.3783725834329593,
        "B": 0.45520366916996036,
        "A": 0.41416321051142324,
        "E": 0.3908716541872435,
        "L": 0.40737943112888203,
        "J": 0.38122166056448603,
        "C": 0.4460200162810602,
        "F": 0.392188198900179,
        "H": 0.38811164345457033,
        "K": 0.38787156062076583,
        "D": 0.42299532800725914,
        "I": 0.3592974209642475
    },
    "Average_excess_waiting_time": {
        "G": 84.31957779673508,
        "B": 97.2733538208409,
        "A": 88.25786127034814,
        "E": 86.02762219703988,
        "L": 90.68399503724748,
        "J": 83.85200287812273,
        "C": 97.41889734000421,
        "F": 89.4744723963218,
        "H": 89.69329616835273,
        "K": 87.10433075649206,
        "D": 103.60010414245488,
        "I": 80.6883157075278
    },
    "Holding_per_stop": {
        "B": 13.125,
        "G": 12.76595744680851,
        "E": 18.70967741935484,
        "A": 17.36842105263158,
        "J": 13.404255319148936,
        "L": 21.70212765957447,
        "C": 24,
        "F": 9.67741935483871,
        "H": 29.35483870967742,
        "K": 14.361702127659575,
        "D": 22.82608695652174,
        "I": 16.774193548387096
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "G": 67.375,
            "H": 68.4375,
            "I": 71.375,
            "J": 71.375,
            "K": 70.375,
            "L": 70.875,
            "A": 72.0625,
            "B": 71.1875,
            "C": 71.5,
            "D": 73.33333333333333,
            "E": 72.2,
            "F": 72.86666666666666
        },
        "bus0": {
            "B": 74.875,
            "C": 73.75,
            "D": 72.75,
            "E": 73.875,
            "F": 76.125,
            "G": 76.8125,
            "H": 78.1875,
            "I": 79.4375,
            "J": 79.9375,
            "K": 80.0625,
            "L": 81.0625,
            "A": 79.73333333333333
        },
        "bus5": {
            "A": 62.5625,
            "B": 63.125,
            "C": 65.875,
            "D": 65.75,
            "E": 64.875,
            "F": 66.0625,
            "G": 66.9375,
            "H": 69.25,
            "I": 66.93333333333334,
            "J": 65.13333333333334,
            "K": 64.2,
            "L": 66.26666666666667
        },
        "bus1": {
            "E": 93.1875,
            "F": 94.0625,
            "G": 94.25,
            "H": 95.3125,
            "I": 94.125,
            "J": 94.5,
            "K": 96,
            "L": 97.625,
            "A": 100.25,
            "B": 98.1875,
            "C": 96.875,
            "D": 98.93333333333334
        },
        "bus4": {
            "L": 96.375,
            "A": 97.1875,
            "B": 97.8125,
            "C": 100.0625,
            "D": 104.4375,
            "E": 100.53333333333333,
            "F": 102.8,
            "G": 105.26666666666667,
            "H": 103.86666666666666,
            "I": 103.46666666666667,
            "J": 103.13333333333334,
            "K": 103.2
        },
        "bus3": {
            "J": 62.625,
            "K": 62.1875,
            "L": 63.6875,
            "A": 65.125,
            "B": 66.25,
            "C": 65.4375,
            "D": 69.86666666666666,
            "E": 69,
            "F": 68.46666666666667,
            "G": 68.93333333333334,
            "H": 68.4,
            "I": 67.2
        }
    }
}