{
    "Average_holding_time": 12.83147989734816,
    "Average_travel_time_passengers": 2630.541413667086,
    "Average_speed_busses": 0.04870833333333333,
    "Average_experienced_crowding": 80.65421242610091,
    "Average_covariation_headway": 0.4379233603864201,
    "Average_covariation_headway_stops": {
        "L": 0.3310107769411436,
        "B": 0.3495995474621489,
        "I": 0.33874142752262754,
        "F": 0.3286478720709423,
        "G": 0.34391831613868146,
        "C": 0.3428132702305116,
        "A": 0.3226460967754099,
        "D": 0.32591218670038685,
        "J": 0.3288276089364041,
        "H": 0.3456914471745328,
        "K": 0.3249592971091685,
        "E": 0.32713539893537535
    },
    "Average_excess_waiting_time": {
        "L": 62.069628062708034,
        "B": 66.37792390778952,
        "I": 63.41175871468022,
        "F": 65.69325950236424,
        "G": 64.41155188630074,
        "C": 62.81414683971718,
        "A": 62.3928715294017,
        "D": 61.05258624588828,
        "J": 61.76448308203061,
        "H": 66.57265866340703,
        "K": 62.59429362140344,
        "E": 64.34354636383381
    },
    "Holding_per_stop": {
        "I": 14.387755102040817,
        "L": 14.081632653061224,
        "G": 13.29896907216495,
        "C": 11.938775510204081,
        "B": 13.775510204081632,
        "F": 13.608247422680412,
        "D": 12.551020408163266,
        "J": 12.989690721649485,
        "A": 10.515463917525773,
        "H": 12.989690721649485,
        "K": 12.371134020618557,
        "E": 11.443298969072165
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 75.11764705882354,
            "A": 77.3529411764706,
            "B": 77.58823529411765,
            "C": 77.5,
            "D": 77.5,
            "E": 76.5,
            "F": 77,
            "G": 75.5625,
            "H": 77.4375,
            "I": 80.3125,
            "J": 78.75,
            "K": 78.8125
        },
        "bus0": {
            "B": 53.23529411764706,
            "C": 54,
            "D": 55.94117647058823,
            "E": 56.9375,
            "F": 57.5,
            "G": 57.8125,
            "H": 56.3125,
            "I": 56.0625,
            "J": 56.3125,
            "K": 56.4375,
            "L": 56,
            "A": 55.25
        },
        "bus4": {
            "I": 67.76470588235294,
            "J": 69.17647058823529,
            "K": 70.82352941176471,
            "L": 71.88235294117646,
            "A": 72.3125,
            "B": 72.25,
            "C": 73.375,
            "D": 73.0625,
            "E": 72.0625,
            "F": 71.875,
            "G": 70.75,
            "H": 70.3125
        },
        "bus2": {
            "F": 91.05882352941177,
            "G": 87.6875,
            "H": 89.625,
            "I": 90.875,
            "J": 93.5625,
            "K": 94.5625,
            "L": 93.3125,
            "A": 92.9375,
            "B": 94.6875,
            "C": 95.875,
            "D": 96.3125,
            "E": 95.625
        },
        "bus3": {
            "G": 71.70588235294117,
            "H": 72.47058823529412,
            "I": 73.52941176470588,
            "J": 71.4375,
            "K": 73.0625,
            "L": 73.5,
            "A": 74.125,
            "B": 74.25,
            "C": 73.125,
            "D": 73.0625,
            "E": 73,
            "F": 74.75
        },
        "bus1": {
            "C": 71.41176470588235,
            "D": 71.47058823529412,
            "E": 72.11764705882354,
            "F": 77.4375,
            "G": 80.875,
            "H": 80.3125,
            "I": 81.375,
            "J": 81.625,
            "K": 81.4375,
            "L": 78.4375,
            "A": 76.4375,
            "B": 75.1875
        }
    }
}