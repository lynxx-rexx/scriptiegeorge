{
    "Average_holding_time": 31.385224274406333,
    "Average_travel_time_passengers": 5503.474861531642,
    "Average_speed_busses": 0.03158333333333333,
    "Average_experienced_crowding": 445.25895854050754,
    "Average_covariation_headway": 1.705125727850942,
    "Average_covariation_headway_stops": {
        "D": 1.8363431311272191,
        "A": 1.8249363578299722,
        "B": 1.8380222736539225,
        "I": 1.8411218630030555,
        "E": 1.8549807076833231,
        "F": 1.8623192187610902,
        "C": 1.814082658870875,
        "J": 1.8364358277685366,
        "G": 1.8611010663274226,
        "H": 1.8684969103575688,
        "K": 1.8230535670032333,
        "L": 1.8160161113387507
    },
    "Average_excess_waiting_time": {
        "D": 1665.8264950937762,
        "A": 1645.4701554755475,
        "B": 1656.1922860738848,
        "I": 1612.7355783896812,
        "E": 1687.2742449258815,
        "F": 1690.1019532707805,
        "C": 1639.160334068984,
        "J": 1631.39591363759,
        "G": 1708.0795246536024,
        "H": 1739.2944603878982,
        "K": 1628.1631411745898,
        "L": 1639.8883613458677
    },
    "Holding_per_stop": {
        "B": 31.904761904761905,
        "I": 31.475409836065573,
        "E": 31.846153846153847,
        "D": 33.75,
        "F": 35,
        "A": 28.548387096774192,
        "C": 28.095238095238095,
        "J": 27.540983606557376,
        "G": 33.18181818181818,
        "H": 36,
        "K": 30.983606557377048,
        "L": 27.540983606557376
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "D": 23.636363636363637,
            "E": 26.272727272727273,
            "F": 26,
            "G": 24.727272727272727,
            "H": 24.636363636363637,
            "I": 27.3,
            "J": 26.5,
            "K": 25.9,
            "L": 25.8,
            "A": 25.5,
            "B": 24.7,
            "C": 24.2
        },
        "bus5": {
            "A": 51.36363636363637,
            "B": 51.72727272727273,
            "C": 51.72727272727273,
            "D": 52.27272727272727,
            "E": 52.27272727272727,
            "F": 56.09090909090909,
            "G": 56.36363636363637,
            "H": 58.09090909090909,
            "I": 61.6,
            "J": 60.2,
            "K": 58.1,
            "L": 56.9
        },
        "bus0": {
            "B": 33.72727272727273,
            "C": 33.72727272727273,
            "D": 34.45454545454545,
            "E": 34.18181818181818,
            "F": 34.27272727272727,
            "G": 34.27272727272727,
            "H": 34.90909090909091,
            "I": 39.2,
            "J": 38.1,
            "K": 37.9,
            "L": 39.9,
            "A": 39.8
        },
        "bus4": {
            "I": 86.72727272727273,
            "J": 86.54545454545455,
            "K": 85.36363636363636,
            "L": 89.27272727272727,
            "A": 91.63636363636364,
            "B": 92,
            "C": 91.0909090909091,
            "D": 91.9090909090909,
            "E": 91,
            "F": 90.81818181818181,
            "G": 90.18181818181819,
            "H": 87
        },
        "bus2": {
            "E": 34.63636363636363,
            "F": 34.45454545454545,
            "G": 34.63636363636363,
            "H": 34.54545454545455,
            "I": 35.5,
            "J": 38.2,
            "K": 39.8,
            "L": 39.3,
            "A": 39,
            "B": 39.5,
            "C": 39.3,
            "D": 40.2
        },
        "bus3": {
            "F": 407.1818181818182,
            "G": 411.09090909090907,
            "H": 419.6363636363636,
            "I": 379.6,
            "J": 389,
            "K": 395.3,
            "L": 403.1,
            "A": 412.3,
            "B": 421.7,
            "C": 426.3,
            "D": 436.7,
            "E": 442.7
        }
    }
}