{
    "Average_holding_time": 17.142857142857142,
    "Average_travel_time_passengers": 2861.6585502470284,
    "Average_speed_busses": 0.04666666666666667,
    "Average_experienced_crowding": 105.87879178366637,
    "Average_covariation_headway": 0.730800974069883,
    "Average_covariation_headway_stops": {
        "A": 0.6213023638653201,
        "L": 0.6220371529682898,
        "J": 0.6040327687691928,
        "G": 0.6199963110909095,
        "K": 0.6121418403132208,
        "I": 0.6141206785247976,
        "B": 0.5951799651711128,
        "H": 0.6085476225139059,
        "C": 0.5887134713919417,
        "D": 0.573573084717302,
        "E": 0.5807880914752892,
        "F": 0.5910566968142457
    },
    "Average_excess_waiting_time": {
        "A": 158.06057407767486,
        "L": 159.7609831856206,
        "J": 158.16671006017657,
        "G": 167.85610742384472,
        "K": 158.49966642613816,
        "I": 163.59735621554228,
        "B": 150.31768394654569,
        "H": 164.26433966136733,
        "C": 152.44378774863702,
        "D": 148.706017148032,
        "E": 153.32485820837053,
        "F": 159.36128705586543
    },
    "Holding_per_stop": {
        "A": 15.789473684210526,
        "L": 20.842105263157894,
        "J": 15.161290322580646,
        "K": 22.340425531914892,
        "I": 17.096774193548388,
        "G": 19.35483870967742,
        "B": 12.76595744680851,
        "H": 14.021739130434783,
        "C": 15.483870967741936,
        "D": 16.774193548387096,
        "E": 17.419354838709676,
        "F": 18.58695652173913
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "A": 79,
            "B": 79.5625,
            "C": 79.625,
            "D": 79.6875,
            "E": 82.1875,
            "F": 82.5625,
            "G": 82.375,
            "H": 83,
            "I": 81.75,
            "J": 82.75,
            "K": 83.33333333333333,
            "L": 83.46666666666667
        },
        "bus4": {
            "L": 29.875,
            "A": 30.4375,
            "B": 30.6875,
            "C": 31.75,
            "D": 34.25,
            "E": 34.875,
            "F": 35.1875,
            "G": 35.625,
            "H": 32,
            "I": 31.2,
            "J": 31.333333333333332,
            "K": 30.666666666666668
        },
        "bus2": {
            "J": 81.9375,
            "K": 82.1875,
            "L": 81.5625,
            "A": 82.9375,
            "B": 85.0625,
            "C": 86.75,
            "D": 83.8,
            "E": 83.93333333333334,
            "F": 85.06666666666666,
            "G": 83.86666666666666,
            "H": 85.53333333333333,
            "I": 85.4
        },
        "bus0": {
            "G": 116.125,
            "H": 117.625,
            "I": 117.625,
            "J": 116.8125,
            "K": 117.375,
            "L": 116.5625,
            "A": 121.13333333333334,
            "B": 119.8,
            "C": 121.13333333333334,
            "D": 121,
            "E": 121.13333333333334,
            "F": 122.13333333333334
        },
        "bus3": {
            "K": 48.5,
            "L": 49.4375,
            "A": 49.625,
            "B": 50.0625,
            "C": 51.3125,
            "D": 51.625,
            "E": 51.6875,
            "F": 49.6,
            "G": 51.8,
            "H": 50.6,
            "I": 49.13333333333333,
            "J": 51.266666666666666
        },
        "bus1": {
            "I": 101,
            "J": 100.1875,
            "K": 100.8125,
            "L": 102.25,
            "A": 106.125,
            "B": 106.125,
            "C": 108.73333333333333,
            "D": 109.33333333333333,
            "E": 109.66666666666667,
            "F": 107.73333333333333,
            "G": 107.73333333333333,
            "H": 108.2
        }
    }
}