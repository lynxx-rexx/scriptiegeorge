{
    "Average_holding_time": 14.737302977232925,
    "Average_travel_time_passengers": 2747.732543175801,
    "Average_speed_busses": 0.04758333333333333,
    "Average_experienced_crowding": 96.06484581548413,
    "Average_covariation_headway": 0.5842625387771593,
    "Average_covariation_headway_stops": {
        "E": 0.4836919887261015,
        "D": 0.48245324312097887,
        "A": 0.49693267206098857,
        "H": 0.4844802173183857,
        "G": 0.4844758275104396,
        "I": 0.48714571587190797,
        "F": 0.48370265496691833,
        "B": 0.4836056935095892,
        "J": 0.4850231952130977,
        "K": 0.4736566205928489,
        "C": 0.48542425164385855,
        "L": 0.4985981004377049
    },
    "Average_excess_waiting_time": {
        "E": 108.33494115093112,
        "D": 111.61924228876649,
        "A": 116.82510265303091,
        "H": 106.58366150848889,
        "G": 108.83213397481586,
        "I": 105.30938686085068,
        "F": 110.5249362748213,
        "B": 112.38251640804913,
        "J": 106.28753152308917,
        "K": 105.06026770750998,
        "C": 114.61710766139788,
        "L": 114.92822544932886
    },
    "Holding_per_stop": {
        "E": 12.31578947368421,
        "H": 18.75,
        "G": 19.263157894736842,
        "A": 11.808510638297872,
        "I": 13.608247422680412,
        "D": 15.473684210526315,
        "F": 12.947368421052632,
        "B": 12.446808510638299,
        "J": 18.125,
        "K": 14.6875,
        "C": 14.361702127659575,
        "L": 12.947368421052632
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "E": 83.25,
            "F": 82.3125,
            "G": 82.6875,
            "H": 84.3125,
            "I": 85.8125,
            "J": 85.9375,
            "K": 86.9375,
            "L": 90.875,
            "A": 90.1875,
            "B": 89.13333333333334,
            "C": 89.26666666666667,
            "D": 89.66666666666667
        },
        "bus0": {
            "D": 79.4375,
            "E": 78.8125,
            "F": 80.0625,
            "G": 77.1875,
            "H": 79.6875,
            "I": 80.125,
            "J": 80.25,
            "K": 80.5625,
            "L": 81.5,
            "A": 85.53333333333333,
            "B": 83.6,
            "C": 84.8
        },
        "bus5": {
            "A": 96.375,
            "B": 97.3125,
            "C": 98.1875,
            "D": 96.75,
            "E": 98.5,
            "F": 98,
            "G": 98.75,
            "H": 98.1875,
            "I": 98.9375,
            "J": 100.375,
            "K": 100.5,
            "L": 102.6
        },
        "bus3": {
            "H": 46.25,
            "I": 46.9375,
            "J": 47.5,
            "K": 48.1875,
            "L": 47.3125,
            "A": 47.8125,
            "B": 47.25,
            "C": 47.5625,
            "D": 48.8125,
            "E": 49,
            "F": 48.8125,
            "G": 49.25
        },
        "bus2": {
            "G": 62.125,
            "H": 63.1875,
            "I": 62.9375,
            "J": 63,
            "K": 62.8125,
            "L": 63.4375,
            "A": 64.8125,
            "B": 66.8125,
            "C": 67.8125,
            "D": 66.875,
            "E": 64.33333333333333,
            "F": 63.8
        },
        "bus4": {
            "I": 88.70588235294117,
            "J": 91.3125,
            "K": 92.5,
            "L": 92.375,
            "A": 93.6875,
            "B": 94,
            "C": 95.3125,
            "D": 95.4375,
            "E": 94.1875,
            "F": 91.75,
            "G": 94.5625,
            "H": 94.375
        }
    }
}