{
    "Average_holding_time": 13.71951219512195,
    "Average_travel_time_passengers": 2643.19520341707,
    "Average_speed_busses": 0.04783333333333333,
    "Average_experienced_crowding": 79.49345112596716,
    "Average_covariation_headway": 0.3290187712296898,
    "Average_covariation_headway_stops": {
        "G": 0.2392384890063273,
        "E": 0.2216136644864427,
        "I": 0.24587597847829867,
        "C": 0.2338908092452194,
        "L": 0.22394121128844263,
        "F": 0.22976407117138364,
        "H": 0.2208462310091021,
        "J": 0.2309241859546871,
        "A": 0.22791280000970243,
        "D": 0.22895318052408983,
        "K": 0.22223576433081127,
        "B": 0.22523583639150652
    },
    "Average_excess_waiting_time": {
        "G": 48.841266253387175,
        "E": 50.098307601634986,
        "I": 50.70180946679119,
        "C": 51.66174840490834,
        "L": 49.473951884965004,
        "F": 49.21632668499393,
        "H": 48.94848696709914,
        "J": 52.63070602429627,
        "A": 52.35040554775827,
        "D": 52.9837370452783,
        "K": 51.05544609907656,
        "B": 53.520987556805665
    },
    "Holding_per_stop": {
        "G": 13.608247422680412,
        "I": 14.845360824742269,
        "L": 14.0625,
        "F": 13.75,
        "C": 17.36842105263158,
        "E": 13.125,
        "H": 12.8125,
        "J": 18,
        "A": 8.842105263157896,
        "D": 13.578947368421053,
        "K": 16.105263157894736,
        "B": 8.526315789473685
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 79.23529411764706,
            "H": 81.4375,
            "I": 80.375,
            "J": 81.4375,
            "K": 81.1875,
            "L": 80.625,
            "A": 80.25,
            "B": 81.75,
            "C": 82.875,
            "D": 84.3125,
            "E": 84.875,
            "F": 83.375
        },
        "bus1": {
            "E": 70.125,
            "F": 68.25,
            "G": 70.1875,
            "H": 70.75,
            "I": 71.5,
            "J": 74.8125,
            "K": 74.5,
            "L": 74.9375,
            "A": 76.75,
            "B": 77.5,
            "C": 75.93333333333334,
            "D": 75.2
        },
        "bus4": {
            "I": 74.70588235294117,
            "J": 75.8125,
            "K": 76.9375,
            "L": 78.8125,
            "A": 78.125,
            "B": 78.5,
            "C": 78.75,
            "D": 80.5,
            "E": 79.9375,
            "F": 78.9375,
            "G": 79.4375,
            "H": 79.8125
        },
        "bus0": {
            "C": 76.3125,
            "D": 76.5625,
            "E": 78.5,
            "F": 80.1875,
            "G": 79.0625,
            "H": 80.3125,
            "I": 82.8125,
            "J": 81.4375,
            "K": 81.25,
            "L": 81.5,
            "A": 82.66666666666667,
            "B": 83.26666666666667
        },
        "bus5": {
            "L": 82.125,
            "A": 82,
            "B": 82.1875,
            "C": 82.8125,
            "D": 82,
            "E": 81.875,
            "F": 82.25,
            "G": 84.4375,
            "H": 83.5,
            "I": 85.125,
            "J": 87.8125,
            "K": 88
        },
        "bus2": {
            "F": 61.1875,
            "G": 61.875,
            "H": 63.9375,
            "I": 65.5,
            "J": 65.75,
            "K": 65.9375,
            "L": 67,
            "A": 66.75,
            "B": 66.4375,
            "C": 65.875,
            "D": 64.75,
            "E": 66.6875
        }
    }
}3
        }
    }
}