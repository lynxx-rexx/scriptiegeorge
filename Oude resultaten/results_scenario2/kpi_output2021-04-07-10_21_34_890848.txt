{
    "Average_holding_time": 16.893464637421665,
    "Average_travel_time_passengers": 2860.1080966826216,
    "Average_speed_busses": 0.04654166666666667,
    "Average_experienced_crowding": 105.04523021180289,
    "Average_covariation_headway": 0.7338935372428649,
    "Average_covariation_headway_stops": {
        "D": 0.6027219436920116,
        "A": 0.5998160628743251,
        "B": 0.6014410825405647,
        "C": 0.6011000915687416,
        "I": 0.6021204513902774,
        "F": 0.5901969336534392,
        "E": 0.5859746653355512,
        "J": 0.5850881380968779,
        "G": 0.5944833679054985,
        "K": 0.5816914387960009,
        "H": 0.5969624822600024,
        "L": 0.5899031481921256
    },
    "Average_excess_waiting_time": {
        "D": 156.86902769876326,
        "A": 159.7560637997126,
        "B": 159.52080095367444,
        "C": 156.58771987621424,
        "I": 157.53684741600824,
        "F": 150.4285780013799,
        "E": 155.55206101795835,
        "J": 151.59437858597948,
        "G": 154.42315347511152,
        "K": 152.8722201472724,
        "H": 158.0110181858263,
        "L": 158.96552473320003
    },
    "Holding_per_stop": {
        "D": 19.78723404255319,
        "C": 14.361702127659575,
        "B": 13.870967741935484,
        "I": 16.774193548387096,
        "F": 16.451612903225808,
        "A": 15.806451612903226,
        "E": 14.193548387096774,
        "J": 17.741935483870968,
        "G": 17.741935483870968,
        "K": 18.70967741935484,
        "H": 19.032258064516128,
        "L": 18.26086956521739
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "D": 46.6875,
            "E": 46.875,
            "F": 46.875,
            "G": 47.9375,
            "H": 47.5,
            "I": 49.5625,
            "J": 49,
            "K": 48.9375,
            "L": 48.06666666666667,
            "A": 48.06666666666667,
            "B": 48.06666666666667,
            "C": 48.06666666666667
        },
        "bus5": {
            "A": 123.3125,
            "B": 122.875,
            "C": 123.375,
            "D": 122.3125,
            "E": 124.86666666666666,
            "F": 125.2,
            "G": 124.26666666666667,
            "H": 127.2,
            "I": 128.06666666666666,
            "J": 126.93333333333334,
            "K": 128.6,
            "L": 130.4
        },
        "bus0": {
            "B": 101.5,
            "C": 104,
            "D": 105.5,
            "E": 104.75,
            "F": 100.8,
            "G": 102.06666666666666,
            "H": 104.33333333333333,
            "I": 105.93333333333334,
            "J": 108.06666666666666,
            "K": 109.13333333333334,
            "L": 108.13333333333334,
            "A": 106.6
        },
        "bus1": {
            "C": 74.8125,
            "D": 74.4375,
            "E": 75.4375,
            "F": 76.0625,
            "G": 76.125,
            "H": 76.25,
            "I": 76.375,
            "J": 75.26666666666667,
            "K": 73.53333333333333,
            "L": 75,
            "A": 76.13333333333334,
            "B": 77.33333333333333
        },
        "bus4": {
            "I": 77.75,
            "J": 77.625,
            "K": 78.875,
            "L": 79.5,
            "A": 80.0625,
            "B": 82.125,
            "C": 83,
            "D": 86.73333333333333,
            "E": 86.13333333333334,
            "F": 85.66666666666667,
            "G": 83.93333333333334,
            "H": 83.06666666666666
        },
        "bus3": {
            "F": 38.6875,
            "G": 40.125,
            "H": 40.25,
            "I": 39.125,
            "J": 38.875,
            "K": 39.5625,
            "L": 40.6875,
            "A": 41.5,
            "B": 42,
            "C": 41.2,
            "D": 39.6,
            "E": 38.86666666666667
        }
    }
}