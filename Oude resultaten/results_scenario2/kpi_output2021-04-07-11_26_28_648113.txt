{
    "Average_holding_time": 17.497797356828194,
    "Average_travel_time_passengers": 2689.7727323911035,
    "Average_speed_busses": 0.04729166666666667,
    "Average_experienced_crowding": 82.82923397428762,
    "Average_covariation_headway": 0.36217081851943245,
    "Average_covariation_headway_stops": {
        "B": 0.2845404983747647,
        "A": 0.28390092558107866,
        "L": 0.2665329110516195,
        "H": 0.2870015567776491,
        "I": 0.2726996188599619,
        "F": 0.2938085818678815,
        "C": 0.273504748186297,
        "J": 0.24799554539708465,
        "G": 0.2816773711968171,
        "D": 0.27259929646109354,
        "K": 0.25886421967839784,
        "E": 0.2987819580402911
    },
    "Average_excess_waiting_time": {
        "B": 59.58327451220657,
        "A": 63.13482834235697,
        "L": 61.87316318382142,
        "H": 63.75479659687471,
        "I": 61.1958900783078,
        "F": 64.94651117324122,
        "C": 59.03809806583507,
        "J": 58.635758207153856,
        "G": 64.3647346774257,
        "D": 61.344375767712734,
        "K": 62.231839067999374,
        "E": 67.76913738186096
    },
    "Holding_per_stop": {
        "B": 14.842105263157896,
        "A": 8.842105263157896,
        "I": 23.05263157894737,
        "H": 17.68421052631579,
        "L": 19.46808510638298,
        "F": 17.36842105263158,
        "C": 20.842105263157894,
        "J": 16.914893617021278,
        "D": 12.631578947368421,
        "G": 20.74468085106383,
        "K": 20.425531914893618,
        "E": 17.23404255319149
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 83.375,
            "C": 84.125,
            "D": 83.5,
            "E": 85.75,
            "F": 86,
            "G": 86.875,
            "H": 85.375,
            "I": 88.0625,
            "J": 88.375,
            "K": 87.9375,
            "L": 87.3125,
            "A": 86.8
        },
        "bus5": {
            "A": 79.625,
            "B": 82.375,
            "C": 82.875,
            "D": 82,
            "E": 82.9375,
            "F": 83.5,
            "G": 83,
            "H": 83.125,
            "I": 84.4375,
            "J": 85.86666666666666,
            "K": 83,
            "L": 84.06666666666666
        },
        "bus4": {
            "L": 84.5625,
            "A": 84.625,
            "B": 85.5625,
            "C": 84.6875,
            "D": 85.3125,
            "E": 84.5625,
            "F": 83.9375,
            "G": 84.6875,
            "H": 85.75,
            "I": 87.66666666666667,
            "J": 88.46666666666667,
            "K": 91.6
        },
        "bus2": {
            "H": 66.5,
            "I": 67.3125,
            "J": 68.1875,
            "K": 68.8125,
            "L": 69.9375,
            "A": 69.3125,
            "B": 70.1875,
            "C": 67.875,
            "D": 69.5625,
            "E": 68.8,
            "F": 70.73333333333333,
            "G": 69.73333333333333
        },
        "bus3": {
            "I": 69,
            "J": 71.375,
            "K": 71.375,
            "L": 72.9375,
            "A": 74.125,
            "B": 74.125,
            "C": 73.3125,
            "D": 72.75,
            "E": 72,
            "F": 72.75,
            "G": 73.5625,
            "H": 71.33333333333333
        },
        "bus1": {
            "F": 69,
            "G": 70.875,
            "H": 72.1875,
            "I": 72.125,
            "J": 73.1875,
            "K": 72.6875,
            "L": 74.3125,
            "A": 75.375,
            "B": 73,
            "C": 72.33333333333333,
            "D": 70.93333333333334,
            "E": 71.8
        }
    }
}": 64.33333333333333,
            "J": 63.53333333333333
        }
    }
}