{
    "Average_holding_time": 14.973451327433628,
    "Average_travel_time_passengers": 2779.55820400777,
    "Average_speed_busses": 0.04708333333333333,
    "Average_experienced_crowding": 97.24418794080731,
    "Average_covariation_headway": 0.6313751770069582,
    "Average_covariation_headway_stops": {
        "J": 0.5104831655824994,
        "D": 0.5263900781374656,
        "B": 0.5281553880127055,
        "F": 0.5372889770990736,
        "E": 0.5142293630794367,
        "K": 0.5172211078356006,
        "C": 0.5305388238269059,
        "G": 0.5266993924356119,
        "L": 0.5374367030824287,
        "H": 0.5159130613632579,
        "A": 0.5213320984330359,
        "I": 0.5132059243634272
    },
    "Average_excess_waiting_time": {
        "J": 123.85612113822827,
        "D": 125.80561265362854,
        "B": 127.27533866621673,
        "F": 125.37317509350663,
        "E": 119.6175517278744,
        "K": 123.12998132346598,
        "C": 130.23061652381438,
        "G": 124.2886484482379,
        "L": 131.772088985705,
        "H": 122.76368783691345,
        "A": 128.45358925625146,
        "I": 126.11779189287347
    },
    "Holding_per_stop": {
        "E": 13.263157894736842,
        "F": 13.263157894736842,
        "J": 15,
        "D": 12.127659574468085,
        "B": 13.085106382978724,
        "K": 15.957446808510639,
        "G": 17.05263157894737,
        "C": 17.872340425531913,
        "L": 18.829787234042552,
        "H": 14.361702127659575,
        "A": 11.808510638297872,
        "I": 17.096774193548388
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "J": 81.0625,
            "K": 80.875,
            "L": 80.9375,
            "A": 79,
            "B": 79.375,
            "C": 79.1875,
            "D": 80.5,
            "E": 83,
            "F": 82.5,
            "G": 84.2,
            "H": 85.8,
            "I": 85
        },
        "bus1": {
            "D": 54,
            "E": 56,
            "F": 56.625,
            "G": 55.375,
            "H": 55.25,
            "I": 54.875,
            "J": 55.75,
            "K": 55.4,
            "L": 57.333333333333336,
            "A": 58.666666666666664,
            "B": 58,
            "C": 56.8
        },
        "bus0": {
            "B": 84.5,
            "C": 83.8125,
            "D": 83.375,
            "E": 83.9375,
            "F": 85.625,
            "G": 87.4375,
            "H": 86.6875,
            "I": 88.1875,
            "J": 89.4,
            "K": 91.53333333333333,
            "L": 90.06666666666666,
            "A": 89.8
        },
        "bus3": {
            "F": 82.8125,
            "G": 82.5625,
            "H": 81.9375,
            "I": 82,
            "J": 81.0625,
            "K": 81.9375,
            "L": 83.4375,
            "A": 85.3125,
            "B": 86.875,
            "C": 87.75,
            "D": 85.93333333333334,
            "E": 86.93333333333334
        },
        "bus2": {
            "E": 57.9375,
            "F": 59.375,
            "G": 60.875,
            "H": 62.625,
            "I": 62.875,
            "J": 63.875,
            "K": 65.0625,
            "L": 64.4375,
            "A": 64.6875,
            "B": 62.2,
            "C": 62.4,
            "D": 61.53333333333333
        },
        "bus5": {
            "K": 97.9375,
            "L": 98.75,
            "A": 97,
            "B": 99.25,
            "C": 98.9375,
            "D": 99.5625,
            "E": 101.4375,
            "F": 100.125,
            "G": 97.375,
            "H": 97.8125,
            "I": 103.06666666666666,
            "J": 106.4
        }
    }
}