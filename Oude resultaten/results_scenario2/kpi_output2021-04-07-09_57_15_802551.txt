{
    "Average_holding_time": 16.48148148148148,
    "Average_travel_time_passengers": 2648.0195754367383,
    "Average_speed_busses": 0.04725,
    "Average_experienced_crowding": 77.94079920482895,
    "Average_covariation_headway": 0.24230190439933363,
    "Average_covariation_headway_stops": {
        "A": 0.17647044993630398,
        "E": 0.19804388018893276,
        "G": 0.17497592774823562,
        "L": 0.17299039670082195,
        "H": 0.18559598842810995,
        "K": 0.15913393851216612,
        "B": 0.16455240180463265,
        "I": 0.15762051822327913,
        "F": 0.16725470274269233,
        "C": 0.17127242998312955,
        "J": 0.17193642723605412,
        "D": 0.18913783109379553
    },
    "Average_excess_waiting_time": {
        "A": 46.00095034295231,
        "E": 50.605881177911385,
        "G": 48.134184872834055,
        "L": 47.52304683463478,
        "H": 47.44629340446397,
        "K": 47.165705487840796,
        "B": 46.27378858665662,
        "I": 47.170056922003596,
        "F": 48.576466314106995,
        "C": 48.63786615330662,
        "J": 50.06340278344953,
        "D": 51.37698484542352
    },
    "Holding_per_stop": {
        "A": 10.736842105263158,
        "L": 18.63157894736842,
        "H": 15.789473684210526,
        "G": 13.894736842105264,
        "E": 22.105263157894736,
        "B": 10.421052631578947,
        "K": 15,
        "I": 13.085106382978724,
        "C": 20.106382978723403,
        "F": 22.02127659574468,
        "J": 22.02127659574468,
        "D": 14.042553191489361
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "A": 73.5,
            "B": 72.875,
            "C": 74.625,
            "D": 75.1875,
            "E": 77.1875,
            "F": 76.6875,
            "G": 77.375,
            "H": 76.9375,
            "I": 78.5625,
            "J": 77.625,
            "K": 77.125,
            "L": 79.4
        },
        "bus0": {
            "E": 71.625,
            "F": 74.75,
            "G": 76,
            "H": 75.5625,
            "I": 76.75,
            "J": 77.75,
            "K": 76.25,
            "L": 77.75,
            "A": 78.46666666666667,
            "B": 78.93333333333334,
            "C": 78.06666666666666,
            "D": 76
        },
        "bus1": {
            "G": 78.6875,
            "H": 79.125,
            "I": 80.4375,
            "J": 79.4375,
            "K": 80.4375,
            "L": 79.5,
            "A": 81.75,
            "B": 83.25,
            "C": 84.8125,
            "D": 84.66666666666667,
            "E": 85.4,
            "F": 85.46666666666667
        },
        "bus4": {
            "L": 74.0625,
            "A": 74.375,
            "B": 76.75,
            "C": 78.6875,
            "D": 81.125,
            "E": 80.25,
            "F": 79.125,
            "G": 78.75,
            "H": 78.875,
            "I": 79.875,
            "J": 81.26666666666667,
            "K": 81.46666666666667
        },
        "bus2": {
            "H": 70.875,
            "I": 72.25,
            "J": 72.25,
            "K": 72.9375,
            "L": 73.125,
            "A": 72.9375,
            "B": 72,
            "C": 73.5,
            "D": 74.0625,
            "E": 73.375,
            "F": 74.66666666666667,
            "G": 74.6
        },
        "bus3": {
            "K": 73.5625,
            "L": 73.0625,
            "A": 72.375,
            "B": 74.25,
            "C": 74.8125,
            "D": 76.6875,
            "E": 75.5,
            "F": 76.75,
            "G": 79.375,
            "H": 80.2,
            "I": 78.33333333333333,
            "J": 77.86666666666666
        }
    }
}