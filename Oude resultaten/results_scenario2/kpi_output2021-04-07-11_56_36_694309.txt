{
    "Average_holding_time": 18.393013100436683,
    "Average_travel_time_passengers": 2694.427927831503,
    "Average_speed_busses": 0.04770833333333333,
    "Average_experienced_crowding": 84.37104457838741,
    "Average_covariation_headway": 0.4246948376793902,
    "Average_covariation_headway_stops": {
        "L": 0.323301379058496,
        "J": 0.31251885176516647,
        "C": 0.3358762509460681,
        "I": 0.31239577102868554,
        "B": 0.34606096289917004,
        "E": 0.31012893815447035,
        "D": 0.30843337690950773,
        "K": 0.31957391251757833,
        "A": 0.29867693488526226,
        "F": 0.29508085720013894,
        "G": 0.2964571269303638,
        "H": 0.318742233845351
    },
    "Average_excess_waiting_time": {
        "L": 67.76176862689238,
        "J": 66.34210205637464,
        "C": 69.11701989146206,
        "I": 68.35985463338244,
        "B": 72.36504318965086,
        "E": 64.9041620230189,
        "D": 65.05184333256369,
        "K": 68.56981468352768,
        "A": 65.00669779071552,
        "F": 64.39740401933943,
        "G": 66.27675523356936,
        "H": 71.64350428689454
    },
    "Holding_per_stop": {
        "J": 19.375,
        "C": 26.25,
        "L": 22.5,
        "B": 16.875,
        "E": 15.9375,
        "I": 17.05263157894737,
        "K": 21.157894736842106,
        "D": 17.5,
        "A": 7.2631578947368425,
        "F": 15.157894736842104,
        "G": 12.947368421052632,
        "H": 28.72340425531915
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 74.5625,
            "A": 74.8125,
            "B": 76,
            "C": 77.625,
            "D": 77,
            "E": 75.9375,
            "F": 77.625,
            "G": 78,
            "H": 78.3125,
            "I": 79.125,
            "J": 79.1875,
            "K": 78.33333333333333
        },
        "bus4": {
            "J": 81.6875,
            "K": 83.0625,
            "L": 83.375,
            "A": 83.3125,
            "B": 84.6875,
            "C": 85.375,
            "D": 86.75,
            "E": 86.3125,
            "F": 88,
            "G": 90,
            "H": 85.73333333333333,
            "I": 85.66666666666667
        },
        "bus1": {
            "C": 58.4375,
            "D": 61.125,
            "E": 60.8125,
            "F": 61.3125,
            "G": 59.8125,
            "H": 60.1875,
            "I": 61.9375,
            "J": 60.9375,
            "K": 60.5,
            "L": 61.3125,
            "A": 61.9375,
            "B": 62.75
        },
        "bus3": {
            "I": 84.1875,
            "J": 83.75,
            "K": 86.4375,
            "L": 88.9375,
            "A": 89.75,
            "B": 88,
            "C": 86.9375,
            "D": 86.9375,
            "E": 85.25,
            "F": 87.66666666666667,
            "G": 89.13333333333334,
            "H": 89.8
        },
        "bus0": {
            "B": 74.3125,
            "C": 76.5,
            "D": 77.125,
            "E": 75.9375,
            "F": 75.5625,
            "G": 76.875,
            "H": 76.6875,
            "I": 77.4375,
            "J": 77.5625,
            "K": 80.75,
            "L": 81.3125,
            "A": 80.06666666666666
        },
        "bus2": {
            "E": 79.5,
            "F": 80.9375,
            "G": 82.3125,
            "H": 83.375,
            "I": 81.8125,
            "J": 83.6875,
            "K": 83.125,
            "L": 83.5,
            "A": 83.8125,
            "B": 83.4375,
            "C": 83.1875,
            "D": 84.6875
        }
    }
}