{
    "Average_holding_time": 24.160714285714285,
    "Average_travel_time_passengers": 2753.8368634626954,
    "Average_speed_busses": 0.04666666666666667,
    "Average_experienced_crowding": 86.94094015613652,
    "Average_covariation_headway": 0.45008245281851494,
    "Average_covariation_headway_stops": {
        "G": 0.3307839409744093,
        "E": 0.36390184717889296,
        "J": 0.34209499625925016,
        "D": 0.3541887746631927,
        "H": 0.3365195147642889,
        "C": 0.3725007032038495,
        "F": 0.3338899688407658,
        "I": 0.33797750786899255,
        "K": 0.3277091237040777,
        "L": 0.3074353767907351,
        "A": 0.3222153364050717,
        "B": 0.35841757013749853
    },
    "Average_excess_waiting_time": {
        "G": 76.35272955493554,
        "E": 84.0047932047882,
        "J": 77.54018712399119,
        "D": 84.05477810407916,
        "H": 75.27678791796245,
        "C": 90.05977061339343,
        "F": 79.29762803205557,
        "I": 78.62401089447775,
        "K": 75.92849806062162,
        "L": 75.83747701618262,
        "A": 79.39130507119091,
        "B": 89.0399094883594
    },
    "Holding_per_stop": {
        "E": 26.170212765957448,
        "G": 30.319148936170212,
        "J": 20.425531914893618,
        "D": 21.29032258064516,
        "H": 21.382978723404257,
        "F": 28.387096774193548,
        "C": 29.032258064516128,
        "I": 26.48936170212766,
        "K": 23.617021276595743,
        "L": 18.06451612903226,
        "A": 22.5,
        "B": 22.17391304347826
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 93.5625,
            "H": 92.0625,
            "I": 94.3125,
            "J": 94.5625,
            "K": 93.3125,
            "L": 92.4375,
            "A": 89.66666666666667,
            "B": 93.8,
            "C": 95.46666666666667,
            "D": 95.8,
            "E": 97.53333333333333,
            "F": 98.2
        },
        "bus2": {
            "E": 93,
            "F": 91.9375,
            "G": 91.875,
            "H": 92.8125,
            "I": 94.25,
            "J": 94,
            "K": 94,
            "L": 96.66666666666667,
            "A": 99.53333333333333,
            "B": 99.86666666666666,
            "C": 100.73333333333333,
            "D": 99.4
        },
        "bus5": {
            "J": 77.0625,
            "K": 77.25,
            "L": 78,
            "A": 81.25,
            "B": 81.8125,
            "C": 81.0625,
            "D": 80,
            "E": 80.5,
            "F": 83.33333333333333,
            "G": 83.93333333333334,
            "H": 85.26666666666667,
            "I": 82.46666666666667
        },
        "bus1": {
            "D": 50.0625,
            "E": 50.875,
            "F": 51.1875,
            "G": 51.8125,
            "H": 53.75,
            "I": 55.0625,
            "J": 53.13333333333333,
            "K": 52.8,
            "L": 51.93333333333333,
            "A": 53.46666666666667,
            "B": 53.733333333333334,
            "C": 53.53333333333333
        },
        "bus4": {
            "H": 76.5625,
            "I": 78.3125,
            "J": 79.3125,
            "K": 79.9375,
            "L": 80.6875,
            "A": 79.6875,
            "B": 79.75,
            "C": 79.25,
            "D": 77.73333333333333,
            "E": 78.6,
            "F": 81.4,
            "G": 81.4
        },
        "bus0": {
            "C": 69,
            "D": 70.875,
            "E": 70.875,
            "F": 69.9375,
            "G": 69,
            "H": 67.46666666666667,
            "I": 70.2,
            "J": 71.06666666666666,
            "K": 71.26666666666667,
            "L": 72.2,
            "A": 72.86666666666666,
            "B": 74
        }
    }
}