{
    "Average_holding_time": 20.53050397877984,
    "Average_travel_time_passengers": 2721.454090191584,
    "Average_speed_busses": 0.047125,
    "Average_experienced_crowding": 83.75100787377097,
    "Average_covariation_headway": 0.3630631435588496,
    "Average_covariation_headway_stops": {
        "G": 0.3033880205622366,
        "A": 0.31995986021067624,
        "D": 0.31922508974482827,
        "C": 0.32383795747887917,
        "B": 0.31432180326930664,
        "F": 0.31220671843791087,
        "H": 0.2897403080560346,
        "E": 0.3051549513973547,
        "I": 0.29290863080441176,
        "J": 0.2847115686311217,
        "K": 0.29626277104766147,
        "L": 0.30893122644966103
    },
    "Average_excess_waiting_time": {
        "G": 65.0577728777368,
        "A": 75.27139634357258,
        "D": 69.95598036883126,
        "C": 71.6125165637635,
        "B": 71.81332694796293,
        "F": 68.7012702762633,
        "H": 63.93230806571751,
        "E": 69.39755398993884,
        "I": 66.54582650912681,
        "J": 66.34222039165616,
        "K": 70.25709487188726,
        "L": 74.99711253889916
    },
    "Holding_per_stop": {
        "G": 19.57894736842105,
        "D": 21.473684210526315,
        "C": 21.157894736842106,
        "B": 21.06382978723404,
        "F": 23.05263157894737,
        "A": 18.387096774193548,
        "H": 20.842105263157894,
        "E": 20.425531914893618,
        "I": 21.06382978723404,
        "J": 21.382978723404257,
        "K": 20.106382978723403,
        "L": 17.741935483870968
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "G": 89.625,
            "H": 90.75,
            "I": 90,
            "J": 90.5,
            "K": 89.125,
            "L": 91.5625,
            "A": 93.5625,
            "B": 95.1875,
            "C": 94.4375,
            "D": 95.6875,
            "E": 98.33333333333333,
            "F": 97.2
        },
        "bus5": {
            "A": 92.5625,
            "B": 91.75,
            "C": 90.0625,
            "D": 93,
            "E": 93.75,
            "F": 94.375,
            "G": 92.33333333333333,
            "H": 93.53333333333333,
            "I": 95.86666666666666,
            "J": 96.06666666666666,
            "K": 97.66666666666667,
            "L": 99.13333333333334
        },
        "bus2": {
            "D": 63.25,
            "E": 61.75,
            "F": 61.5,
            "G": 61.5625,
            "H": 63.5,
            "I": 63.25,
            "J": 63.5625,
            "K": 64.6875,
            "L": 65.0625,
            "A": 66.1875,
            "B": 66,
            "C": 67.8
        },
        "bus1": {
            "C": 71.625,
            "D": 72.9375,
            "E": 75.9375,
            "F": 77.125,
            "G": 74.25,
            "H": 75.25,
            "I": 75.4375,
            "J": 76.6875,
            "K": 78.6875,
            "L": 78.2,
            "A": 78.2,
            "B": 76.06666666666666
        },
        "bus0": {
            "B": 79,
            "C": 77.9375,
            "D": 76.9375,
            "E": 77.125,
            "F": 78.375,
            "G": 79,
            "H": 81.625,
            "I": 81.4375,
            "J": 82.4,
            "K": 83.53333333333333,
            "L": 82.26666666666667,
            "A": 85.06666666666666
        },
        "bus3": {
            "F": 57.1875,
            "G": 57.875,
            "H": 59.1875,
            "I": 60.4375,
            "J": 61.3125,
            "K": 61.25,
            "L": 62.375,
            "A": 64.1875,
            "B": 62.3125,
            "C": 61.3125,
            "D": 62.266666666666666,
            "E": 60.733333333333334
        }
    }
}