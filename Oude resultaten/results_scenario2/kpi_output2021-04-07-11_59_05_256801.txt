{
    "Average_holding_time": 21.218861209964412,
    "Average_travel_time_passengers": 2750.913991093177,
    "Average_speed_busses": 0.04683333333333333,
    "Average_experienced_crowding": 88.05971698223834,
    "Average_covariation_headway": 0.46634355177762893,
    "Average_covariation_headway_stops": {
        "I": 0.3954756678326011,
        "L": 0.41046001854496766,
        "K": 0.3800158046000753,
        "H": 0.3701850376975423,
        "C": 0.36726535772616264,
        "B": 0.3795210094115145,
        "J": 0.39041421090739703,
        "A": 0.3813385867966509,
        "D": 0.3633396048156422,
        "E": 0.34727111395544263,
        "F": 0.34976452699854144,
        "G": 0.36083885462583626
    },
    "Average_excess_waiting_time": {
        "I": 91.742612762992,
        "L": 93.71824261346086,
        "K": 88.05307853892737,
        "H": 87.9722254058234,
        "C": 80.441807005145,
        "B": 85.3985197017821,
        "J": 92.04847431072557,
        "A": 88.49513060047087,
        "D": 81.46857598595852,
        "E": 80.35121898597248,
        "F": 82.91743370191313,
        "G": 86.98937696734231
    },
    "Holding_per_stop": {
        "I": 25.53191489361702,
        "L": 30.638297872340427,
        "C": 19.894736842105264,
        "B": 18.19148936170213,
        "K": 17.419354838709676,
        "H": 19.032258064516128,
        "J": 22.903225806451612,
        "A": 11.808510638297872,
        "D": 24.25531914893617,
        "E": 23.617021276595743,
        "F": 23.225806451612904,
        "G": 18.06451612903226
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "I": 80.125,
            "J": 81.1875,
            "K": 81.8125,
            "L": 82.375,
            "A": 83,
            "B": 82.875,
            "C": 84.125,
            "D": 85.86666666666666,
            "E": 86.13333333333334,
            "F": 88.53333333333333,
            "G": 87.06666666666666,
            "H": 85.53333333333333
        },
        "bus5": {
            "L": 74.75,
            "A": 73.8125,
            "B": 74.125,
            "C": 74.9375,
            "D": 74.8125,
            "E": 74.6875,
            "F": 76.5625,
            "G": 77.1875,
            "H": 80.26666666666667,
            "I": 80.93333333333334,
            "J": 80.13333333333334,
            "K": 80.8
        },
        "bus4": {
            "K": 73.875,
            "L": 73.625,
            "A": 76.25,
            "B": 75.0625,
            "C": 75.8125,
            "D": 76.125,
            "E": 78.5625,
            "F": 79.8,
            "G": 81.26666666666667,
            "H": 79.66666666666667,
            "I": 79.2,
            "J": 78
        },
        "bus2": {
            "H": 76.75,
            "I": 78,
            "J": 78.5625,
            "K": 79.4375,
            "L": 81.125,
            "A": 84.125,
            "B": 83.93333333333334,
            "C": 82.93333333333334,
            "D": 81.86666666666666,
            "E": 82.93333333333334,
            "F": 81.8,
            "G": 81.13333333333334
        },
        "bus1": {
            "C": 87.75,
            "D": 86.5625,
            "E": 85.75,
            "F": 86.3125,
            "G": 88,
            "H": 89.25,
            "I": 90.625,
            "J": 92.75,
            "K": 94.53333333333333,
            "L": 95.26666666666667,
            "A": 95.8,
            "B": 94
        },
        "bus0": {
            "B": 56.5,
            "C": 57.4375,
            "D": 58.6875,
            "E": 59.375,
            "F": 61.3125,
            "G": 60.9375,
            "H": 61.375,
            "I": 59.3125,
            "J": 58.266666666666666,
            "K": 59.266666666666666,
            "L": 59.86666666666667,
            "A": 59.666666666666664
        }
    }
}