{
    "Average_holding_time": 7.1416382252559725,
    "Average_travel_time_passengers": 2569.6032636826917,
    "Average_speed_busses": 0.04883333333333333,
    "Average_experienced_crowding": 103.5166939574527,
    "Average_covariation_headway": 0.23192884357295573,
    "Average_covariation_headway_stops": {
        "H": 0.10759177313788443,
        "F": 0.1022923202193902,
        "J": 0.08381705815613805,
        "D": 0.08856436627918694,
        "A": 0.08335169440855734,
        "B": 0.08348048941570152,
        "I": 0.06702695183795296,
        "K": 0.0557935651614862,
        "G": 0.05591444476582332,
        "E": 0.08139664425136872,
        "C": 0.06720867585309219,
        "L": 0.060620367856401874
    },
    "Average_excess_waiting_time": {
        "H": 36.581060169184525,
        "F": 36.99856045648261,
        "J": 34.788720672168495,
        "D": 36.412901659607655,
        "A": 36.409194680223266,
        "B": 35.46016876181028,
        "I": 35.56924753601743,
        "K": 35.034330666788435,
        "G": 36.004104023993534,
        "E": 37.526759178977045,
        "C": 36.746740836688616,
        "L": 36.81705191938971
    },
    "Holding_per_stop": {
        "H": 9.795918367346939,
        "J": 6.73469387755102,
        "D": 0.6122448979591837,
        "B": 1.530612244897959,
        "F": 43.775510204081634,
        "A": 1.2244897959183674,
        "K": 0.30612244897959184,
        "I": 4.285714285714286,
        "E": 4.329896907216495,
        "G": 8.350515463917526,
        "L": 2.783505154639175,
        "C": 1.8556701030927836
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 127,
            "I": 124.88235294117646,
            "J": 118.82352941176471,
            "K": 111.47058823529412,
            "L": 99.6875,
            "A": 92,
            "B": 83.125,
            "C": 76.0625,
            "D": 74.375,
            "E": 72.75,
            "F": 95.8125,
            "G": 127
        },
        "bus2": {
            "F": 83.82352941176471,
            "G": 117.58823529411765,
            "H": 123,
            "I": 120.29411764705883,
            "J": 116.0625,
            "K": 109.3125,
            "L": 98.375,
            "A": 89.5,
            "B": 81.625,
            "C": 74.8125,
            "D": 72.1875,
            "E": 71.1875
        },
        "bus4": {
            "J": 124.82352941176471,
            "K": 116.47058823529412,
            "L": 107.47058823529412,
            "A": 96.23529411764706,
            "B": 86.375,
            "C": 79.625,
            "D": 77.1875,
            "E": 76.625,
            "F": 105.3125,
            "G": 137.4375,
            "H": 143.8125,
            "I": 139.5
        },
        "bus1": {
            "D": 69.52941176470588,
            "E": 69.47058823529412,
            "F": 93.70588235294117,
            "G": 123.125,
            "H": 128.5,
            "I": 126.4375,
            "J": 119.4375,
            "K": 110.75,
            "L": 101.3125,
            "A": 92.8125,
            "B": 82.3125,
            "C": 77.1875
        },
        "bus5": {
            "A": 87.70588235294117,
            "B": 78.23529411764706,
            "C": 73.875,
            "D": 72.8125,
            "E": 71.4375,
            "F": 94.375,
            "G": 123.25,
            "H": 129.375,
            "I": 126.125,
            "J": 119.6875,
            "K": 111.0625,
            "L": 102.6875
        },
        "bus0": {
            "B": 77.17647058823529,
            "C": 71.88235294117646,
            "D": 69.70588235294117,
            "E": 68.1875,
            "F": 90.5,
            "G": 122.9375,
            "H": 130.8125,
            "I": 127.75,
            "J": 120,
            "K": 112.75,
            "L": 101.3125,
            "A": 90.75
        }
    }
}