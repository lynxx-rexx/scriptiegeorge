{
    "Average_holding_time": 22.311827956989248,
    "Average_travel_time_passengers": 2698.544119478471,
    "Average_speed_busses": 0.0465,
    "Average_experienced_crowding": 81.36601392092702,
    "Average_covariation_headway": 0.2574636021297079,
    "Average_covariation_headway_stops": {
        "L": 0.18860001585993233,
        "C": 0.20892442989818683,
        "B": 0.20490638817419335,
        "F": 0.18822756095950374,
        "I": 0.19454471800784054,
        "J": 0.17391496069252776,
        "G": 0.17033512127394446,
        "A": 0.1835478275376635,
        "D": 0.17782552159143242,
        "K": 0.16971165467271263,
        "E": 0.17396073393177158,
        "H": 0.17984061130497056
    },
    "Average_excess_waiting_time": {
        "L": 54.21127277224417,
        "C": 55.116252675456735,
        "B": 55.86930541698797,
        "F": 53.84256081581361,
        "I": 57.047038034491436,
        "J": 52.28222282702569,
        "G": 53.82761751544217,
        "A": 55.38155207286792,
        "D": 52.559005750460244,
        "K": 54.07800992427906,
        "E": 53.878854678957566,
        "H": 56.62798699184981
    },
    "Holding_per_stop": {
        "C": 23.617021276595743,
        "F": 25.806451612903224,
        "L": 23.870967741935484,
        "B": 18.06451612903226,
        "J": 28.387096774193548,
        "I": 19.35483870967742,
        "D": 24.516129032258064,
        "G": 27.096774193548388,
        "A": 11.612903225806452,
        "K": 22.580645161290324,
        "E": 23.225806451612904,
        "H": 19.565217391304348
    },
    "Actions_tfl_edges": {
        "AB": [
            10,
            40,
            44
        ],
        "BC": [
            16,
            36,
            42
        ],
        "EF": [
            13,
            37,
            44
        ],
        "HI": [
            14,
            40,
            39
        ],
        "IJ": [
            13,
            41,
            40
        ],
        "KL": [
            12,
            37,
            45
        ],
        "CD": [
            13,
            40,
            41
        ],
        "FG": [
            13,
            37,
            43
        ],
        "LA": [
            16,
            36,
            41
        ],
        "JK": [
            17,
            32,
            44
        ],
        "DE": [
            13,
            34,
            46
        ],
        "GH": [
            11,
            41,
            41
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 80.25,
            "A": 81.8125,
            "B": 80.3125,
            "C": 82.875,
            "D": 84.375,
            "E": 83.8125,
            "F": 84.5,
            "G": 84.4,
            "H": 85.13333333333334,
            "I": 86.46666666666667,
            "J": 85.53333333333333,
            "K": 86.93333333333334
        },
        "bus1": {
            "C": 76.6875,
            "D": 75.75,
            "E": 74.5,
            "F": 75.6875,
            "G": 77.9375,
            "H": 81,
            "I": 82.25,
            "J": 81.86666666666666,
            "K": 81.06666666666666,
            "L": 80,
            "A": 78.73333333333333,
            "B": 81.53333333333333
        },
        "bus0": {
            "B": 74.0625,
            "C": 75.0625,
            "D": 74.8125,
            "E": 77.875,
            "F": 79.5625,
            "G": 80.5,
            "H": 80.125,
            "I": 78.2,
            "J": 78.46666666666667,
            "K": 77.6,
            "L": 80.2,
            "A": 81.13333333333334
        },
        "bus2": {
            "F": 74.25,
            "G": 75.8125,
            "H": 76.9375,
            "I": 77.5625,
            "J": 75.8125,
            "K": 74.75,
            "L": 76.0625,
            "A": 76.6,
            "B": 74.13333333333334,
            "C": 77.06666666666666,
            "D": 77.6,
            "E": 78
        },
        "bus3": {
            "I": 82.875,
            "J": 82.9375,
            "K": 81.9375,
            "L": 83.5625,
            "A": 82.5625,
            "B": 80.9375,
            "C": 81.06666666666666,
            "D": 82.8,
            "E": 84.66666666666667,
            "F": 86.53333333333333,
            "G": 88,
            "H": 88.33333333333333
        },
        "bus4": {
            "J": 78.625,
            "K": 79.9375,
            "L": 78.75,
            "A": 79,
            "B": 78.1875,
            "C": 80.8125,
            "D": 80.2,
            "E": 80.46666666666667,
            "F": 84.06666666666666,
            "G": 85.53333333333333,
            "H": 85.86666666666666,
            "I": 83.4
        }
    }
}