{
    "Average_holding_time": 26.886005560704355,
    "Average_travel_time_passengers": 2808.1639082140787,
    "Average_speed_busses": 0.044958333333333336,
    "Average_experienced_crowding": 86.56352354925782,
    "Average_covariation_headway": 0.3238252187541447,
    "Average_covariation_headway_stops": {
        "C": 0.28367523188612037,
        "D": 0.284534237487509,
        "G": 0.25797204764395176,
        "I": 0.26200905637882543,
        "F": 0.24870888251389972,
        "B": 0.28757899938217,
        "E": 0.2568762782822345,
        "H": 0.25286814289980164,
        "J": 0.23474291763287478,
        "K": 0.23123880342777367,
        "L": 0.2437798704085863,
        "A": 0.281628919790715
    },
    "Average_excess_waiting_time": {
        "C": 82.4859067437244,
        "D": 80.33357485136685,
        "G": 73.53342803618824,
        "I": 73.88682856004255,
        "F": 73.76091536302152,
        "B": 84.25826981718996,
        "E": 77.19755271278808,
        "H": 74.13498795504813,
        "J": 71.52114536548919,
        "K": 72.88999091789145,
        "L": 76.57231668814228,
        "A": 85.3612426296774
    },
    "Holding_per_stop": {
        "C": 29.333333333333332,
        "D": 25,
        "I": 29.67032967032967,
        "G": 23.406593406593405,
        "F": 24.666666666666668,
        "H": 29.666666666666668,
        "E": 27.666666666666668,
        "J": 27,
        "B": 26.292134831460675,
        "K": 31,
        "L": 30.337078651685392,
        "A": 18.53932584269663
    },
    "Actions_tfl_edges": {
        "AB": [
            5,
            72,
            13
        ],
        "BC": [
            7,
            66,
            17
        ],
        "CD": [
            8,
            70,
            13
        ],
        "EF": [
            4,
            71,
            16
        ],
        "FG": [
            6,
            71,
            14
        ],
        "HI": [
            7,
            64,
            20
        ],
        "DE": [
            9,
            69,
            12
        ],
        "IJ": [
            6,
            66,
            19
        ],
        "GH": [
            7,
            72,
            12
        ],
        "JK": [
            3,
            68,
            19
        ],
        "KL": [
            5,
            66,
            19
        ],
        "LA": [
            1,
            72,
            16
        ]
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "C": 81.33333333333333,
            "D": 82.86666666666666,
            "E": 82.13333333333334,
            "F": 82.86666666666666,
            "G": 84.26666666666667,
            "H": 84.06666666666666,
            "I": 87.8,
            "J": 88.86666666666666,
            "K": 89.93333333333334,
            "L": 87.93333333333334,
            "A": 87.73333333333333,
            "B": 88.46666666666667
        },
        "bus2": {
            "D": 70.53333333333333,
            "E": 72.26666666666667,
            "F": 73.4,
            "G": 73.46666666666667,
            "H": 76,
            "I": 77,
            "J": 76.66666666666667,
            "K": 78.33333333333333,
            "L": 77.66666666666667,
            "A": 76.73333333333333,
            "B": 75.33333333333333,
            "C": 74.53333333333333
        },
        "bus4": {
            "G": 69.75,
            "H": 69.5625,
            "I": 69.8,
            "J": 70.86666666666666,
            "K": 69.6,
            "L": 68.86666666666666,
            "A": 68.73333333333333,
            "B": 69.73333333333333,
            "C": 69.6,
            "D": 70.46666666666667,
            "E": 71.86666666666666,
            "F": 73
        },
        "bus5": {
            "I": 88.75,
            "J": 87.5625,
            "K": 90.53333333333333,
            "L": 94.06666666666666,
            "A": 94.33333333333333,
            "B": 92.93333333333334,
            "C": 95.2,
            "D": 96.73333333333333,
            "E": 98.46666666666667,
            "F": 99.4,
            "G": 96.93333333333334,
            "H": 97.53333333333333
        },
        "bus3": {
            "F": 76.46666666666667,
            "G": 75.73333333333333,
            "H": 76.86666666666666,
            "I": 79.13333333333334,
            "J": 82.06666666666666,
            "K": 82.53333333333333,
            "L": 83.33333333333333,
            "A": 83.6,
            "B": 85.8,
            "C": 84.33333333333333,
            "D": 84.2,
            "E": 83.46666666666667
        },
        "bus0": {
            "B": 88.4,
            "C": 87.4,
            "D": 88.13333333333334,
            "E": 87.6,
            "F": 87.53333333333333,
            "G": 89.06666666666666,
            "H": 89.33333333333333,
            "I": 88.13333333333334,
            "J": 87.8,
            "K": 88.46666666666667,
            "L": 93.07142857142857,
            "A": 92.71428571428571
        }
    }
}