{
    "Average_holding_time": 20.25202520252025,
    "Average_travel_time_passengers": 2729.0600909520726,
    "Average_speed_busses": 0.04629166666666667,
    "Average_experienced_crowding": 80.83120430239609,
    "Average_covariation_headway": 0.19740172460624475,
    "Average_covariation_headway_stops": {
        "L": 0.15387890489610628,
        "G": 0.1704822590419671,
        "F": 0.1298376077450953,
        "J": 0.130744561077886,
        "C": 0.15947899187273756,
        "E": 0.13185587890617595,
        "A": 0.13931165147496682,
        "H": 0.1482635038385267,
        "K": 0.12267982192493601,
        "D": 0.13875251581633344,
        "B": 0.13122628974744294,
        "I": 0.14795487975597116
    },
    "Average_excess_waiting_time": {
        "L": 52.36170775740953,
        "G": 52.05245933288154,
        "F": 49.71783958870583,
        "J": 49.56898117747437,
        "C": 54.330587437297424,
        "E": 51.601989129604135,
        "A": 52.50168362309216,
        "H": 51.443679678015314,
        "K": 51.152808786882076,
        "D": 54.30626371583173,
        "B": 53.57504114524812,
        "I": 53.33126958356149
    },
    "Holding_per_stop": {
        "L": 22.580645161290324,
        "G": 21.612903225806452,
        "F": 20.967741935483872,
        "J": 20.967741935483872,
        "C": 28.06451612903226,
        "E": 24.516129032258064,
        "A": 8.478260869565217,
        "H": 19.032258064516128,
        "K": 24.456521739130434,
        "D": 19.23913043478261,
        "B": 14.021739130434783,
        "I": 18.91304347826087
    },
    "Actions_tfl_edges": {
        "BC": [
            4,
            76,
            13
        ],
        "DE": [
            14,
            65,
            14
        ],
        "EF": [
            7,
            79,
            8
        ],
        "FG": [
            10,
            77,
            7
        ],
        "IJ": [
            5,
            72,
            16
        ],
        "KL": [
            8,
            68,
            17
        ],
        "LA": [
            7,
            67,
            19
        ],
        "GH": [
            12,
            57,
            24
        ],
        "JK": [
            7,
            76,
            10
        ],
        "CD": [
            7,
            68,
            18
        ],
        "AB": [
            14,
            59,
            19
        ],
        "HI": [
            13,
            60,
            20
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 82,
            "A": 83.6875,
            "B": 82.5625,
            "C": 83.875,
            "D": 85.125,
            "E": 84.5625,
            "F": 84.4,
            "G": 83,
            "H": 85.46666666666667,
            "I": 87.06666666666666,
            "J": 89.26666666666667,
            "K": 88.66666666666667
        },
        "bus3": {
            "G": 73.875,
            "H": 75,
            "I": 75.625,
            "J": 75.0625,
            "K": 75.9375,
            "L": 75.625,
            "A": 75.5,
            "B": 76.13333333333334,
            "C": 76.13333333333334,
            "D": 76.4,
            "E": 75.4,
            "F": 76.4
        },
        "bus2": {
            "F": 73.9375,
            "G": 75.1875,
            "H": 74.25,
            "I": 72.0625,
            "J": 72.875,
            "K": 75.25,
            "L": 77.06666666666666,
            "A": 77.4,
            "B": 78.13333333333334,
            "C": 77.73333333333333,
            "D": 79.4,
            "E": 79
        },
        "bus4": {
            "J": 77.125,
            "K": 76.8125,
            "L": 77.375,
            "A": 75.0625,
            "B": 74.75,
            "C": 77.1875,
            "D": 79.86666666666666,
            "E": 81.13333333333334,
            "F": 82,
            "G": 81,
            "H": 82.33333333333333,
            "I": 80.46666666666667
        },
        "bus0": {
            "C": 76.625,
            "D": 76.625,
            "E": 76.8125,
            "F": 75.0625,
            "G": 75.375,
            "H": 77.2,
            "I": 77.53333333333333,
            "J": 80.33333333333333,
            "K": 82.8,
            "L": 83,
            "A": 82.6,
            "B": 83.13333333333334
        },
        "bus1": {
            "E": 80.25,
            "F": 82.375,
            "G": 85.25,
            "H": 87.3125,
            "I": 88.3125,
            "J": 88.13333333333334,
            "K": 86,
            "L": 87.2,
            "A": 85.53333333333333,
            "B": 85.66666666666667,
            "C": 85.4,
            "D": 84.86666666666666
        }
    }
}