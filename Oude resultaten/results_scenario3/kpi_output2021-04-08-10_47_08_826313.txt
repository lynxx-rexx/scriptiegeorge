{
    "Average_holding_time": 68.33333333333333,
    "Average_travel_time_passengers": 4887.6069435252,
    "Average_speed_busses": 0.03,
    "Average_experienced_crowding": 290.30212518635454,
    "Average_covariation_headway": 1.4598784814389143,
    "Average_covariation_headway_stops": {
        "L": 1.2545107597182483,
        "J": 1.3276192811191183,
        "C": 1.2626607797635783,
        "I": 1.3135778690585713,
        "B": 1.2427205266267691,
        "F": 1.2952920213874424,
        "K": 1.2710854027863305,
        "D": 1.2651590513205022,
        "A": 1.2653551281433526,
        "G": 1.2964521471940351,
        "E": 1.2572026640287612,
        "H": 1.285718726236573
    },
    "Average_excess_waiting_time": {
        "L": 925.2932994015912,
        "J": 1021.9017287852485,
        "C": 919.331336635943,
        "I": 1009.8690317388075,
        "B": 906.8143018185101,
        "F": 968.8751174821684,
        "K": 1057.7380985062177,
        "D": 933.7674062865412,
        "A": 944.6093912264191,
        "G": 980.7723851353221,
        "E": 935.464435263847,
        "H": 983.7869027698991
    },
    "Holding_per_stop": {
        "L": 68.27586206896552,
        "J": 58.57142857142857,
        "C": 68.5,
        "B": 78.8135593220339,
        "F": 69.34426229508196,
        "I": 63.38709677419355,
        "K": 73.15789473684211,
        "D": 73.5,
        "A": 56.89655172413793,
        "G": 64.91803278688525,
        "E": 81.5,
        "H": 63.9344262295082
    },
    "Actions_tfl_edges": {
        "AB": [
            6,
            38,
            15
        ],
        "BC": [
            2,
            46,
            12
        ],
        "EF": [
            7,
            41,
            13
        ],
        "HI": [
            12,
            37,
            13
        ],
        "IJ": [
            11,
            42,
            10
        ],
        "KL": [
            12,
            33,
            13
        ],
        "LA": [
            10,
            36,
            12
        ],
        "JK": [
            10,
            42,
            11
        ],
        "CD": [
            11,
            37,
            12
        ],
        "FG": [
            10,
            41,
            10
        ],
        "DE": [
            9,
            37,
            14
        ],
        "GH": [
            10,
            42,
            9
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 8.6,
            "A": 10.4,
            "B": 11.9,
            "C": 13,
            "D": 13.2,
            "E": 13.7,
            "F": 12.3,
            "G": 12.5,
            "H": 12.4,
            "I": 11.8,
            "J": 10.4,
            "K": 9.444444444444445
        },
        "bus4": {
            "J": 12.909090909090908,
            "K": 13.7,
            "L": 15.1,
            "A": 16.5,
            "B": 17.6,
            "C": 18.3,
            "D": 16.5,
            "E": 16.8,
            "F": 16.6,
            "G": 16,
            "H": 15.1,
            "I": 14.7
        },
        "bus1": {
            "C": 325.6,
            "D": 329.5,
            "E": 330.4,
            "F": 336.8,
            "G": 340.2,
            "H": 345.8,
            "I": 347.8,
            "J": 355.5,
            "K": 361.9,
            "L": 344.77777777777777,
            "A": 352.55555555555554,
            "B": 358.1111111111111
        },
        "bus3": {
            "I": 53.54545454545455,
            "J": 54.81818181818182,
            "K": 62,
            "L": 61.6,
            "A": 61,
            "B": 60.2,
            "C": 60.8,
            "D": 61.2,
            "E": 58.3,
            "F": 58.4,
            "G": 58.6,
            "H": 58.3
        },
        "bus0": {
            "B": 32,
            "C": 33.6,
            "D": 33.2,
            "E": 33.7,
            "F": 35.9,
            "G": 35.5,
            "H": 35.9,
            "I": 37.1,
            "J": 35.6,
            "K": 35.111111111111114,
            "L": 36.77777777777778,
            "A": 34.77777777777778
        },
        "bus2": {
            "F": 220.0909090909091,
            "G": 221.8181818181818,
            "H": 225.0909090909091,
            "I": 229.72727272727272,
            "J": 230.54545454545453,
            "K": 246.4,
            "L": 247.8,
            "A": 248.7,
            "B": 249.8,
            "C": 249.4,
            "D": 248.5,
            "E": 245.8
        }
    }
}