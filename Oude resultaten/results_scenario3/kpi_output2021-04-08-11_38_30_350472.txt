{
    "Average_holding_time": 20.513290559120072,
    "Average_travel_time_passengers": 2799.2546274803904,
    "Average_speed_busses": 0.04545833333333334,
    "Average_experienced_crowding": 83.05606893794857,
    "Average_covariation_headway": 0.24899633209475114,
    "Average_covariation_headway_stops": {
        "F": 0.2044811859162895,
        "B": 0.18962522996985656,
        "C": 0.20296091758512214,
        "D": 0.19701638128868373,
        "K": 0.20902007035260442,
        "I": 0.1992683173232032,
        "G": 0.17901318290102575,
        "J": 0.19902717496335715,
        "E": 0.16687320227134125,
        "L": 0.20238244700802363,
        "H": 0.18155695934720895,
        "A": 0.17156086961151995
    },
    "Average_excess_waiting_time": {
        "F": 62.03565139454406,
        "B": 64.5691841036089,
        "C": 64.40812504182651,
        "D": 61.77056049874858,
        "K": 65.34905956990445,
        "I": 62.67812020776927,
        "G": 59.75830473319331,
        "J": 65.03476441171858,
        "E": 58.80876503257991,
        "L": 66.36801368765538,
        "H": 62.510151657602705,
        "A": 66.59860544722414
    },
    "Holding_per_stop": {
        "F": 20.869565217391305,
        "C": 30.32967032967033,
        "D": 22.747252747252748,
        "K": 24.725274725274726,
        "I": 20.10989010989011,
        "B": 15,
        "G": 19.12087912087912,
        "E": 18.791208791208792,
        "J": 18.13186813186813,
        "L": 22.087912087912088,
        "H": 21.0989010989011,
        "A": 13
    },
    "Actions_tfl_edges": {
        "AB": [
            13,
            59,
            19
        ],
        "BC": [
            7,
            68,
            16
        ],
        "CD": [
            8,
            70,
            14
        ],
        "EF": [
            6,
            74,
            12
        ],
        "HI": [
            7,
            75,
            10
        ],
        "JK": [
            5,
            72,
            15
        ],
        "FG": [
            7,
            68,
            17
        ],
        "DE": [
            5,
            69,
            17
        ],
        "KL": [
            4,
            69,
            18
        ],
        "IJ": [
            8,
            70,
            13
        ],
        "GH": [
            5,
            67,
            19
        ],
        "LA": [
            5,
            68,
            18
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "F": 73.6875,
            "G": 75.5,
            "H": 75.75,
            "I": 77.5625,
            "J": 76.06666666666666,
            "K": 75,
            "L": 75.73333333333333,
            "A": 75.86666666666666,
            "B": 74.46666666666667,
            "C": 75.46666666666667,
            "D": 76.46666666666667,
            "E": 78.4
        },
        "bus0": {
            "B": 82.86666666666666,
            "C": 83.66666666666667,
            "D": 83.93333333333334,
            "E": 83.53333333333333,
            "F": 82.13333333333334,
            "G": 82.93333333333334,
            "H": 81.4,
            "I": 82.8,
            "J": 85,
            "K": 86.6,
            "L": 86.8,
            "A": 88.66666666666667
        },
        "bus1": {
            "C": 83.8125,
            "D": 85,
            "E": 85.2,
            "F": 86.53333333333333,
            "G": 87.06666666666666,
            "H": 86.73333333333333,
            "I": 86.53333333333333,
            "J": 87,
            "K": 86.13333333333334,
            "L": 86.73333333333333,
            "A": 87.2,
            "B": 89.2
        },
        "bus2": {
            "D": 76.125,
            "E": 77.6875,
            "F": 79.5,
            "G": 80.26666666666667,
            "H": 81.8,
            "I": 83.73333333333333,
            "J": 84.46666666666667,
            "K": 84.26666666666667,
            "L": 85.33333333333333,
            "A": 84.33333333333333,
            "B": 83.8,
            "C": 81.33333333333333
        },
        "bus5": {
            "K": 80,
            "L": 79.5,
            "A": 81.46666666666667,
            "B": 85.26666666666667,
            "C": 84.73333333333333,
            "D": 85.6,
            "E": 86.2,
            "F": 84.73333333333333,
            "G": 83.73333333333333,
            "H": 82.6,
            "I": 82.6,
            "J": 83.26666666666667
        },
        "bus4": {
            "I": 76.125,
            "J": 75.875,
            "K": 75.33333333333333,
            "L": 77.06666666666666,
            "A": 78.46666666666667,
            "B": 80.26666666666667,
            "C": 80.86666666666666,
            "D": 77.73333333333333,
            "E": 77.26666666666667,
            "F": 77.93333333333334,
            "G": 78.53333333333333,
            "H": 80.6
        }
    }
}F": 86.26666666666667
        }
    }
}