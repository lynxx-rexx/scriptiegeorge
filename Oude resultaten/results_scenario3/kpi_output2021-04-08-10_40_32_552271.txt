{
    "Average_holding_time": 19.107939339875113,
    "Average_travel_time_passengers": 2755.668658465549,
    "Average_speed_busses": 0.04670833333333333,
    "Average_experienced_crowding": 87.26666640873327,
    "Average_covariation_headway": 0.46787037285101585,
    "Average_covariation_headway_stops": {
        "K": 0.370667371194585,
        "A": 0.36787468101046206,
        "D": 0.3726639529634139,
        "J": 0.36138257739796364,
        "I": 0.3437006072728522,
        "H": 0.33877651942417425,
        "B": 0.36820041605358445,
        "E": 0.35629293415222013,
        "L": 0.3700440810323223,
        "F": 0.35673580584988107,
        "C": 0.38025509765687426,
        "G": 0.34622436107822857
    },
    "Average_excess_waiting_time": {
        "K": 83.24030975305027,
        "A": 83.5340674627916,
        "D": 85.27999162223728,
        "J": 82.88538246347235,
        "I": 80.30109575311468,
        "H": 81.53899971777912,
        "B": 85.29852364206636,
        "E": 83.10845822329253,
        "L": 84.75379649213306,
        "F": 84.69196882370034,
        "C": 89.21326923906219,
        "G": 84.61488863079677
    },
    "Holding_per_stop": {
        "K": 20.425531914893618,
        "A": 15.957446808510639,
        "D": 20.425531914893618,
        "J": 19.148936170212767,
        "I": 18.70967741935484,
        "H": 20,
        "B": 17.872340425531913,
        "E": 17.419354838709676,
        "L": 18.51063829787234,
        "F": 21.93548387096774,
        "C": 18.06451612903226,
        "G": 20.869565217391305
    },
    "Actions_tfl_edges": {
        "CD": [
            30,
            33,
            31
        ],
        "GH": [
            30,
            28,
            35
        ],
        "HI": [
            37,
            27,
            30
        ],
        "IJ": [
            32,
            29,
            33
        ],
        "JK": [
            33,
            33,
            29
        ],
        "LA": [
            34,
            34,
            27
        ],
        "KL": [
            40,
            26,
            28
        ],
        "AB": [
            30,
            38,
            26
        ],
        "DE": [
            37,
            24,
            33
        ],
        "BC": [
            32,
            38,
            24
        ],
        "EF": [
            33,
            28,
            32
        ],
        "FG": [
            29,
            30,
            34
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "K": 61.5625,
            "L": 60.875,
            "A": 62.625,
            "B": 62.875,
            "C": 64.3125,
            "D": 64.4375,
            "E": 64.125,
            "F": 65,
            "G": 65.9375,
            "H": 65.86666666666666,
            "I": 64.86666666666666,
            "J": 64.4
        },
        "bus5": {
            "A": 64.8125,
            "B": 64.125,
            "C": 66.3125,
            "D": 67.25,
            "E": 66.625,
            "F": 68.4375,
            "G": 71.5,
            "H": 71.75,
            "I": 72.4,
            "J": 70.6,
            "K": 69.06666666666666,
            "L": 70.86666666666666
        },
        "bus0": {
            "D": 95.875,
            "E": 94.5,
            "F": 93.125,
            "G": 94,
            "H": 93.875,
            "I": 95.5,
            "J": 97.625,
            "K": 98.86666666666666,
            "L": 98.73333333333333,
            "A": 100.6,
            "B": 101.93333333333334,
            "C": 100.93333333333334
        },
        "bus3": {
            "J": 79.125,
            "K": 79.125,
            "L": 79.3125,
            "A": 79.25,
            "B": 78.6875,
            "C": 81.0625,
            "D": 81.25,
            "E": 80.4,
            "F": 83.46666666666667,
            "G": 85.8,
            "H": 85.73333333333333,
            "I": 84.86666666666666
        },
        "bus2": {
            "I": 85.25,
            "J": 85.25,
            "K": 87.4375,
            "L": 90.4375,
            "A": 91.625,
            "B": 93.25,
            "C": 95,
            "D": 94.26666666666667,
            "E": 91.6,
            "F": 91.33333333333333,
            "G": 90,
            "H": 89.13333333333334
        },
        "bus1": {
            "H": 70.375,
            "I": 71.5,
            "J": 72.875,
            "K": 72.5,
            "L": 72.375,
            "A": 74.66666666666667,
            "B": 73.93333333333334,
            "C": 73.66666666666667,
            "D": 73,
            "E": 73.8,
            "F": 75.66666666666667,
            "G": 75.6
        }
    }
}