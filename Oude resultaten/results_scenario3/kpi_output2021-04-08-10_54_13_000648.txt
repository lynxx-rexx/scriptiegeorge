{
    "Average_holding_time": 73.933236574746,
    "Average_travel_time_passengers": 5107.153247241381,
    "Average_speed_busses": 0.028708333333333332,
    "Average_experienced_crowding": 307.19624404732804,
    "Average_covariation_headway": 1.411363286713609,
    "Average_covariation_headway_stops": {
        "G": 1.2470069471048757,
        "I": 1.2555568205204095,
        "D": 1.2390101389780694,
        "H": 1.2467258377561983,
        "A": 1.2833086487754692,
        "J": 1.2962964103583563,
        "B": 1.2221904912619193,
        "E": 1.2302177871399838,
        "K": 1.2884737995457247,
        "F": 1.2290127463877893,
        "C": 1.225064693924945,
        "L": 1.284543723892575
    },
    "Average_excess_waiting_time": {
        "G": 1017.3446035661557,
        "I": 1009.8985303970474,
        "D": 994.2025422758672,
        "H": 1007.1004588290891,
        "A": 1027.2657655019125,
        "J": 1026.0080740091955,
        "B": 1043.4753489508419,
        "E": 994.6689076315411,
        "K": 1028.7117619404517,
        "F": 1005.4374061111287,
        "C": 986.6698585104277,
        "L": 1036.0465683225991
    },
    "Holding_per_stop": {
        "I": 74.7457627118644,
        "D": 78.21428571428571,
        "H": 76.03448275862068,
        "A": 73,
        "J": 74.23728813559322,
        "G": 77.36842105263158,
        "B": 70.9090909090909,
        "E": 68.57142857142857,
        "K": 72.20338983050847,
        "F": 75.53571428571429,
        "C": 72,
        "L": 74.23728813559322
    },
    "Actions_tfl_edges": {
        "CD": [
            12,
            35,
            9
        ],
        "FG": [
            9,
            40,
            8
        ],
        "GH": [
            10,
            40,
            8
        ],
        "HI": [
            11,
            39,
            9
        ],
        "IJ": [
            11,
            39,
            10
        ],
        "LA": [
            14,
            38,
            8
        ],
        "DE": [
            11,
            36,
            9
        ],
        "AB": [
            11,
            41,
            8
        ],
        "JK": [
            10,
            38,
            11
        ],
        "BC": [
            9,
            34,
            12
        ],
        "EF": [
            12,
            35,
            9
        ],
        "KL": [
            14,
            37,
            8
        ]
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "G": 4.1,
            "H": 4.5,
            "I": 4.9,
            "J": 4.9,
            "K": 5.5,
            "L": 6.1,
            "A": 6.1,
            "B": 6,
            "C": 6,
            "D": 5.777777777777778,
            "E": 5.666666666666667,
            "F": 4.777777777777778
        },
        "bus3": {
            "I": 33.5,
            "J": 33.4,
            "K": 33.7,
            "L": 33.7,
            "A": 33.2,
            "B": 34.22222222222222,
            "C": 35,
            "D": 36.111111111111114,
            "E": 37.44444444444444,
            "F": 37,
            "G": 37.55555555555556,
            "H": 38.44444444444444
        },
        "bus0": {
            "D": 48.4,
            "E": 49.9,
            "F": 51.3,
            "G": 50.5,
            "H": 51.2,
            "I": 50.5,
            "J": 51.4,
            "K": 49.3,
            "L": 47.4,
            "A": 48.3,
            "B": 53,
            "C": 52.77777777777778
        },
        "bus2": {
            "H": 4.4,
            "I": 4.6,
            "J": 4.5,
            "K": 4.6,
            "L": 4.7,
            "A": 5.2,
            "B": 4.555555555555555,
            "C": 4.555555555555555,
            "D": 4.666666666666667,
            "E": 4.555555555555555,
            "F": 4,
            "G": 4.555555555555555
        },
        "bus5": {
            "A": 264.2,
            "B": 269.3,
            "C": 274.3,
            "D": 277.3,
            "E": 279.8,
            "F": 281.7,
            "G": 284.1,
            "H": 285.8,
            "I": 293.7,
            "J": 290,
            "K": 289,
            "L": 294
        },
        "bus4": {
            "J": 344.4,
            "K": 347.8,
            "L": 346.1,
            "A": 357.8,
            "B": 366.5,
            "C": 356,
            "D": 360.6666666666667,
            "E": 367.3333333333333,
            "F": 378.55555555555554,
            "G": 377.3333333333333,
            "H": 374.6666666666667,
            "I": 383.1111111111111
        }
    }
}