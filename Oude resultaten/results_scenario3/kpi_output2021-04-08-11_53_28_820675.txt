{
    "Average_holding_time": 17.294117647058822,
    "Average_travel_time_passengers": 2733.6152167502264,
    "Average_speed_busses": 0.04604166666666667,
    "Average_experienced_crowding": 81.89836281412629,
    "Average_covariation_headway": 0.3105909180716208,
    "Average_covariation_headway_stops": {
        "F": 0.22863531499556158,
        "I": 0.24610411234413346,
        "B": 0.23131055061588832,
        "C": 0.24088947812009204,
        "J": 0.24286574807493608,
        "K": 0.2646054311975868,
        "G": 0.2181154928644958,
        "D": 0.22070269443982612,
        "L": 0.2400618012573927,
        "A": 0.22308915768484042,
        "E": 0.2166375713326653,
        "H": 0.22874793785115535
    },
    "Average_excess_waiting_time": {
        "F": 64.07535634672348,
        "I": 68.71985397856633,
        "B": 64.5947183947427,
        "C": 64.16997509172398,
        "J": 66.29280718142837,
        "K": 67.82699494714257,
        "G": 64.25476100457115,
        "D": 63.13032476325054,
        "L": 65.47887722669674,
        "A": 64.9690385766242,
        "E": 64.25888790728288,
        "H": 67.61378953051235
    },
    "Holding_per_stop": {
        "F": 19.891304347826086,
        "C": 20,
        "J": 18.58695652173913,
        "K": 18.387096774193548,
        "I": 20.869565217391305,
        "B": 13.695652173913043,
        "L": 22.17391304347826,
        "D": 11.73913043478261,
        "G": 20.869565217391305,
        "E": 17.282608695652176,
        "A": 7.826086956521739,
        "H": 16.153846153846153
    },
    "Actions_tfl_edges": {
        "AB": [
            0,
            93,
            0
        ],
        "BC": [
            0,
            93,
            0
        ],
        "EF": [
            0,
            93,
            0
        ],
        "HI": [
            0,
            92,
            0
        ],
        "IJ": [
            0,
            93,
            0
        ],
        "JK": [
            0,
            93,
            0
        ],
        "FG": [
            0,
            92,
            0
        ],
        "CD": [
            0,
            93,
            0
        ],
        "KL": [
            0,
            93,
            0
        ],
        "LA": [
            0,
            92,
            0
        ],
        "DE": [
            0,
            92,
            0
        ],
        "GH": [
            0,
            92,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus2": {
            "F": 76.6875,
            "G": 78.3125,
            "H": 80.4375,
            "I": 80.875,
            "J": 81.26666666666667,
            "K": 82.66666666666667,
            "L": 83,
            "A": 80.86666666666666,
            "B": 81.4,
            "C": 81.6,
            "D": 81.66666666666667,
            "E": 82.93333333333334
        },
        "bus3": {
            "I": 67.0625,
            "J": 67.6875,
            "K": 68.25,
            "L": 66.53333333333333,
            "A": 66.4,
            "B": 67.4,
            "C": 69.8,
            "D": 69.66666666666667,
            "E": 68.53333333333333,
            "F": 68.86666666666666,
            "G": 70.06666666666666,
            "H": 71.26666666666667
        },
        "bus0": {
            "B": 79.25,
            "C": 81,
            "D": 81.5625,
            "E": 83.9375,
            "F": 85.26666666666667,
            "G": 85,
            "H": 86.06666666666666,
            "I": 85.46666666666667,
            "J": 85,
            "K": 85.46666666666667,
            "L": 84,
            "A": 84.13333333333334
        },
        "bus1": {
            "C": 76.625,
            "D": 79.1875,
            "E": 77.875,
            "F": 79.25,
            "G": 79.5,
            "H": 79.2,
            "I": 80.06666666666666,
            "J": 80.4,
            "K": 83.6,
            "L": 82.46666666666667,
            "A": 83.06666666666666,
            "B": 82.33333333333333
        },
        "bus4": {
            "J": 63.6875,
            "K": 64.25,
            "L": 64.8125,
            "A": 65.8125,
            "B": 69.13333333333334,
            "C": 69.53333333333333,
            "D": 68.46666666666667,
            "E": 69.26666666666667,
            "F": 70.06666666666666,
            "G": 68.06666666666666,
            "H": 65.8,
            "I": 67
        },
        "bus5": {
            "K": 89.25,
            "L": 89.875,
            "A": 91.625,
            "B": 92.5625,
            "C": 93,
            "D": 96.2,
            "E": 96.13333333333334,
            "F": 94.33333333333333,
            "G": 92.66666666666667,
            "H": 93,
            "I": 94,
            "J": 95.13333333333334
        }
    }
}