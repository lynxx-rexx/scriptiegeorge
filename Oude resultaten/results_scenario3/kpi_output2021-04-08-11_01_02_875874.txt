{
    "Average_holding_time": 15.357458075904677,
    "Average_travel_time_passengers": 2713.0748635630243,
    "Average_speed_busses": 0.04720833333333333,
    "Average_experienced_crowding": 83.68224014741038,
    "Average_covariation_headway": 0.37225710135982026,
    "Average_covariation_headway_stops": {
        "I": 0.29459798466225495,
        "A": 0.2659247323502201,
        "E": 0.2965228893890109,
        "K": 0.2869358963223253,
        "H": 0.2982957690013621,
        "F": 0.2904377008377322,
        "J": 0.27962817381489985,
        "L": 0.2557470895795945,
        "G": 0.2669915740535909,
        "B": 0.26219671634522207,
        "C": 0.2608996764094476,
        "D": 0.2674773998428781
    },
    "Average_excess_waiting_time": {
        "I": 64.50195323384673,
        "A": 59.349593881340525,
        "E": 69.9090840191185,
        "K": 62.8815687057122,
        "H": 66.8900975562658,
        "F": 65.55999845022143,
        "J": 64.00818409416769,
        "L": 59.73917605849152,
        "G": 63.23240773467478,
        "B": 60.57063859015892,
        "C": 62.65977835605969,
        "D": 65.5761803562105
    },
    "Holding_per_stop": {
        "A": 7.578947368421052,
        "I": 13.578947368421053,
        "K": 17.05263157894737,
        "F": 16.595744680851062,
        "H": 19.894736842105264,
        "E": 18.19148936170213,
        "J": 15.957446808510639,
        "B": 12,
        "L": 17.36842105263158,
        "G": 14.042553191489361,
        "C": 15.957446808510639,
        "D": 16.129032258064516
    },
    "Actions_tfl_edges": {
        "DE": [
            13,
            43,
            38
        ],
        "EF": [
            18,
            42,
            35
        ],
        "GH": [
            13,
            43,
            39
        ],
        "HI": [
            13,
            47,
            36
        ],
        "JK": [
            12,
            48,
            35
        ],
        "LA": [
            14,
            44,
            37
        ],
        "AB": [
            15,
            49,
            31
        ],
        "IJ": [
            13,
            43,
            39
        ],
        "KL": [
            12,
            48,
            35
        ],
        "FG": [
            13,
            40,
            41
        ],
        "BC": [
            15,
            48,
            32
        ],
        "CD": [
            14,
            40,
            40
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "I": 81.9375,
            "J": 83.3125,
            "K": 85.5,
            "L": 86.1875,
            "A": 87.1875,
            "B": 88.1875,
            "C": 89.375,
            "D": 88.25,
            "E": 88.1875,
            "F": 87.86666666666666,
            "G": 86.33333333333333,
            "H": 86.66666666666667
        },
        "bus5": {
            "A": 79.1875,
            "B": 78,
            "C": 79.125,
            "D": 81.125,
            "E": 81.9375,
            "F": 83.25,
            "G": 85.8125,
            "H": 86.125,
            "I": 86.8125,
            "J": 85.5625,
            "K": 85.33333333333333,
            "L": 83.66666666666667
        },
        "bus0": {
            "E": 73.0625,
            "F": 73.5625,
            "G": 74,
            "H": 74.5,
            "I": 74.375,
            "J": 75.9375,
            "K": 76.3125,
            "L": 75.125,
            "A": 75.8,
            "B": 77.46666666666667,
            "C": 77.8,
            "D": 79
        },
        "bus4": {
            "K": 74.9375,
            "L": 78.25,
            "A": 77.9375,
            "B": 77.875,
            "C": 76.875,
            "D": 77.4375,
            "E": 76.6875,
            "F": 77.4375,
            "G": 77.5625,
            "H": 78.6875,
            "I": 76.53333333333333,
            "J": 76.8
        },
        "bus2": {
            "H": 66.4375,
            "I": 67.375,
            "J": 68.375,
            "K": 68.5,
            "L": 68.5,
            "A": 69.25,
            "B": 69.75,
            "C": 70.875,
            "D": 70.5,
            "E": 71,
            "F": 69.8,
            "G": 70
        },
        "bus1": {
            "F": 78.6875,
            "G": 81.3125,
            "H": 82.8125,
            "I": 84.875,
            "J": 85.875,
            "K": 85.25,
            "L": 83.3125,
            "A": 84.125,
            "B": 81.75,
            "C": 81.33333333333333,
            "D": 82.46666666666667,
            "E": 82
        }
    }
}