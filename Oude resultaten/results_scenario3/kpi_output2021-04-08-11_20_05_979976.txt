{
    "Average_holding_time": 22.875341219290263,
    "Average_travel_time_passengers": 2779.9966345429125,
    "Average_speed_busses": 0.04579166666666667,
    "Average_experienced_crowding": 83.74675994676099,
    "Average_covariation_headway": 0.2553149785298473,
    "Average_covariation_headway_stops": {
        "F": 0.19599803823986717,
        "D": 0.18415558967848092,
        "K": 0.19920576516392707,
        "A": 0.20171158223318222,
        "H": 0.19619621880806173,
        "L": 0.1939889221067299,
        "G": 0.18943310211761094,
        "B": 0.16538182647014665,
        "E": 0.18377982037915433,
        "I": 0.17723894880487168,
        "C": 0.1883832121205081,
        "J": 0.18679552270906044
    },
    "Average_excess_waiting_time": {
        "F": 60.30267356804217,
        "D": 59.3172979267751,
        "K": 62.559503543427525,
        "A": 59.22026735141873,
        "H": 60.403437408651826,
        "L": 60.43380669137997,
        "G": 61.292797962235454,
        "B": 57.96469415036103,
        "E": 60.56712960003921,
        "I": 59.582154078280155,
        "C": 61.39135314114691,
        "J": 62.64562526347214
    },
    "Holding_per_stop": {
        "A": 15,
        "F": 20.869565217391305,
        "H": 31.956521739130434,
        "D": 19.565217391304348,
        "K": 23.47826086956522,
        "L": 25.434782608695652,
        "B": 17.608695652173914,
        "G": 29.01098901098901,
        "I": 22.417582417582416,
        "E": 21.75824175824176,
        "J": 22.417582417582416,
        "C": 25.054945054945055
    },
    "Actions_tfl_edges": {
        "CD": [
            6,
            38,
            48
        ],
        "EF": [
            12,
            37,
            43
        ],
        "GH": [
            6,
            36,
            50
        ],
        "JK": [
            9,
            41,
            42
        ],
        "KL": [
            11,
            39,
            42
        ],
        "LA": [
            11,
            45,
            37
        ],
        "AB": [
            16,
            31,
            45
        ],
        "FG": [
            10,
            33,
            49
        ],
        "HI": [
            9,
            37,
            46
        ],
        "DE": [
            11,
            41,
            40
        ],
        "BC": [
            15,
            29,
            48
        ],
        "IJ": [
            9,
            41,
            41
        ]
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "F": 78,
            "G": 78.5,
            "H": 80.5,
            "I": 80,
            "J": 78.86666666666666,
            "K": 79.6,
            "L": 81.86666666666666,
            "A": 82.4,
            "B": 82.46666666666667,
            "C": 83.66666666666667,
            "D": 82.26666666666667,
            "E": 82.6
        },
        "bus0": {
            "D": 80.875,
            "E": 81.875,
            "F": 83.25,
            "G": 84.8125,
            "H": 85.86666666666666,
            "I": 87.66666666666667,
            "J": 87.8,
            "K": 88.06666666666666,
            "L": 87.73333333333333,
            "A": 88.46666666666667,
            "B": 89.66666666666667,
            "C": 88.6
        },
        "bus3": {
            "K": 80.4375,
            "L": 81.5,
            "A": 81.125,
            "B": 82.53333333333333,
            "C": 83.66666666666667,
            "D": 84.86666666666666,
            "E": 85.66666666666667,
            "F": 85.2,
            "G": 85.2,
            "H": 87.26666666666667,
            "I": 86.53333333333333,
            "J": 85.93333333333334
        },
        "bus5": {
            "A": 87.4375,
            "B": 86.375,
            "C": 86.25,
            "D": 86.0625,
            "E": 85.6875,
            "F": 87.13333333333334,
            "G": 89.46666666666667,
            "H": 91.46666666666667,
            "I": 92.6,
            "J": 93.93333333333334,
            "K": 94.13333333333334,
            "L": 93.46666666666667
        },
        "bus2": {
            "H": 73,
            "I": 74.75,
            "J": 78.125,
            "K": 78.375,
            "L": 78,
            "A": 77.86666666666666,
            "B": 78.6,
            "C": 77.66666666666667,
            "D": 79.06666666666666,
            "E": 78.46666666666667,
            "F": 79.06666666666666,
            "G": 78.13333333333334
        },
        "bus4": {
            "L": 70.3125,
            "A": 70.5,
            "B": 71.9375,
            "C": 70.6,
            "D": 71.13333333333334,
            "E": 73.13333333333334,
            "F": 72.6,
            "G": 71.06666666666666,
            "H": 72.06666666666666,
            "I": 73.06666666666666,
            "J": 73.8,
            "K": 75.4
        }
    }
}