{
    "Average_holding_time": 0,
    "Average_travel_time_passengers": 6170.986226293444,
    "Average_speed_busses": 0.031041666666666665,
    "Average_experienced_crowding": 578.9778708131025,
    "Average_covariation_headway": 2.181494911179598,
    "Average_covariation_headway_stops": {
        "A": 2.097494885197357,
        "J": 2.11054987445659,
        "C": 2.0986738591467176,
        "I": 2.112185960984151,
        "D": 2.1016232075204995,
        "H": 2.1022195873249223,
        "B": 2.0924919198626273,
        "E": 2.1079200792010195,
        "K": 2.085074946683502,
        "F": 2.089905487529876,
        "L": 2.079862945848328,
        "G": 2.074771923616902
    },
    "Average_excess_waiting_time": {
        "A": 2125.5446293948476,
        "J": 2113.248139007806,
        "C": 2141.145285740284,
        "I": 2205.6657832936858,
        "D": 2133.0024918131767,
        "H": 2200.248435716128,
        "B": 2141.254132118525,
        "E": 2169.745131167471,
        "K": 2092.928434577431,
        "F": 2163.25210879528,
        "L": 2107.594573059182,
        "G": 2160.8729846785736
    },
    "Holding_per_stop": {
        "A": 0,
        "I": 0,
        "J": 0,
        "C": 0,
        "D": 0,
        "H": 0,
        "B": 0,
        "E": 0,
        "K": 0,
        "F": 0,
        "L": 0,
        "G": 0
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "A": 7.090909090909091,
            "B": 7,
            "C": 6.7272727272727275,
            "D": 7.909090909090909,
            "E": 8.363636363636363,
            "F": 8.818181818181818,
            "G": 9.181818181818182,
            "H": 7.7272727272727275,
            "I": 7.545454545454546,
            "J": 7.9,
            "K": 7.6,
            "L": 7.3
        },
        "bus4": {
            "J": 517.2,
            "K": 529.4,
            "L": 540.2,
            "A": 545.3,
            "B": 551.4,
            "C": 557.3,
            "D": 567.2,
            "E": 571.7,
            "F": 582.5,
            "G": 597.1,
            "H": 601.4,
            "I": 604.1
        },
        "bus0": {
            "C": 5.7272727272727275,
            "D": 5,
            "E": 5,
            "F": 5,
            "G": 4.818181818181818,
            "H": 4.818181818181818,
            "I": 4.7272727272727275,
            "J": 5.6,
            "K": 5.8,
            "L": 6.1,
            "A": 6.3,
            "B": 5.9
        },
        "bus3": {
            "I": 31.454545454545453,
            "J": 29.6,
            "K": 30.9,
            "L": 30.3,
            "A": 31,
            "B": 31,
            "C": 29.4,
            "D": 32.1,
            "E": 32.1,
            "F": 33.7,
            "G": 34.2,
            "H": 34
        },
        "bus1": {
            "D": 56,
            "E": 57,
            "F": 57.90909090909091,
            "G": 59.81818181818182,
            "H": 62.27272727272727,
            "I": 59.90909090909091,
            "J": 65.2,
            "K": 66.5,
            "L": 65.7,
            "A": 64.1,
            "B": 62.4,
            "C": 62.4
        },
        "bus2": {
            "H": 13.090909090909092,
            "I": 12.727272727272727,
            "J": 14,
            "K": 14.4,
            "L": 14,
            "A": 14.3,
            "B": 15.1,
            "C": 16.3,
            "D": 15.1,
            "E": 14.9,
            "F": 15.1,
            "G": 15.1
        }
    }
}