{
    "Average_holding_time": 0,
    "Average_travel_time_passengers": 5808.628680935491,
    "Average_speed_busses": 0.0325,
    "Average_experienced_crowding": 538.1185771996036,
    "Average_covariation_headway": 2.104982398092214,
    "Average_covariation_headway_stops": {
        "B": 2.0634910415459116,
        "K": 2.051637820566355,
        "I": 2.051569076421195,
        "G": 2.0559423647857327,
        "A": 2.04720624694212,
        "F": 2.050607668562857,
        "L": 2.046664690095375,
        "C": 2.0545827998573554,
        "J": 2.037937598816231,
        "H": 2.0273888740967685,
        "D": 2.048171724092015,
        "E": 2.0368268530546554
    },
    "Average_excess_waiting_time": {
        "B": 1971.3569171906818,
        "K": 1955.1633617436607,
        "I": 1943.5919030202092,
        "G": 1999.7040233412633,
        "A": 1954.881440708624,
        "F": 2002.8221307739798,
        "L": 1968.252694375488,
        "C": 1979.031269710935,
        "J": 1943.9001710039545,
        "H": 1912.4545417236848,
        "D": 1988.2651095109861,
        "E": 1990.051737351069
    },
    "Holding_per_stop": {
        "B": 0,
        "K": 0,
        "A": 0,
        "G": 0,
        "I": 0,
        "F": 0,
        "L": 0,
        "C": 0,
        "J": 0,
        "H": 0,
        "D": 0,
        "E": 0
    },
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 500.09090909090907,
            "C": 501.3636363636364,
            "D": 507,
            "E": 516.8181818181819,
            "F": 524,
            "G": 527.4545454545455,
            "H": 508.9,
            "I": 515.1,
            "J": 527.5,
            "K": 535.6,
            "L": 536.7,
            "A": 543.1
        },
        "bus4": {
            "K": 14.909090909090908,
            "L": 14.272727272727273,
            "A": 13.454545454545455,
            "B": 14.727272727272727,
            "C": 16.09090909090909,
            "D": 16.818181818181817,
            "E": 16.818181818181817,
            "F": 16.454545454545453,
            "G": 15.818181818181818,
            "H": 17.4,
            "I": 17.3,
            "J": 16.3
        },
        "bus3": {
            "I": 9.727272727272727,
            "J": 10.363636363636363,
            "K": 11.545454545454545,
            "L": 12.636363636363637,
            "A": 12.909090909090908,
            "B": 12.454545454545455,
            "C": 12.818181818181818,
            "D": 11.272727272727273,
            "E": 10.363636363636363,
            "F": 10.272727272727273,
            "G": 9.636363636363637,
            "H": 9.7
        },
        "bus2": {
            "G": 75.25,
            "H": 78.72727272727273,
            "I": 77.36363636363636,
            "J": 76.27272727272727,
            "K": 77.72727272727273,
            "L": 77.81818181818181,
            "A": 78.9090909090909,
            "B": 79.45454545454545,
            "C": 81.54545454545455,
            "D": 83.27272727272727,
            "E": 82.9090909090909,
            "F": 81.45454545454545
        },
        "bus5": {
            "A": 23.181818181818183,
            "B": 24,
            "C": 26.818181818181817,
            "D": 26.272727272727273,
            "E": 26.727272727272727,
            "F": 26.818181818181817,
            "G": 27.545454545454547,
            "H": 27.2,
            "I": 27.4,
            "J": 26.6,
            "K": 25.2,
            "L": 25.4
        },
        "bus1": {
            "F": 6.75,
            "G": 6.916666666666667,
            "H": 7.454545454545454,
            "I": 8.090909090909092,
            "J": 8.363636363636363,
            "K": 8.818181818181818,
            "L": 8,
            "A": 7,
            "B": 6.2727272727272725,
            "C": 6.2727272727272725,
            "D": 6.2727272727272725,
            "E": 6.181818181818182
        }
    }
}