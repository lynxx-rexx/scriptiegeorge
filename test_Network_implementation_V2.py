import pytest
from Network_implementation_V2 import NetworkEnv
import json
import gym
import numpy as np
import random


# class TestNetworkEnv:
#     def setup(self):
#         node_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']
#
#         adjacency_matrix = [
#             [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
#             [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#             [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#             [0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#             [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#             [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
#             [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0],
#             [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
#             [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
#             [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
#             [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
#             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
#             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
#             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
#             [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
#         ]
#
#         kwargs_dict = {
#             'edges': adjacency_matrix,
#             'node_names': node_list,
#             'num_agents': 3,
#             'routes': [node_list, node_list, node_list],
#             'resetted': False,
#             'qmix': False,
#             'holding_fixed': False,
#             'incl_pass_stops': False
#
#         }
#
#         self.network = NetworkEnv(**kwargs_dict)
#
#     def test_obs_action_rew_returns(self):
#         obs = {}
#         done = False
#         while not done:
#             action = {}
#             if obs:
#                 assert rew
#                 for bus in obs:
#                     action[bus] = 1
#             obs, rew, done, info = self.network.step(action)
#
#     def test_action_taken(self):
#         ..


@pytest.fixture()
def example_network_object():
    node_list = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
    ]

    adjacency_matrix = [
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]

    # kwargs_dict = {
    #     'edges': adjacency_matrix,
    #     'node_names': node_list,
    #     'num_agents': 3,
    #     'routes': [node_list, node_list, node_list],
    #     'resetted': False,
    #     'qmix': False,
    #     'holding_fixed': False,
    #     'incl_pass_stops': False
    #
    # }

    kwargs_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(2),
    "end_trigger_range": [500, 4000],
    "final_reward_option": "off",
    "qmix": False,
    "holding_fixed": True,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "headway_diff",
    "reward_function_option": "headway_diff",
    "num_agents": 3,
    "routes": [node_list] * 3,
    "capacity_busses": [999] * 3,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 15,
    "traffic_light_on_edges": [False, False, False, False, False, False, False, True, True, True, False, False, False, False, False ],
    "cycle_props": [None, None, None, None, None, None, None, (90, 65), (90, 65), (90, 65), None, None, None, None, None],
    "arrival_function_nodes": ["poisson"] * 15,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0],
        [0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
        [0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833]*15,
    "boarding_rate": 5,
    "alighting_rate": 5,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("gamma", 150, 15, 80, 220),
        "BC": ("gamma", 150, 15, 80, 220),
        "CD": ("gamma", 150, 15, 80, 220),
        "DE": ("gamma", 150, 15, 80, 220),
        "EF": ("gamma", 150, 15, 80, 220),
        "FG": ("gamma", 150, 15, 80, 220),
        "GH": ("gamma", 150, 15, 80, 220),
        "HI": ("gamma", 150, 15, 80, 220),
        "IJ": ("gamma", 150, 15, 80, 220),
        "JK": ("gamma", 150, 15, 80, 220),
        "KL": ("gamma", 150, 15, 80, 220),
        "LM": ("gamma", 150, 15, 80, 220),
        "MN": ("gamma", 150, 15, 80, 220),
        "NO": ("gamma", 150, 15, 80, 220),
        "OA": ("gamma", 150, 15, 80, 220),
    },
}

    return NetworkEnv(**kwargs_dict)


@pytest.mark.init
def test_num_of_busses(example_network_object):
    assert len(example_network_object.busses) == 0


@pytest.mark.init
def test_bus_position(example_network_object):
    example_network_object.run_simulation()
    assert (
        example_network_object.busses[1].position
        in example_network_object.position_list
    )


@pytest.mark.simulation
def test_edge_node_rel_position(example_network_object):
    # example_network_object.run_simulation()
    example_network_object.step({})


@pytest.mark.observation
def test_local_obs_equal_to_obs_space(example_network_object):
    obs_space_busses = example_network_object.observation_space_busses

    example_network_object.run_simulation()
    obs = example_network_object.generate_local_observations()
    assert obs_space_busses.shape[0] == len(next(iter(obs.values())))



@pytest.mark.observation
def test_time_aware_range(example_network_object):
    obs = {}
    done = {"__all__": False}
    counter = 0
    while not done["__all__"]:
        action = {}
        if obs:
            for bus in obs:
                action[bus] = 0
        obs, rew, done, info = example_network_object.step(action)
        for key, value in obs.items():
            if key < example_network_object.num_agents:
                assert value[-1] >= -1, "The time awareness observation was too small."
                assert value[-1] <= 1, "The time awareness observation was too large."
                assert value[0] >= 0, "The backward headway was negative."
                assert value[1] >= 0, "The forward headway was negative."
                assert value[0] <= len(
                    example_network_object.position_list_obs
                ), "The backward headway was larger than possible."
                assert value[1] <= len(
                    example_network_object.position_list_obs
                ), "The forward headway was larger than possible."

        if counter == 0:
            for key, value in obs.items():
                if key < example_network_object.num_agents:
                    assert (
                        value[2] <= -0.95
                    ), "The time awareness observation may have started to high, or the first action was very late. \
                                                Please make sure that the latter is the case, otherwise something is wrong."
        if done["__all__"]:
            for key, value in obs.items():
                if key < example_network_object.num_agents:
                    assert value[2] == 1, "The time awareness observation did not end at 1."

        counter += 1



@pytest.mark.rllib_communication
def test_obs_action_rew_returns(example_network_object):
    obs = {}
    done = {"__all__": False}
    while not done["__all__"]:
        action = {}
        if obs:
            assert rew, "The reward was not sent simultaneously with the observation."

            for bus_or_tfl in obs:
                if bus_or_tfl < example_network_object.num_agents:
                    action[bus_or_tfl] = random.choice([1,2,3,4,5,6])
                else:
                    action[bus_or_tfl] = 0
        obs, rew, done, info = example_network_object.step(action)
    assert done["__all__"]


@pytest.mark.action_implementation
def test_all_actions_taken(example_network_object):
    obs = {}
    done = {"__all__": False}
    while not done["__all__"]:
        action = {}
        if obs:
            for bus in obs:
                action[bus] = 1
        obs, rew, done, info = example_network_object.step(action)
        for bus in example_network_object.busses:
            assert (
                bus.hold is None
            ), "Bus {} received an action last step, but did not get to take it.".format(
                bus.agent_number
            )
        for edge in example_network_object.edge_list:
            if edge.contains_traffic_light:
                assert (edge.traffic_light_action is None), "Traffic light {} received an action last step, but did not get to take it.".format(edge.name)


@pytest.mark.trip_logging
def test_trip_times_and_logging(example_network_object):
    log_file_str = example_network_object.log_file_trips
    obs = {}
    done = {"__all__": False}
    while not done["__all__"]:
        action = {}
        if obs:
            for bus in obs:
                action[bus] = 1
        obs, rew, done, info = example_network_object.step(action)

    with open(log_file_str, "r") as reading_file:
        trip_data = json.load(reading_file)
        counter = 0
        id_list = []
        for key, value in trip_data.items():
            id_list.append(key)
            if counter < 100:
                assert value["start_node"] != value["destination"]
                assert value["start_time"] < value["arrival_time"]
                assert (
                    value["waiting_time"] + value["travel_time"]
                    >= value["total_system_time"] - 0.001
                )
                assert (
                    value["waiting_time"] + value["travel_time"]
                    <= value["total_system_time"] + 0.001
                )
                assert (
                    value["start_time"] + value["waiting_time"] + value["travel_time"]
                    >= value["arrival_time"] - 0.001
                )
                assert (
                    value["start_time"] + value["waiting_time"] + value["travel_time"]
                    <= value["arrival_time"] + 0.001
                )

                counter += 1

        assert len(id_list) == len(set(id_list))
