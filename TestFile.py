# import pandas as pd
import numpy as np
import simpy
import random

# import statistics
# from functools import wraps, partial
import matplotlib.pyplot as plt
import itertools
import gym

# import ray
# from ray import rllib
# from ray import tune
import json
import gym
import pickle

import mlflow

from _collections import defaultdict

import copy
import seaborn as sns

import scipy.stats as stats
import math
import statistics

import pandas as pd




#
# # DIT IS OM EEN GOEDE DESTINATION DISTRIBUTION TE KRIJGEN!!!!!
# lijst = []
#
# for i in range (12):
#     lijst.append([])
#     counter = 0
#     for j in range(12):
#         diff = (i +10 ) -12
#         if counter > i and counter < (i +11):
#             lijst[i].append(0.1)
#         elif counter <= diff:
#             lijst[i].append(0.1)
#         else:
#             lijst[i].append(0)
#         counter += 1
#
# print(lijst)
#
# print( len( ('a', 5)  ))
# print( type( ('a', 5)  ))
#
# print( len( (5,)  ))
# print( type( (5,) ))
#




# # CHECK VERDELING
#
# import statistics
#
#
# mean_log = 4
#
# std_original = 0.18
#
# mean_original = np.log(mean_log)-1/2*std_original**2
#
# xs = np.random.lognormal(mean_original, std_original, 1000000)
#
# xs = [min(x, 8) for x in xs]
# xs = [max(x, 1.5) for x in xs]
#
#
# def get_lognormal_time_fixed_minimum(mean, stdev, fixed_minimum, fixed_maximum):
#     """
#     Log normal distribution with mean and std and fixed minimum as inputted
#     The minimum cannot be exceeded negatively, and will be subtracted first and added later again from the mean
#     Finally, the maximum can also not be exeeded and the returned values are redrawn if they exceed the fixed_maximum value
#     """
#     mean_log = (mean - fixed_minimum) / 60
#     std_original = stdev / 60
#     mean_original = np.log(mean_log) - 1 / 2 * std_original ** 2
#
#     delay_distrib = np.random.lognormal(mean_original, std_original)
#     delay_distrib = delay_distrib * 60
#     delay_distrib += fixed_minimum
#
#     while delay_distrib > fixed_maximum:
#         delay_distrib = np.random.lognormal(mean_original, std_original)
#         delay_distrib = delay_distrib * 60
#         delay_distrib += fixed_minimum
#
#     return delay_distrib
#
#
# xs = []
# i = 0
# while i < 1000000:
#     value = get_lognormal_time_fixed_minimum(150, 20, 75, 300)
#     # while value > 480:
#     #     value = get_lognormal_time_fixed_minimum(240, 20, 120, 480)
#     xs.append(value)
#     i += 1
#
# plt.hist(xs, bins=100, density=True)
# plt.show()



# # CHECK HEADWAY NORMALISING CONSTANT
#
# print((41/75)*20)
#
#
# getal = 0
#
# for i in range(40):
#     getal += (i+1)
#
# print(getal/75)
#
#
# test_lijst = []
#
# for i in range(75):
#     remainder = i % 75
#     diff = remainder - 40
#
#     if diff > 0:
#         delay = 0
#     else:
#         delay = -diff
#
#     test_lijst.append(delay)
#
# print(statistics.mean(test_lijst))




# # # PLOT VAN LEARNING CURVE
# plot_training = False
# plt.rcParams.update({'font.size': 18})
#
# if plot_training:
#     data = pd.read_csv('progress_scenario_2_example.csv')
#
#     # print(data['episode_reward_mean'])
#
#     data['episode_reward_mean'].plot()
#     plt.title('Training reward mean')
#     plt.xlabel('Training iteration')
#     plt.ylabel('Episode reward mean')
#     plt.show()




# example data
x = np.array([0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0])
y = np.exp(-x)
# xerr = 0.1
yerr = 0.2


fig, ax = plt.subplots(figsize=(7, 4))



# Plot a series with lower and upper limits in y
yerr = np.array([[0.2, 0.5, 0.6, 0.7, 0.8, 0.2, 0.9 ,0.8, 0.2, 0.2],
                 [0.5, 0.5, 0.6, 0.7, 0.8, 0.2, 0.9 ,0.8, 0.2, 0.2]])


# do the plotting
ax.errorbar(x, y, yerr=yerr,
            marker='o', markersize=8,
            linestyle='none')

# tidy up the figure
ax.set_xlim((0, 5.5))
ax.set_title('Errorbar upper and lower limits')
plt.show()




lijst1 = [5, 4, 3]
lijst2 = [3, 4, 5]
lijst3 = [3, 4, 5]
diff = []

lijst4 = zip(lijst1, lijst2, lijst3)
for i, j, k in lijst4:
    diff.append(i-j+2*k)

print(diff)