from functools import partial, wraps
import simpy
import random
import matplotlib.pyplot as plt

def patch_resource(resource, pre=None, post=None):
    """Patch *resource* so that it calls the callable *pre* before each
    put/get/request/release operation and the callable *post* after each
    operation.  The only argument to these functions is the resource
    instance.
    """
    def get_wrapper(func):
        # Generate a wrapper for put/get/request/release
        @wraps(func)
        def wrapper(*args, **kwargs):
            # This is the actual wrapper
            # Call "pre" callback
            if pre:
                pre(resource)
            # Perform actual operation
            ret = func(*args, **kwargs)
            # Call "post" callback
            if post:
                post(resource)
            return ret
        return wrapper
    # Replace the original operations with our wrapper
    for name in ['put', 'get', 'request', 'release']:
        if hasattr(resource, name):
            setattr(resource, name, get_wrapper(getattr(resource, name)))

def monitor(data, resource):
    """This is our monitoring callback."""
    item = (
        resource._env.now,  # The current simulation time
        resource.count,  # The number of users
        len(resource.queue),  # The number of queued processes
    )
    data.append(item)



def test_process(env, res):
    with res.request() as req:
        yield req
        yield env.timeout(random.randint(1,9))

#
# EIGEN UITLEG
#
# de patch resource functie zorgt ervoor dat alle methoden van een resource die verandering brengen in de patch_resource
# , dus put, get, request en release, dat die gewrapt worden. Je moet zelf dan nog opgeven met welke functie ze gewrapt moeten worden
# en of ze zowel voor het gebruik als na het gebruik gewrapt moeten worden. De monitor functie die je opgeeft moet maar 1 agrument nodig hebben
# --> de resource. Als hij meer argumenten nodig heeft kun je met partial ervoor zorgen dat hij nog maar 1 argument nodig heeft.




env = simpy.Environment()
res = simpy.Resource(env, capacity=1)
data = []
# Bind *data* as first argument to monitor()
# see https://docs.python.org/3/library/functools.html#functools.partial
monitor = partial(monitor, data)
patch_resource(res, post=monitor)  # Patches (only) this resource instance

for i in range(20):
    env.process(test_process(env, res))
env.run(until=200)

print(data[:][0])

queue_list = [i[2] for i in data]
x_list = [i[0] for i in data]


plt.plot(x_list, queue_list)
plt.show()

