import gym
import numpy as np
import mlflow
import statistics

from Network_implementation_V2 import NetworkEnv

# CHECKLIST
# is the result folder set correctly?
# is the scenario set correctly?
# is the observation option set to "benchmark"?
# is the headway normalising constant calculated correctly?

node_list = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
]

adjacency_matrix = [
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
]


obs_type = "diff"


kwargs_dict = {
    "test_run": False,
    "monitoring": True,
    "run_logging": False,
    "passenger_logging": False,
    "kpi_logging": True,
    "result_folder": "results_holding_scenario5\\",
    "observation_space_busses": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    "observation_space_tfl": gym.spaces.Box(
        low=np.array([-45]), high=np.array([45]), shape=(1,),
    ),
    # "action_space_busses": gym.spaces.Box(low=0, high=240, shape=(1,)),#gym.spaces.Discrete(6),
    "action_space_busses": gym.spaces.Discrete(6),
    "action_space_tfl": gym.spaces.Discrete(3),
    "end_trigger_range": [4000, 4001],
    "final_reward_option": "travel_time_only",
    "qmix": False,
    "holding_fixed": True,
    # "holding_fixed": False,
    "holding_times": {"0": 0, "1": 30, "2": 60, "3": 90, "4": 120, "5": 150, "6": 180},
    "ta": False,
    "observation_option": "simple_headway_diff",
    "reward_function_option": "headway_diff",
    "holding_punish_param": 0,
    "num_agents": 6,
    "routes": [node_list] * 6,
    "capacity_busses": [9999] * 6,
    "node_names": node_list,
    "slot_capacity_nodes": [1] * 12,
    "traffic_light_on_edges": [
        False,
        False,
        True,
        False,
        False,
        True,
        True,
        True,
        False,
        True,
        False,
        False,
    ],  # False, False, False ],
    "cycle_props": [
        None,
        None,
        (75, 40),
        None,
        None,
        (75, 40),
        (75, 40),
        (75, 40),
        None,
        (75, 40),
        None,
        None,
    ],  # None, None, None],# (75, 40)
    "arrival_function_nodes": ["poisson"] * 12,
    # "destination_distribution_nodes": ["random"]*15,
    "destination_distribution_nodes": [
        [0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0],
        [0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1],
        [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0],
    ],
    "pass_arrival_rate_nodes": [0.00833, 0.00833, 0.013333, 0.01666, 0.01666, 0.05, 0.06667, 0.03333, 0.01666, 0.0125, 0.01, 0.00833 ],
    "boarding_rate": 0.3333,
    "alighting_rate": 0.556,
    "dwell_time_function": ("max_board_or_alight", "stochastic"),
    "adjacency_matrix": adjacency_matrix,
    "travel_time_edges": {
        "AB": ("lognormal", 360, 20, 180, 720),
        "BC": ("lognormal", 360, 20, 180, 720),
        "CD": ("lognormal", 300, 20, 150, 600),
        "DE": ("lognormal", 240, 20, 120, 480),
        "EF": ("lognormal", 240, 20, 120, 480),
        "FG": ("lognormal", 120, 20, 60, 240),
        "GH": ("lognormal", 90, 20, 45, 180),
        "HI": ("lognormal", 150, 20, 75, 300),
        "IJ": ("lognormal", 240, 20, 120, 480),
        "JK": ("lognormal", 240, 20, 120, 480),
        "KL": ("lognormal", 240, 20, 120, 480),
        "LA": ("lognormal", 300, 20, 150, 600),
    },
    #     "LM": 240,
    #     "MN": 240,
    #     "NO": 240,
    #     "OA": 240,
    # },
    "training_run": False
}







temp_travel_time_list = []

for key, value in kwargs_dict['travel_time_edges'].items():
    if type(value) is tuple:
        temp_travel_time_list.append(value[1])
    else:
        temp_travel_time_list.append(value)

planned_driving_time_between_busses = sum(temp_travel_time_list) / kwargs_dict['num_agents']

average_delay_tfl = 41 / 75 * 20

planned_driving_time_between_busses += (sum(kwargs_dict['traffic_light_on_edges']) * average_delay_tfl) / kwargs_dict['num_agents']

headway_normalising_constant = max(
    2 * statistics.mean(kwargs_dict['pass_arrival_rate_nodes']) / kwargs_dict['boarding_rate'] * planned_driving_time_between_busses,
    2 * 15) + planned_driving_time_between_busses


# FOR HETEROGENEOUS ROUTES
headway_normalising_constant2 = 0
for i in kwargs_dict['pass_arrival_rate_nodes']:
    headway_normalising_constant2 += max(
        i / kwargs_dict['boarding_rate'] * planned_driving_time_between_busses, 15)

headway_normalising_constant2 /= 6
headway_normalising_constant2 += planned_driving_time_between_busses
headway_normalising_constant2 += 10

print(headway_normalising_constant)
print(headway_normalising_constant2)


def execute_control(example_network_object):
    obs = {}
    done = {"__all__": False}
    while not done["__all__"]:
        action = {}
        if obs:
            for bus in obs:
                if bus < 6:
                    forward_headway = obs[bus][0]

                    forward_headway_seconds = forward_headway * headway_normalising_constant

                    #todo check of deze goed staat 1 of 2
                    diff_with_planned_headway = forward_headway_seconds - headway_normalising_constant#2

                    if obs_type == "diff":
                        diff_backward_forward = obs[bus][0]
                        diff_backward_forward_seconds = diff_backward_forward * headway_normalising_constant

                    if obs_type == 'forward':
                        if diff_with_planned_headway > -15:
                            action[bus] = 0
                        elif diff_with_planned_headway > -45:
                            action[bus] = 1
                        elif diff_with_planned_headway > -75:
                            action[bus] = 2
                        elif diff_with_planned_headway > -105:
                            action[bus] = 3
                        elif diff_with_planned_headway > -135:
                            action[bus] = 4
                        elif diff_with_planned_headway > -165:
                            action[bus] = 5
                        else:
                            action[bus] = 6
                    elif obs_type == 'backward':
                        if diff_with_planned_headway < 15:
                            action[bus] = 0
                        elif diff_with_planned_headway < 45:
                            action[bus] = 1
                        elif diff_with_planned_headway < 75:
                            action[bus] = 2
                        elif diff_with_planned_headway < 105:
                            action[bus] = 3
                        elif diff_with_planned_headway < 135:
                            action[bus] = 4
                        elif diff_with_planned_headway < 165:
                            action[bus] = 5
                        else:
                            action[bus] = 6
                    elif obs_type == 'diff':
                        if diff_backward_forward_seconds < 15:
                            action[bus] = 0
                        elif diff_backward_forward_seconds < 45:
                            action[bus] = 1
                        elif diff_backward_forward_seconds < 75:
                            action[bus] = 2
                        elif diff_backward_forward_seconds < 105:
                            action[bus] = 3
                        elif diff_backward_forward_seconds < 135:
                            action[bus] = 4
                        elif diff_backward_forward_seconds < 165:
                            action[bus] = 5
                        else:
                            action[bus] = 6

                elif bus >= 6:
                    action[bus] = 0

        obs, rew, done, info = example_network_object.step(action)


if __name__ =="__main__":
    for i in range(10):
        example_network_object = NetworkEnv(**kwargs_dict)
        execute_control(example_network_object)
