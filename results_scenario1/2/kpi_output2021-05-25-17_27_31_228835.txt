{
    "Average_holding_time": 27.03152364273205,
    "Average_holding_actions": [
        146,
        985,
        0,
        11,
        0,
        0
    ],
    "Average_travel_time_passengers": 2030.5979831866393,
    "Average_passenger_waiting_time": 315.377223898122,
    "Average_passenger_in_vehicle_time": 1715.2207592884367,
    "Average_speed_busses": 0.04758333333333333,
    "Average_experienced_crowding": 58.76554754103698,
    "Average_covariation_headway": 0.17100917253325656,
    "Average_covariation_headway_stops": {
        "C": 0.13725338784264718,
        "G": 0.15367256242916497,
        "I": 0.14150743793018503,
        "J": 0.14044230671772162,
        "K": 0.14534763244819196,
        "L": 0.14617912595425558,
        "D": 0.1147882551968895,
        "A": 0.12282447024947617,
        "H": 0.13171202602011903,
        "B": 0.12207713870826561,
        "E": 0.1159470370811863,
        "F": 0.13115340591092647
    },
    "Average_excess_waiting_time": {
        "C": 52.81555388015869,
        "G": 57.99067346367707,
        "I": 57.01767609275822,
        "J": 54.83681465204302,
        "K": 53.62058863983083,
        "L": 51.95607794799389,
        "D": 52.8693808105387,
        "A": 51.618311841253956,
        "H": 57.64187531439245,
        "B": 53.22746808817527,
        "E": 54.648078096344534,
        "F": 57.660852826613336
    },
    "Holding_per_stop": {
        "C": 25.625,
        "L": 27.5,
        "K": 27.5,
        "J": 27.789473684210527,
        "G": 26.526315789473685,
        "I": 28.105263157894736,
        "A": 27.1875,
        "D": 25.57894736842105,
        "H": 27.127659574468087,
        "B": 28.105263157894736,
        "E": 26.526315789473685,
        "F": 26.80851063829787
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 60.125,
            "D": 60.125,
            "E": 60.625,
            "F": 60.375,
            "G": 61.25,
            "H": 59.1875,
            "I": 61.5625,
            "J": 61.5,
            "K": 61.9375,
            "L": 60.4375,
            "A": 61,
            "B": 63.6
        },
        "bus1": {
            "G": 55.75,
            "H": 55.125,
            "I": 54.4375,
            "J": 53.6875,
            "K": 54.5,
            "L": 55.4375,
            "A": 56.125,
            "B": 56.75,
            "C": 58.9375,
            "D": 58.46666666666667,
            "E": 59.733333333333334,
            "F": 58.2
        },
        "bus2": {
            "I": 54.25,
            "J": 54.4375,
            "K": 55.75,
            "L": 55.375,
            "A": 56.25,
            "B": 56.8125,
            "C": 57.75,
            "D": 55.75,
            "E": 57.4375,
            "F": 57.666666666666664,
            "G": 57.6,
            "H": 57.666666666666664
        },
        "bus3": {
            "J": 57.5625,
            "K": 57.8125,
            "L": 58.8125,
            "A": 57.8125,
            "B": 59,
            "C": 58.375,
            "D": 56.9375,
            "E": 55.9375,
            "F": 58.375,
            "G": 57.3125,
            "H": 58.6,
            "I": 58.666666666666664
        },
        "bus4": {
            "K": 51.0625,
            "L": 50.5,
            "A": 50.0625,
            "B": 50.75,
            "C": 50.8125,
            "D": 50.75,
            "E": 53.125,
            "F": 53.8125,
            "G": 54.1875,
            "H": 57.1875,
            "I": 58.375,
            "J": 54.46666666666667
        },
        "bus5": {
            "L": 55.5625,
            "A": 56.0625,
            "B": 55.5,
            "C": 56.1875,
            "D": 58.125,
            "E": 58.875,
            "F": 59.75,
            "G": 57.5625,
            "H": 59.125,
            "I": 59.75,
            "J": 61,
            "K": 59.125
        }
    },
    "Obs_and_actions_busses": {
        "4": 350.2588494253957,
        "1": 15.503305912645272,
        "0": -151.90688161054365
    },
    "Obs_and_actions_tfls": {}
}