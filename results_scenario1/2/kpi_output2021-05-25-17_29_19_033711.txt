{
    "Average_holding_time": 26.416739319965128,
    "Average_holding_actions": [
        149,
        994,
        0,
        4,
        0,
        0
    ],
    "Average_travel_time_passengers": 2044.9422300166111,
    "Average_passenger_waiting_time": 311.1385489797785,
    "Average_passenger_in_vehicle_time": 1733.8036810367419,
    "Average_speed_busses": 0.04779166666666667,
    "Average_experienced_crowding": 58.87275870317268,
    "Average_covariation_headway": 0.13314452751130623,
    "Average_covariation_headway_stops": {
        "B": 0.09436631126828628,
        "E": 0.10120964926965416,
        "F": 0.10333373463542146,
        "G": 0.11192744763174878,
        "I": 0.10431428220457224,
        "L": 0.099957572665376,
        "J": 0.0780375559461187,
        "C": 0.07150055800848436,
        "H": 0.08928902168198798,
        "A": 0.06932864281879784,
        "K": 0.07829822820019323,
        "D": 0.07332509810640282
    },
    "Average_excess_waiting_time": {
        "B": 50.63577239724566,
        "E": 52.72723329534449,
        "F": 51.203986235756304,
        "G": 50.22999909234994,
        "I": 49.445342116416896,
        "L": 50.91324716410679,
        "J": 49.594020673953025,
        "C": 51.06130674266808,
        "H": 50.57895709228046,
        "A": 50.97172389998332,
        "K": 51.483047924591915,
        "D": 52.82585396929119
    },
    "Holding_per_stop": {
        "I": 25.625,
        "B": 25.9375,
        "G": 25.3125,
        "F": 28.4375,
        "L": 27.1875,
        "E": 27.473684210526315,
        "H": 25.625,
        "J": 26.25,
        "C": 26.210526315789473,
        "A": 26.210526315789473,
        "K": 25.57894736842105,
        "D": 27.157894736842106
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 55.875,
            "C": 55.5625,
            "D": 55.625,
            "E": 56.3125,
            "F": 57.6875,
            "G": 59.6875,
            "H": 60,
            "I": 60.4375,
            "J": 59.9375,
            "K": 60.875,
            "L": 60.9375,
            "A": 58.8
        },
        "bus1": {
            "E": 53.5625,
            "F": 55.5625,
            "G": 56.3125,
            "H": 56.5625,
            "I": 57.1875,
            "J": 56.625,
            "K": 57.5625,
            "L": 57.9375,
            "A": 57.125,
            "B": 56.375,
            "C": 57.6,
            "D": 56.86666666666667
        },
        "bus2": {
            "F": 54.1875,
            "G": 55.5,
            "H": 55.6875,
            "I": 58.25,
            "J": 59.0625,
            "K": 58.4375,
            "L": 56,
            "A": 56,
            "B": 56.0625,
            "C": 56.25,
            "D": 56.3125,
            "E": 57
        },
        "bus3": {
            "G": 54.5625,
            "H": 55.625,
            "I": 56,
            "J": 58.125,
            "K": 57.5,
            "L": 58.0625,
            "A": 57.8125,
            "B": 56.75,
            "C": 58.3125,
            "D": 58.875,
            "E": 58.0625,
            "F": 58.5625
        },
        "bus4": {
            "I": 55.875,
            "J": 54.75,
            "K": 54.8125,
            "L": 56.3125,
            "A": 57.5625,
            "B": 57.8125,
            "C": 60,
            "D": 60.25,
            "E": 60.5625,
            "F": 59.875,
            "G": 60.4375,
            "H": 58.875
        },
        "bus5": {
            "L": 56.5625,
            "A": 56.6875,
            "B": 58,
            "C": 58.5625,
            "D": 58,
            "E": 60.375,
            "F": 61.0625,
            "G": 62.3125,
            "H": 60.0625,
            "I": 59.6875,
            "J": 59.375,
            "K": 59.53333333333333
        }
    },
    "Obs_and_actions_busses": {
        "0": -105.91067622903938,
        "1": 10.879371371072006,
        "4": 329.0147814408153
    },
    "Obs_and_actions_tfls": {}
}