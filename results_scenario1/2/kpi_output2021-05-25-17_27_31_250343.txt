{
    "Average_holding_time": 26.51832460732984,
    "Average_holding_actions": [
        157,
        981,
        0,
        8,
        0,
        0
    ],
    "Average_travel_time_passengers": 2022.2232214382898,
    "Average_passenger_waiting_time": 308.10590970422174,
    "Average_passenger_in_vehicle_time": 1714.1173117339897,
    "Average_speed_busses": 0.04775,
    "Average_experienced_crowding": 57.45814215159907,
    "Average_covariation_headway": 0.16946741758213962,
    "Average_covariation_headway_stops": {
        "C": 0.12270086370051761,
        "E": 0.10889143967254629,
        "I": 0.13878192171049034,
        "J": 0.13139809029704913,
        "K": 0.1314149843688286,
        "L": 0.1334116446294708,
        "A": 0.1088454396641013,
        "F": 0.08457759872257177,
        "D": 0.09825895639197928,
        "B": 0.10400367245769289,
        "G": 0.08701928959395153,
        "H": 0.11446414461453033
    },
    "Average_excess_waiting_time": {
        "C": 51.265387212165024,
        "E": 50.187441576314825,
        "I": 55.78213741333104,
        "J": 53.491533234590804,
        "K": 51.47991411134308,
        "L": 50.0096730829743,
        "A": 50.44256404924329,
        "F": 50.52002551871675,
        "D": 51.214225268022346,
        "B": 51.59428851803966,
        "G": 52.343984342713384,
        "H": 55.61337608176251
    },
    "Holding_per_stop": {
        "L": 27.52577319587629,
        "E": 26.875,
        "K": 26.5625,
        "J": 25.57894736842105,
        "C": 26.25,
        "I": 25.263157894736842,
        "A": 25.9375,
        "F": 26.842105263157894,
        "D": 27.473684210526315,
        "B": 25.9375,
        "G": 27.157894736842106,
        "H": 26.80851063829787
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 55.375,
            "D": 56.875,
            "E": 55.625,
            "F": 58.4375,
            "G": 58.875,
            "H": 59.5625,
            "I": 59.8125,
            "J": 58.75,
            "K": 59.25,
            "L": 57.6875,
            "A": 59.0625,
            "B": 59.25
        },
        "bus1": {
            "E": 53.8125,
            "F": 55.125,
            "G": 56.1875,
            "H": 57.125,
            "I": 58.6875,
            "J": 58.375,
            "K": 58.6875,
            "L": 59.125,
            "A": 58.25,
            "B": 58.375,
            "C": 58.25,
            "D": 57.8125
        },
        "bus2": {
            "I": 55.375,
            "J": 55.1875,
            "K": 55,
            "L": 57.5,
            "A": 58,
            "B": 58.5625,
            "C": 59.125,
            "D": 58.8125,
            "E": 61.3125,
            "F": 61.625,
            "G": 59.266666666666666,
            "H": 58.6
        },
        "bus3": {
            "J": 60.8125,
            "K": 61.4375,
            "L": 62.125,
            "A": 62.3125,
            "B": 61.8125,
            "C": 62.125,
            "D": 63.875,
            "E": 60.4375,
            "F": 63.3125,
            "G": 64.1875,
            "H": 64.9375,
            "I": 64.33333333333333
        },
        "bus4": {
            "K": 48.25,
            "L": 50.5,
            "A": 50.8125,
            "B": 51.5625,
            "C": 51.5,
            "D": 51.3125,
            "E": 51.1875,
            "F": 51.5625,
            "G": 51.25,
            "H": 52.375,
            "I": 53.5625,
            "J": 52
        },
        "bus5": {
            "L": 47.529411764705884,
            "A": 48.25,
            "B": 47.0625,
            "C": 47.625,
            "D": 49.8125,
            "E": 50.125,
            "F": 50.125,
            "G": 50.1875,
            "H": 51.1875,
            "I": 51.875,
            "J": 50.5625,
            "K": 49.75
        }
    },
    "Obs_and_actions_busses": {
        "4": 450.8448389150535,
        "0": -120.94726586662028,
        "1": 11.507979965395396
    },
    "Obs_and_actions_tfls": {}
}