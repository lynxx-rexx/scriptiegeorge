{
    "Average_holding_time": 25.841325196163904,
    "Average_holding_actions": [
        171,
        972,
        0,
        4,
        0,
        0
    ],
    "Average_travel_time_passengers": 2013.0240621589949,
    "Average_passenger_waiting_time": 313.083957266629,
    "Average_passenger_in_vehicle_time": 1699.940104892291,
    "Average_speed_busses": 0.04779166666666667,
    "Average_experienced_crowding": 57.492449792778366,
    "Average_covariation_headway": 0.16966732023340714,
    "Average_covariation_headway_stops": {
        "B": 0.11336166879889761,
        "E": 0.11977014342437013,
        "G": 0.11526702055864878,
        "J": 0.12089917864641161,
        "K": 0.12248610301230548,
        "L": 0.12403660869973589,
        "C": 0.09584322018922932,
        "H": 0.09939781205525294,
        "A": 0.10459128968076431,
        "F": 0.09510843008194404,
        "I": 0.10159773184117628,
        "D": 0.09760455545969057
    },
    "Average_excess_waiting_time": {
        "B": 48.942401824386025,
        "E": 50.97299642665064,
        "G": 50.86407543669179,
        "J": 52.937489311849106,
        "K": 51.30205748023354,
        "L": 49.81684286067667,
        "C": 49.471622372141496,
        "H": 51.5362198148847,
        "A": 49.936924522222455,
        "F": 50.96819245451138,
        "I": 53.30461011725322,
        "D": 51.21668284162695
    },
    "Holding_per_stop": {
        "B": 25.625,
        "G": 24.375,
        "L": 26.25,
        "K": 26.25,
        "E": 26.5625,
        "J": 24.94736842105263,
        "H": 26.842105263157894,
        "A": 26.875,
        "C": 26.25,
        "F": 24.31578947368421,
        "I": 25.57894736842105,
        "D": 26.210526315789473
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 57.470588235294116,
            "C": 59.5,
            "D": 59,
            "E": 59.9375,
            "F": 60.4375,
            "G": 60.3125,
            "H": 59,
            "I": 58.3125,
            "J": 58.25,
            "K": 57.8125,
            "L": 58.5625,
            "A": 59.6875
        },
        "bus1": {
            "E": 56.1875,
            "F": 58.375,
            "G": 58.5625,
            "H": 60.375,
            "I": 63.8125,
            "J": 64.1875,
            "K": 63.9375,
            "L": 63.9375,
            "A": 62.5,
            "B": 60.875,
            "C": 61.1875,
            "D": 59.875
        },
        "bus2": {
            "G": 55.375,
            "H": 57.1875,
            "I": 56.875,
            "J": 57.375,
            "K": 56.9375,
            "L": 59.375,
            "A": 59.0625,
            "B": 58.375,
            "C": 59.0625,
            "D": 59.9375,
            "E": 59.375,
            "F": 58.1875
        },
        "bus3": {
            "J": 58.0625,
            "K": 57.4375,
            "L": 57.6875,
            "A": 56.8125,
            "B": 57.1875,
            "C": 56.375,
            "D": 56.4375,
            "E": 57,
            "F": 57.5625,
            "G": 58,
            "H": 58.875,
            "I": 60.6
        },
        "bus4": {
            "K": 49.6875,
            "L": 49.375,
            "A": 49.5625,
            "B": 48.625,
            "C": 49.875,
            "D": 48.625,
            "E": 48.875,
            "F": 50.5,
            "G": 50.9375,
            "H": 51.125,
            "I": 50.4375,
            "J": 51.625
        },
        "bus5": {
            "L": 48.64705882352941,
            "A": 49.0625,
            "B": 49.125,
            "C": 48.5625,
            "D": 48.6875,
            "E": 50.25,
            "F": 50.125,
            "G": 50.75,
            "H": 49.9375,
            "I": 49.375,
            "J": 50,
            "K": 50.0625
        }
    },
    "Obs_and_actions_busses": {
        "0": -118.84064341986931,
        "1": 16.074017069877357,
        "4": 388.99270816610743
    },
    "Obs_and_actions_tfls": {}
}1.8637608438839
    },
    "Obs_and_actions_tfls": {}
}