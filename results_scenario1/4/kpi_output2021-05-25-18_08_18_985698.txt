{
    "Average_holding_time": 75.43032786885246,
    "Average_holding_actions": [
        0,
        45,
        412,
        0,
        14,
        0
    ],
    "Average_travel_time_passengers": 2387.249005507491,
    "Average_passenger_waiting_time": 366.37407036514554,
    "Average_passenger_in_vehicle_time": 2020.8749351422653,
    "Average_speed_busses": 0.04066666666666666,
    "Average_experienced_crowding": 67.71137033164891,
    "Average_covariation_headway": 0.1143087823646827,
    "Average_covariation_headway_stops": {
        "C": 0.12292619210038062,
        "D": 0.1270957821551761,
        "E": 0.12925781281323537,
        "F": 0.1329068281852787,
        "I": 0.10994960142429902,
        "K": 0.10139099736846877,
        "L": 0.06765662497364772,
        "G": 0.09995992275481584,
        "J": 0.07161413955219924,
        "A": 0.0636224596613535,
        "H": 0.08351752402415832,
        "B": 0.08667719974726382
    },
    "Average_excess_waiting_time": {
        "C": 110.09451295081146,
        "D": 107.80870530129562,
        "E": 105.79765848317231,
        "F": 104.08791874784339,
        "I": 104.18730262554425,
        "K": 103.74049274710427,
        "L": 104.08970439413434,
        "G": 103.45830089250512,
        "J": 104.04100831929367,
        "A": 106.09196167789571,
        "H": 104.69019745951965,
        "B": 109.77854820248405
    },
    "Holding_per_stop": {
        "K": 74.26829268292683,
        "F": 76.82926829268293,
        "D": 74.81481481481481,
        "E": 77.1951219512195,
        "C": 74.81481481481481,
        "I": 77.92682926829268,
        "L": 73.70370370370371,
        "G": 75.36585365853658,
        "J": 76.29629629629629,
        "A": 75.55555555555556,
        "H": 75.55555555555556,
        "B": 72.75
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "C": 64.42857142857143,
            "D": 65.35714285714286,
            "E": 66.85714285714286,
            "F": 65.78571428571429,
            "G": 69.07142857142857,
            "H": 68.53846153846153,
            "I": 67.38461538461539,
            "J": 67.84615384615384,
            "K": 68.84615384615384,
            "L": 70.3076923076923,
            "A": 68.38461538461539,
            "B": 68.3076923076923
        },
        "bus1": {
            "D": 63.42857142857143,
            "E": 65.5,
            "F": 65,
            "G": 64.35714285714286,
            "H": 64,
            "I": 63.214285714285715,
            "J": 66.23076923076923,
            "K": 67.46153846153847,
            "L": 67.3076923076923,
            "A": 68.23076923076923,
            "B": 68.07692307692308,
            "C": 68.46153846153847
        },
        "bus2": {
            "E": 61.357142857142854,
            "F": 61.142857142857146,
            "G": 62.57142857142857,
            "H": 61.285714285714285,
            "I": 61.142857142857146,
            "J": 62.357142857142854,
            "K": 62.642857142857146,
            "L": 62.15384615384615,
            "A": 65.38461538461539,
            "B": 65.84615384615384,
            "C": 68.92307692307692,
            "D": 69
        },
        "bus3": {
            "F": 62.357142857142854,
            "G": 64.57142857142857,
            "H": 66.78571428571429,
            "I": 68.5,
            "J": 68.07142857142857,
            "K": 69.92857142857143,
            "L": 69.78571428571429,
            "A": 69.42857142857143,
            "B": 69.53846153846153,
            "C": 69.6923076923077,
            "D": 68.46153846153847,
            "E": 68.07692307692308
        },
        "bus4": {
            "I": 62.92857142857143,
            "J": 63,
            "K": 64.92857142857143,
            "L": 66,
            "A": 66.57142857142857,
            "B": 68.57142857142857,
            "C": 68.64285714285714,
            "D": 68.46153846153847,
            "E": 66.38461538461539,
            "F": 66.92307692307692,
            "G": 64.76923076923077,
            "H": 68
        },
        "bus5": {
            "K": 66.35714285714286,
            "L": 66.28571428571429,
            "A": 66.42857142857143,
            "B": 70.5,
            "C": 69.14285714285714,
            "D": 72.21428571428571,
            "E": 71.71428571428571,
            "F": 70.92307692307692,
            "G": 70.61538461538461,
            "H": 70.6923076923077,
            "I": 71.38461538461539,
            "J": 71.84615384615384
        }
    },
    "Obs_and_actions_busses": {
        "5": 388.1079090088442,
        "3": 32.53352690497631,
        "2": -40.62168429698199,
        "1": -227.08895957145066
    },
    "Obs_and_actions_tfls": {}
}