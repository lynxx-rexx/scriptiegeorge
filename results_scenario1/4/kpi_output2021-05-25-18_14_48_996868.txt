{
    "Average_holding_time": 77.9876160990712,
    "Average_holding_actions": [
        0,
        32,
        352,
        0,
        14,
        0
    ],
    "Average_travel_time_passengers": 2376.28611002294,
    "Average_passenger_waiting_time": 366.748877764238,
    "Average_passenger_in_vehicle_time": 2009.537232258623,
    "Average_speed_busses": 0.040375,
    "Average_experienced_crowding": 67.27366080496573,
    "Average_covariation_headway": 0.10768447484412007,
    "Average_covariation_headway_stops": {
        "B": 0.10363834416847008,
        "D": 0.09629358320278907,
        "F": 0.09371470555865329,
        "J": 0.1183263255961614,
        "K": 0.12333025663800658,
        "L": 0.12666264552968623,
        "A": 0.0854712688112576,
        "G": 0.05702978852206302,
        "C": 0.06749437096205838,
        "E": 0.06368786006607471,
        "H": 0.05266040429152104,
        "I": 0.08127203318222767
    },
    "Average_excess_waiting_time": {
        "B": 106.2204705927474,
        "D": 105.8013064862227,
        "F": 105.63641828249541,
        "J": 112.23806842094643,
        "K": 110.19339151741258,
        "L": 108.11386391407018,
        "A": 107.24680883587115,
        "G": 106.06866530380626,
        "C": 106.40079744887186,
        "E": 106.21894426399211,
        "H": 108.04823570072722,
        "I": 111.79240563171601
    },
    "Holding_per_stop": {
        "L": 78.51851851851852,
        "F": 76.29629629629629,
        "B": 76.29629629629629,
        "K": 80,
        "D": 78.51851851851852,
        "J": 78.375,
        "A": 78.51851851851852,
        "G": 77.4074074074074,
        "C": 79.25925925925925,
        "E": 78.88888888888889,
        "H": 76.125,
        "I": 77.625
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus0": {
            "B": 65,
            "C": 66.78571428571429,
            "D": 65.35714285714286,
            "E": 66.57142857142857,
            "F": 66,
            "G": 65.21428571428571,
            "H": 68.61538461538461,
            "I": 68.53846153846153,
            "J": 68.15384615384616,
            "K": 69,
            "L": 71.07692307692308,
            "A": 71.38461538461539
        },
        "bus1": {
            "D": 62.57142857142857,
            "E": 63.642857142857146,
            "F": 64.14285714285714,
            "G": 65,
            "H": 65.78571428571429,
            "I": 67.28571428571429,
            "J": 66.07692307692308,
            "K": 66.38461538461539,
            "L": 66.15384615384616,
            "A": 66.61538461538461,
            "B": 66.76923076923077,
            "C": 66.92307692307692
        },
        "bus2": {
            "F": 62.57142857142857,
            "G": 64.21428571428571,
            "H": 65.21428571428571,
            "I": 65.71428571428571,
            "J": 66.78571428571429,
            "K": 67.85714285714286,
            "L": 68.92307692307692,
            "A": 67.6923076923077,
            "B": 70.76923076923077,
            "C": 70.38461538461539,
            "D": 67.6923076923077,
            "E": 67.07692307692308
        },
        "bus3": {
            "J": 65.21428571428571,
            "K": 65.07142857142857,
            "L": 63.92857142857143,
            "A": 63.07142857142857,
            "B": 63.38461538461539,
            "C": 65.3076923076923,
            "D": 65.38461538461539,
            "E": 67.46153846153847,
            "F": 65.15384615384616,
            "G": 66,
            "H": 66.38461538461539,
            "I": 68.6923076923077
        },
        "bus4": {
            "K": 64.71428571428571,
            "L": 65.92857142857143,
            "A": 65.28571428571429,
            "B": 65.64285714285714,
            "C": 66.92857142857143,
            "D": 65.07692307692308,
            "E": 67.46153846153847,
            "F": 66.76923076923077,
            "G": 66,
            "H": 66.61538461538461,
            "I": 67.46153846153847,
            "J": 67.84615384615384
        },
        "bus5": {
            "L": 61.642857142857146,
            "A": 62.785714285714285,
            "B": 66.28571428571429,
            "C": 65.21428571428571,
            "D": 66.78571428571429,
            "E": 69.14285714285714,
            "F": 67.92307692307692,
            "G": 68.3076923076923,
            "H": 68.84615384615384,
            "I": 70.3076923076923,
            "J": 68.53846153846153,
            "K": 66.6923076923077
        }
    },
    "Obs_and_actions_busses": {
        "2": -48.21602671654519,
        "1": -214.73707099768828,
        "3": 25.13700305921113,
        "5": 357.25467439339633
    },
    "Obs_and_actions_tfls": {}
}