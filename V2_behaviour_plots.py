import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json
from _collections import defaultdict

# https://coolors.co/50514f-f25f5c-ffe066-247ba0-70c1b3
plt.rcParams.update({'font.size': 18})


scenario_holding_data_list = []
scenario_no_data_list = []
for i in range(5):
    if i != 2:
        with open('results_holding_scenario' + str(i+1) + '\\average_kpis.json', 'r') as reading_file:
            scenario_holding_data_list.append(json.load(reading_file))
        with open('results_no_scenario' + str(i+1) + '\\average_kpis.json', 'r') as reading_file:
            scenario_no_data_list.append(json.load(reading_file))
    if i == 2:
        with open('results_holding_scenario' + str(i) + '\\average_kpis.json', 'r') as reading_file:
            scenario_holding_data_list.append(json.load(reading_file))
        with open('results_no_scenario' + str(i) + '\\average_kpis.json', 'r') as reading_file:
            scenario_no_data_list.append(json.load(reading_file))



scenario_names = ['Idealised', 'Stochastic', 'Stochastic - Traffic Light Control', 'Deterministic - Heterogeneous', 'Realistic']





# GEMIDDELDE METRIC WAARDEN VOOR ELK SCENARIO (holding benchmark & no control)

average_holding_holding = []
average_holding_no = []

average_travel_time_holding = []
average_travel_time_no = []
average_in_time_holding = []
average_in_time_no = []
average_waiting_time_holding = []
average_waiting_time_no = []

average_crowding_holding = []
average_crowding_no = []
average_coef_holding = []
average_coef_no = []
average_excess_holding = []
average_excess_no = []




for i in range(5):
    average_holding_holding.append(scenario_holding_data_list[i]['Average_holding_time'][0])
    average_holding_no.append(scenario_no_data_list[i]['Average_holding_time'][0])

    average_travel_time_holding.append(scenario_holding_data_list[i]['Average_travel_time_passengers'][0])
    average_travel_time_no.append(scenario_no_data_list[i]['Average_travel_time_passengers'][0])
    average_in_time_holding.append(scenario_holding_data_list[i]['Average_passenger_in_vehicle_time'][0])
    average_in_time_no.append(scenario_no_data_list[i]['Average_passenger_in_vehicle_time'][0])
    average_waiting_time_holding.append(scenario_holding_data_list[i]['Average_passenger_waiting_time'][0])
    average_waiting_time_no.append(scenario_no_data_list[i]['Average_passenger_waiting_time'][0])

    average_crowding_holding.append(scenario_holding_data_list[i]['Average_experienced_crowding'][0])
    average_crowding_no.append(scenario_no_data_list[i]['Average_experienced_crowding'][0])

    temp_list_coef = []
    temp_list_coef_no = []
    temp_list_excess = []
    temp_list_excess_no = []
    for dictionary_stops in scenario_holding_data_list[i]['Average_covariation_headway_stops']:
        for key, value in dictionary_stops.items():
            temp_list_coef.append(value)
    for dictionary_stops in scenario_no_data_list[i]['Average_covariation_headway_stops']:
        for key, value in dictionary_stops.items():
            temp_list_coef_no.append(value)

    for dictionary_stops in scenario_holding_data_list[i]['Average_excess_waiting_time']:
        for key, value in dictionary_stops.items():
            temp_list_excess.append(value)
    for dictionary_stops in scenario_no_data_list[i]['Average_excess_waiting_time']:
        for key, value in dictionary_stops.items():
            temp_list_excess_no.append(value)


    average_coef_holding.append(np.mean(temp_list_coef))
    average_coef_no.append(np.mean(temp_list_coef_no))

    average_excess_holding.append(np.mean(temp_list_excess))
    average_excess_no.append(np.mean(temp_list_excess_no))


# nu heb ik voor beide benchmarks voor elke metric een lijst met de gemiddelde waarden van de metrics voor alle 5 scenarios














########################################################################################################################
# BEHAVIOUR & GENERAL PERFORMANCE
# ik wil de vergelijking doen met de beste policy per scenario en de benchmark

best_scenario_list = [2, 3, 4, 2, 2]

scenario_abbreviations = ['ID', 'ST', 'TL', 'DH', 'RE']

best_scenario_data_list = []

for i in range(5):
    with open('results_scenario' + str(i+1) + '\\' + str(best_scenario_list[i]) + '\\average_kpis.json', 'r') as reading_file:
        best_scenario_data_list.append(json.load(reading_file))



### GENERAL PERFORMANCE

# 4 +1 plots met elk de waarden van de beste policy per scenario vs de waarde van de benchmark


holding_data_policies, waiting_data_policies, in_data_policies, exp_data_policies, coef_data_policies, excess_data_policies = [], [], [], [], [], []

for i in range(5):
    # data = pd.read_csv('results_scenario' + str(scenario) + '\\' + str(i) + '\\' + 'average_kpis.json')
    with open('results_scenario' + str(i+1) + '\\' + str(best_scenario_list[i]) + '\\' + 'average_kpis.json', 'r') as reading_file:
        data = json.load(reading_file)

        holding_data_policies.append(data['Average_holding_time'][0])
        waiting_data_policies.append(data['Average_passenger_waiting_time'][0])
        in_data_policies.append(data['Average_passenger_in_vehicle_time'][0])
        exp_data_policies.append(data['Average_experienced_crowding'][0])

        temp_list_coef = []
        temp_list_coef_no = []
        temp_list_excess = []
        temp_list_excess_no = []
        for dictionary_stops in data['Average_covariation_headway_stops']:
            for key, value in dictionary_stops.items():
                temp_list_coef.append(value)

        for dictionary_stops in data['Average_excess_waiting_time']:
            for key, value in dictionary_stops.items():
                temp_list_excess.append(value)


        coef_data_policies.append(np.mean(temp_list_coef))
        excess_data_policies.append(np.mean(temp_list_excess))




# HOLDING


N = 5
ind = np.arange(N)
width = 0.35
plt.bar(ind-width/2, holding_data_policies, width, color='#247BA0', label='MARL')
plt.bar(ind+width/2, average_holding_holding, width, color='#F25F5C', label='Benchmark')

print("Holding")
print(holding_data_policies)
print(average_holding_holding)
print(average_holding_no)

plt.ylabel('Time (s)')
plt.xlabel('Scenario')
plt.title('Average holding time per stop')

# if ylim[1]:
#     plt.ylim(0, average_crowding_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, scenario_abbreviations)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='best')
plt.show()










# TRAVEL TIME

N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,1)
plt.bar(ind-width/2, waiting_data_policies, width, label='Waiting time MARL', color='#247BA0')
plt.bar(ind-width/2, in_data_policies, width, bottom=waiting_data_policies, label='In vehicle time MARL', color='#247BA0', alpha=0.5)#70C1B3
plt.bar(ind+width/2, average_waiting_time_holding, width, label='Waiting time benchmark', color='#F25F5C')
plt.bar(ind+width/2, average_in_time_holding, width, bottom=average_waiting_time_holding, label='In vehicle time benchmark', color='#F25F5C', alpha=0.5)#FFE066

print('\nTotal travel time')
print([waiting_data_policies[i] + in_data_policies[i] for i in range(5)])
print([average_waiting_time_holding[i] + average_in_time_holding[i] for i in range(5)])
print([average_waiting_time_no[i] + average_in_time_no[i] for i in range(5)])


print('\nWaiting time')
print(waiting_data_policies)
print(average_waiting_time_holding)
print(average_waiting_time_no)

print('\nIn vehicle time')
print(in_data_policies)
print(average_in_time_holding)
print(average_in_time_no)


plt.ylabel('Time (s)')
# plt.xlabel('Percentile')
plt.title('Average total travel time')
# plt.ylim(0, 4000)


# if ylim[0]:
#     plt.ylim(0, int((average_in_time_holding[scenario-1] + average_waiting_time_holding[scenario-1]) * 1.1) )
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, scenario_abbreviations)
plt.legend(loc='center right', ncol=2, prop={'size': 12})
# plt.legend(loc='best')
# plt.show()




# EXPERIENCED CROWDING

N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,2)
plt.bar(ind-width/2, exp_data_policies, width, color='#247BA0', label='MARL')
plt.bar(ind+width/2, average_crowding_holding, width, color='#F25F5C', label='Benchmark')


print('\nExperienced crowing')
print(exp_data_policies)
print(average_crowding_holding)
print(average_crowding_no)




plt.ylabel('Number of passengers')
# plt.xlabel('Percentile')
plt.title('Average experienced crowding')

# if ylim[1]:
#     plt.ylim(0, average_crowding_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, scenario_abbreviations)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='center right')
# plt.show()





# COEFF OF VAR

N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,3)
plt.bar(ind-width/2, coef_data_policies, width, color='#247BA0', label='MARL')
plt.bar(ind+width/2, average_coef_holding, width, color='#F25F5C', label='Benchmark')

print('\nCoef of var')
print(coef_data_policies)
print(average_coef_holding)
print(average_coef_no)



plt.ylabel('Coefficient of variation')
plt.xlabel('Scenario')
plt.title('Average coefficient of variation of the headways')

# if ylim[2]:
#     plt.ylim(0, average_coef_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, scenario_abbreviations)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='center right')
# plt.show()





# EXCESS WAITING TIME


N = 5
ind = np.arange(N)
width = 0.35
plt.subplot(2,2,4)
plt.bar(ind-width/2, excess_data_policies, width, color='#247BA0', label='MARL')
plt.bar(ind+width/2, average_excess_holding, width, color='#F25F5C', label='Benchmark')

print('\nExcess waiting time')
print(excess_data_policies)
print(average_excess_holding)
print(average_excess_no)



plt.ylabel('Time (s)')
plt.xlabel('Scenario')
plt.title('Average excess waiting time')

# if ylim[3]:
#     plt.ylim(0, average_excess_holding[scenario-1] * 1.1)
# plt.xticks(ind + width / 2, ('0-5', '5-15', '15-50', '50-85', '85-95'))
plt.xticks(ind, scenario_abbreviations)
# plt.legend(loc='best', ncol=2)
plt.legend(loc='center right')
plt.show()














### ACTIONS BUSSES

# vergeten de 90 action op te slaan.... wat ga ik doen?
# opties: gemiddelde nemen, plot weglaten, open zijn, opschuiven (zoals bij vorige versie), alles opnieuw evalueren
# beste optie: voor nu weglaten, miss bij final version wel erin
# actions_busses = defaultdict(lambda : [0, 0, 0, 0, 0, 0, 0])



for i in range(5):
    temp_action_list = [0, 0, 0, 0, 0, 0]
    for small_list in best_scenario_data_list[i]['Average_holding_actions']:
        for j in range(6):
            # if j < 3:
            temp_action_list[j] += small_list[j]
            # else:
            #     temp_action_list[j+1] += small_list[j]

    temp_action_list = [ i / sum(temp_action_list) for i in temp_action_list ]


    temp_action_list_holding = [0, 0, 0, 0, 0, 0]
    for small_list in scenario_holding_data_list[i]['Average_holding_actions']:
        for j in range(6):
            # if j < 3:
            temp_action_list_holding[j] += small_list[j]
            # else:
            #     temp_action_list[j+1] += small_list[j]

    temp_action_list_holding = [ i / sum(temp_action_list_holding) for i in temp_action_list_holding ]





    N=6
    ind = np.arange(N)
    width = 0.35
    plt.subplot(2,3, i+1 )
    plt.bar(ind - width/2, temp_action_list, width, color='#247BA0', label='MARL')
    plt.bar(ind + width /2, temp_action_list_holding, width, color='#F25F5C', label= 'Holding benchmark')

    if i == 0 or i == 3:
        plt.ylabel('Fraction')
    if i == 2 or i == 3 or i == 4:
        plt.xlabel('Action')
        plt.xticks(ind, ['0', '30', '60', '120', '150', '180'])
    else:
        plt.xticks(ind, ['', '', '', '', '', ''])

    plt.title(scenario_names[i])
    plt.legend(loc='best', framealpha=0.5)

    if i == 4:
        plt.show()





# TFL ACTIONS



for i in [2, 4]:
    temp_action_list = [0, 0, 0]
    tfl_actions_list = best_scenario_data_list[i]['Actions_tfl_edges']
    for mini_dict in tfl_actions_list:
        for key, value in mini_dict.items():
            for j in range(3):
                temp_action_list[j] += value[j]
    temp_action_list = [ k/np.sum(temp_action_list) for k in temp_action_list ]

    N=3
    ind = np.arange(N)
    width = 0.35
    plt.subplot(1,2, int(i/2) )
    plt.bar(ind, temp_action_list, width, color='#247BA0')

    if i == 2:
        plt.ylabel('Fraction')
    plt.xlabel('Action')
    plt.title(scenario_names[i])

    plt.xticks(ind, ['Nothing', '-10 s delay', '+10 s delay'])
    # plt.legend(loc='best')

    if i == 4:
        plt.show()








### POLICIES BUSSES
#todo
# wil ik de locatie nog toevoegen aan de obs plot voor scenario 4 & 5?

obs_data_marl_scenarios_list = []
obs_data_benchmark_scenarios_list = []

# get a list with the average obs per action for each marl scenario
for scenario_data in best_scenario_data_list:
    temp_dict = defaultdict(lambda : [])
    for action_dict in scenario_data['Obs_and_actions_busses']:
        for key, value in action_dict.items():
            temp_dict[key].append(value)
    obs_data_marl_scenarios_list.append(temp_dict)
    # obs_data_marl_scenarios_list.append( { key: np.mean(value) for key, value in temp_dict.items() } )


# get a list with the average obs per action for each benchmark scenario
for scenario_data in scenario_holding_data_list:
    temp_dict = defaultdict(lambda : [])
    for action_dict in scenario_data['Obs_and_actions_busses']:
        for key, value in action_dict.items():
            temp_dict[key].append(value)
    obs_data_benchmark_scenarios_list.append(temp_dict)
    # obs_data_benchmark_scenarios_list.append( { key: np.mean(value) for key, value in temp_dict.items() } )






actions = [0, 1, 2, 3, 4, 5, 6]


# for each scenario
for i in range(5):
    p15_obs_list_marl, p50_obs_list_marl, p85_obs_list_marl = [], [], []
    p15_obs_list_benchmark, p50_obs_list_benchmark, p85_obs_list_benchmark = [], [], []
    actions_present = []
    actions_present_benchmark = []

    # for each action
    for action in actions:
        try:
            action_data = obs_data_marl_scenarios_list[i][str(action)]
            p15_obs_list_marl.append(np.percentile(action_data, 15))
            p50_obs_list_marl.append(np.percentile(action_data, 50))
            p85_obs_list_marl.append(np.percentile(action_data, 85))
            actions_present.append(action)
        except:
            pass
        try:
            action_data_benchmark = obs_data_benchmark_scenarios_list[i][str(action)]
            p15_obs_list_benchmark.append(np.percentile(action_data_benchmark, 15))
            p50_obs_list_benchmark.append(np.percentile(action_data_benchmark, 50))
            p85_obs_list_benchmark.append(np.percentile(action_data_benchmark, 85))
            actions_present_benchmark.append(action)

        except:
            pass

    # plt.subplot(2,3, i+1)
    # plt.plot(actions_present, p50_obs_list_marl, color='#247BA0', label='MARL')
    # plt.plot(actions_present_benchmark, p50_obs_list_benchmark, color='#F25F5C', label='Benchmark')
    # plt.fill_between(actions_present, p15_obs_list_marl, p85_obs_list_marl, alpha=0.2, facecolor='#247BA0')
    # plt.fill_between(actions_present_benchmark, p15_obs_list_benchmark, p85_obs_list_benchmark, alpha=0.2, facecolor='#F25F5C')
    # # plt.fill_between(actions_present_p5, policy_busses15_p5, policy_busses85_p5, alpha=0.2, facecolor='#F25F5C')

#    -------------------------------------------------------------------------------------------------------------------

    plt.subplot(2,3, i+1)

    lo_err_marl, up_err_marl = [], []
    comb_list_marl = zip(p15_obs_list_marl, p50_obs_list_marl, p85_obs_list_marl)

    lo_err_bench, up_err_bench = [], []
    comb_list_bench = zip(p15_obs_list_benchmark, p50_obs_list_benchmark, p85_obs_list_benchmark)

    for low, mid, upp in comb_list_marl:
        lo_err_marl.append(mid-low)
        up_err_marl.append(upp-mid)

    for low, mid, upp in comb_list_bench:
        lo_err_bench.append(mid-low)
        up_err_bench.append(upp-mid)

    y_bandwidth_marl = np.array([lo_err_marl, up_err_marl])
    y_bandwidth_bench = np.array([lo_err_bench, up_err_bench])

    plt.errorbar(actions_present, p50_obs_list_marl, yerr=y_bandwidth_marl,
                marker='o', markersize=8, fillstyle='right',
                linestyle='none', label='MARL', color='#247BA0')#, alpha=0.7)

    plt.errorbar(actions_present_benchmark, p50_obs_list_benchmark, yerr=y_bandwidth_bench,
                marker='o', markersize=8, fillstyle='left',
                linestyle='none', label='Benchmark', color='#F25F5C')#, alpha=0.7)


    #    -------------------------------------------------------------------------------------------------------------------

    if i == 0 or i == 3:
        plt.ylabel('Observation received')
    plt.xticks(np.arange(7),  ['0', '30', '60', '90', '120', '150', '180'])

    if i != 0 and i != 1:
        plt.xlabel('Holding time (s)')
    plt.title(scenario_names[i])

    plt.legend(loc='best', framealpha=0.3)

    if i == 4:
        plt.show()



