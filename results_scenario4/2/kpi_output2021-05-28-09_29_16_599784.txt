{
    "Average_holding_time": 59.60040983606557,
    "Average_holding_actions": [
        49,
        446,
        224,
        24,
        125,
        0
    ],
    "Average_travel_time_passengers": 2356.990646980618,
    "Average_passenger_waiting_time": 366.4424181876367,
    "Average_passenger_in_vehicle_time": 1990.5482287928855,
    "Average_speed_busses": 0.04066666666666666,
    "Average_experienced_crowding": 93.98173450903732,
    "Average_covariation_headway": 0.24118366605990876,
    "Average_covariation_headway_stops": {
        "I": 0.15553834740110076,
        "E": 0.12412498763450143,
        "J": 0.16179649474380398,
        "K": 0.1477323000974914,
        "D": 0.11920295711011965,
        "A": 0.14521817760536154,
        "F": 0.09083345095552331,
        "L": 0.12725718462140698,
        "B": 0.1093103554882136,
        "G": 0.09025643847922316,
        "H": 0.12578695237572887,
        "C": 0.1004986430630176
    },
    "Average_excess_waiting_time": {
        "I": 104.55791236143523,
        "E": 98.66740540120196,
        "J": 102.9388265683167,
        "K": 99.54499649742519,
        "D": 100.84469958903014,
        "A": 98.98210940185231,
        "F": 98.13105719403728,
        "L": 99.68186537041817,
        "B": 98.73662330239472,
        "G": 99.90398839487659,
        "H": 104.5389310522404,
        "C": 101.86144064325867
    },
    "Holding_per_stop": {
        "E": 36.58536585365854,
        "K": 76.82926829268293,
        "I": 76.29629629629629,
        "J": 77.5609756097561,
        "A": 96.95121951219512,
        "D": 32.22222222222222,
        "F": 47.407407407407405,
        "L": 76.82926829268293,
        "B": 50,
        "G": 44.81481481481482,
        "H": 64.5,
        "C": 34.44444444444444
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus2": {
            "I": 113.78571428571429,
            "J": 106.5,
            "K": 97.28571428571429,
            "L": 82.78571428571429,
            "A": 72.21428571428571,
            "B": 63.785714285714285,
            "C": 56.07692307692308,
            "D": 51,
            "E": 51.69230769230769,
            "F": 78.38461538461539,
            "G": 119.38461538461539,
            "H": 127.07692307692308
        },
        "bus1": {
            "E": 50,
            "F": 76.64285714285714,
            "G": 113,
            "H": 121.5,
            "I": 119.57142857142857,
            "J": 111.64285714285714,
            "K": 101.64285714285714,
            "L": 88.42857142857143,
            "A": 80.61538461538461,
            "B": 69.38461538461539,
            "C": 63.15384615384615,
            "D": 56.61538461538461
        },
        "bus3": {
            "J": 103.21428571428571,
            "K": 93,
            "L": 82.64285714285714,
            "A": 69.28571428571429,
            "B": 58.642857142857146,
            "C": 54.642857142857146,
            "D": 51,
            "E": 49.23076923076923,
            "F": 73.38461538461539,
            "G": 107.3076923076923,
            "H": 119.07692307692308,
            "I": 117.07692307692308
        },
        "bus4": {
            "K": 93.5,
            "L": 82.28571428571429,
            "A": 70.57142857142857,
            "B": 62.785714285714285,
            "C": 56.357142857142854,
            "D": 55,
            "E": 53.642857142857146,
            "F": 82.46153846153847,
            "G": 118,
            "H": 124.76923076923077,
            "I": 118.38461538461539,
            "J": 110.6923076923077
        },
        "bus0": {
            "D": 52.857142857142854,
            "E": 51.357142857142854,
            "F": 78.42857142857143,
            "G": 112.57142857142857,
            "H": 123,
            "I": 120.28571428571429,
            "J": 115.57142857142857,
            "K": 105.53846153846153,
            "L": 94.3076923076923,
            "A": 83.46153846153847,
            "B": 71,
            "C": 62.76923076923077
        },
        "bus5": {
            "A": 76.5,
            "B": 65.21428571428571,
            "C": 59.714285714285715,
            "D": 53.92857142857143,
            "E": 53.285714285714285,
            "F": 80.85714285714286,
            "G": 120.71428571428571,
            "H": 127.5,
            "I": 124.53846153846153,
            "J": 114.53846153846153,
            "K": 104.6923076923077,
            "L": 93.6923076923077
        }
    },
    "Obs_and_actions_busses": {
        "5": 147.97773660461215,
        "4": 146.6249999962506,
        "3": 18.574074074444283,
        "1": -33.248667776278864,
        "2": -36.56925300004164,
        "0": -204.82143106836713
    },
    "Obs_and_actions_tfls": {}
}2335794443,
        "0": -166.65553849631252
    },
    "Obs_and_actions_tfls": {}
}