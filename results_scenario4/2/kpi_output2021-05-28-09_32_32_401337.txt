{
    "Average_holding_time": 61.39318885448916,
    "Average_holding_actions": [
        47,
        404,
        254,
        31,
        124,
        0
    ],
    "Average_travel_time_passengers": 2374.3522126218236,
    "Average_passenger_waiting_time": 387.2195718167599,
    "Average_passenger_in_vehicle_time": 1987.1326408049474,
    "Average_speed_busses": 0.040375,
    "Average_experienced_crowding": 97.25850822856768,
    "Average_covariation_headway": 0.2967939062682654,
    "Average_covariation_headway_stops": {
        "H": 0.27158601153588346,
        "I": 0.283834071043494,
        "J": 0.27518163941025975,
        "K": 0.25558808895817864,
        "L": 0.2551276962267549,
        "D": 0.2235836544510938,
        "A": 0.2279671099359324,
        "E": 0.20823556102365923,
        "F": 0.2023053219878441,
        "B": 0.21760728150980715,
        "G": 0.2176784131462543,
        "C": 0.22166704889235167
    },
    "Average_excess_waiting_time": {
        "H": 127.49412922913893,
        "I": 127.04081119042206,
        "J": 122.63097017962701,
        "K": 117.01977991926464,
        "L": 113.98228169280736,
        "D": 115.23233403269342,
        "A": 111.88656085246714,
        "E": 114.86825468573682,
        "F": 115.95629472766586,
        "B": 113.56879238420271,
        "G": 120.23327804560284,
        "C": 116.90890800068269
    },
    "Holding_per_stop": {
        "I": 71.48148148148148,
        "H": 59.625,
        "L": 69.14634146341463,
        "J": 77.03703703703704,
        "K": 70.60975609756098,
        "D": 35.18518518518518,
        "A": 99.8780487804878,
        "E": 39.75,
        "B": 57.407407407407405,
        "F": 53.25,
        "C": 43.875,
        "G": 58.10126582278481
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus1": {
            "H": 118.57142857142857,
            "I": 115.28571428571429,
            "J": 108.14285714285714,
            "K": 98.78571428571429,
            "L": 86.6923076923077,
            "A": 75.53846153846153,
            "B": 64.53846153846153,
            "C": 56.69230769230769,
            "D": 53.53846153846154,
            "E": 52.53846153846154,
            "F": 79.92307692307692,
            "G": 119.15384615384616
        },
        "bus2": {
            "I": 107.14285714285714,
            "J": 97.21428571428571,
            "K": 88.42857142857143,
            "L": 78.57142857142857,
            "A": 69.07142857142857,
            "B": 59,
            "C": 50.92307692307692,
            "D": 49.46153846153846,
            "E": 48.23076923076923,
            "F": 73.76923076923077,
            "G": 111,
            "H": 120.07692307692308
        },
        "bus3": {
            "J": 102.92857142857143,
            "K": 94.57142857142857,
            "L": 81.21428571428571,
            "A": 69.21428571428571,
            "B": 59.142857142857146,
            "C": 55.214285714285715,
            "D": 52.92307692307692,
            "E": 51.76923076923077,
            "F": 76.46153846153847,
            "G": 111.76923076923077,
            "H": 122.61538461538461,
            "I": 119.38461538461539
        },
        "bus4": {
            "K": 81.5,
            "L": 72.85714285714286,
            "A": 63.642857142857146,
            "B": 56.285714285714285,
            "C": 52.642857142857146,
            "D": 49.357142857142854,
            "E": 48.857142857142854,
            "F": 68.53846153846153,
            "G": 98.53846153846153,
            "H": 107.61538461538461,
            "I": 102.15384615384616,
            "J": 95.92307692307692
        },
        "bus5": {
            "L": 92,
            "A": 80.07142857142857,
            "B": 70.07142857142857,
            "C": 61.714285714285715,
            "D": 59.857142857142854,
            "E": 59.142857142857146,
            "F": 93.07142857142857,
            "G": 135.21428571428572,
            "H": 145.46153846153845,
            "I": 138.69230769230768,
            "J": 125,
            "K": 112.38461538461539
        },
        "bus0": {
            "D": 55.5,
            "E": 53.285714285714285,
            "F": 80.14285714285714,
            "G": 121.71428571428571,
            "H": 133.78571428571428,
            "I": 129.64285714285714,
            "J": 121.84615384615384,
            "K": 112.07692307692308,
            "L": 97.46153846153847,
            "A": 86.92307692307692,
            "B": 74.38461538461539,
            "C": 66.46153846153847
        }
    },
    "Obs_and_actions_busses": {
        "5": 217.4960734416321,
        "4": 133.51612903128972,
        "2": -16.217715471271635,
        "3": 20.091743119266088,
        "1": -41.15353381239041,
        "0": -493.06333597131487
    },
    "Obs_and_actions_tfls": {}
}