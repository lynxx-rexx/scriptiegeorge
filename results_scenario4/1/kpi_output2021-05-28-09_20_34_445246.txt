{
    "Average_holding_time": 78.96626768226334,
    "Average_holding_actions": [
        24,
        188,
        248,
        300,
        29,
        0
    ],
    "Average_travel_time_passengers": 2483.111326830113,
    "Average_passenger_waiting_time": 385.5132744809618,
    "Average_passenger_in_vehicle_time": 2097.5980523489793,
    "Average_speed_busses": 0.03829166666666667,
    "Average_experienced_crowding": 99.18295576897282,
    "Average_covariation_headway": 0.23189808349431248,
    "Average_covariation_headway_stops": {
        "G": 0.14039415107964515,
        "I": 0.1432191440565232,
        "F": 0.10165899810101532,
        "K": 0.14465111674395473,
        "H": 0.12192751703913791,
        "D": 0.10582033020192062,
        "B": 0.11332410526641803,
        "J": 0.11981940919047661,
        "L": 0.1183644583946786,
        "E": 0.06671853158105684,
        "C": 0.08596296386126143,
        "A": 0.09752939984108218
    },
    "Average_excess_waiting_time": {
        "G": 122.28939932364017,
        "I": 121.42871518901433,
        "F": 121.20269941506263,
        "K": 121.18767655869641,
        "H": 122.4200863377335,
        "D": 122.26254922955161,
        "B": 121.91454253742802,
        "J": 121.75733338327103,
        "L": 120.62410705744594,
        "E": 121.74187621148809,
        "C": 122.73739199834733,
        "A": 123.04536464882926
    },
    "Holding_per_stop": {
        "G": 85.32467532467533,
        "I": 91.55844155844156,
        "K": 81.42857142857143,
        "F": 86.84210526315789,
        "H": 82.20779220779221,
        "B": 73.24675324675324,
        "D": 78.94736842105263,
        "J": 84.15584415584415,
        "L": 76.36363636363636,
        "E": 82.5,
        "A": 45.78947368421053,
        "C": 78.94736842105263
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus3": {
            "G": 108.84615384615384,
            "H": 116.61538461538461,
            "I": 111.38461538461539,
            "J": 106.07692307692308,
            "K": 95.15384615384616,
            "L": 84.61538461538461,
            "A": 73.38461538461539,
            "B": 62.92307692307692,
            "C": 56.76923076923077,
            "D": 53,
            "E": 52.25,
            "F": 80.58333333333333
        },
        "bus4": {
            "I": 125.53846153846153,
            "J": 115.92307692307692,
            "K": 104.61538461538461,
            "L": 92.15384615384616,
            "A": 78.53846153846153,
            "B": 68.76923076923077,
            "C": 62.23076923076923,
            "D": 57.15384615384615,
            "E": 55.07692307692308,
            "F": 85.15384615384616,
            "G": 128.91666666666666,
            "H": 140.33333333333334
        },
        "bus2": {
            "F": 81.15384615384616,
            "G": 119.61538461538461,
            "H": 132.07692307692307,
            "I": 125.15384615384616,
            "J": 118.3076923076923,
            "K": 107.61538461538461,
            "L": 96,
            "A": 86,
            "B": 73.3076923076923,
            "C": 67.25,
            "D": 63.25,
            "E": 61.5
        },
        "bus5": {
            "K": 98.92307692307692,
            "L": 87.38461538461539,
            "A": 76.07692307692308,
            "B": 66.23076923076923,
            "C": 63,
            "D": 58.84615384615385,
            "E": 57.30769230769231,
            "F": 83.6923076923077,
            "G": 118.92307692307692,
            "H": 130.84615384615384,
            "I": 125.91666666666667,
            "J": 118.08333333333333
        },
        "bus1": {
            "D": 54,
            "E": 51.53846153846154,
            "F": 79.46153846153847,
            "G": 122.92307692307692,
            "H": 135.3846153846154,
            "I": 129.53846153846155,
            "J": 121.92307692307692,
            "K": 110.6923076923077,
            "L": 96.15384615384616,
            "A": 83.25,
            "B": 71.16666666666667,
            "C": 63.666666666666664
        },
        "bus0": {
            "B": 62.69230769230769,
            "C": 60.46153846153846,
            "D": 56,
            "E": 57.15384615384615,
            "F": 85,
            "G": 127.46153846153847,
            "H": 136.53846153846155,
            "I": 130.92307692307693,
            "J": 121.53846153846153,
            "K": 108.75,
            "L": 95.41666666666667,
            "A": 79.91666666666667
        }
    },
    "Obs_and_actions_busses": {
        "1": -103.93914632965881,
        "4": 88.26156166277892,
        "2": -44.36798771078068,
        "3": 5.46973341142944,
        "5": -5.206896553104453,
        "0": -169.81991627201535
    },
    "Obs_and_actions_tfls": {}
}