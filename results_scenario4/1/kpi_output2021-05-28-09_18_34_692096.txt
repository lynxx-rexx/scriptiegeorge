{
    "Average_holding_time": 77.34273318872017,
    "Average_holding_actions": [
        28,
        227,
        231,
        272,
        54,
        0
    ],
    "Average_travel_time_passengers": 2479.7629798813114,
    "Average_passenger_waiting_time": 387.1318695950278,
    "Average_passenger_in_vehicle_time": 2092.6311102861537,
    "Average_speed_busses": 0.03841666666666667,
    "Average_experienced_crowding": 99.34403297618789,
    "Average_covariation_headway": 0.2368536699848181,
    "Average_covariation_headway_stops": {
        "I": 0.1533024541066197,
        "E": 0.11988346578892078,
        "K": 0.14668680896939792,
        "D": 0.1085757804558537,
        "A": 0.12658332024349336,
        "C": 0.10020342662982368,
        "J": 0.1394653328436622,
        "F": 0.09520566362643182,
        "L": 0.11725064993364431,
        "B": 0.10265164555618339,
        "G": 0.09659484812435515,
        "H": 0.1092715323940063
    },
    "Average_excess_waiting_time": {
        "I": 124.11941989742706,
        "E": 117.0694209718078,
        "K": 122.69264141273902,
        "D": 118.79770795933206,
        "A": 121.49779465018895,
        "C": 120.15224563702884,
        "J": 125.32735805039079,
        "F": 117.09901384766926,
        "L": 121.78106394208447,
        "B": 122.33057625936908,
        "G": 119.46114264951734,
        "H": 122.39007315962596
    },
    "Holding_per_stop": {
        "I": 88.05194805194805,
        "E": 79.23076923076923,
        "A": 45.1948051948052,
        "K": 86.1038961038961,
        "D": 82.59740259740259,
        "C": 81.03896103896103,
        "J": 81.3157894736842,
        "F": 79.87012987012987,
        "L": 72.85714285714286,
        "B": 76.57894736842105,
        "G": 75.58441558441558,
        "H": 79.73684210526316
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus3": {
            "I": 126.53846153846153,
            "J": 117.6923076923077,
            "K": 109,
            "L": 95.61538461538461,
            "A": 80.3076923076923,
            "B": 68,
            "C": 58.84615384615385,
            "D": 56.46153846153846,
            "E": 57.23076923076923,
            "F": 89.41666666666667,
            "G": 131.66666666666666,
            "H": 142.25
        },
        "bus2": {
            "E": 52.38461538461539,
            "F": 77.38461538461539,
            "G": 116.84615384615384,
            "H": 124.84615384615384,
            "I": 122.84615384615384,
            "J": 116.38461538461539,
            "K": 106.53846153846153,
            "L": 96.07692307692308,
            "A": 85.38461538461539,
            "B": 71.92307692307692,
            "C": 63.92307692307692,
            "D": 58.166666666666664
        },
        "bus4": {
            "K": 104.46153846153847,
            "L": 91.38461538461539,
            "A": 81.6923076923077,
            "B": 70.23076923076923,
            "C": 61.53846153846154,
            "D": 55.38461538461539,
            "E": 55.84615384615385,
            "F": 84.6923076923077,
            "G": 122.07692307692308,
            "H": 132.69230769230768,
            "I": 129.66666666666666,
            "J": 123.83333333333333
        },
        "bus1": {
            "D": 56.30769230769231,
            "E": 52.38461538461539,
            "F": 83.3076923076923,
            "G": 125.07692307692308,
            "H": 134.3846153846154,
            "I": 131.46153846153845,
            "J": 123.07692307692308,
            "K": 110.76923076923077,
            "L": 98.6923076923077,
            "A": 89.46153846153847,
            "B": 75,
            "C": 64.58333333333333
        },
        "bus5": {
            "A": 75,
            "B": 65.3076923076923,
            "C": 58.76923076923077,
            "D": 53.61538461538461,
            "E": 52.46153846153846,
            "F": 79.6923076923077,
            "G": 121.92307692307692,
            "H": 131.3846153846154,
            "I": 123.23076923076923,
            "J": 114,
            "K": 105.83333333333333,
            "L": 93.91666666666667
        },
        "bus0": {
            "C": 59.84615384615385,
            "D": 54.92307692307692,
            "E": 53.53846153846154,
            "F": 78.61538461538461,
            "G": 115.76923076923077,
            "H": 125.76923076923077,
            "I": 121.53846153846153,
            "J": 112.23076923076923,
            "K": 102,
            "L": 93.46153846153847,
            "A": 83.33333333333333,
            "B": 71.91666666666667
        }
    },
    "Obs_and_actions_busses": {
        "4": 109.72090553339991,
        "2": -54.338786438681716,
        "3": 21.158019371842425,
        "1": -114.25297764702118,
        "0": -150.32139642464216,
        "5": 11.270398905873368
    },
    "Obs_and_actions_tfls": {}
}