{
    "Average_holding_time": 50.5765407554672,
    "Average_holding_actions": [
        301,
        142,
        263,
        116,
        6,
        0
    ],
    "Average_travel_time_passengers": 2282.5198913994336,
    "Average_passenger_waiting_time": 352.48912108758174,
    "Average_passenger_in_vehicle_time": 1930.0307703117542,
    "Average_speed_busses": 0.041916666666666665,
    "Average_experienced_crowding": 90.45965861772873,
    "Average_covariation_headway": 0.2300344375852334,
    "Average_covariation_headway_stops": {
        "H": 0.14111194103565255,
        "E": 0.11978623634051014,
        "J": 0.11264460014773862,
        "D": 0.10015288321711513,
        "B": 0.10497890087592127,
        "C": 0.09802879812384196,
        "I": 0.09628495116511428,
        "K": 0.08423236797549136,
        "F": 0.10961712573687221,
        "G": 0.09630956346157102,
        "L": 0.07047893303559416,
        "A": 0.08871281292127732
    },
    "Average_excess_waiting_time": {
        "H": 88.4910041927746,
        "E": 85.48304496614776,
        "J": 86.01975262656924,
        "D": 86.50963945858484,
        "B": 90.83583721737966,
        "C": 88.69973390976662,
        "I": 86.73621236278996,
        "K": 86.06340611737403,
        "F": 86.87941300028331,
        "G": 87.61488662999932,
        "L": 88.0154540419577,
        "A": 91.6574063242619
    },
    "Holding_per_stop": {
        "H": 59.285714285714285,
        "J": 88.21428571428571,
        "E": 29.647058823529413,
        "D": 11.071428571428571,
        "C": 20,
        "B": 27.83132530120482,
        "K": 97.85714285714286,
        "F": 40.714285714285715,
        "I": 77.5,
        "L": 100.8433734939759,
        "G": 46.42857142857143,
        "A": 7.590361445783133
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus4": {
            "H": 112.46666666666667,
            "I": 108.28571428571429,
            "J": 98.78571428571429,
            "K": 88.42857142857143,
            "L": 78.28571428571429,
            "A": 69.5,
            "B": 59.5,
            "C": 51.92857142857143,
            "D": 50.142857142857146,
            "E": 50.642857142857146,
            "F": 77.92857142857143,
            "G": 112.64285714285714
        },
        "bus3": {
            "E": 47.4,
            "F": 72.07142857142857,
            "G": 107.64285714285714,
            "H": 118.21428571428571,
            "I": 114.28571428571429,
            "J": 105.92857142857143,
            "K": 93.5,
            "L": 84.78571428571429,
            "A": 74.28571428571429,
            "B": 64.85714285714286,
            "C": 57.642857142857146,
            "D": 53.214285714285715
        },
        "bus5": {
            "J": 103.13333333333334,
            "K": 94.92857142857143,
            "L": 83.35714285714286,
            "A": 70,
            "B": 59.714285714285715,
            "C": 55.785714285714285,
            "D": 51.785714285714285,
            "E": 49.07142857142857,
            "F": 76.28571428571429,
            "G": 113.28571428571429,
            "H": 122.21428571428571,
            "I": 119
        },
        "bus2": {
            "D": 46.285714285714285,
            "E": 44.714285714285715,
            "F": 71.71428571428571,
            "G": 112,
            "H": 118.5,
            "I": 114.64285714285714,
            "J": 106.14285714285714,
            "K": 97.85714285714286,
            "L": 84.28571428571429,
            "A": 73,
            "B": 61.785714285714285,
            "C": 54.07142857142857
        },
        "bus0": {
            "B": 61.142857142857146,
            "C": 53.857142857142854,
            "D": 52.357142857142854,
            "E": 51.5,
            "F": 77.57142857142857,
            "G": 113.5,
            "H": 124.07142857142857,
            "I": 121.42857142857143,
            "J": 113.21428571428571,
            "K": 101.5,
            "L": 88.6923076923077,
            "A": 76.6923076923077
        },
        "bus1": {
            "C": 53.42857142857143,
            "D": 51,
            "E": 47.285714285714285,
            "F": 70.92857142857143,
            "G": 105.64285714285714,
            "H": 113.57142857142857,
            "I": 114.07142857142857,
            "J": 106.42857142857143,
            "K": 96.07142857142857,
            "L": 83.71428571428571,
            "A": 73.71428571428571,
            "B": 62.84615384615385
        }
    },
    "Obs_and_actions_busses": {
        "5": 325.2239710552509,
        "4": 83.72816964476527,
        "3": -28.702247190842915,
        "0": -66.12818791255103,
        "2": 9.076045627452462,
        "1": 31.53521126704239
    },
    "Obs_and_actions_tfls": {}
}