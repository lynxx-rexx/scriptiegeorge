{
    "Average_holding_time": 48.112648221343875,
    "Average_holding_actions": [
        353,
        118,
        237,
        99,
        10,
        0
    ],
    "Average_travel_time_passengers": 2280.6142932414177,
    "Average_passenger_waiting_time": 354.79501447196117,
    "Average_passenger_in_vehicle_time": 1925.8192787693486,
    "Average_speed_busses": 0.042166666666666665,
    "Average_experienced_crowding": 91.01346822065423,
    "Average_covariation_headway": 0.23708109611681433,
    "Average_covariation_headway_stops": {
        "I": 0.16999713660030086,
        "F": 0.11837177174842327,
        "K": 0.12177142848933874,
        "A": 0.10813825332984958,
        "B": 0.09837238898262499,
        "C": 0.10337587148822615,
        "J": 0.1265509386465953,
        "G": 0.11084996533790298,
        "L": 0.09341611854502897,
        "D": 0.08008869811240706,
        "H": 0.13317397539555134,
        "E": 0.09635244130819208
    },
    "Average_excess_waiting_time": {
        "I": 91.55798759249097,
        "F": 85.41960592483895,
        "K": 86.73006695759227,
        "A": 86.32121248885443,
        "B": 84.04302961440254,
        "C": 82.6524187656201,
        "J": 88.83447580974035,
        "G": 87.05279902710896,
        "L": 86.82159975083601,
        "D": 83.25732615575902,
        "H": 90.55754412226338,
        "E": 86.30826723352203
    },
    "Holding_per_stop": {
        "I": 73.57142857142857,
        "K": 91.07142857142857,
        "C": 13.411764705882353,
        "B": 18.352941176470587,
        "F": 39.88235294117647,
        "A": 18.571428571428573,
        "J": 82.5,
        "D": 21.88235294117647,
        "L": 97.14285714285714,
        "G": 42.5,
        "E": 29.642857142857142,
        "H": 50
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus3": {
            "I": 108.26666666666667,
            "J": 103.42857142857143,
            "K": 93.35714285714286,
            "L": 82.28571428571429,
            "A": 71.14285714285714,
            "B": 59.5,
            "C": 54.5,
            "D": 50.714285714285715,
            "E": 50.714285714285715,
            "F": 78,
            "G": 114.21428571428571,
            "H": 120.71428571428571
        },
        "bus2": {
            "F": 70.73333333333333,
            "G": 106.21428571428571,
            "H": 120.57142857142857,
            "I": 117.78571428571429,
            "J": 109.42857142857143,
            "K": 98.5,
            "L": 89.5,
            "A": 76.57142857142857,
            "B": 67.5,
            "C": 60.92857142857143,
            "D": 53.92857142857143,
            "E": 51.785714285714285
        },
        "bus4": {
            "K": 86.8,
            "L": 79.64285714285714,
            "A": 70.92857142857143,
            "B": 61.57142857142857,
            "C": 54.07142857142857,
            "D": 50.214285714285715,
            "E": 49,
            "F": 72.64285714285714,
            "G": 108.71428571428571,
            "H": 118.28571428571429,
            "I": 112,
            "J": 101.28571428571429
        },
        "bus5": {
            "A": 73.35714285714286,
            "B": 60.857142857142854,
            "C": 56.857142857142854,
            "D": 50,
            "E": 49.714285714285715,
            "F": 76.14285714285714,
            "G": 112.64285714285714,
            "H": 122.35714285714286,
            "I": 117,
            "J": 109.57142857142857,
            "K": 100.42857142857143,
            "L": 89.28571428571429
        },
        "bus0": {
            "B": 59.46666666666667,
            "C": 51.42857142857143,
            "D": 49.357142857142854,
            "E": 45.785714285714285,
            "F": 73.57142857142857,
            "G": 110.07142857142857,
            "H": 118.85714285714286,
            "I": 114.21428571428571,
            "J": 108.14285714285714,
            "K": 97.71428571428571,
            "L": 86.21428571428571,
            "A": 74.42857142857143
        },
        "bus1": {
            "C": 52,
            "D": 47.333333333333336,
            "E": 47.642857142857146,
            "F": 73.78571428571429,
            "G": 110.07142857142857,
            "H": 122.42857142857143,
            "I": 118.28571428571429,
            "J": 110.42857142857143,
            "K": 99.42857142857143,
            "L": 87.92857142857143,
            "A": 75.14285714285714,
            "B": 63.214285714285715
        }
    },
    "Obs_and_actions_busses": {
        "4": 113.38222675953327,
        "5": 318.73372405263086,
        "1": 58.43725609891536,
        "2": 27.04219409274253,
        "0": -83.54916993514763,
        "3": -27.50973568827121
    },
    "Obs_and_actions_tfls": {}
}