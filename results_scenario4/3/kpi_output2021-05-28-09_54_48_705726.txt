{
    "Average_holding_time": 69.3015873015873,
    "Average_holding_actions": [
        0,
        191,
        441,
        91,
        40,
        0
    ],
    "Average_travel_time_passengers": 2408.001518453569,
    "Average_passenger_waiting_time": 383.92886910852525,
    "Average_passenger_in_vehicle_time": 2024.0726493449447,
    "Average_speed_busses": 0.039375,
    "Average_experienced_crowding": 97.23221992325047,
    "Average_covariation_headway": 0.2340707975712362,
    "Average_covariation_headway_stops": {
        "G": 0.16028421996029849,
        "E": 0.1370145526749964,
        "J": 0.14087933216629914,
        "K": 0.14794269313071165,
        "L": 0.15087855143524934,
        "H": 0.14982728352913444,
        "B": 0.12685910064378395,
        "A": 0.11862888832062016,
        "I": 0.1273033782344924,
        "F": 0.11109527541532216,
        "C": 0.1035044273785599,
        "D": 0.11314305169194531
    },
    "Average_excess_waiting_time": {
        "G": 115.78908953680241,
        "E": 113.64885637270856,
        "J": 114.75947370487273,
        "K": 112.90992255818713,
        "L": 110.6542235244188,
        "H": 116.50179125658235,
        "B": 109.35526720002207,
        "A": 110.2714293035088,
        "I": 116.10199451853481,
        "F": 113.78458756727338,
        "C": 110.71801470708152,
        "D": 113.82072951514903
    },
    "Holding_per_stop": {
        "G": 58.10126582278481,
        "L": 64.875,
        "K": 67.9746835443038,
        "J": 70.25316455696202,
        "E": 51.265822784810126,
        "B": 85.44303797468355,
        "H": 65.38461538461539,
        "A": 110.88607594936708,
        "I": 68.07692307692308,
        "F": 57.30769230769231,
        "C": 72.53164556962025,
        "D": 59.23076923076923
    },
    "Actions_tfl_edges": {},
    "Load_per_bus_per_stop": {
        "bus2": {
            "G": 112.64285714285714,
            "H": 121.14285714285714,
            "I": 115,
            "J": 106.84615384615384,
            "K": 99.38461538461539,
            "L": 87.38461538461539,
            "A": 76,
            "B": 64.38461538461539,
            "C": 57.61538461538461,
            "D": 53.76923076923077,
            "E": 54.15384615384615,
            "F": 80.84615384615384
        },
        "bus1": {
            "E": 51.142857142857146,
            "F": 81.07692307692308,
            "G": 121.76923076923077,
            "H": 131.23076923076923,
            "I": 126.15384615384616,
            "J": 118.92307692307692,
            "K": 109.92307692307692,
            "L": 96.07692307692308,
            "A": 83.53846153846153,
            "B": 70.61538461538461,
            "C": 61.23076923076923,
            "D": 53.46153846153846
        },
        "bus3": {
            "J": 109.35714285714286,
            "K": 98.84615384615384,
            "L": 88.53846153846153,
            "A": 77.23076923076923,
            "B": 67,
            "C": 59.69230769230769,
            "D": 54.15384615384615,
            "E": 53,
            "F": 80.92307692307692,
            "G": 116.84615384615384,
            "H": 128.84615384615384,
            "I": 125.61538461538461
        },
        "bus4": {
            "K": 101.28571428571429,
            "L": 90,
            "A": 76.46153846153847,
            "B": 67.61538461538461,
            "C": 59.53846153846154,
            "D": 57.38461538461539,
            "E": 56.15384615384615,
            "F": 84.23076923076923,
            "G": 126.53846153846153,
            "H": 136,
            "I": 128.76923076923077,
            "J": 120.61538461538461
        },
        "bus5": {
            "L": 76.64285714285714,
            "A": 66.64285714285714,
            "B": 60.214285714285715,
            "C": 54.76923076923077,
            "D": 51.38461538461539,
            "E": 47.61538461538461,
            "F": 78.07692307692308,
            "G": 113.3076923076923,
            "H": 119,
            "I": 113.76923076923077,
            "J": 106.15384615384616,
            "K": 95.23076923076923
        },
        "bus0": {
            "B": 67.92857142857143,
            "C": 63.285714285714285,
            "D": 61.23076923076923,
            "E": 57.38461538461539,
            "F": 86.15384615384616,
            "G": 125.07692307692308,
            "H": 138.15384615384616,
            "I": 132.69230769230768,
            "J": 121.61538461538461,
            "K": 110.15384615384616,
            "L": 96.53846153846153,
            "A": 85.84615384615384
        }
    },
    "Obs_and_actions_busses": {
        "2": -3.8692982024171956,
        "4": 144.098474970158,
        "5": 78.24999999975027,
        "3": 32.70329670357153,
        "1": -132.09568041302632
    },
    "Obs_and_actions_tfls": {}
}