{
    "Average_holding_time": 20.651785714285715,
    "Average_holding_actions": [
        611,
        409,
        0,
        0,
        31,
        0
    ],
    "Average_travel_time_passengers": 2068.923695080043,
    "Average_passenger_waiting_time": 324.3583869647692,
    "Average_passenger_in_vehicle_time": 1744.5653081152734,
    "Average_speed_busses": 0.04666666666666667,
    "Average_experienced_crowding": 58.494521422905166,
    "Average_covariation_headway": 0.27944058547357375,
    "Average_covariation_headway_stops": {
        "H": 0.21136955604245036,
        "F": 0.19850645082024895,
        "B": 0.2094706310327243,
        "K": 0.20446790210335786,
        "C": 0.21821890808300232,
        "A": 0.19564360575770123,
        "I": 0.182408386005775,
        "L": 0.18276956011001527,
        "D": 0.20460666315862874,
        "G": 0.207520497868794,
        "J": 0.20704800550636804,
        "E": 0.17494698599646422
    },
    "Average_excess_waiting_time": {
        "H": 55.695298255223975,
        "F": 54.3210717570895,
        "B": 56.41781963034839,
        "K": 56.396507332709746,
        "C": 56.31335378435767,
        "A": 56.379489621427126,
        "I": 53.62087409504085,
        "L": 55.705096821937104,
        "D": 56.09123657352711,
        "G": 57.09255874119964,
        "J": 58.36065783045308,
        "E": 53.4142077500473
    },
    "Holding_per_stop": {
        "H": 22.340425531914892,
        "C": 20.425531914893618,
        "B": 17.23404255319149,
        "K": 20,
        "F": 20.425531914893618,
        "A": 20.967741935483872,
        "I": 20,
        "D": 24.516129032258064,
        "G": 18.387096774193548,
        "L": 20,
        "J": 21.93548387096774,
        "E": 21.612903225806452
    },
    "Actions_tfl_edges": {
        "AB": [
            94,
            0,
            0
        ],
        "BC": [
            95,
            0,
            0
        ],
        "EF": [
            94,
            0,
            0
        ],
        "GH": [
            94,
            0,
            0
        ],
        "JK": [
            94,
            0,
            0
        ],
        "LA": [
            94,
            0,
            0
        ],
        "HI": [
            94,
            0,
            0
        ],
        "CD": [
            94,
            0,
            0
        ],
        "KL": [
            93,
            0,
            0
        ],
        "FG": [
            94,
            0,
            0
        ],
        "IJ": [
            93,
            0,
            0
        ],
        "DE": [
            93,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus3": {
            "H": 55.5625,
            "I": 56.1875,
            "J": 55.125,
            "K": 55.3125,
            "L": 54.875,
            "A": 54.6875,
            "B": 57,
            "C": 55.93333333333333,
            "D": 56.6,
            "E": 57.333333333333336,
            "F": 58.8,
            "G": 58.6
        },
        "bus2": {
            "F": 54,
            "G": 54.4375,
            "H": 53.75,
            "I": 53.875,
            "J": 56.5,
            "K": 59.75,
            "L": 61.3125,
            "A": 61.4,
            "B": 60.06666666666667,
            "C": 59,
            "D": 59.2,
            "E": 58.733333333333334
        },
        "bus0": {
            "B": 54.125,
            "C": 54.4375,
            "D": 54.5625,
            "E": 54.9375,
            "F": 54.5,
            "G": 52.8125,
            "H": 53.125,
            "I": 54.13333333333333,
            "J": 53.733333333333334,
            "K": 54.333333333333336,
            "L": 53.733333333333334,
            "A": 55.666666666666664
        },
        "bus4": {
            "K": 58.4375,
            "L": 59.5625,
            "A": 61.5,
            "B": 62.125,
            "C": 63.625,
            "D": 63.0625,
            "E": 62.266666666666666,
            "F": 60.53333333333333,
            "G": 60.266666666666666,
            "H": 60.06666666666667,
            "I": 59.46666666666667,
            "J": 62
        },
        "bus1": {
            "C": 54.5625,
            "D": 55.1875,
            "E": 57.0625,
            "F": 59.25,
            "G": 61.0625,
            "H": 59.5625,
            "I": 59.375,
            "J": 59.375,
            "K": 59.06666666666667,
            "L": 57.4,
            "A": 56.333333333333336,
            "B": 56.333333333333336
        },
        "bus5": {
            "A": 51.625,
            "B": 52.75,
            "C": 55.8125,
            "D": 58.5,
            "E": 55.25,
            "F": 54.875,
            "G": 54.266666666666666,
            "H": 53.666666666666664,
            "I": 53.8,
            "J": 55,
            "K": 53.8,
            "L": 54.13333333333333
        }
    },
    "Obs_and_actions_busses": {
        "0": -134.78371729664,
        "1": 99.73022228556535,
        "5": 328.73680149264743,
        "3": 233.5902900168377
    },
    "Obs_and_actions_tfls": {
        "0": 135.47205763692213
    }
}