{
    "Average_holding_time": 18.90556045895852,
    "Average_holding_actions": [
        763,
        206,
        74,
        90,
        0,
        0
    ],
    "Average_travel_time_passengers": 2042.4285702293214,
    "Average_passenger_waiting_time": 323.0076140830616,
    "Average_passenger_in_vehicle_time": 1719.4209561462758,
    "Average_speed_busses": 0.04720833333333333,
    "Average_experienced_crowding": 58.140666070041156,
    "Average_covariation_headway": 0.2947564733425155,
    "Average_covariation_headway_stops": {
        "K": 0.23624033128184674,
        "J": 0.23577251570849542,
        "G": 0.1981905431190982,
        "A": 0.2389338468814569,
        "C": 0.21588283438302047,
        "F": 0.19191604755523464,
        "L": 0.21491155947399104,
        "B": 0.22725417534824963,
        "H": 0.19118621410227063,
        "D": 0.20923839413450773,
        "I": 0.21303932569054368,
        "E": 0.21200782331795848
    },
    "Average_excess_waiting_time": {
        "K": 54.90486159240953,
        "J": 56.70416129704904,
        "G": 49.57993809310233,
        "A": 56.19913536978265,
        "C": 52.42801416908242,
        "F": 50.303806448974626,
        "L": 54.14809703641606,
        "B": 55.91360825614686,
        "H": 50.875196228451614,
        "D": 53.24344130872703,
        "I": 54.99525295761089,
        "E": 55.69662383330285
    },
    "Holding_per_stop": {
        "K": 20.526315789473685,
        "G": 12,
        "A": 25.57894736842105,
        "J": 16.105263157894736,
        "C": 18,
        "F": 17.23404255319149,
        "L": 19.78723404255319,
        "B": 23.617021276595743,
        "H": 13.72340425531915,
        "D": 23.617021276595743,
        "I": 16.914893617021278,
        "E": 19.78723404255319
    },
    "Actions_tfl_edges": {
        "BC": [
            95,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "FG": [
            95,
            0,
            0
        ],
        "IJ": [
            95,
            0,
            0
        ],
        "JK": [
            96,
            0,
            0
        ],
        "LA": [
            95,
            0,
            0
        ],
        "KL": [
            95,
            0,
            0
        ],
        "GH": [
            95,
            0,
            0
        ],
        "AB": [
            95,
            0,
            0
        ],
        "CD": [
            95,
            0,
            0
        ],
        "HI": [
            94,
            0,
            0
        ],
        "DE": [
            94,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "K": 51.1875,
            "L": 52.375,
            "A": 53.4375,
            "B": 52.5625,
            "C": 52.8125,
            "D": 54.9375,
            "E": 54,
            "F": 54.5625,
            "G": 53.8125,
            "H": 53.5,
            "I": 53.8,
            "J": 55.53333333333333
        },
        "bus3": {
            "J": 52,
            "K": 53.125,
            "L": 53.8125,
            "A": 53.375,
            "B": 55,
            "C": 54.6875,
            "D": 55.8125,
            "E": 57.25,
            "F": 56.266666666666666,
            "G": 55.2,
            "H": 54.46666666666667,
            "I": 55.666666666666664
        },
        "bus2": {
            "G": 58.0625,
            "H": 57.25,
            "I": 59.25,
            "J": 58.375,
            "K": 58.375,
            "L": 57.25,
            "A": 58.3125,
            "B": 58.4375,
            "C": 59.4375,
            "D": 60.0625,
            "E": 61.53333333333333,
            "F": 61.93333333333333
        },
        "bus5": {
            "A": 52.875,
            "B": 54.4375,
            "C": 54.1875,
            "D": 55.5625,
            "E": 55.5,
            "F": 55.6875,
            "G": 57.625,
            "H": 57,
            "I": 57,
            "J": 58.375,
            "K": 57.666666666666664,
            "L": 56.6
        },
        "bus0": {
            "C": 53.9375,
            "D": 55.25,
            "E": 56.375,
            "F": 55.8125,
            "G": 56.6875,
            "H": 54.625,
            "I": 55,
            "J": 56.0625,
            "K": 56.875,
            "L": 56.5625,
            "A": 57.4,
            "B": 56
        },
        "bus1": {
            "F": 58.125,
            "G": 57.8125,
            "H": 59.75,
            "I": 60.25,
            "J": 60.125,
            "K": 60.1875,
            "L": 61.125,
            "A": 60.1875,
            "B": 59.333333333333336,
            "C": 61.266666666666666,
            "D": 62.6,
            "E": 59.666666666666664
        }
    },
    "Obs_and_actions_busses": {
        "0": -110.11816095545758,
        "4": 299.23208935121346,
        "2": 193.8189554362714,
        "1": 121.81091118425454
    },
    "Obs_and_actions_tfls": {
        "0": 137.26399439381063
    }
}   },
    "Obs_and_actions_tfls": {
        "0": 136.48946057710913
    }
}635934
    }
}