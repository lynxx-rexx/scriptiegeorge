{
    "Average_holding_time": 20.54318788958148,
    "Average_holding_actions": [
        705,
        231,
        92,
        69,
        0,
        0
    ],
    "Average_travel_time_passengers": 2050.9404655438007,
    "Average_passenger_waiting_time": 330.63315536315105,
    "Average_passenger_in_vehicle_time": 1720.3073101806538,
    "Average_speed_busses": 0.04679166666666667,
    "Average_experienced_crowding": 58.97651934862887,
    "Average_covariation_headway": 0.3153579506874751,
    "Average_covariation_headway_stops": {
        "L": 0.2312867487895872,
        "H": 0.24463411504031105,
        "B": 0.2233652959676657,
        "E": 0.1907053606036457,
        "G": 0.23733837230965735,
        "J": 0.2360090175333768,
        "A": 0.22583580444482787,
        "F": 0.20005788400511948,
        "I": 0.24544721765274893,
        "C": 0.21706090649781648,
        "K": 0.21974046184640209,
        "D": 0.19009115247011416
    },
    "Average_excess_waiting_time": {
        "L": 57.30136745747154,
        "H": 60.93986582394632,
        "B": 55.85390108091485,
        "E": 53.759998915194615,
        "G": 60.83910831965352,
        "J": 58.45985980377242,
        "A": 58.227064081030846,
        "F": 57.56799357395522,
        "I": 61.86142584433918,
        "C": 57.138316287404564,
        "K": 58.21083925945908,
        "D": 55.43125016564716
    },
    "Holding_per_stop": {
        "H": 20.425531914893618,
        "L": 20.106382978723403,
        "B": 22.659574468085108,
        "E": 17.23404255319149,
        "G": 15.161290322580646,
        "J": 25.851063829787233,
        "A": 18.829787234042552,
        "I": 26.129032258064516,
        "C": 23.29787234042553,
        "K": 22.580645161290324,
        "F": 16.451612903225808,
        "D": 17.741935483870968
    },
    "Actions_tfl_edges": {
        "AB": [
            95,
            0,
            0
        ],
        "DE": [
            94,
            0,
            0
        ],
        "FG": [
            94,
            0,
            0
        ],
        "GH": [
            94,
            0,
            0
        ],
        "IJ": [
            94,
            0,
            0
        ],
        "KL": [
            94,
            0,
            0
        ],
        "HI": [
            94,
            0,
            0
        ],
        "LA": [
            94,
            0,
            0
        ],
        "BC": [
            94,
            0,
            0
        ],
        "EF": [
            94,
            0,
            0
        ],
        "JK": [
            94,
            0,
            0
        ],
        "CD": [
            94,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "L": 57.875,
            "A": 56.5,
            "B": 55.625,
            "C": 56.1875,
            "D": 55.3125,
            "E": 56.5625,
            "F": 56.4375,
            "G": 57.0625,
            "H": 57.4,
            "I": 59.8,
            "J": 60.733333333333334,
            "K": 60.13333333333333
        },
        "bus3": {
            "H": 55.4375,
            "I": 55.4375,
            "J": 54.625,
            "K": 55.75,
            "L": 58.0625,
            "A": 57.375,
            "B": 58.375,
            "C": 59.75,
            "D": 61.06666666666667,
            "E": 60.733333333333334,
            "F": 60.733333333333334,
            "G": 60.6
        },
        "bus0": {
            "B": 56.0625,
            "C": 57.375,
            "D": 58.125,
            "E": 58.75,
            "F": 58.3125,
            "G": 58.8125,
            "H": 58.75,
            "I": 61.53333333333333,
            "J": 61.666666666666664,
            "K": 60.733333333333334,
            "L": 60.93333333333333,
            "A": 60.53333333333333
        },
        "bus1": {
            "E": 53.9375,
            "F": 55.6875,
            "G": 55.0625,
            "H": 57.25,
            "I": 57.3125,
            "J": 57.0625,
            "K": 56.1875,
            "L": 57.8,
            "A": 58.4,
            "B": 56.8,
            "C": 56.13333333333333,
            "D": 57.266666666666666
        },
        "bus2": {
            "G": 56.25,
            "H": 56.625,
            "I": 58.625,
            "J": 58.625,
            "K": 58.875,
            "L": 58.625,
            "A": 58.8125,
            "B": 59.86666666666667,
            "C": 60.4,
            "D": 59.733333333333334,
            "E": 60.93333333333333,
            "F": 60.4
        },
        "bus4": {
            "J": 49,
            "K": 48.375,
            "L": 50.0625,
            "A": 50.9375,
            "B": 50.0625,
            "C": 50.75,
            "D": 50.5625,
            "E": 50.4375,
            "F": 51.6,
            "G": 52,
            "H": 52.06666666666667,
            "I": 52.666666666666664
        }
    },
    "Obs_and_actions_busses": {
        "0": -126.43288694139953,
        "1": 119.47169263813056,
        "2": 194.08906869019066,
        "4": 298.37558492606695,
        "3": 234.05917411623244
    },
    "Obs_and_actions_tfls": {
        "0": 134.5281375446582
    }
}