{
    "Average_holding_time": 24.955277280858677,
    "Average_holding_actions": [
        651,
        242,
        106,
        119,
        0,
        0
    ],
    "Average_travel_time_passengers": 2095.325838814157,
    "Average_passenger_waiting_time": 342.23645628585314,
    "Average_passenger_in_vehicle_time": 1753.0893825282976,
    "Average_speed_busses": 0.04658333333333333,
    "Average_experienced_crowding": 62.662427084817054,
    "Average_covariation_headway": 0.39383896659556195,
    "Average_covariation_headway_stops": {
        "A": 0.32626648032176736,
        "L": 0.30504004936622264,
        "J": 0.31585354486202155,
        "K": 0.31329327678580027,
        "H": 0.3158021809085359,
        "C": 0.29681793982810584,
        "B": 0.3017121502313534,
        "D": 0.28291606465377783,
        "I": 0.30926440665295746,
        "E": 0.2650606405628654,
        "F": 0.2701740678710234,
        "G": 0.3146441302458927
    },
    "Average_excess_waiting_time": {
        "A": 73.25873021993021,
        "L": 72.44803489321987,
        "J": 76.89025855222906,
        "K": 75.68225865831323,
        "H": 77.10683044822991,
        "C": 67.75463748211018,
        "B": 70.25853872541222,
        "D": 67.16973190291509,
        "I": 77.73051202284006,
        "E": 66.04089722615788,
        "F": 68.62843904775502,
        "G": 78.65529954457566
    },
    "Holding_per_stop": {
        "A": 28.085106382978722,
        "L": 25.161290322580644,
        "K": 25.483870967741936,
        "C": 28.085106382978722,
        "J": 26.129032258064516,
        "H": 20.967741935483872,
        "B": 27.76595744680851,
        "D": 26.80851063829787,
        "I": 23.47826086956522,
        "E": 21.93548387096774,
        "F": 20,
        "G": 25.434782608695652
    },
    "Actions_tfl_edges": {
        "BC": [
            95,
            0,
            0
        ],
        "GH": [
            93,
            0,
            0
        ],
        "IJ": [
            93,
            0,
            0
        ],
        "JK": [
            94,
            0,
            0
        ],
        "KL": [
            94,
            0,
            0
        ],
        "LA": [
            94,
            0,
            0
        ],
        "AB": [
            94,
            0,
            0
        ],
        "CD": [
            94,
            0,
            0
        ],
        "HI": [
            93,
            0,
            0
        ],
        "DE": [
            94,
            0,
            0
        ],
        "EF": [
            93,
            0,
            0
        ],
        "FG": [
            93,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus5": {
            "A": 49.3125,
            "B": 48.875,
            "C": 50.375,
            "D": 50.9375,
            "E": 50.875,
            "F": 48.5625,
            "G": 47,
            "H": 48.125,
            "I": 49.86666666666667,
            "J": 50.8,
            "K": 53,
            "L": 52.6
        },
        "bus4": {
            "L": 47.3125,
            "A": 45.875,
            "B": 47.5625,
            "C": 50.25,
            "D": 52.75,
            "E": 52.3125,
            "F": 51.8125,
            "G": 50.46666666666667,
            "H": 50.46666666666667,
            "I": 50.733333333333334,
            "J": 48.2,
            "K": 47.333333333333336
        },
        "bus2": {
            "J": 60,
            "K": 59.0625,
            "L": 58.4375,
            "A": 59.25,
            "B": 58.9375,
            "C": 58.53333333333333,
            "D": 59.666666666666664,
            "E": 62.2,
            "F": 65.6,
            "G": 65.8,
            "H": 65.26666666666667,
            "I": 64.86666666666666
        },
        "bus3": {
            "K": 54.1875,
            "L": 53.875,
            "A": 53.875,
            "B": 53,
            "C": 54.75,
            "D": 56.125,
            "E": 56.6,
            "F": 56.8,
            "G": 56.53333333333333,
            "H": 56.2,
            "I": 56.266666666666666,
            "J": 55.46666666666667
        },
        "bus1": {
            "H": 60.875,
            "I": 63.5625,
            "J": 64,
            "K": 64.25,
            "L": 64.625,
            "A": 64.53333333333333,
            "B": 66.2,
            "C": 66.13333333333334,
            "D": 65.2,
            "E": 63.46666666666667,
            "F": 64.2,
            "G": 63.733333333333334
        },
        "bus0": {
            "C": 67.125,
            "D": 67.875,
            "E": 68.0625,
            "F": 68.3125,
            "G": 68.0625,
            "H": 70.75,
            "I": 71.125,
            "J": 71.75,
            "K": 72.46666666666667,
            "L": 72.46666666666667,
            "A": 71.2,
            "B": 70.93333333333334
        }
    },
    "Obs_and_actions_busses": {
        "0": -167.51231964212622,
        "1": 124.14586432167039,
        "4": 334.3593454445154,
        "2": 205.6996769003343
    },
    "Obs_and_actions_tfls": {
        "0": 134.75683748573346
    }
}