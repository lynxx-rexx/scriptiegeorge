{
    "Average_holding_time": 19.24229074889868,
    "Average_holding_actions": [
        641,
        287,
        198,
        0,
        9,
        0
    ],
    "Average_travel_time_passengers": 2041.3917480319371,
    "Average_passenger_waiting_time": 329.7687834611313,
    "Average_passenger_in_vehicle_time": 1711.6229645708074,
    "Average_speed_busses": 0.04729166666666667,
    "Average_experienced_crowding": 57.75238748371478,
    "Average_covariation_headway": 0.3201955545179231,
    "Average_covariation_headway_stops": {
        "D": 0.23791779198863872,
        "H": 0.2576676167345667,
        "B": 0.2574034276153552,
        "G": 0.2407286270615866,
        "J": 0.22414053845341536,
        "F": 0.21315580925569577,
        "I": 0.23497979279690656,
        "K": 0.21959993617127793,
        "E": 0.20688538648488808,
        "C": 0.23399952711816235,
        "L": 0.2247101258411331,
        "A": 0.24882364649326688
    },
    "Average_excess_waiting_time": {
        "D": 57.042874536124714,
        "H": 55.96549431969629,
        "B": 61.183657661290056,
        "G": 55.15573237969619,
        "J": 51.24624035597856,
        "F": 53.904956397310855,
        "I": 54.15044692874176,
        "K": 53.93073132860212,
        "E": 54.90932545830327,
        "C": 59.63910015359693,
        "L": 55.82442429931541,
        "A": 61.053068461574526
    },
    "Holding_per_stop": {
        "H": 17.8125,
        "D": 20.74468085106383,
        "G": 17.68421052631579,
        "J": 19.6875,
        "F": 16.595744680851062,
        "B": 20.74468085106383,
        "K": 21.157894736842106,
        "I": 17.36842105263158,
        "E": 19.78723404255319,
        "C": 21.382978723404257,
        "L": 17.872340425531913,
        "A": 20.106382978723403
    },
    "Actions_tfl_edges": {
        "AB": [
            95,
            0,
            0
        ],
        "CD": [
            95,
            0,
            0
        ],
        "EF": [
            95,
            0,
            0
        ],
        "FG": [
            95,
            0,
            0
        ],
        "GH": [
            96,
            0,
            0
        ],
        "IJ": [
            96,
            0,
            0
        ],
        "HI": [
            96,
            0,
            0
        ],
        "DE": [
            94,
            0,
            0
        ],
        "JK": [
            96,
            0,
            0
        ],
        "BC": [
            94,
            0,
            0
        ],
        "KL": [
            95,
            0,
            0
        ],
        "LA": [
            94,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus1": {
            "D": 56,
            "E": 56.1875,
            "F": 54.3125,
            "G": 55,
            "H": 55.4375,
            "I": 56.125,
            "J": 56.8125,
            "K": 56.1875,
            "L": 56.46666666666667,
            "A": 57,
            "B": 58.13333333333333,
            "C": 57.93333333333333
        },
        "bus4": {
            "H": 53.75,
            "I": 53.6875,
            "J": 56.0625,
            "K": 55.625,
            "L": 55,
            "A": 55.5625,
            "B": 56.8125,
            "C": 59.1875,
            "D": 57.8125,
            "E": 59.125,
            "F": 60.5,
            "G": 59.333333333333336
        },
        "bus0": {
            "B": 52.5,
            "C": 52.9375,
            "D": 53.125,
            "E": 54.625,
            "F": 55.25,
            "G": 56.6875,
            "H": 58.1875,
            "I": 59.625,
            "J": 59.0625,
            "K": 58,
            "L": 56.86666666666667,
            "A": 55.86666666666667
        },
        "bus3": {
            "G": 47.6875,
            "H": 48.9375,
            "I": 50,
            "J": 52.5625,
            "K": 51.4375,
            "L": 51.3125,
            "A": 51.875,
            "B": 52.875,
            "C": 52.625,
            "D": 50.53333333333333,
            "E": 50.86666666666667,
            "F": 51.6
        },
        "bus5": {
            "J": 55.875,
            "K": 56.25,
            "L": 59.5625,
            "A": 60.3125,
            "B": 60.375,
            "C": 59.1875,
            "D": 57.4375,
            "E": 59.9375,
            "F": 58.4375,
            "G": 57.0625,
            "H": 60.1875,
            "I": 60.4
        },
        "bus2": {
            "F": 50.5625,
            "G": 51.9375,
            "H": 53.375,
            "I": 53.125,
            "J": 52.875,
            "K": 53.25,
            "L": 54.875,
            "A": 52.6875,
            "B": 53.666666666666664,
            "C": 54.266666666666666,
            "D": 54.333333333333336,
            "E": 53.6
        }
    },
    "Obs_and_actions_busses": {
        "1": 96.39996024147018,
        "0": -144.66416010832472,
        "2": 235.48098558245692,
        "5": 443.23803230537084
    },
    "Obs_and_actions_tfls": {
        "0": 138.35209340430976
    }
}