import pandas as pd
import numpy as np
import simpy as sp
import random
import statistics

def main():
    env = sp.Environment()
    env.process(traffic_light(env))
    env.run(until=400)
    print('Simulation complete ')


def traffic_light(env):
    while True:
        print('the ligth turned green at t=' +str(env.now))
        yield env.timeout(30)
        print('light turned yellow at t=' +str(env.now))
        yield env.timeout(5)
        print('light turned red at t=' + str(env.now))
        yield env.timeout(20)


if __name__ == "__main__":
    main()
