{
    "Average_holding_time": 54.58045409674235,
    "Average_holding_actions": [
        577,
        43,
        64,
        55,
        51,
        176
    ],
    "Average_travel_time_passengers": 2297.6203387842274,
    "Average_passenger_waiting_time": 362.73569295060787,
    "Average_passenger_in_vehicle_time": 1934.8846458336604,
    "Average_speed_busses": 0.042208333333333334,
    "Average_experienced_crowding": 64.64319451483256,
    "Average_covariation_headway": 0.30489827964063615,
    "Average_covariation_headway_stops": {
        "K": 0.2266741532540091,
        "F": 0.20877747003646385,
        "I": 0.2240496885261712,
        "L": 0.22102619657934602,
        "B": 0.19659538257708592,
        "D": 0.2370824640482537,
        "G": 0.21078449914027644,
        "A": 0.17597937722203924,
        "C": 0.23236954199195145,
        "E": 0.21958044374420105,
        "J": 0.1989587843632351,
        "H": 0.22377037314430917
    },
    "Average_excess_waiting_time": {
        "K": 94.22928105173997,
        "F": 91.21029729140031,
        "I": 94.95199114339334,
        "L": 93.27423982676123,
        "B": 88.38512253101317,
        "D": 94.91871249101052,
        "G": 92.2594205664044,
        "A": 88.13918522810519,
        "C": 95.33119966103811,
        "E": 94.50827140899804,
        "J": 92.73565127565945,
        "H": 96.91431235376615
    },
    "Holding_per_stop": {
        "F": 57.1764705882353,
        "L": 56.11764705882353,
        "B": 55.411764705882355,
        "D": 54.35294117647059,
        "K": 55.411764705882355,
        "I": 55.357142857142854,
        "G": 52.5,
        "A": 49.285714285714285,
        "C": 50,
        "E": 56.07142857142857,
        "J": 59.642857142857146,
        "H": 53.57142857142857
    },
    "Actions_tfl_edges": {
        "AB": [
            85,
            0,
            0
        ],
        "CD": [
            85,
            0,
            0
        ],
        "EF": [
            85,
            0,
            0
        ],
        "HI": [
            85,
            0,
            0
        ],
        "JK": [
            85,
            0,
            0
        ],
        "KL": [
            86,
            0,
            0
        ],
        "FG": [
            85,
            0,
            0
        ],
        "LA": [
            85,
            0,
            0
        ],
        "BC": [
            85,
            0,
            0
        ],
        "DE": [
            85,
            0,
            0
        ],
        "IJ": [
            84,
            0,
            0
        ],
        "GH": [
            84,
            0,
            0
        ]
    },
    "Load_per_bus_per_stop": {
        "bus4": {
            "K": 62.266666666666666,
            "L": 61.57142857142857,
            "A": 61.214285714285715,
            "B": 61.142857142857146,
            "C": 62.42857142857143,
            "D": 62.642857142857146,
            "E": 64.35714285714286,
            "F": 65.71428571428571,
            "G": 65.5,
            "H": 65.71428571428571,
            "I": 66.85714285714286,
            "J": 67.78571428571429
        },
        "bus2": {
            "F": 60.733333333333334,
            "G": 61.42857142857143,
            "H": 63.357142857142854,
            "I": 63.285714285714285,
            "J": 64.07142857142857,
            "K": 68.42857142857143,
            "L": 70.28571428571429,
            "A": 70.64285714285714,
            "B": 69.71428571428571,
            "C": 67.92857142857143,
            "D": 66.57142857142857,
            "E": 66
        },
        "bus3": {
            "I": 64.07142857142857,
            "J": 63.642857142857146,
            "K": 66.14285714285714,
            "L": 66.21428571428571,
            "A": 67.28571428571429,
            "B": 67.71428571428571,
            "C": 66.5,
            "D": 67.78571428571429,
            "E": 67.07142857142857,
            "F": 67.78571428571429,
            "G": 68.42857142857143,
            "H": 69.64285714285714
        },
        "bus5": {
            "L": 61.93333333333333,
            "A": 62.642857142857146,
            "B": 60.785714285714285,
            "C": 60.785714285714285,
            "D": 60.357142857142854,
            "E": 57.92857142857143,
            "F": 60.785714285714285,
            "G": 62.42857142857143,
            "H": 63.714285714285715,
            "I": 65,
            "J": 64.78571428571429,
            "K": 65.07142857142857
        },
        "bus0": {
            "B": 58.666666666666664,
            "C": 58.53333333333333,
            "D": 57.785714285714285,
            "E": 59.5,
            "F": 60.42857142857143,
            "G": 61.642857142857146,
            "H": 61.357142857142854,
            "I": 60.214285714285715,
            "J": 61.57142857142857,
            "K": 62.785714285714285,
            "L": 61.785714285714285,
            "A": 64.64285714285714
        },
        "bus1": {
            "D": 60.666666666666664,
            "E": 62.5,
            "F": 62.285714285714285,
            "G": 63.57142857142857,
            "H": 64.07142857142857,
            "I": 64.71428571428571,
            "J": 63.42857142857143,
            "K": 63.07142857142857,
            "L": 64.28571428571429,
            "A": 63.5,
            "B": 63.714285714285715,
            "C": 64.07142857142857
        }
    },
    "Obs_and_actions_busses": {
        "6": 266.37833340546723,
        "3": 88.70509610487343,
        "0": -186.78110944450742,
        "1": 28.239117774483287,
        "5": 150.30215443789038,
        "4": 120.64128492867488,
        "2": 59.55907948704257
    },
    "Obs_and_actions_tfls": {
        "0": 115.21303219371012
    }
}